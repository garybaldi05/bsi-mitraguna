# BSI MITRAGUNA


## Clone Repo

you can clone a repo using Git Kraken using https clone, add this url :

```sh
https://gitlab.com/garybaldi05/bsi-mitraguna.git
```

or you can clone it using git :

```sh
cd your_project_folder_path
git clone git@gitlab.com:garybaldi05/bsi-mitraguna.git
```


## Getting started

Create Database :

```sh
CREATE DATABASE dbBsiMitraguna
```

to config your database and env go to :

```
For Dev :
-> application
  -> config 
    -> development
       ~ database.php
       ~ config.php
```

please configure database according your enviroment

---
## For Add A Plugin/lib Using Composer
Go to your project root folder run the following code

Example :

```sh
cd your_project_folder
composer require phpoffice/phpspreadsheet
```

For initalizing lib use :

```sh
cd your_project_folder_path
composer install
```
