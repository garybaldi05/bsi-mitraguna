<?php
defined('BASEPATH') OR exit('No direct script access allowed');

global $table, $tableUser, $tableProduk, 
       $tableProdukType, $tableTypeManfaat, $tableJenisPekerjaan,
       $tableAkad ;

$table = DB_NAME_PREFIX.'tm_klaim_peralihan';
$tableUser = DB_NAME_PREFIX.'tm_user';
$tableProduk = DB_NAME_PREFIX.'tm_product_list';
$tableProdukType = DB_NAME_PREFIX.'tm_product_type';
$tableJenisPekerjaan = DB_NAME_PREFIX.'tm_jenis_pekerjaan';
$tableAkad = DB_NAME_PREFIX.'tm_jenis_akad';
$tableTypeManfaat = DB_NAME_PREFIX.'tm_type_manfaat';
$tableLog = DB_NAME_PREFIX.'tl_log_data';

class KlaimPeralihan_m extends CI_Model
{

    function get_tables($cari)
        {
            // Ambil data limit per page
            $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$cari['length']}");
            // Ambil data start
            $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$cari['start']}"); 

            $tables = $GLOBALS['table'];

            $query = "SELECT *
                      FROM ".$tables." a
                      WHERE a.c_id is not null ";

            $countQuery = "
                          SELECT*
                        FROM ".$tables." a
                        WHERE a.c_id is not null ";

            if(!empty($cari['search']['value'])){
                $searchVal = $cari['search']['value'];
                $query .= "AND no_aplikasi like '%$searchVal%' OR reffnumber like '%$searchVal%' ";
                $countQuery .= "AND no_aplikasi like '%$searchVal%' OR reffnumber like '%$searchVal%' ";
            }

            if(!empty($cari['startdate']) && !empty($cari['enddate']) ){
                $startdate = $cari['startdate']." 00:00:00";
                $enddate = $cari['enddate']." 23:59:59";

                $query .= "AND a.createdon BETWEEN '$startdate' AND '$enddate' ";
                $countQuery .= "AND a.createdon BETWEEN '$startdate' AND '$enddate' ";
            }

            if(!empty($cari['cabang_search'])){
                $area = $cari['cabang_search'];

                $query .= "AND a.kode_cabang = '$area' ";
                $countQuery .= "AND a.kode_cabang = '$area' ";
            }

            if(!empty($cari['status_klaim_search']) ){
                $status = $cari['status_klaim_search'];
                    $query .= "AND a.status_klaim = '$status' ";
                    $countQuery .= "AND a.status_klaim = '$status' ";
            }

            $query .= " 
            
            ORDER BY a.createdon DESC OFFSET ".$start." ROWS FETCH NEXT ".$limit." ROWS ONLY";
            //var_dump($query);exit();

            $sql_data = $this->db->query($query);
            $sql_count = $this->db->query($countQuery)->result_array();
            
            
            $data = $sql_data->result_array();

            

            $callback = array(    
                'draw' => $_POST['draw'], // Ini dari datatablenya    
                'recordsTotal' => count($sql_count),    
                'recordsFiltered'=>count($sql_count),    
                'data'=>$data
            );
            return $callback; // Convert array $callback ke json
        }

    function get_history($id,$cari)
    {

         // Ambil data limit per page
         $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$cari['length']}");
         // Ambil data start
         $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$cari['start']}"); 

         $tables = $GLOBALS['table'];

         $query = "SELECT 
                     a.no_aplikasi as c_noap, 
                     a.no_aplikasi, 
                     (SELECT count(x.c_id) FROM tm_penutupan x WHERE x.no_aplikasi = a.no_aplikasi) as count_deb,
                     a.nama_cabang, 
                     a.reffnumber, 
                     a.nama_asuransi, 
                     a.nama, 
                     a.ktp, 
                     a.npwp, 
                     a.tanggal_lahir,
                     a.tanggal_akad,
                     a.nomor_akad,
                     a.nomor_agunan,
                     a.createdon,
                     a.polis,
                     a.coverage_fee_mitra,
                     a.coverage_asuransi,
                     a.coverage_pinalti,
                     a.plafond,
                     a.premi,
                     a.premi_before_admin,
                     a.bulan_tenor,
                     a.usia_saat_akad,
                     a.usia_selesai_akad,
                     a.atribusi,
                     c.nama_produk,
                     d.nama as tipe_produk,
                     e.nama as jenis_pekerjaan
         
                   FROM ".$tables." a
                   LEFT JOIN tm_cabang b on b.kode_cabang = a.kode_cabang
                   LEFT JOIN tm_product_list c on c.c_id = a.produk
                   LEFT JOIN tm_product_type d on d.c_id = a.tipe_produk
                   LEFT JOIN tm_jenis_pekerjaan e on e.c_id = a.jenis_pekerjaan
                   WHERE a.no_aplikasi ='$id' AND a.status IN (1,2) ";

         $countQuery = "
                         SELECT 
                             a.no_aplikasi, 
                             a.nama_cabang, 
                             a.reffnumber, 
                             a.nama_asuransi, 
                             a.nama, 
                             a.ktp, 
                             a.npwp, 
                             a.tanggal_lahir,
                             a.tanggal_akad,
                             a.nomor_akad,
                             a.nomor_agunan,
                             a.createdon,
                             a.polis,
                             a.coverage_fee_mitra,
                             a.coverage_asuransi,
                             a.coverage_pinalti,
                             a.plafond,
                             a.premi,
                             a.premi_before_admin,
                             a.bulan_tenor,
                             a.usia_saat_akad,
                             a.usia_selesai_akad,
                             a.atribusi,
                             c.nama_produk,
                             d.nama,
                             e.nama
                         FROM ".$tables." a
                         LEFT JOIN tm_cabang b on b.kode_cabang = a.kode_cabang
                         LEFT JOIN tm_product_list c on c.c_id = a.produk
                         LEFT JOIN tm_product_type d on d.c_id = a.tipe_produk
                         LEFT JOIN tm_jenis_pekerjaan e on e.c_id = a.jenis_pekerjaan
                         WHERE a.no_aplikasi ='$id' AND a.status IN (1,2) ";

         $query .= " 
         GROUP BY 
                 a.no_aplikasi, 
                 a.nama_cabang, 
                 a.reffnumber, 
                 a.nama_asuransi, 
                 a.nama, 
                 a.ktp, 
                 a.npwp, 
                 a.tanggal_lahir,
                 a.tanggal_akad,
                 a.nomor_akad,
                 a.nomor_agunan,
                 a.createdon,
                 a.polis,
                 a.coverage_fee_mitra,
                 a.coverage_asuransi,
                 a.coverage_pinalti,
                 a.premi,
                 a.plafond,
                 a.premi_before_admin,
                 a.bulan_tenor,
                 a.usia_saat_akad,
                 a.usia_selesai_akad,
                 a.atribusi,
                 c.nama_produk,
                 d.nama,
                 e.nama
         ORDER BY a.createdon DESC OFFSET ".$start." ROWS FETCH NEXT ".$limit." ROWS ONLY";
         //var_dump($query);exit();

         $countQuery .= " 
         GROUP BY 
                 a.no_aplikasi, 
                 a.nama_cabang, 
                 a.reffnumber, 
                 a.nama_asuransi, 
                 a.nama, 
                 a.ktp, 
                 a.npwp, 
                 a.tanggal_lahir,
                 a.tanggal_akad,
                 a.nomor_akad,
                 a.nomor_agunan,
                 a.createdon,
                 a.polis,
                 a.coverage_fee_mitra,
                 a.coverage_asuransi,
                 a.coverage_pinalti,
                 a.premi,
                 a.plafond,
                 a.premi_before_admin,
                 a.bulan_tenor,
                 a.usia_saat_akad,
                 a.usia_selesai_akad,
                 a.atribusi,
                 c.nama_produk,
                 d.nama,
                 e.nama ";

         $sql_data = $this->db->query($query);
         $sql_count = $this->db->query($countQuery)->result_array();
         
         
         $data = $sql_data->result_array();

         

         $callback = array(    
             'draw' => $_POST['draw'], // Ini dari datatablenya    
             'recordsTotal' => count($sql_count),    
             'recordsFiltered'=>count($sql_count),    
             'data'=>$data
         );
         return $callback; // Convert array $callback ke json
    }

    public function getDatabyId($id){

        $query = "SELECT *
                    FROM ".$GLOBALS['table']." a 
        WHERE a.c_id = '$id' ORDER BY a.createdon DESC";
      
        $query = $this->db->query($query);
        
        $return = $query->row();

        return $return;
    }

    public function getHistoryData($id){
        $query = "SELECT c_id, createdon, dataold, datanew, response, status, actionname
                  FROM tl_cn_his
                  WHERE datanew like '%$id%' ORDER BY createdon DESC";

        $query = $this->db->query($query);
        
        $return = $query->result_array();

        return $return;
    }

    
    function get_capem($id)
    {
        $query = $this->db->query("select * from BNI_NEW.dbo.MasterCabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    function getDatabyBatch($batch,$status){
        $query = "SELECT * FROM BNI_NEW.dbo.Penutupan WHERE batch = '$batch' ";

        if($status == 'success'){
            $query .= "AND status = '1' ";
        }else{
            $query .= "AND status != '1' ";
        }

        $result = $this->db->query($query);
        
        $return = $result->result_array();

        return json_encode($return);
    }

    function getInsurance(){
        $query = $this->db->query("SELECT a.nama, a.id_insurance, b.share_value, b.max_share_value, b.min_share_value
                                     FROM BNI_NEW.dbo.MasterInsurance a
                                     LEFT JOIN BNI_NEW.dbo.MasterShare b
                                     ON a.kode_asuransi = b.kode_broker
                                      WHERE a.status = '1' AND b.kode_broker like '%INS%' ORDER BY a.nama ASC ")->result_array();
        return $query;
    }
}