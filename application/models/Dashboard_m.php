<?php
defined('BASEPATH') OR exit('No direct script access allowed');

global $tablepenutupan, $tableUser, $tableProduk, 
       $tableProdukType, $tableTypeManfaat, $tableJenisPekerjaan,
       $tableAkad ;

$tablepenutupan = DB_NAME_PREFIX.'tm_penutupan';
$tableklaim = DB_NAME_PREFIX.'tm_klaim';
$tableUser = DB_NAME_PREFIX.'tm_user';
$tableProduk = DB_NAME_PREFIX.'tm_product_list';
$tableProdukType = DB_NAME_PREFIX.'tm_product_type';
$tableJenisPekerjaan = DB_NAME_PREFIX.'tm_jenis_pekerjaan';
$tableAkad = DB_NAME_PREFIX.'tm_jenis_akad';
$tableTypeManfaat = DB_NAME_PREFIX.'tm_type_manfaat';
$tableLog = DB_NAME_PREFIX.'tl_log_data';

class Dashboard_m extends CI_Model
{

    function getSlidePenutupan()
    {
        $query = "SELECT * FROM (
                        SELECT 
                        'Debitur Free Cover (CAC)' as title,
                        COUNT(c_id) as val1,
                        SUM(plafond) as val2,
                        SUM(premi) as val3,
                        1 as sort
                        FROM ".$GLOBALS['tablepenutupan']."
                        WHERE status_cbc is null AND status_data = '1'
                  
                     UNION
                  
                     SELECT
                        'Debitur Non Free Cover (CBC)' as title,
                        COUNT(c_id) as val1,
                        SUM(plafond) as val2,
                        SUM(premi) as val3,
                        2 as sort
                        FROM ".$GLOBALS['tablepenutupan']."
                        WHERE status_cbc = '1' AND status_data = '1'
                    ) a
                  ORDER BY a.sort ASC
                 ";

        $return = $this->db->query($query);
        
        return $return->result_array();
    }

    function getSlideKlaim()
    {
        $query = "SELECT * FROM (
                        SELECT 
                        'Total Klaim Diajukan' as title,
                        COUNT(c_id) as val1,
                        SUM(os_klaim) as val2,
                        SUM(hak_klaim) as val3,
                        1 as sort
                        FROM ".$GLOBALS['tableklaim']."
                  
                     UNION
                  
                     SELECT
                        'Total OS Diajukan' as title,
                        COUNT(c_id) as val1,
                        SUM(os_klaim) as val2,
                        SUM(hak_klaim) as val3,
                        2 as sort
                        FROM ".$GLOBALS['tableklaim']."
                        WHERE status not in (5, 0)
                    UNION
                
                    SELECT
                        'Total Klaim Dibayar' as title,
                        COUNT(c_id) as val1,
                        SUM(os_klaim) as val2,
                        SUM(hak_klaim) as val3,
                        3 as sort
                        FROM ".$GLOBALS['tableklaim']."
                        WHERE status = 6
                    ) a
                  ORDER BY a.sort ASC
                 ";
        
        $return = $this->db->query($query);
        
        return $return->result_array();
    }

    function getMitragunaDashboard($idins){
        $query = "SELECT 
                'MITRAGUNA' as produk,
                (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '3' AND id_asuransi = '$idins') as premi,
                0 AS claim_paid
                FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '3' GROUP BY produk ";

        $return = $this->db->query($query);
        
        return $return->row();
    }

    function getPrapenDashboard($idins){
        $query = "SELECT 
                'PRA PENSIUN' as produk,
                (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '4' AND id_asuransi = '$idins') as premi,
                0 AS claim_paid
                FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '4' GROUP BY produk ";

        $return = $this->db->query($query);
        
        return $return->row();
    }

    function getPensiunDashboard($idins){
        $query = "SELECT 
                'PENSIUN' as produk,
                (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '2' AND id_asuransi = '$idins') as premi,
                0 AS claim_paid
                FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '2' GROUP BY produk ";

        $return = $this->db->query($query);
        
        return $return->row();
    }

    function getPenutupanByInsurance()
    {
        $query = "
                    SELECT
                     a.nama_asuransi,
                     (SELECT COUNT(c_id) FROM ".$GLOBALS['tablepenutupan']." aa WHERE aa.nama_asuransi = a.nama_asuransi) AS deb_count,
                     (SELECT SUM(plafond) FROM ".$GLOBALS['tablepenutupan']." aa WHERE aa.nama_asuransi = a.nama_asuransi) AS plafond,
                     (SELECT SUM(premi) FROM ".$GLOBALS['tablepenutupan']." aa WHERE aa.nama_asuransi = a.nama_asuransi) AS premi
                    FROM ".$GLOBALS['tablepenutupan']." a 
                    WHERE status_data = 1 AND status = 1
                    GROUP BY a.nama_asuransi ORDER BY a.nama_asuransi ASC
                 ";

        $return = $this->db->query($query);

        return $return->result_array();
    }

    function getKlaimByInsurance()
    {
        $query = "
                    SELECT
                     a.nama_asuransi,
                     (SELECT COUNT(c_id) FROM ".$GLOBALS['tableklaim']." aa WHERE aa.nama_asuransi = a.nama_asuransi) AS deb_count,
                     (SELECT SUM(os_klaim) FROM ".$GLOBALS['tableklaim']." aa WHERE aa.nama_asuransi = a.nama_asuransi) AS os_klaim,
                     (SELECT SUM(hak_klaim) FROM ".$GLOBALS['tableklaim']." aa WHERE aa.nama_asuransi = a.nama_asuransi) AS hak_klaim
                    FROM ".$GLOBALS['tableklaim']." a 
                    WHERE status_data = 1 AND status = 1
                    GROUP BY a.nama_asuransi ORDER BY a.nama_asuransi ASC
                 ";

        $return = $this->db->query($query);

        return $return->result_array();
    }

    public function getDatabyId($id,$type){

        if($type == '1'){
            $query = "SELECT TOP(1) * FROM ".$GLOBALS['table']." WHERE no_aplikasi = '$id' ORDER BY createdon DESC";
        }else{
            $query = "SELECT TOP(1) * FROM ".$GLOBALS['table']." WHERE c_id = '$id' ORDER BY createdon DESC";
        }

        $query = $this->db->query($query);
        
        $return = $query->row();

        return $return;
    }

    public function getHistoryData($id){
        $query = "SELECT c_id, createdon, dataold, datanew, response, status, actionname
                  FROM tl_cn_his
                  WHERE datanew like '%$id%' ORDER BY createdon DESC";

        $query = $this->db->query($query);
        
        $return = $query->result_array();

        return $return;
    }

    
    function get_capem($id)
    {
        $query = $this->db->query("select * from BNI_NEW.dbo.MasterCabang
		where id_induk = '$id'
		order by id_induk ASC")->result();
        return $query;
    }

    function getDatabyBatch($batch,$status){
        $query = "SELECT * FROM BNI_NEW.dbo.Penutupan WHERE batch = '$batch' ";

        if($status == 'success'){
            $query .= "AND status = '1' ";
        }else{
            $query .= "AND status != '1' ";
        }

        $result = $this->db->query($query);
        
        $return = $result->result_array();

        return json_encode($return);
    }

    function getInsurance(){
        $query = $this->db->query("SELECT a.nama, a.id_insurance, b.share_value, b.max_share_value, b.min_share_value
                                     FROM BNI_NEW.dbo.MasterInsurance a
                                     LEFT JOIN BNI_NEW.dbo.MasterShare b
                                     ON a.kode_asuransi = b.kode_broker
                                      WHERE a.status = '1' AND b.kode_broker like '%INS%' ORDER BY a.nama ASC ")->result_array();
        return $query;
    }
}