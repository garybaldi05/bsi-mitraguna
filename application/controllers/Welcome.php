<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
        parent::__construct();
    }

	function index()
	{
		$data['title']  		= "Home";
		$data['a'] = 'TEST';
        
		$this->template->load('template','content',$data);
	}

}

