<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainsvc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Service_m');

        $valuesInHeader = $this->input->request_headers();

        if(isset($valuesInHeader['Authorization']) === false){
            // exit();
            $return = array(
                'Result' => array(
                    'status_code'    => '99',
                    'message' => 'Access denied, please check your token data'
                )
            );

            header('Content-Type: application/json');
            echo json_encode($return);
            die;

        }else{
            $jwt = $valuesInHeader['Authorization'];

            $token = null;
    
            if(!empty($jwt)) {
                if (preg_match('/bearer\s(\S+)/', $jwt, $matches)) {
                    $token = $matches[1];
                }
            }

            if(is_null($token) || empty($token)) {
                $return = array("Result"=>array('status_code' => '98', "message"=>"Token Empty"));
               
                header('Content-Type: application/json');
                echo json_encode($return);
                die;
            }

            $authsession = $this->Service_m->getAuthorize($token);
            
            if(!empty($authsession)){
                
                $now = New DateTime();
                $activeToken = new DateTime($authsession->c_active_until);
                
                if($now > $activeToken){
                    $return = array("Result"=>array('status_code' => '97', "message"=>"Token Has Expired"));
               
                    header('Content-Type: application/json');
                    echo json_encode($return);
                    die;
                }

            }else{
                $return = array("Result"=>array('status_code' => '96', "message"=>"Token does not match"));
               
                header('Content-Type: application/json');
                echo json_encode($return);
                die;
            }
        }
    }

   
    function get_capem()
    {

        date_default_timezone_set('Asia/Jakarta');

        $validHeader = GET_METHOD;
        $this->checkHeaders($validHeader);

        try{

            $data = $this->input->raw_input_stream;
            $data_json = json_decode($data, true);
            $id = $data_json['id'];

            $data=$this->Service_m->get_capem($id);
    
            return $this->output
            ->set_content_type('application/json')
            ->set_output($data);
            die;

        }catch(Exception $e){
            $return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '13', "message"=>$msgError));

            $this->writeLog('GET-INSURANCE-DATA-MIDSVC','ERROR',TR_DATA,NULL,json_encode($error));

            return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($return));

        }

    }

    public function getInsuranceData(){

        date_default_timezone_set('Asia/Jakarta');

        $validHeader = GET_METHOD;
        $this->checkHeaders($validHeader);

        try{
            $data = $this->Service_m->getInsurance();
            return $this->output
            ->set_content_type('application/json')
            ->set_output($data);
            die;

        }catch(Exception $e){
            $return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLog('GET-INSURANCE-DATA-MIDSVC','ERROR',TR_DATA,NULL,json_encode($error));
            
            return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($return));


        }
    }

    public function checkHeaders($validHeader){
        try{
            $reqHeader = $this->input->server('REQUEST_METHOD');

            if(strtoupper($reqHeader) != strtoupper($validHeader)){
                $return = array("Result"=>array('status_code' => '12', "message"=>"Invalid ".$reqHeader." METHOD on this function"));
                header('Content-Type: application/json');
                echo json_encode($return);
                die();
            }

        }catch(Exception $e){

        }
    }

    public function writeLog($actionname,$status,$trigger,$jsonold,$jsonnew){

        try{
            $this->db->trans_off();

            // list of database variable
            $table = "";
            $db = DB_NAME_PREFIX;
            $uniqeKey = '';

            //datetime variable
            date_default_timezone_set('Asia/Jakarta');
            $todays = date("Y-m-d H:i:s");
            $username = 'ADMIN'; //for testing only change it to session user
            // $usernme = $this->session->userdata('KodeUser');


            if($trigger == TR_POSTING){
                $table = $db.'PostingHistory';

            }elseif($trigger == TR_SHARE){
                $table = $db.'ShareHistory';

            }elseif($trigger == TR_DATA){
                $table = $db.'DataHistory';
            }

            $this->db->trans_begin();

            $query = "
                INSERT INTO $table
                (
                    actionname,
                    status,
                    dataold,
                    datanew,
                    createdon,
                    createdby
                )
                VALUES
                (
                    '$actionname',
                    '$status',
                    '$jsonold',
                    '$jsonnew',
                    '$todays',
                    '$username'
                )
            ";

            $this->db->query($query);

            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
            }
            else
            {
                    $this->db->trans_commit();
            }

        }catch(Exception $e){
            $this->db->trans_rollback();
            throw new DomainException($e->getMessage(), 0, $e);
        }

    }
}

   
