<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once './vendor/autoload.php';
include_once (dirname(__FILE__) . "/../Master_Controller.php");

global $table, $tableUser, $tableProduk, 
       $tableProdukType, $tableTypeManfaat, $tableJenisPekerjaan,
       $tableAkad,$tableCabang ;

$table = DB_NAME_PREFIX.'tm_penutupan';
$tableUser = DB_NAME_PREFIX.'tm_user';
$tableProduk = DB_NAME_PREFIX.'tm_product_list';
$tableProdukType = DB_NAME_PREFIX.'tm_product_type';
$tableJenisPekerjaan = DB_NAME_PREFIX.'tm_jenis_pekerjaan';
$tableAkad = DB_NAME_PREFIX.'tm_jenis_akad';
$tableTypeManfaat = DB_NAME_PREFIX.'tm_type_manfaat';
$tableLog = DB_NAME_PREFIX.'tl_log_data';
$tableCabang = DB_NAME_PREFIX.'tm_cabang';

class Penutupansvc extends Master_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');

        //Sementara untuk auth kita offkan dlu

        /*
        $valuesInHeader = $this->input->request_headers();

        if(isset($valuesInHeader['Authorization']) === false){
            // exit();
            $return = array(
                'Result' => array(
                    'status_code'    => '99',
                    'message' => 'Access denied, please check your token data'
                )
            );

            header('Content-Type: application/json');
            echo json_encode($return);
            die;

        }else{
            $jwt = $valuesInHeader['Authorization'];

            $token = null;
    
            if(!empty($jwt)) {
                if (preg_match('/bearer\s(\S+)/', $jwt, $matches)) {
                    $token = $matches[1];
                }
            }

            if(is_null($token) || empty($token)) {
                $return = array("Result"=>array('status_code' => '98', "message"=>"Token Empty"));
               
                header('Content-Type: application/json');
                echo json_encode($return);
                die;
            }

            $authsession = $this->Service_m->getAuthorize($token);
            
            if(!empty($authsession)){
                
                $now = New DateTime();
                $activeToken = new DateTime($authsession->c_active_until);
                
                if($now > $activeToken){
                    $return = array("Result"=>array('status_code' => '97', "message"=>"Token Has Expired"));
               
                    header('Content-Type: application/json');
                    echo json_encode($return);
                    die;
                }

            }else{
                $return = array("Result"=>array('status_code' => '96', "message"=>"Token does not match"));
               
                header('Content-Type: application/json');
                echo json_encode($return);
                die;
            }
        } 
        */
    }

   
    function requestCN(){
        date_default_timezone_set('Asia/Jakarta');

        // $validHeader = POST_METHOD;
        // $this->checkHeaders($validHeader);

        try{

            $rawdata = $this->input->raw_input_stream;
            $data_json = json_decode($rawdata, true);
            // $dbFlpp = $this->load->database('dbFlpp', TRUE);
            // $dbGriya = $this->load->database('dbGriya', TRUE);
            $ktp = trim($data_json['no_identitas']);
            $reffnum = trim($data_json['reffnumber']);
            $noaplikasi = trim($data_json['no_aplikasi']);
            $iscbc = false;
            $ket_cbc = null;
            $status_cbc = 0;
            $next_cbc = false;

            $this->db->select('*');
            $this->db->like('kode_cabang', $data_json['unit_kerja_cabang'], 'both');
            $cabang = $this->db->get($GLOBALS['tableCabang'])->row();

            $this->db->select('*');
            $this->db->like('nama_produk', $data_json['produk'],'both');
            $produkData = $this->db->get($GLOBALS['tableProduk'])->row();
            $rate_ujroh = $produkData->rate_ujroh;

            $this->db->select('*');
            $this->db->like('nama', $data_json['type_manfaat'],'both');
            $type_manfaat = $this->db->get($GLOBALS['tableTypeManfaat'])->row();

            $this->db->select('c_id');
            $this->db->WHERE('no_aplikasi', $noaplikasi);
            $this->db->WHERE('ktp', $ktp);
            $exData = $this->db->get($GLOBALS['table'])->result_array();
            
            $paramsins = trim($data_json['insurance_name']);
            $expins = explode('/',$paramsins);
            $asuransi = strtoupper($expins[1]);

            //$qFlpp = "SELECT * FROM dbBsi.dbo.tm_penutupan WHERE status = '1' AND (p_klaim_1 is null OR p_klaim_2 is null) AND LTRIM(RTRIM(no_ktp)) = '$ktp' ";
            // $qGriya = "SELECT * FROM dbBsiGriya.dbo.penutupan WHERE status = '1' AND (p_klaim_1 is null OR p_klaim_2 is null) AND LTRIM(RTRIM(no_identitas)) = '$ktp'  ";
            $qMainDb = "SELECT ISNULL(SUM(plafond),0) as total_akumulasi FROM ".$GLOBALS['table']." WHERE status = '3' AND status_klaim is null AND LTRIM(RTRIM(ktp)) = '$ktp'  ";
            
            //Begin Transaction For BSI FLPP Database
            //$checkDataFlpp = $dbFlpp->query($qFlpp)->row();
            //Close Connection to BSI FLPP Database

            //Begin Transaction For BSI GRIYA Database
            // $dbGriya->trans_start();
            // $checkDataGriya = $dbGriya->query($qGriya)->row();
            // $dbGriya->trans_off();
            //Close Connection to BSI GRIYA Database
            
            //$plafondFlpp = (float)$checkDataFlpp->plafond;
            // $plafondGriya = (float)$checkDataFlpp->plafond;

            $plafond = (float)$data_json['plafond'];
            $expCoverage = explode(';',$data_json['type_coverage_amount']);
            $atribusi = trim($data_json['atribusi']);
            $coveragePlafond = (float)$expCoverage[0];
            $coverageFeeMitra = (float)$expCoverage[1];
            $coveragePinalti = (float)$expCoverage[2];
            $coverageAsuransi = (float)$expCoverage[3];
            $coverageType = '';
            if(strtoupper($atribusi) == 'TIDAK'){
                $coverageType = 'REGULER (NO ATRIB)';
            }else{
                if($coveragePlafond > 0 && $coverageAsuransi > 0 && $coverageFeeMitra > 0 && $coveragePinalti <= 0){
                    $coverageType = 'PLAFOND + FEE MITRA + FEE ASURANSI';
                }elseif($coveragePlafond > 0  && $coveragePinalti > 0 && $coverageFeeMitra > 0 && $coverageAsuransi <= 0){
                    $coverageType = 'PLAFOND + FEE MITRA + PINALTI';
                }elseif($coveragePlafond > 0  && $coveragePinalti > 0 && $coverageFeeMitra > 0 && $coverageAsuransi > 0){
                    $coverageType = 'ALL COVER';
                }elseif($coveragePlafond > 0  && $coveragePinalti > 0 && $coverageFeeMitra <= 0 && $coverageAsuransi > 0){
                    $coverageType = 'PLAFOND + FEE ASURANSI + PINALTI';
                }elseif($coveragePlafond > 0  && $coveragePinalti > 0 && $coverageFeeMitra <= 0 && $coverageAsuransi <= 0){
                    $coverageType = 'PLAFOND + PINALTI';
                }elseif($coveragePlafond > 0  && $coveragePinalti <= 0 && $coverageFeeMitra > 0 && $coverageAsuransi <= 0){
                    $coverageType = 'PLAFOND + FEE MITRA';
                }elseif($coveragePlafond > 0  && $coveragePinalti <= 0 && $coverageFeeMitra <= 0 && $coverageAsuransi > 0){
                    $coverageType = 'PLAFOND + FEE ASURANSI';
                }
            }

            $type_coverages = $this->db->query("SELECT * FROM tm_type_coverage WHERE nama = '$coverageType' ")->row();
            $type_coverage = $type_coverages->c_id;

            $jangkaWaktu = trim($data_json['tenor']);
            $tanggalLahir = trim($data_json['tgl_lahir']);
            $needle = ';';
            $pos = strpos($data_json['tgl_akad'],$needle);

            $mulaiAkad = null;
            $akhirAkad = null;
            $checkDateStart = true;
            $checkDateEnd = true;
           
            if($pos !== false){
                $expAkad = explode(';',trim($data_json['tgl_akad']));
                $mulaiAkad = $expAkad[0];
                $akhirAkad = $expAkad[1];

                $checkDateStart = $this->validateDate($mulaiAkad,'Ymd');
                $checkDateEnd = $this->validateDate($akhirAkad,'Ymd');

                if(!$checkDateStart && !$checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  $reffnum,
                    "noAplikasi"            => $noaplikasi,
                    "responseCode"          => "10",
                    "responseDesc"          => 'Tanggal Akad tidak sesuai format : YYYYMMDD'
                    );
    
                    $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));
    
                    header('Content-Type: application/json');
                    echo json_encode($return);
                    die;
                }

                if(!$checkDateStart && $checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  $reffnum,
                    "noAplikasi"            => $noaplikasi,
                    "responseCode"          => "10",
                    "responseDesc"          => 'Tanggal Mulai Akad tidak sesuai format : YYYYMMDD'
                    );
    
                    $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));
    
                    header('Content-Type: application/json');
                    echo json_encode($return);
                    die;
                }

                if($checkDateStart && !$checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  $reffnum,
                    "noAplikasi"            => $noaplikasi,
                    "responseCode"          => "10",
                    "responseDesc"          => 'Tanggal Akhir Kredit Akad tidak sesuai format : YYYYMMDD'
                    );
    
                    $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));
    
                    header('Content-Type: application/json');
                    echo json_encode($return);
                    die;
                }

                if(!$checkDateStart && !$checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  $reffnum,
                    "noAplikasi"            => $noaplikasi,
                    "responseCode"          => "10",
                    "responseDesc"          => 'Tanggal Akad & Tanggal Akhir Kredit Akad tidak sesuai format : YYYYMMDD'
                    );
    
                    $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));
    
                    header('Content-Type: application/json');
                    echo json_encode($return);
                    die;
                }

            }else{
                $return = array(
                    "reffnumber"           =>  $reffnum,
                   "noAplikasi"            => $noaplikasi,
                   "responseCode"          => "10",
                   "responseDesc"          => 'Tanggal Akad tidak sesuai format : YYYYMMDD;YYYYMMDD'
                );

                $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));

                header('Content-Type: application/json');
                echo json_encode($return);
                die;
            }

            $today = date('Y-m-d');

            $akumulasiPlafond = $this->db->query($qMainDb)->row()->total_akumulasi;

            $akumulasi = $plafond + $akumulasiPlafond;

            $qAsuransi = "SELECT * FROM tm_asuransi WHERE nama = '$asuransi' ";

            $dataAsuransi = $this->db->query($qAsuransi)->row();

            //GET Max Plafond, Max Usia, Min Usia, Max Jk per type manfaat

            $qCbcValidation = "SELECT * FROM tm_type_manfaat_setting 
                                 WHERE id_asuransi = '$dataAsuransi->c_id' AND id_produk = '$produkData->c_id' 
                                 AND id_type_manfaat = '$type_manfaat->c_id' AND status = '1' ";

            $getCbcValidation = $this->db->query($qCbcValidation)->row();

            if(empty($getCbcValidation)){
                $return = array(
                    "reffnumber" => $reffnum,
                    "no_aplikasi" => $noaplikasi,
                    "keterangan" => "Data Debitur tidak dapat di submit mohon dilakukan pengecekan kembali",
                    "status" => '10',
                    "error" => 'CBC Validation Not Found'
                );

                $this->writeLog('CN_REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');

                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            }

            if($akumulasi > (float)$dataAsuransi->max_plafond){
                $return = array(
                    "reffnumber" => $reffnum,
                    "no_aplikasi" => $noaplikasi,
                    "keterangan" => "Debitur telah melewati batas akumulasi plafond, akumulasi : ".number_format($akumulasi,0,',','.').
                                    ", Maksimal Limit Asuransi : ".number_format($dataAsuransi->max_plafond,0,',','.'),
                    "status" => '10',
                    "error" => 'Akumulasi Debitur'
                );

                $this->writeLog('CN_REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');

                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                
            }

            if($plafond > (float)$getCbcValidation->max_plafond){

                // $qAsuransi = "SELECT b.nama FROM tm_type_manfaat_setting a
                //                 LEFT JOIN tm_asuransi b ON b.c_id = a.id_asuransi
                //                  WHERE a.id_produk = '$produkData->c_id' AND a.id_type_manfaat = '$type_manfaat->c_id'
                //                   AND a.status = '1' AND a.max_plafond > '$plafond' ";

                // $maxAsuransi = $this->db->query($qAsuransi)->result_array();
                // $arrIns = '';

                // if(!empty($maxAsuransi)){
                //     $countIns = 1;
                //     foreach ($maxAsuransi as $value) {
                //         if(count($maxAsuransi) > $countIns){
                //             $arrIns .= $value['nama'].',';
                //         }else{
                //             $arrIns .= $value['nama'];
                //         }
                //     }

                //     $return = array(
                //         "reffnumber" => $reffnum,
                //         "no_aplikasi" => $noaplikasi,
                //         "keterangan" => "Plafond melebihi maximum plafond asuransi yang dipilih, silahkan pilih asuransi : ". $arrIns." untuk mengcover debitur terkait",
                //         "status" => '10',
                //         "error" => 'Plafond melewati batas max asuransi'
                //     );

				// 	$this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));
	
				// 	$this->output->set_header('HTTP/1.0 200 OK');
				// 	$this->output->set_header('HTTP/1.1 200 OK');

				// 	return $this->output
				// 	->set_status_header(200)
				// 	->set_content_type('application/json')
				// 	->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

                // }else{
                    $iscbc = true;
					$status_cbc = 1;
                    $ket_cbc = "Plafond debitur Melebihi maximum plafond Asuransi yang dipilih, Maximum plafond yg berjalan dengan asuransi yg di pilih adalah : Rp. ".number_format($getCbcValidation->max_plafond,0,',','.');
                // }
            }

            // $existingData = $this->db->get_where('tm_penutupan', array('no_aplikasi' => $noaplikasi))->row();
            
            // $this->db->select('*');
            // $this->db->where('no_aplikasi', $noaplikasi);
            // $existingData = $this->db->get($GLOBALS['table'])->row();
            $existingData = $this->db->query("SELECT * FROM ".$GLOBALS['table']." WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi' ORDER BY createdon DESC, c_id DESC")->row();
            $existingDataCbc = $this->db->query("SELECT * FROM ".$GLOBALS['table']." WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi' AND status_cbc = '1' ORDER BY createdon DESC, c_id DESC")->row();

            // var_dump($existingData);exit();
            if(!empty($existingData)){
                if($ktp != $existingData->ktp){
                    $return = array(
                        "reffnumber" => $reffnum,
                        "no_aplikasi" => $noaplikasi,
                        "keterangan" => "Data No. Identitas Debitur Pertanggungan tidak sesuai dengan data sebelumnya",
                        "status" => '10'
                    );
                    $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));

                    return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return));
                    die;
                }

                if($reffnum == $existingData->reffnumber){
                    $return = array(
                        "reffnumber" => $reffnum,
                        "no_aplikasi" => $noaplikasi,
                        "keterangan" => "Data Reffnumber tidak bisa sama dengan data sebelumnya",
                        "status" => '10'
                    );
                    $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));

                    return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return));
                    die;
                }
                

                $datefix = date('Y-m-d',strtotime($tanggalLahir));
                if($datefix != $existingData->tanggal_lahir){

                    $updatedDate = $this->db->query("SELECT tanggal_lahir, is_birth_update FROM ".$GLOBALS['table']." WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi' ORDER BY createdon DESC, c_id DESC")->row();

                    if($updatedDate->is_birth_update = 1){
                        $tanggalLahir = $updatedDate->tanggal_lahir;

                    }else{
                        $return = array(
                            "reffnumber" => $reffnum,
                            "no_aplikasi" => $noaplikasi,
                            "keterangan" => "Data Tanggal Lahir Debitur Pertanggungan tidak sesuai dengan data sebelumnya",
                            "status" => '10'
                        );
                        $this->writeLog('CN-REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));
    
                        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($return));
                        die;
                    }
                    
                }
            }
            if(!empty($existingDataCbc)){
                if(!empty($existingDataCbc->fixed_rate)){

                    if($plafond <> (float)$existingData->plafond){
                        $next_cbc = true;
                    }

                    if($type_coverages->nama != $existingData->type_coverage){
                        $next_cbc = true;
                    }

                    if($type_manfaat->c_id != $existingData->type_manfaat){
                        $next_cbc = true;
                    }

                    if($produkData->c_id != $existingData->produk){
                        $next_cbc = true;
                    }

                    if($produkData->c_id != $existingData->produk){
                        $next_cbc = true;
                    }

                    if($jangkaWaktu != $existingData->tenor){
                        $next_cbc = true;
                    }

                    if(date('Y-m-d',strtotime($tanggalLahir)) != $existingData->tanggal_lahir){
                        $next_cbc = true;
                    }

                    if(date('Y-m-d',strtotime($mulaiAkad)) != $existingData->tanggal_mulai){
                        $next_cbc = true;
                    }

                    if(date('Y-m-d',strtotime($akhirAkad)) != $existingData->tanggal_akhir){
                        $next_cbc = true;
                    }

                    if(!$next_cbc){
                        $this->generateCbcCn($data_json,$existingData->c_id);
                        die();
                    }

                }
            }

            $maxIns = (float)$dataAsuransi->max_plafond;
            $adminFee =(float)$dataAsuransi->admin_fee;

            $tenor_bulan = '';
            $usia_saat_akad = '';
            $usia_akhir_akad = '';

            //initialize transaction variable

            $feeAsuransi = 0;
            $totalBiaya = 0;
            $feeUjroh = 0;
            $premi = 0;
            $premiFinal = 0;
            $premiBeforeAdmin = 0;
            $premiFinal = 0;
            $biayaNasabah = 0;
            $biayaAtribusi = 0;
            $limitAsuransi = 0;
            $coverageType = '-';
            $rate = 0;
            $ratePercent = 0;
            $coverageType = '-';
            $status = 1;

            //CHECK Tanggal Akad Valid Atau Tidak

            if(!$checkDateStart || !$checkDateEnd){
                $status = 2;
                $tenor_bulan = '- (Tanggal Akad Tidak Sesuai)';
                $usia_saat_akad = '- (Tanggal Akad Tidak Sesuai)';
                $usia_akhir_akad = '- (Tanggal Akad Tidak Sesuai)';

                $feeAsuransi = 0;
                $coveragePinalti = 0;
                $totalBiaya = 0;
                $premi = 0;
                $premiFinal = 0;
                $premiBeforeAdmin = 0;
                $premiFinal = 0;
                $biayaNasabah = 0;
                $biayaAtribusi = 0;
                $limitAsuransi = 0;
                $coverageType = '-';
                $rate = 0;
                $ratePercent = 0;
                $coverageType = $type_coverages->nama;

            }else{
            
                $date1 = $tanggalLahir;
                $date2 = $mulaiAkad;
                $date3 = $akhirAkad;

                $diffAge = abs(strtotime($date2) - strtotime($date1));
                $diffAgeEnd = abs(strtotime($date3) - strtotime($date1));
                $diffTenor = abs(strtotime($date3) - strtotime($date2));

                $yearsAge = floor($diffAge / (365*60*60*24));
                $monthsAge = floor(($diffAge - $yearsAge * 365*60*60*24) / (30*60*60*24));
                $daysAge = floor(($diffAge - $yearsAge * 365*60*60*24 - $monthsAge*30*60*60*24)/ (60*60*24));

                $yearsAgeEnd = floor($diffAgeEnd / (365*60*60*24));
                $monthsAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24) / (30*60*60*24));
                $daysAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24 - $monthsAgeEnd*30*60*60*24)/ (60*60*24));

                $yearsTenor = floor($diffTenor / (365*60*60*24));
                $monthsTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24) / (30*60*60*24));
                $daysTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24 - $monthsTenor*30*60*60*24)/ (60*60*24));

                $monthsAgeEndRound = $monthsAgeEnd;
                $yearsAgeEndRound = $yearsAgeEnd;

                $monthsAgeRound = $monthsAge;
                $yearsAgeRound = $yearsAge;

                if($daysAgeEnd > 14){
                    $monthsAgeEndRound = $monthsAgeEnd + 1;
                }

                if($monthsAgeEndRound > 6){
                    $yearsAgeEndRound = $yearsAgeEnd + 1;
                }

                if($daysAge > 14){
                    $monthsAgeRound = $monthsAge + 1;
                }

                if($monthsAgeRound > 6){
                    $yearsAgeRound = $yearsAge + 1;
                }

                $tenorMonth = ($yearsTenor * 12) + $monthsTenor;

                $tenor_bulan = $data_json['tenor'].' ( '.$tenorMonth.' Bulan '.$daysTenor.' Hari )';
                $usia_saat_akad = $yearsAgeRound.' Tahun ( '.$yearsAge.' Tahun '.$monthsAge.' Bulan '.$daysAge.' Hari )';
                $usia_akhir_akad = $yearsAgeEndRound.' Tahun ( '.$yearsAgeEnd.' Tahun '.$monthsAgeEnd.' Bulan '.$daysAgeEnd.' Hari )';
                

                // printf("%d years, %d months, %d days\n", $yearsAge, $monthsAge, $daysAge);
                // echo "</br>";
                // printf("%d years, %d months, %d days\n", $yearsTenor, $monthsTenor, $daysTenor);

                $round = false;
                if($daysTenor = 0 && $monthsTenor = 0){
                    $round = true;
                }

                // if($daysTenor > 14){
                //     $monthsTenor = $monthsTenor + 1;
                // }

                $notValidYears = false;

                if($yearsTenor > 0){
                    // if($monthsTenor > 6){
                    //     $yearsTenor = $yearsTenor + 1;
                    // }
                }else{
                    $round = true;
                    $notValidYears = true;
                    $yearsTenor = 1;
                }

                if($yearsAgeRound > $getCbcValidation->max_usia || $yearsAgeRound < $getCbcValidation->min_usia){
                    $iscbc = true;
                    $status_cbc = 1;
                    $ket_cbc = "Usia debitur tidak termasuk data maximum / minimum asuransi yang dipilih, usia max / min yg berjalan dengan asuransi yg di pilih adalah max : ".$getCbcValidation->max_usia. " / min : $getCbcValidation->min_usia";
                }

                if($yearsTenor > $getCbcValidation->max_jangka_waktu){
                    $iscbc = true;
                    $status_cbc = 1;
                    $ket_cbc = "Jangka Waktu Melebihi Maximum jangka waktu asuransi yang dipilih, maximum jangka waktu yg berjalan dengan asuransi yg di pilih adalah : ".$getCbcValidation->max_jangka_waktu." Tahun";
                }

                if(!$iscbc){

                    $db_rate = 'tm_rate';
                    $rateAskrida = false;
                    $ins_id = $dataAsuransi->c_id;
                    $rateAskrida = false;

                    if($produkData == '2'){
                        $db_rate = 'tm_rate_pensiun';
                    }elseif($produkData == '4'){
                        $db_rate = 'tm_rate_prapensiun';
                    }
                    if($type_manfaat->c_id == '3'){
                        $yearsAge = 0;
                    }

                    $qRate = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '$dataAsuransi->c_id' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id' ";
                    $getRate = $this->db->query($qRate)->row();
                    
                    //var_dump($qRate);exit();
                    
                    if(empty($getRate)){
                        
                        $qRate = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id' ";
                        $getRate2 = $this->db->query($qRate)->row();
                        $rateAskrida = true;
                       if(empty($getRate2)){
                            $return = array(
                                "reffnumber" => $reffnum,
                                "no_aplikasi" => $noaplikasi,
                                "keterangan" => "Rate tidak ditemukan untuk usia : ".$yearsAge.", tenor : ".$yearsTenor. " Tahun Dan Asuransi : ".$dataAsuransi->alias,
                                "status" => '10'
                            );

                            $this->writeLog('CN_REQUEST','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return));


                            $this->output->set_header('HTTP/1.0 200 OK');
                            $this->output->set_header('HTTP/1.1 200 OK');
                            

                            return $this->output
                            ->set_status_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                            die;
                       }
                       
                       $getRate = $getRate2;
                    }

                    $finalDivider = 100;
                    if($notValidYears){
                        $qRateMonth = "SELECT * FROM tm_rate_month WHERE bulan = '$monthsTenor'";
                        $getRateMonth = $this->db->query($qRateMonth)->row();
                        $finalDivider = $getRateMonth->rate;
                    }

                    $ratePercent = (float)$getRate->rate;

                    if(!$round){
                        if($yearsTenor < 15){
                            $yearsTenor = $yearsTenor + 1;
                        }
                        
                        $qRateUp = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '$dataAsuransi->c_id' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id'";

                        if($rateAskrida){
                            $qRateUp = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id'";
                        }
                        
                        //var_dump($qRateUp);exit();

                        $getRateUp = $this->db->query($qRateUp)->row();
                        $rateUpDown = (((float)$getRateUp->rate * 1) / 100) - (($ratePercent * 1) / 100);
                        $rateDivid = 0.08 * $rateUpDown;
                        $rateDown = (((float)$getRate->rate * 1) / 100);
                        $ratePercent = ($rateDown + $rateDivid) * 100;

                    }

                    $rate = (($ratePercent * 1) / 100);

                    $hasil = "";
                    if($type_coverage == '1' || $type_coverage == '2'){

                        // TYPE COVERAGE = Reguler (Tidak Ada Atribusi)

                        $feeAsuransi = $plafond * $rate;
                        $coveragePinalti = 0;
                        $premi = $feeAsuransi;
                        $premiFinal = round($premi * (($finalDivider * 1) / 100));
                        $premiBeforeAdmin = $premiFinal;
                        $premiFinal = $premiFinal + $adminFee;
                        $biayaNasabah = $feeAsuransi;
                        $biayaAtribusi = $totalBiaya - $feeAsuransi;
                        $limitAsuransi = $plafond;
                        $coverageType = $type_coverages->nama;
                        $biayaAtribusi = $totalBiaya - $biayaNasabah;
                        $hasil = "OKEH Satu";
                        $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;

                    }else{
                        if($type_coverage == '5' || $type_coverage == '3'){

                            // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE ASURANSI

                            $dividerRate = 1 - $rate;
                            $biayaNasabah = 0;
                            $amountDivider = ($plafond * $rate) / $dividerRate;
                            $amountRate = $plafond + $amountDivider;
                            $feeAsuransi = $rate * round($amountRate);
                            
                            $totalBiaya = $feeAsuransi + $coveragePinalti;

                            $limitAsuransi = $plafond + $feeAsuransi;
                            $premiDived = round($limitAsuransi * $rate);
                            $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                            $premiBeforeAdmin = $premiFinal;
                            $premiFinal = $premiFinal + $adminFee;
                            $coverageType = $type_coverages->nama;
                            $biayaAtribusi = $totalBiaya - $biayaNasabah;
                            $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                            $hasil = "OKEH Dua";

                        }elseif($type_coverage == '7' || $type_coverage == '4'){

                            // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI

                            $feeAsuransi = $rate * ($plafond + $coveragePinalti);
                            $totalBiaya = $feeAsuransi + $coveragePinalti;
                            $biayaNasabah = $feeAsuransi;
                            $limitAsuransi = $plafond + $coveragePinalti;
                            $biayaAtribusi = $totalBiaya - $biayaNasabah;

                            $premiDived = round($limitAsuransi * $rate);
                            $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                            $premiBeforeAdmin = $premiFinal;
                            $premiFinal = $premiFinal + $adminFee;
                            $coverageType = $type_coverages->nama;
                            $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                            $hasil = "OKEH Tiga";

                        }elseif($type_coverage == '6' || $type_coverage == '8'){

                            // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI + FEE ASURANSI (ALL COVER)
                            
                            $dividerRate = 1 - $rate;
                            $plafondFinal = $plafond + $coveragePinalti;
                            $amountDivider = $rate * ($plafondFinal / $dividerRate);
                            $feeAsuransi = $rate * ($plafondFinal + $amountDivider);

                            $biayaNasabah = 0;
                            $totalBiaya = $feeAsuransi + $coveragePinalti;
                            $limitAsuransi = $plafondFinal + $feeAsuransi + $coveragePinalti;
                            $biayaAtribusi = $totalBiaya + $biayaNasabah;

                            $premiDived = round($limitAsuransi * $rate);
                            $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                            $premiBeforeAdmin = $premiFinal;
                            $premiFinal = $premiFinal + $adminFee;
                            $coverageType = $type_coverages->nama;
                            $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                            $hasil = "OKEH Empat";
                            

                        }
                    }

                }else{

                    $status = 1;
                    $hasil = 'cbc';
                    //CBC hanya set coverage type nya saja
                    $coverageType = $type_coverages->nama;

                }
            }

            $this->db->select('c_id');
            $this->db->WHERE('kode_type', $data_json['tipe_produk']);
            $type_produk = $this->db->get($GLOBALS['tableProdukType'])->row()->c_id;
            
            $this->db->select('c_id');
            $this->db->like('kode_produk', $data_json['produk']);
            $produkId = $this->db->get($GLOBALS['tableProduk'])->row()->c_id;

            $this->db->select('c_id');
            $this->db->where('id_produk', $produkId);
            $this->db->where('kode_pekerjaan', $data_json['jenis_pekerjaan']);
            $jenis_pekerjaan = $this->db->get($GLOBALS['tableJenisPekerjaan'])->row()->c_id;

            $this->db->select('c_id');
            $this->db->like('kode_akad', $data_json['jenis_akad']);
            $jenis_akad = $this->db->get($GLOBALS['tableAkad'])->row()->c_id;


            $insArr = array(
                'id_asuransi' => $dataAsuransi->c_id,
                'reffnumber' => $reffnum,
                'sales_code' => $data_json['sales_code_non_marketing'],
                'no_aplikasi' => $noaplikasi,
                'kode_cabang' => $cabang->kode_cabang,
                'nama_cabang' => $cabang->nama,
                'nama' => $data_json['nama_nasabah'],
                'ktp' => $ktp,
                'alamat' => $data_json['alamat_sesuai_ktp'],
                'kodepos' => $data_json['kode_pos'],
                'jenis_kelamin' => $data_json['jenis_kelamin'],
                'tanggal_lahir' => date('Y-m-d',strtotime($tanggalLahir)),
                'nama_asuransi' => $dataAsuransi->nama,
                'no_hp' => $data_json['no_hp'],
                'npwp' => $data_json['npwp'],
                'produk' => $produkId,
                'tipe_produk' => $type_produk,
                'jenis_pekerjaan' => $jenis_pekerjaan,
                'nama_instansi' => $data_json['nama_instansi'],
                'jenis_agunan' => $data_json['jenis_agunan'],
                'nomor_agunan' => $data_json['nomor_agunan'],
                'nilai_agunan' => $data_json['nilai_agunan'],
                'jenis_akad' => $jenis_akad,
                'nomor_akad' => $data_json['nomor_akad'],
                'plafond' => (float)$data_json['plafond'],
                'tanggal_akad' => $checkDateStart ? date('Y-m-d',strtotime($mulaiAkad)) : NULL,
                'tanggal_mulai' => $checkDateStart ? date('Y-m-d',strtotime($mulaiAkad)) : NULL,
                'tanggal_akhir' => $checkDateEnd ? date('Y-m-d',strtotime($akhirAkad)) : NULL,
                'atribusi' => $atribusi,
                'nilai_pengajuan' => (float)$data_json['nilai_pengajuan'],
                'tenor' => $data_json['tenor'],
                'margin' => $data_json['margin'],
                'tanggal_pencairan' => $data_json['tgl_pencairan'],
                'premi' => $premiFinal,
                'tanggal_bayar_ijk' => empty($data_json['tgl_bayar_ijk']) ? null : $data_json['tgl_bayar_ijk'],
                'no_ld' => $data_json['no_ld'],
                'type_coverage' => $coverageType,
                'coverage_plafond' => $coveragePlafond,
                'coverage_fee_mitra' => $coverageFeeMitra,
                'coverage_pinalti' => $coveragePinalti,
                'coverage_asuransi' => $coverageAsuransi,
                'type_manfaat' => $type_manfaat->c_id,
                'ratedef' => $data_json['ratedef'],
                'fee_asuransi' => $feeAsuransi,
                'total_biaya' => $totalBiaya,
                'biaya_nasabah' => $biayaNasabah,
                'limit_asuransi' => $limitAsuransi,
                'biaya_atribusi' => $biayaAtribusi,
                'fee_ujroh' => $feeUjroh,
                'biaya_admin' => $adminFee,
                'premi_before_admin' => $premiBeforeAdmin,
                'bulan_tenor' => $tenor_bulan,
                'usia_saat_akad' => $usia_saat_akad,
                'usia_selesai_akad' => $usia_akhir_akad,
                'fixed_rate' => $ratePercent,
                'coverage_type' => $coverageType,
                'status' => $status,
                'status_data' => 1,
                'status_cbc' => $status_cbc,
                'ket_cbc' => $ket_cbc,
                'createdon' => date('Y-m-d H:i:s'),
                'createdby' => '2',
                'stscurl' => 0
            );
            
            $insert = $this->db->insert($GLOBALS['table'], $insArr);

            if($insert){
                $generatePdf = $this->generateCN($noaplikasi,$reffnum);

                $pdf = '-';
                // var_dump($generatePdf);exit();
                $path = './upload/dok_cn/'.$generatePdf;
                $pdf = chunk_split(base64_encode(file_get_contents($path)));

                $this->db->query("UPDATE ".$GLOBALS['table']." SET cnPdf = '$generatePdf' WHERE no_aplikasi = '$noaplikasi' AND reffnumber = '$reffnum' ");
                
                if(!empty($exData)){
                    $this->db->query("UPDATE ".$GLOBALS['table']." SET status_data = '0' WHERE no_aplikasi = '$noaplikasi' AND reffnumber != '$reffnum' "); 
                }

                if(!$iscbc){
                    
                    $return = array(
                        'reffnumber' => $reffnum,
                        'no_aplikasi' => $noaplikasi,
                        'no_covernote' => $noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => $premiFinal,
                        'coverage' => $limitAsuransi,
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Free Cover (CAC) ",
                        'status' => '00',
                        'flag_spk' => 'Sukses',
                        'file_pdf' => $pdf
                    );

                    $returnHis = array(
                        'reffnumber' => $reffnum,
                        'no_aplikasi' => $noaplikasi,
                        'no_covernote' => $noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => $premiFinal,
                        'coverage' => $limitAsuransi,
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Free Cover (CAC) ",
                        'status' => '00',
                        'flag_spk' => 'Sukses',
                        'file_pdf' => substr($pdf,0,45)."..."
                    );

                }else{
                    $return = array(
                        'reffnumber' => $reffnum,
                        'no_aplikasi' => $noaplikasi,
                        'no_covernote' => $noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => $premiFinal,
                        'coverage' => $limitAsuransi,
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Case By Case (CBC) ",
                        'status' => '00',
                        'flag_spk' => 'Sukses',
                        'file_pdf' => $pdf
                    );

                    $returnHis = array(
                        'reffnumber' => $reffnum,
                        'no_aplikasi' => $noaplikasi,
                        'no_covernote' => $noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => $premiFinal,
                        'coverage' => $limitAsuransi,
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Case By Case (CBC) ",
                        'status' => '00',
                        'flag_spk' => 'Sukses',
                        'file_pdf' => substr($pdf,0,45)."..."
                    );
                }

            }else{
                $return = array(
                    "reffnumber" => $reffnum,
                    "no_aplikasi" => $noaplikasi,
                    "keterangan" => "Opps.. Proses gagal mohon dicoba kembali",
                    "status" => '10'
                );

                $returnHis = array(
                    "reffnumber" => $reffnum,
                    "no_aplikasi" => $noaplikasi,
                    "keterangan" => "Opps.. Proses gagal mohon dicoba kembali",
                    "status" => '10'
                );
            }

            $this->writeLog('CN-REQUEST','SUCCESS',TR_CN_HIS,NULL,$rawdata,json_encode($returnHis));

            return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($return));
            die;

        }catch(Exception $e){
            $return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLog('CN-REQUEST','ERROR',TR_CN_HIS,NULL,$rawdata,json_encode($error));
            
            return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($return));
        }
    }


    function generateCbcCn($data_json,$c_id){
        
        $this->db->select('*');
        $this->db->WHERE('c_id', $c_id);
        $exData = $this->db->get($GLOBALS['table'])->row();
        
        $noaplikasi = $exData->no_aplikasi;
        $reffnum = $data_json['reffnumber'];

        $insArr = array(
            'id_asuransi' => $exData->id_asuransi,
            'reffnumber' => $data_json['reffnumber'],
            'sales_code' => $data_json['sales_code_non_marketing'],
            'no_aplikasi' => $exData->no_aplikasi,
            'kode_cabang' => $exData->kode_cabang,
            'nama_cabang' => $exData->nama_cabang,
            'nama' => $data_json['nama_nasabah'],
            'ktp' => $exData->ktp,
            'alamat' => $data_json['alamat_sesuai_ktp'],
            'kodepos' => $data_json['kode_pos'],
            'jenis_kelamin' => $data_json['jenis_kelamin'],
            'tanggal_lahir' => $exData->tanggal_lahir,
            'nama_asuransi' => $exData->nama_asuransi,
            'no_hp' => $exData->no_hp,
            'npwp' => $exData->npwp,
            'produk' => $exData->produk,
            'tipe_produk' => $exData->tipe_produk,
            'jenis_pekerjaan' => $exData->jenis_pekerjaan,
            'nama_instansi' => $data_json['nama_instansi'],
            'jenis_agunan' => $data_json['jenis_agunan'],
            'nomor_agunan' => $data_json['nomor_agunan'],
            'nilai_agunan' => $data_json['nilai_agunan'],
            'jenis_akad' => $exData->jenis_akad,
            'nomor_akad' => $data_json['nomor_akad'],
            'plafond' => (float)$data_json['plafond'],
            'tanggal_akad' => $exData->tanggal_akad,
            'tanggal_mulai' => $exData->tanggal_mulai,
            'tanggal_akhir' => $exData->tanggal_akhir,
            'atribusi' => $exData->atribusi,
            'nilai_pengajuan' => (float)$data_json['nilai_pengajuan'],
            'tenor' => $exData->tenor,
            'margin' => $exData->margin,
            'tanggal_pencairan' => $data_json['tgl_pencairan'],
            'premi' => $exData->premi,
            'tanggal_bayar_ijk' => empty($data_json['tgl_bayar_ijk']) ? null : $data_json['tgl_bayar_ijk'],
            'no_ld' => $data_json['no_ld'],
            'type_coverage' => $exData->type_coverage,
            'coverage_plafond' => $exData->coverage_plafond,
            'coverage_fee_mitra' => $exData->coverage_fee_mitra,
            'coverage_pinalti' => $exData->coverage_pinalti,
            'coverage_asuransi' => $exData->coverage_asuransi,
            'type_manfaat' => $exData->type_manfaat,
            'ratedef' => $data_json['ratedef'],
            'fee_asuransi' => $exData->fee_asuransi,
            'total_biaya' => $exData->total_biaya,
            'biaya_nasabah' => $exData->biaya_nasabah,
            'limit_asuransi' => $exData->limit_asuransi,
            'biaya_atribusi' => $exData->biaya_atribusi,
            'fee_ujroh' => $exData->fee_ujroh,
            'biaya_admin' => $exData->biaya_admin,
            'premi_before_admin' => $exData->premi_before_admin,
            'bulan_tenor' => $exData->bulan_tenor,
            'usia_saat_akad' => $exData->usia_saat_akad,
            'usia_selesai_akad' => $exData->usia_selesai_akad,
            'fixed_rate' => $exData->fixed_rate,
            'coverage_type' => $exData->coverage_type,
            'status' => 1,
            'status_data' => 1,
            'status_cbc' => 1,
            'ket_cbc' => null,
            'createdon' => date('Y-m-d H:i:s'),
            'createdby' => '2',
            'stscurl' => 0
        );

        $insert = $this->db->insert($GLOBALS['table'], $insArr);
        

        if($insert){
            $id = $this->db->insert_id();
            $generatePdf = $this->generateCN2($id);

            $pdf = '-';
            // var_dump($generatePdf);exit();
            $path = './upload/dok_cn/'.$generatePdf;
            $pdf = chunk_split(base64_encode(file_get_contents($path)));

            $this->db->query("UPDATE ".$GLOBALS['table']." SET cnPdf = '$generatePdf' WHERE no_aplikasi = '$noaplikasi' AND reffnumber = '$reffnum' ");
            
            if(!empty($exData)){
                $this->db->query("UPDATE ".$GLOBALS['table']." SET status_data = '0' WHERE no_aplikasi = '$noaplikasi' AND reffnumber != '$reffnum' "); 
            }

            $return = array(
                'reffnumber' => $reffnum,
                'no_aplikasi' => $noaplikasi,
                'no_covernote' => $noaplikasi.'-'.$reffnum,
                'tgl_covernote' => date('Y-m-d'),
                'nilai_ijk' => $exData->premi,
                'coverage' => $exData->limit_asuransi,
                "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Case By Case (CBC) ",
                'status' => '00',
                'flag_spk' => 'Sukses',
                'file_pdf' => $pdf
            );

            $returnHis = array(
                'reffnumber' => $reffnum,
                'no_aplikasi' => $noaplikasi,
                'no_covernote' => $noaplikasi.'-'.$reffnum,
                'tgl_covernote' => date('Y-m-d'),
                'nilai_ijk' => $exData->premi,
                'coverage' => $exData->limit_asuransi,
                "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Case By Case (CBC) ",
                'status' => '00',
                'flag_spk' => 'Sukses',
                'file_pdf' => substr($pdf,0,45)."..."
            );

        }else{
            $return = array(
                "reffnumber" => $reffnum,
                "no_aplikasi" => $noaplikasi,
                "keterangan" => "Opps.. Proses gagal mohon dicoba kembali",
                "status" => '10'
            );

            $returnHis = array(
                "reffnumber" => $reffnum,
                "no_aplikasi" => $noaplikasi,
                "keterangan" => "Opps.. Proses gagal mohon dicoba kembali",
                "status" => '10'
            );
        }

        $this->writeLog('CN-REQUEST','SUCCESS',TR_CN_HIS,NULL,json_encode($data_json),json_encode($returnHis));

        header('Content-Type: application/json');
        echo json_encode($return);
        die();

    }

    

    public function checkHeaders($validHeader){
        try{
            $reqHeader = $this->input->server('REQUEST_METHOD');

            if(strtoupper($reqHeader) != strtoupper($validHeader)){
                $return = array("Result"=>array('status_code' => '12', "message"=>"Invalid ".$reqHeader." METHOD on this function"));
                header('Content-Type: application/json');
                echo json_encode($return);
                die();
            }

        }catch(Exception $e){

        }
    }
}

   
