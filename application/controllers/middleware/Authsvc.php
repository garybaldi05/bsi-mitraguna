<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authsvc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Service_m');
        $this->load->model('Penutupan_m');
    }

    function Authorization(){
        $validHeader = POST_METHOD;
        $reqHeader = $this->input->server('REQUEST_METHOD');

        if(strtoupper($reqHeader) != strtoupper($validHeader)){
            $return = array("Result"=>array('status_code' => '12', "message"=>"Invalid Request METHOD on this function"));

            $this->writeLog('GET-AUTH-MIDSVC','ERROR',TR_DATA,NULL,json_encode($return));
                
            return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($return));

            exit();
        }
       
        try{
            $start = new DateTime();

            $data = $this->input->raw_input_stream;
            $data_json = json_decode($data, true);

            $clientid = $data_json['client_id'];
            $client_secret = $data_json['client_secret'];

            $checkAuth = "SELECT * FROM td_authsession WHERE c_username = '$clientid' ORDER BY c_createdon DESC";

            $dataAuth = $this->db->query($checkAuth)->row();
            
            if(!empty($dataAuth)){
                $datenow = new DateTime();
                $datevalid = new DateTime($dataAuth->c_active_until);

                if($datenow < $datevalid){
                    $active = $datevalid->getTimestamp();
                    $return = array("Result"=>array('status_code' => '00', "message"=>"Success, token still valid", "token" => $dataAuth->c_token, "valid_until" => $active));
                  
                    $this->writeLog('GET-AUTH-MIDSVC','SUCCESS',TR_DATA,$data,json_encode($return));
                  
                    return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return));

                    exit();
                    
                }
            }

            $user = $this->Service_m->getToken($clientid,$client_secret);

            if(!empty($user)){
                if($user->Password != $client_secret){
                    $return = array("Result"=>array('status_code' => '10', "message"=>"Invalid Password"));
                    
                    $this->writeLog('GET-AUTH-MIDSVC','ERROR',TR_DATA,$data,json_encode($return));

                    return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return));

                    exit();
              
                }

                $token = openssl_random_pseudo_bytes(25);
                $token = bin2hex($token);

                date_default_timezone_set('Asia/Jakarta');
                
               
                $datetime = $start->modify('+1 day');
                $validuntil = $datetime->getTimestamp();
                
                $this->db->trans_begin();

                $insertQuery = "
                insert into td_authsession (
                    c_username,
                    c_token,
                    c_active_until,
                    c_createdon,
                    c_createdby
                    ) 
                    values (
                    '".$clientid."',
                    '".$token."',
                    '".$datetime->format('Y-m-d H:i:s')."',
                    '".$start->format("Y-m-d H:i:s")."',
                    '".$clientid."'
                    )
                ";

                $insert = $this->db->query($insertQuery);

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    
                    $return = array("Result"=>array('status_code' => '10', "message"=>"Error, Please Contact your system Administrator", "token" => "", "valid_until" => ""));
                    
                    $this->writeLog('GET-AUTH-MIDSVC','ERROR',TR_DATA,$data,json_encode($return));

                    return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return));

                    exit();
               

                    
                }
                else
                {
                    $this->db->trans_commit();

                    
                    $return = array("Result"=>array('status_code' => '00', "message"=>"Success", "token" => $token, "valid_until" => $validuntil));
                    
                    $this->writeLog('GET-AUTH-MIDSVC','SUCCESS',TR_DATA,$data,json_encode($return));

                    return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return));

                    exit();
              
                }
            }else{
                $return = array("Result"=>array('status_code' => '13', "message"=>"User not Found", "token" => "", "valid_until" => ""));
                    
                $this->writeLog('GET-AUTH-MIDSVC','ERROR',TR_DATA,$data,json_encode($return));

                return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($return));

                exit();
            }
        }catch(Exception $e){
            $this->db->trans_rollback();
           
            $return = array("Result"=>array('status_code' => '10', "message"=>"Error While Fecthing data onto DB, Please Contact your system Administrator", "token" => "", "valid_until" => ""));

            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($return));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLog('GET-AUTH-MIDSVC','ERROR',TR_DATA,$data,json_encode($error));
        }

    }

    function get_capem()
    {
        $id=$this->input->post('id');
        $data=$this->Penutupan_m->get_capem($id);
        echo json_encode($data);
    }

    public function checkHeaders($validHeader){
        try{
            $reqHeader = $this->input->server('REQUEST_METHOD');

            if(strtoupper($reqHeader) != strtoupper($validHeader)){
              return true;
            }else{
              return false;
            }

        }catch(Exception $e){

        }
    }

    public function writeLog($actionname,$status,$trigger,$jsonold,$jsonnew){

        try{
            $this->db->trans_off();

            // list of database variable
            $table = "";
            $db = DB_NAME_PREFIX;
            $uniqeKey = '';

            //datetime variable
            date_default_timezone_set('Asia/Jakarta');
            $todays = date("Y-m-d H:i:s");
            $username = 'ADMIN'; //for testing only change it to session user
            // $usernme = $this->session->userdata('KodeUser');


            if($trigger == TR_POSTING){
                $table = $db.'PostingHistory';

            }elseif($trigger == TR_SHARE){
                $table = $db.'ShareHistory';

            }elseif($trigger == TR_DATA){
                $table = $db.'DataHistory';
            }

            $this->db->trans_begin();

            $query = "
                INSERT INTO $table
                (
                    actionname,
                    status,
                    dataold,
                    datanew,
                    createdon,
                    createdby
                )
                VALUES
                (
                    '$actionname',
                    '$status',
                    '$jsonold',
                    '$jsonnew',
                    '$todays',
                    '$username'
                )
            ";

            $this->db->query($query);

            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
            }
            else
            {
                    $this->db->trans_commit();
            }

        }catch(Exception $e){
            $this->db->trans_rollback();
            throw new DomainException($e->getMessage(), 0, $e);
        }

    }
}

   
