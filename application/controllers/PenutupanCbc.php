<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once './vendor/autoload.php';
include_once (dirname(__FILE__) . "/./Master_Controller.php");

global $table, $tableUser, $tableProduk, 
       $tableProdukType, $tableTypeManfaat, $tableJenisPekerjaan,
       $tableAkad, $tableCabang ;

$table = DB_NAME_PREFIX.'tm_penutupan';
$tableUser = DB_NAME_PREFIX.'tm_user';
$tableProduk = DB_NAME_PREFIX.'tm_product_list';
$tableProdukType = DB_NAME_PREFIX.'tm_product_type';
$tableJenisPekerjaan = DB_NAME_PREFIX.'tm_jenis_pekerjaan';
$tableAkad = DB_NAME_PREFIX.'tm_jenis_akad';
$tableTypeManfaat = DB_NAME_PREFIX.'tm_type_manfaat';
$tableCabang = DB_NAME_PREFIX.'tm_cabang';
$tableLog = DB_NAME_PREFIX.'tl_log_data';

class PenutupanCbc extends Master_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('PenutupanCbc_m','mainModel');

		$userSession = $this->session->userdata('Username');
        if(!isset($userSession)){
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }

    }

	function index()
	{
		$this->template->load('template','penutupancbc/list');
    }
    
	function view_data(){
		$cari = $_POST;
		$data = $this->mainModel->get_tables($cari);

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
		die;

	}

	function view(){
		$id = base64_decode($_GET['data']);
		$type = $_GET['type'];
		
		$data['editData'] = $this->mainModel->getDatabyId($id,$type);

		$this->template->load('template','penutupancbc/view',$data);
	}

	function edit(){
		$id = base64_decode($_GET['data']);
		$type = '1';
		
		$data['editData'] = $this->mainModel->getDatabyId($id,$type);

		$this->template->load('template','penutupancbc/edit',$data);
	}

	function input(){
		$this->template->load('template','penutupancbc/input');
	}

	function History(){
		$id = base64_decode($_GET['data']);
		
		$this->db->select('*');
		$this->db->where('no_aplikasi',$id);
		$this->db->order_by('createdon', 'DESC');
		$oneData = $this->db->get('tm_penutupan')->row();

		$data['oneData'] = $oneData;

		$this->template->load('template','penutupancbc/history',$data);
	}

	function getJenisPekerjaan(){
		$id = $_GET['id'];

		$this->db->select('c_id, nama');
		$this->db->where('id_produk',$id);
		$this->db->order_by('nama', 'ASC');
		$oneData = $this->db->get('tm_jenis_pekerjaan')->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;

	}

	function update(){
		ini_set('max_execution_time', '0');
        ini_set("memory_limit","-1");

		extract($_POST);
		$today = date('Y-m-d');
		$dateon = date('Y-m-d H:i:s');
        try{
        $createdby = strtoupper($this->session->userdata('KodeUser'));
        $userSession = $this->session->userdata('UserId');
        $file = $_FILES['file']['name'];
        $file2 = $_FILES['file2']['name'];

        $fileName = str_replace(' ', '', $file);
        $explode = explode(".",$fileName);
        $date = date("Ymd_His");
        $fileexp = $explode[0].'_'.$date.'.'.$explode[1];

		$fileName2 = str_replace(' ', '', $file2);
        $explode2 = explode(".",$fileName);
        $fileexp2 = $explode2[0].'_'.$date.'.'.$explode2[1];

        $filenames = '';
        
        
        $config['upload_path'] = './upload/dok_cn/';
        $config['file_name'] = $fileexp;
        // $config['allowed_types'] = 'xls|xlsx|csv';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;

		$config2['upload_path'] = './upload/dok_cn/';
        $config2['file_name'] = $fileexp2;
        // $config['allowed_types'] = 'xls|xlsx|csv';
        $config2['allowed_types'] = '*';
        $config2['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);
        
        if(! $this->upload->do_upload('file') ){
			var_dump($this->upload->display_errors());
			exit();
        }else{
            $uploadData = $this->upload->data(); 
            $filenames = $uploadData['file_name'];

			$this->upload->initialize($config2);
			if(! $this->upload->do_upload('file2') ){
				var_dump($this->upload->display_errors());
				exit();
			}else{
				$uploadData2 = $this->upload->data(); 
				$filenames2 = $uploadData2['file_name'];
			
			}
           
        }

		

		$this->db->select('*');
		$this->db->where('c_id',$c_id);
		$data = $this->db->get('tm_penutupan')->row();

		// var_dump($data);exit();

		$this->db->select('rate_ujroh');
            $this->db->WHERE('c_id', $data->produk);
            $rate_ujroh = $this->db->get($GLOBALS['tableProduk'])->row()->rate_ujroh;

		$qAsuransi = "SELECT * FROM tm_asuransi WHERE c_id = '$data->id_asuransi' ";

        $dataAsuransi = $this->db->query($qAsuransi)->row();

		$maxIns = (float)$dataAsuransi->max_plafond;
        $adminFee =(float)$dataAsuransi->admin_fee;

		$coveragePlafond = (float)$data->plafond;
		$plafond = (float)$data->plafond;
		$coverageFeeMitra = (float)$data->coverage_fee_mitra;
		$coveragePinalti = (float)$data->coverage_pinalti;
		$coverageAsuransi = (float)$data->coverage_asuransi;

		$atribusi = $data->atribusi;
		// if($coverageFeeMitra > 0 && ($coverageAsuransi > 0 || $$coveragePinalti > 0)){
		// 	$atribusi = 'YA';
		// }

		$date1 = $data->tanggal_lahir;
		$date2 = $data->tanggal_mulai;
		$date3 = $data->tanggal_akhir;

		$diffAge = abs(strtotime($date2) - strtotime($date1));
		$diffAgeEnd = abs(strtotime($date3) - strtotime($date1));
		$diffTenor = abs(strtotime($date3) - strtotime($date2));

		$yearsAge = floor($diffAge / (365*60*60*24));
		$monthsAge = floor(($diffAge - $yearsAge * 365*60*60*24) / (30*60*60*24));
		$daysAge = floor(($diffAge - $yearsAge * 365*60*60*24 - $monthsAge*30*60*60*24)/ (60*60*24));
		
		$yearsAgeEnd = floor($diffAgeEnd / (365*60*60*24));
		$monthsAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24) / (30*60*60*24));
		$daysAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24 - $monthsAgeEnd*30*60*60*24)/ (60*60*24));

		$yearsTenor = floor($diffTenor / (365*60*60*24));
		$monthsTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24) / (30*60*60*24));
		$daysTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24 - $monthsTenor*30*60*60*24)/ (60*60*24));

		$monthsAgeEndRound = $monthsAgeEnd;
		$yearsAgeEndRound = $yearsAgeEnd;

		$monthsAgeRound = $monthsAge;
		$yearsAgeRound = $yearsAge;

		if($daysAgeEnd > 14){
			$monthsAgeEndRound = $monthsAgeEnd + 1;
		}

		if($monthsAgeEndRound > 6){
			$yearsAgeEndRound = $yearsAgeEnd + 1;
		}

		if($daysAge > 14){
			$monthsAgeRound = $monthsAge + 1;
		}

		if($monthsAgeRound > 6){
			$yearsAgeRound = $yearsAge + 1;
		}

		$tenorMonth = ($yearsTenor * 12) + $monthsTenor;

		$tenor_bulan = $data->tenor.' ( '.$tenorMonth.' Bulan '.$daysTenor.' Hari )';
		$usia_saat_akad = $yearsAgeRound.' Tahun ( '.$yearsAge.' Tahun '.$monthsAge.' Bulan '.$daysAge.' Hari )';
		$usia_akhir_akad = $yearsAgeEndRound.' Tahun ( '.$yearsAgeEnd.' Tahun '.$monthsAgeEnd.' Bulan '.$daysAgeEnd.' Hari )';
		

		// printf("%d years, %d months, %d days\n", $yearsAge, $monthsAge, $daysAge);
		// echo "</br>";
		// printf("%d years, %d months, %d days\n", $yearsTenor, $monthsTenor, $daysTenor);

		$round = false;
		if($daysTenor = 0 && $monthsTenor = 0){
			$round = true;
		}

		// if($daysTenor > 14){
		//     $monthsTenor = $monthsTenor + 1;
		// }

		$notValidYears = false;

		if($yearsTenor > 0){
			// if($monthsTenor > 6){
			//     $yearsTenor = $yearsTenor + 1;
			// }
		}else{
			$round = true;
			$notValidYears = true;
			$yearsTenor = 1;
		}
		
		$getRate = $fixed_rate;

		$finalDivider = 100;
		// if($notValidYears){
		// 	$qRateMonth = "SELECT * FROM tm_rate_month WHERE bulan = '$monthsTenor'";
		// 	$getRateMonth = $this->db->query($qRateMonth)->row();
		// 	$finalDivider = $getRateMonth->rate;
		// }

		$ratePercent = ((float)$getRate * 1) / 10;

		// if(!$round){
		// 	$yearsTenor = $yearsTenor + 1;
		// 	$qRateUp = "SELECT * FROM tm_rate WHERE id_asuransi = '$data->id_asuransi' AND usia = '$yearsAge' AND tahun = '$yearsTenor'";
		// 	$getRateUp = $this->db->query($qRate)->row();
		// 	$rateUpDown = (((float)$getRateUp->rate * 1) / 100) - (($ratePercent * 1) / 100);
		// 	$rateDivid = 0.08 * $rateUpDown;
		// 	$rateDown = (((float)$getRate->rate * 1) / 100);
		// 	$ratePercent = ($rateDown + $rateDivid) * 100;

		// }

		$rate = (($ratePercent * 1) / 100);
		// var_dump($rate);exit();
		$feeAsuransi = 0;
		$coveragePinalti = 0;
		$totalBiaya = 0;
		$premi = 0;
		$premiFinal = 0;
		$feeUjroh = 0;
		$premiBeforeAdmin = 0;
		$premiFinal = 0;
		$biayaNasabah = 0;
		$biayaAtribusi = 0;
		$limitAsuransi = 0;
		$coverageType = '-';
		$plafondFinal = 0;
		$coverageType = '-';
		$status = 1;
		$type_coverages = $this->db->query("SELECT * FROM tm_type_coverage WHERE nama = '$data->type_coverage' ")->row();
		if($type_coverages->c_id == '1' || $type_coverages->c_id == '2'){

			// TYPE COVERAGE = Reguler (Tidak Ada Atribusi)

			$feeAsuransi = $plafond * $rate;
			$coveragePinalti = 0;
			$totalBiaya = $feeAsuransi + $coverageFeeMitra;
			$premi = $feeAsuransi;
			$premiFinal = round($premi * (($finalDivider * 1) / 100));
			$premiBeforeAdmin = $premiFinal;
			$premiFinal = $premiFinal + $adminFee;
			$biayaNasabah = $feeAsuransi;
            $biayaAtribusi = $totalBiaya - $feeAsuransi;
			$limitAsuransi = $plafond;
			$coverageType = $data->type_coverage;
			$feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;

		}else{
			
			if($type_coverages->c_id == '5' || $type_coverages->c_id == '3'){

				// TYPE COVERAGE = PLAFOND + FEE MITRA + FEE ASURANSI

				$dividerRate = 1 - $rate;
				$biayaNasabah = 0;
				$amountDivider = ($plafond * $rate) / $dividerRate;
				$amountRate = $plafond + $amountDivider;
				$feeAsuransi = $rate * round($amountRate);
				
				$totalBiaya = $feeAsuransi + $coveragePinalti;

				$limitAsuransi = $plafond + $feeAsuransi;
				$premiDived = round($limitAsuransi * $rate);
				$premiFinal = round($premiDived * (($finalDivider * 1) / 100));
				$premiBeforeAdmin = $premiFinal;
				$premiFinal = $premiFinal + $adminFee;
				$coverageType = $data->type_coverage;
				$biayaAtribusi = $totalBiaya - $biayaNasabah;
				$feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
				$hasil = "OKEH Dua";

			}elseif($type_coverages->c_id == '7' || $type_coverages->c_id == '4'){

				// TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI

				$feeAsuransi = $rate * ($plafond + $coveragePinalti);
				$totalBiaya = $feeAsuransi + $coveragePinalti;
				$biayaNasabah = $feeAsuransi;
				$limitAsuransi = $plafond + $coveragePinalti;
				$biayaAtribusi = $totalBiaya - $biayaNasabah;

				$premiDived = round($limitAsuransi * $rate);
				$premiFinal = round($premiDived * (($finalDivider * 1) / 100));
				$premiBeforeAdmin = $premiFinal;
				$premiFinal = $premiFinal + $adminFee;
				$coverageType = $type_coverages->nama;
				$feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
				$hasil = "OKEH Tiga";

			}elseif($type_coverages->c_id == '6' || $type_coverages->c_id == '8'){

				// TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI + FEE ASURANSI (ALL COVER)
				
				$dividerRate = 1 - $rate;
				$plafondFinal = $plafond + $coveragePinalti;
				$amountDivider = $rate * ($plafondFinal / $dividerRate);
				$feeAsuransi = $rate * ($plafondFinal + $amountDivider);

				$biayaNasabah = 0;
				$totalBiaya = $feeAsuransi + $coveragePinalti;
				$limitAsuransi = $plafondFinal + $feeAsuransi + $coveragePinalti;
				$biayaAtribusi = $totalBiaya + $biayaNasabah;

				$premiDived = round($limitAsuransi * $rate);
				$premiFinal = round($premiDived * (($finalDivider * 1) / 100));
				$premiBeforeAdmin = $premiFinal;
				$premiFinal = $premiFinal + $adminFee;
				$coverageType = $type_coverages->nama;
				$feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
				$hasil = "OKEH Empat";
				

			}
		}
		
		// var_dump($premiFinal,$premiBeforeAdmin);exit();
		$this->db->query("UPDATE ".$GLOBALS['table']." 
						 SET 
							fixed_rate = '$ratePercent',
							fee_asuransi = '$feeAsuransi',
							fee_ujroh = '$feeUjroh',
							total_biaya = '$totalBiaya',
							biaya_nasabah = '$biayaNasabah',
							limit_asuransi = '$limitAsuransi',
							biaya_atribusi = '$biayaAtribusi',
							biaya_admin = '$adminFee',
							premi_before_admin = '$premiBeforeAdmin',
							premi = '$premiFinal',
							dok_cbc = '$filenames',
							updatedby = '$userSession',
							updatedon = '$dateon',
							dok_konfirm_asuransi = '$filenames2'
						WHERE c_id = '$c_id' ");
		
		$generatePdf = $this->generateCN2($c_id);

		$this->db->query("
				UPDATE ".$GLOBALS['table']." 
				SET 
					cnPdf = '$generatePdf'		
				WHERE c_id = '$c_id'
		");

		$return = array(
			'reffnumber' => $data->reffnumber,
			'no_aplikasi' => $data->no_aplikasi,
			'no_covernote' => $data->no_aplikasi.'-'.$data->reffnumber,
			'tgl_covernote' => $today,
			'nilai_ijk' => $premiFinal,
			'coverage' => $limitAsuransi,
			"keterangan" => "Berhasil Melakukan Update Debitur",
			'status' => '00',
			'flag_spk' => 'Sukses'
		);

		$this->db->select('*');
		$this->db->where('c_id',$c_id);
		$dataNew = $this->db->get('tm_penutupan')->result_array();

		$this->writeLog('CN_REQUEST-MANUAL-CBC','SUCCESS',TR_CN_HIS,NULL,json_encode($dataNew),json_encode($return));

		return $this->output
		->set_status_header(200)
		->set_content_type('application/json')
		->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		die;

		}catch(Exception $e){

		}
	}

	function getTypeProduk(){
		$id = $_GET['id'];

		$this->db->select('c_id, nama');
		$this->db->where('id_produk',$id);
		$this->db->order_by('nama', 'ASC');
		$oneData = $this->db->get('tm_product_type')->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;

	}

	function getTypeManfaat(){
		$id = $_GET['id'];

		$this->db->select('c_id, nama');
		$this->db->where('id_produk',$id);
		$this->db->where('status',1);
		$this->db->order_by('nama', 'ASC');
		$oneData = $this->db->get('tm_type_manfaat')->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;

	}

	function getCabang(){
		$id = $_GET['id'];

		$this->db->select('c_area');
		$this->db->where('c_id',$id);
		$this->db->where('status',1);
		$this->db->order_by('nama', 'ASC');
		$oneData = $this->db->get('tm_cabang')->row();

		$this->db->select('c_id,nama');
		$this->db->where('c_area',$oneData->c_area);
		$this->db->where('c_type !=','AREA');
		$this->db->where('status',1);
		$this->db->order_by('nama', 'ASC');
		$cabang = $this->db->get('tm_cabang')->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($cabang));
		die;

	}

	function activity_data(){
		$id = base64_decode($_GET['data']);

		$dataActivity = $this->mainModel->getHistoryData($id);

		for ($i=0; $i < count($dataActivity) ; $i++) { 
			if($dataActivity[$i]['status'] == 'SUCCESS' ){
				$arr = array(
							'colour' => 'text-success',
							'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
							'url' => '/bsi-midsvc-cnreq'
						);
			}elseif($dataActivity[$i]['status'] == 'FAILED' ){
				$arr = array(
						'colour' => 'text-warning',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}elseif($dataActivity[$i]['status'] == 'ERROR' ){
				$arr = array(
						'colour' => 'text-danger',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}

			// array_push($dataActivity[$i], $arr);

			$dataActivity[$i] = $dataActivity[$i] + $arr;
		}

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($dataActivity));
		die;
	}

	function history_data(){
		$id = base64_decode($_POST['data']);
        $cari = $_POST;
		$data = $this->mainModel->get_history($id,$cari);

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
		die;
	}

	function downloadCnNew(){
		$id = base64_decode($_GET['data']);

		$this->db->select('cnPdf');
		$this->db->where('no_aplikasi', $id);
		$this->db->where('status_cbc', 1);
		$this->db->from('tm_penutupan');
		$this->db->order_by('createdon', 'DESC');
		$dataCn = $this->db->get()->row()->cnPdf;

		$this->load->helper('download');
        $name = $dataCn;

        $data = file_get_contents(str_replace(__FILE__,$_SERVER['DOCUMENT_ROOT'] .'/upload/dok_cn/'.$name,__FILE__));
        force_download($name, $data); 
	}

	function downloadCn(){
		$id = base64_decode($_GET['data']);
		$reff = base64_decode($_GET['reff']);

		$this->db->select('cnPdf');
		$this->db->where('no_aplikasi', $id);
		$this->db->where('reffnumber', $reff);
		$this->db->where('status_cbc', 1);
		$this->db->from('tm_penutupan');
		$this->db->order_by('createdon', 'DESC');
		$dataCn = $this->db->get()->row()->cnPdf;

		$this->load->helper('download');
        $name = $dataCn;

        $data = file_get_contents(str_replace(__FILE__,$_SERVER['DOCUMENT_ROOT'] .'/upload/dok_cn/'.$name,__FILE__));
        force_download($name, $data); 
	}

}

