<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once './vendor/autoload.php';
include_once (dirname(__FILE__) . "/./Master_Controller.php");

global $table, $tableUser, $tableProduk, 
       $tableProdukType, $tableTypeManfaat, $tableJenisPekerjaan,
       $tableAkad, $tableCabang ;

$table = DB_NAME_PREFIX.'tm_penutupan';
$tableUser = DB_NAME_PREFIX.'tm_user';
$tableProduk = DB_NAME_PREFIX.'tm_product_list';
$tableProdukType = DB_NAME_PREFIX.'tm_product_type';
$tableJenisPekerjaan = DB_NAME_PREFIX.'tm_jenis_pekerjaan';
$tableAkad = DB_NAME_PREFIX.'tm_jenis_akad';
$tableTypeManfaat = DB_NAME_PREFIX.'tm_type_manfaat';
$tableCabang = DB_NAME_PREFIX.'tm_cabang';
$tableLog = DB_NAME_PREFIX.'tl_log_data';

class Penutupan extends Master_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Penutupan_m','mainModel');
        $this->load->library('Excel');

        $userSession = $this->session->userdata('Username');
        if(!isset($userSession)){
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

	function index()
	{
		$this->template->load('template','penutupan/list');
    }
    
	function view_data(){
		$cari = $_POST;
		$data = $this->mainModel->get_tables($cari);

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
		die;

	}

	function view(){
		$id = base64_decode($_GET['data']);
		$type = $_GET['type'];
		
		$data['editData'] = $this->mainModel->getDatabyId($id,$type);

		$this->template->load('template','penutupan/view',$data);
	}

	function edit(){
		$id = base64_decode($_GET['data']);
		$type = '1';
		
		$data['editData'] = $this->mainModel->getDatabyId($id,$type);

		$this->template->load('template','penutupan/edit',$data);
	}

	function input(){
		$this->template->load('template','penutupan/input');
	}

	function History(){
		$id = base64_decode($_GET['data']);
		
		$this->db->select('*');
		$this->db->where('no_aplikasi',$id);
		$this->db->order_by('createdon', 'DESC');
		$oneData = $this->db->get('tm_penutupan')->row();

		$data['oneData'] = $oneData;

		$this->template->load('template','penutupan/history',$data);
	}

	function getJenisPekerjaan(){
		$id = $_GET['id'];

		$this->db->select('c_id, nama');
		$this->db->where('id_produk',$id);
		$this->db->where('status',1);
		$this->db->order_by('nama', 'ASC');
		$oneData = $this->db->get('tm_jenis_pekerjaan')->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;

	}

	function getTypeProduk(){
		$id = $_GET['id'];

		$this->db->select('c_id, nama');
		$this->db->where('id_produk',$id);
        $this->db->where('status',1);
		$this->db->order_by('nama', 'ASC');
		$oneData = $this->db->get('tm_product_type')->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;

	}

	function getTypeManfaat(){
		$id = $_GET['id'];
		$asuransi = $_GET['id_asuransi'];


		$this->db->select('tm_type_manfaat.c_id, tm_type_manfaat.nama');
        $this->db->from('tm_type_manfaat_setting');
        $this->db->join('tm_type_manfaat', 'tm_type_manfaat.c_id = tm_type_manfaat_setting.id_type_manfaat', 'left');
		$this->db->where('tm_type_manfaat_setting.id_produk',$id);
		$this->db->where('tm_type_manfaat_setting.status',1);
        $this->db->where('tm_type_manfaat_setting.id_asuransi',$asuransi);
		$this->db->order_by('tm_type_manfaat.nama', 'ASC');
		$oneData = $this->db->get()->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;

	}

	function getProduk(){
		$id = $_GET['id'];


		$this->db->select('tm_product_list.c_id, tm_product_list.nama_produk');
        $this->db->from('tm_produk_setting');
        $this->db->join('tm_product_list', 'tm_product_list.c_id = tm_produk_setting.id_produk', 'left');
		$this->db->where('tm_produk_setting.status',1);
        $this->db->where('tm_produk_setting.id_asuransi',$id);
		$this->db->order_by('tm_product_list.nama_produk', 'ASC');
		$oneData = $this->db->get()->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;

	}

	function getCabang(){
		$id = $_GET['id'];

		$this->db->select('c_area');
		$this->db->where('c_id',$id);
		$this->db->where('status',1);
		$this->db->order_by('nama', 'ASC');
		$oneData = $this->db->get('tm_cabang')->row();

		$this->db->select('c_id,nama');
		$this->db->where('c_area',$oneData->c_area);
		$this->db->where('c_type !=','AREA');
		$this->db->where('status',1);
		$this->db->order_by('nama', 'ASC');
		$cabang = $this->db->get('tm_cabang')->result_array();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($cabang));
		die;

	}

	function submitData(){
		extract($_POST);

		date_default_timezone_set('Asia/Jakarta');
        $userSession = $this->session->userdata('UserId');
		try {
			$data_json = $_POST;
			$ktp = trim($ktp);
            // $reffnum = trim($reffnumber).'-MANUAL-PJM';
            $noaplikasi = trim($no_aplikasi);
            $iscbc = false;
			$status_cbc = 0;
			$ket_cbc = null;

			$rawdata = json_encode($_POST);

			$this->db->select('*');
            $this->db->WHERE('c_id', $kode_cabang);
            $cabang = $this->db->get($GLOBALS['tableCabang'])->row();

            $this->db->select('rate_ujroh');
            $this->db->WHERE('c_id', $produk);
            $rate_ujroh = $this->db->get($GLOBALS['tableProduk'])->row()->rate_ujroh;

			$this->db->select('c_id');
            $this->db->WHERE('no_aplikasi', $noaplikasi);
            $exData = $this->db->get($GLOBALS['table'])->result_array();
            $counter = 0;
            if(!empty($exData)){
			    $counter = (int)count($exData) + 1;
            }else{
                $counter = 1;
            }
			$reffnum = date('Ymd').$counter.'112_PJM';

			$qMainDb = "SELECT ISNULL(SUM(plafond),0) as total_akumulasi FROM ".$GLOBALS['table']." WHERE status = '5' AND status_klaim is null AND LTRIM(RTRIM(ktp)) = '$ktp'  ";

            // $paramsins = trim($data_json['nama_asuransi']);
            // $expins = explode('/',$paramsins);
            $asuransi = $nama_asuransi;

			$plafond = (float)str_replace('.','',$data_json['plafond']);
            // $expCoverage = explode(';',$data_json['type_coverage_amount']);
            $coveragePlafond = (float)str_replace('.','',$data_json['plafond']);
            $coverageFeeMitra = (float)str_replace('.','',$data_json['coverage_fee_mitra']);
            $coveragePinalti = (float)str_replace('.','',$data_json['coverage_pinalti']);
            $coverageAsuransi = (float)str_replace('.','',$data_json['coverage_asuransi']);

            $jangkaWaktu = $tenor;
            $tanggalLahir = $data_json['tanggal_lahir'];
            $expAkad = $tanggal_mulai;
			$atribusi = 'TIDAK';
			if($coverageFeeMitra > 0 && ($coverageAsuransi > 0 || $coveragePinalti > 0)){
				$atribusi = 'YA';
			}

            $today = date('Y-m-d');

            $akumulasiPlafond = $this->db->query($qMainDb)->row()->total_akumulasi;

            $akumulasi = $plafond + $akumulasiPlafond;

            $qAsuransi = "SELECT * FROM tm_asuransi WHERE c_id = '$asuransi' ";

            $dataAsuransi = $this->db->query($qAsuransi)->row();


            //GET Max Plafond, Max Usia, Min Usia, Max Jk per type manfaat

            $qCbcValidation = "SELECT * FROM tm_type_manfaat_setting 
                                 WHERE id_asuransi = '$asuransi' AND id_produk = '$produk' 
                                 AND id_type_manfaat = '$type_manfaat' AND status = '1' ";

            $getCbcValidation = $this->db->query($qCbcValidation)->row();

            if(empty($getCbcValidation)){
                $return = array(
                    "reffnumber" => $reffnum,
                    "no_aplikasi" => $noaplikasi,
                    "keterangan" => "Data Debitur tidak dapat di submit mohon dilakukan pengecekan kembali",
                    "status" => '10',
                    "error" => 'CBC Validation Not Found'
                );

                $this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');

                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            }

            if($akumulasi > (float)$dataAsuransi->max_plafond){
                $return = array(
                    "reffnumber" => $reffnum,
                    "no_aplikasi" => $noaplikasi,
                    "keterangan" => "Debitur telah melewati batas akumulasi plafond, akumulasi : ".number_format($akumulasi,0,',','.').
                                    ", Maksimal Limit Asuransi : ".number_format($dataAsuransi->max_plafond,0,',','.'),
                    "status" => '10',
                    "error" => 'Akumulasi Debitur'
                );

                $this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');

                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                
            }

			if($plafond > (float)$getCbcValidation->max_plafond){

                $qAsuransi = "SELECT b.nama FROM tm_type_manfaat_setting a
                                LEFT JOIN tm_asuransi b ON b.c_id = a.id_asuransi
                                 WHERE a.id_produk = '$produk' AND a.id_type_manfaat = '$type_manfaat'
                                  AND a.status = '1' AND a.max_plafond > '$plafond' ";

                $maxAsuransi = $this->db->query($qAsuransi)->result_array();
                $arrIns = '';

                if(!empty($maxAsuransi)){
                    $countIns = 1;
                    foreach ($maxAsuransi as $value) {
                        if(count($maxAsuransi) > $countIns){
                            $arrIns .= $value['nama'].',';
                        }else{
                            $arrIns .= $value['nama'];
                        }
                    }

                    $return = array(
                        "reffnumber" => $reffnum,
                        "no_aplikasi" => $noaplikasi,
                        "keterangan" => "Plafond melebihi maximum plafond asuransi yang dipilih, silahkan pilih asuransi : ". $arrIns." untuk mengcover debitur terkait",
                        "status" => '10',
                        "error" => 'Plafond melewati batas max asuransi'
                    );

					$this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
	
					$this->output->set_header('HTTP/1.0 200 OK');
					$this->output->set_header('HTTP/1.1 200 OK');

					return $this->output
					->set_status_header(200)
					->set_content_type('application/json')
					->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

                }else{
                    $iscbc = true;
					$status_cbc = 1;
                    $ket_cbc = "Plafond debitur Melebihi maximum plafond Asuransi yang dipilih, Maximum plafond yg berjalan dengan asuransi yg di pilih adalah : Rp. ".number_format($getCbcValidation->max_plafond,0,',','.');
                }
            }

			$existingData = $this->db->query("SELECT * FROM ".$GLOBALS['table']." WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi' ORDER BY createdon DESC, c_id DESC")->row();

            // var_dump($existingData);exit();
            if(!empty($existingData)){
                if($ktp != $existingData->ktp){
                    $return = array(
                        "reffnumber" => $reffnum,
                        "no_aplikasi" => $noaplikasi,
                        "keterangan" => "Data No. Identitas Debitur Pertanggungan tidak sesuai dengan data sebelumnya",
                        "status" => '10'
                    );
                    $this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

                    $this->output->set_header('HTTP/1.0 200 OK');
					$this->output->set_header('HTTP/1.1 200 OK');

					return $this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
					die;
                }

                if($reffnum == $existingData->reffnumber){
                    $return = array(
                        "reffnumber" => $reffnum,
                        "no_aplikasi" => $noaplikasi,
                        "keterangan" => "Data Reffnumber tidak bisa sama dengan data sebelumnya ".$counter,
                        "status" => '10'
                    );
                    $this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

					$this->output->set_header('HTTP/1.0 200 OK');
					$this->output->set_header('HTTP/1.1 200 OK');
					

                    return $this->output
					->set_status_header(200)
					->set_content_type('application/json')
					->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
					die;
                }
                

                $datefix = date('Y-m-d',strtotime($tanggalLahir));
                if($datefix != $existingData->tanggal_lahir){

                    $updatedDate = $this->db->query("SELECT tanggal_lahir, is_birth_update FROM ".$GLOBALS['table']." WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi' ORDER BY createdon DESC, c_id DESC")->row();

                    if($updatedDate->is_birth_update = 1){
                        $tanggalLahir = $updatedDate->tanggal_lahir;

                    }else{
                        $return = array(
                            "reffnumber" => $reffnum,
                            "no_aplikasi" => $noaplikasi,
                            "keterangan" => "Data Tanggal Lahir Debitur Pertanggungan tidak sesuai dengan data sebelumnya",
                            "status" => '10'
                        );
                        $this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
						
						$this->output->set_header('HTTP/1.0 200 OK');
						$this->output->set_header('HTTP/1.1 200 OK');
						$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
						$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
						$this->output->set_header('Pragma: no-cache');

                        return $this->output
						->set_status_header(200)
						->set_content_type('application/json', 'utf-8')
						->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
						die;
                    }
                    
                }
                
            }

            $type_coverages = $this->db->query("SELECT nama FROM tm_type_coverage WHERE c_id = '$type_coverage' ")->row();
			$mulaiAkad = $tanggal_mulai;
            $akhirAkad = $tanggal_akhir;
            $maxIns = (float)$dataAsuransi->max_plafond;
            $adminFee =(float)$dataAsuransi->admin_fee;

            $checkDateStart = $this->validateDate($mulaiAkad,'Y-m-d');
            $checkDateEnd = $this->validateDate($akhirAkad,'Y-m-d');

            $tenor_bulan = '';
            $usia_saat_akad = '';
            $usia_akhir_akad = '';

			//initialize transaction variable

            $feeAsuransi = 0;
            $totalBiaya = 0;
            $feeUjroh = 0;
            $premi = 0;
            $premiFinal = 0;
            $premiBeforeAdmin = 0;
            $premiFinal = 0;
            $biayaNasabah = 0;
            $biayaAtribusi = 0;
            $limitAsuransi = 0;
            $coverageType = '-';
            $rate = 0;
            $ratePercent = 0;
            $coverageType = '-';
            $status = 1;
            

            //CHECK Tanggal Akad Valid Atau Tidak

			if(!$checkDateStart || !$checkDateEnd){
                $status = 2;
                $tenor_bulan = '- (Tanggal Akad Tidak Sesuai)';
                $usia_saat_akad = '- (Tanggal Akad Tidak Sesuai)';
                $usia_akhir_akad = '- (Tanggal Akad Tidak Sesuai)';

                $feeAsuransi = 0;
                $coveragePinalti = 0;
                $totalBiaya = 0;
                $premi = 0;
                $premiFinal = 0;
                $premiBeforeAdmin = 0;
                $premiFinal = 0;
                $biayaNasabah = 0;
                $biayaAtribusi = 0;
                $limitAsuransi = 0;
                $coverageType = '-';
                $rate = 0;
                $ratePercent = 0;
                $coverageType = $type_coverages->nama;
                
            }else{
                
                $date1 = $data_json['tanggal_lahir'];
                $date2 = $data_json['tanggal_mulai'];
                $date3 = $data_json['tanggal_akhir'];

                $diffAge = abs(strtotime($date2) - strtotime($date1));
                $diffAgeEnd = abs(strtotime($date3) - strtotime($date1));
                $diffTenor = abs(strtotime($date3) - strtotime($date2));

                $yearsAge = floor($diffAge / (365*60*60*24));
                $monthsAge = floor(($diffAge - $yearsAge * 365*60*60*24) / (30*60*60*24));
                $daysAge = floor(($diffAge - $yearsAge * 365*60*60*24 - $monthsAge*30*60*60*24)/ (60*60*24));
				
                $yearsAgeEnd = floor($diffAgeEnd / (365*60*60*24));
                $monthsAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24) / (30*60*60*24));
                $daysAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24 - $monthsAgeEnd*30*60*60*24)/ (60*60*24));

                $yearsTenor = floor($diffTenor / (365*60*60*24));
                $monthsTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24) / (30*60*60*24));
                $daysTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24 - $monthsTenor*30*60*60*24)/ (60*60*24));

                $monthsAgeEndRound = $monthsAgeEnd;
                $yearsAgeEndRound = $yearsAgeEnd;

                $monthsAgeRound = $monthsAge;
                $yearsAgeRound = $yearsAge;

                if($yearsAge < 18){
                    $return = array(
                        "reffnumber" => $reffnum,
                        "no_aplikasi" => $noaplikasi,
                        "keterangan" => "Usia Minimal Debitur Adalah 18 Tahun",
                        // "keterangan2" => $qRate,
                        "status" => '10'
                    );

                    $this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));


                    $this->output->set_header('HTTP/1.0 200 OK');
                    $this->output->set_header('HTTP/1.1 200 OK');
                    

                    return $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                    die;
                }

                if($daysAgeEnd > 14){
                    $monthsAgeEndRound = $monthsAgeEnd + 1;
                }

                if($monthsAgeEndRound > 6){
                    $yearsAgeEndRound = $yearsAgeEnd + 1;
                }

                if($daysAge > 14){
                    $monthsAgeRound = $monthsAge + 1;
                }

                if($monthsAgeRound > 6){
                    $yearsAgeRound = $yearsAge + 1;
                }

                $tenorMonth = ($yearsTenor * 12) + $monthsTenor;

                $tenor_bulan = $data_json['tenor'].' ( '.$tenorMonth.' Bulan '.$daysTenor.' Hari )';
                $usia_saat_akad = $yearsAgeRound.' Tahun ( '.$yearsAge.' Tahun '.$monthsAge.' Bulan '.$daysAge.' Hari )';
                $usia_akhir_akad = $yearsAgeEndRound.' Tahun ( '.$yearsAgeEnd.' Tahun '.$monthsAgeEnd.' Bulan '.$daysAgeEnd.' Hari )';
                

                // printf("%d years, %d months, %d days\n", $yearsAge, $monthsAge, $daysAge);
                // echo "</br>";
                // printf("%d years, %d months, %d days\n", $yearsTenor, $monthsTenor, $daysTenor);
                $monthsTenorCalculate = $monthsTenor;
                $round = false;
                if($daysTenor < 14 && $monthsTenor = 0){
                    $round = true;
                }

                if($daysTenor > 14){
                    $monthsTenor = $monthsTenor + 1;
                    $daysTenor = 0;
                }

                $notValidYears = false;

                if($yearsTenor > 0){
                    if($monthsTenor > 6){
                        $yearsTenor = $yearsTenor + 1;
                    }
                }else{
                    $round = true;
                    $notValidYears = true;
                    $yearsTenor = 1;
                }

                if($yearsAgeRound > $getCbcValidation->max_usia || $yearsAgeRound < $getCbcValidation->min_usia){
                    $iscbc = true;
                    $status_cbc = 1;
                    $ket_cbc = "Usia debitur tidak termasuk data maximum / minimum asuransi yang dipilih, usia max / min yg berjalan dengan asuransi yg di pilih adalah max : ".$getCbcValidation->max_usia. " / min : $getCbcValidation->min_usia";
                }

                if($yearsTenor > $getCbcValidation->max_jangka_waktu){
                    $iscbc = true;
                    $status_cbc = 1;
                    $ket_cbc = "Jangka Waktu Melebihi Maximum jangka waktu asuransi yang dipilih, maximum jangka waktu yg berjalan dengan asuransi yg di pilih adalah : ".$getCbcValidation->max_jangka_waktu." Tahun";
                }

                if(!$iscbc){
                    
                    $db_rate = 'tm_rate';
                    $rateAskrida = false;
                    $ins_id = $dataAsuransi->c_id;
                    if($produk == '2'){
                        $db_rate = 'tm_rate_pensiun';
                    }elseif($produk == '4'){
                        $db_rate = 'tm_rate_prapensiun';
                    }

                    if($type_manfaat != '1'){
                        $yearsAge = 0;
                    }

                    $qRate = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat' ";
                    $getRate = $this->db->query($qRate)->row();

                    if(empty($getRate)){
                        if($yearsAge < 20 ){
                            $yearsAge2 = 20;
                        }else{
                            $yearsAge2 = $yearsAge;
                        }

                        $qRate = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge2' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat' ";
                        $getRate2 = $this->db->query($qRate)->row();
                        $rateAskrida = true;
                       if(empty($getRate2)){
                            $return = array(
                                "reffnumber" => $reffnum,
                                "no_aplikasi" => $noaplikasi,
                                "keterangan" => "Rate tidak ditemukan untuk usia : ".$yearsAge.", tenor : ".$yearsTenor. " Tahun Dan Asuransi : ".$dataAsuransi->alias,
                                // "keterangan2" => $qRate,
                                "status" => '10'
                            );

                            $this->writeLog('CN_REQUEST-MANUAL','FAILED',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));


                            $this->output->set_header('HTTP/1.0 200 OK');
                            $this->output->set_header('HTTP/1.1 200 OK');
                            

                            return $this->output
                            ->set_status_header(200)
                            ->set_content_type('application/json', 'utf-8')
                            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                            die;
                       }
                       
                       $getRate = $getRate2;
                    }

                    $finalDivider = 100;
                    if($notValidYears){
                        $qRateMonth = "SELECT * FROM tm_rate_month WHERE bulan = '$monthsTenor'";
                        $getRateMonth = $this->db->query($qRateMonth)->row();
                        $finalDivider = $getRateMonth->rate;
                    }

                    $ratePercent = (float)$getRate->rate;

                    if(!$round){
                        if($yearsAge < 20 ){
                            $yearsAge2 = 20;
                        }else{
                            $yearsAge2 = $yearsAge;
                        }

                        $yearsTenor = $yearsTenor + 1;
                        $qRateUp = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge2' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat'";

                        if($rateAskrida){
                            $qRateUp = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge2' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat'";
                        }
                        $monhCalculate = round((float)$monthsTenorCalculate / 12,2,PHP_ROUND_HALF_UP);
                        $getRateUp = $this->db->query($qRateUp)->row();
                        $rateUpDown = (((float)$getRateUp->rate * 1) / 100) - (($ratePercent * 1) / 100);
                        // $rateUpDown = ((1.75 * 1) / 100) - ((1.50 * 1) / 100);
                        $rateDivid = $monhCalculate * $rateUpDown;
                        $rateDown = (((float)$getRate->rate * 1) / 100);
                        // $rateDown = ((1.50 * 1) / 100);
                        $ratePercent = ($rateDown + $rateDivid) * 100;
                        

                    }

                    $rate = (($ratePercent * 1) / 100);
                    // var_dump($rate,$ratePercent,$round,$getRateUp->rate,$getRate->rate,$monhCalculate,$rateUpDown);exit();
                    
                    $hasil = "";
                    if($type_coverage == '1' || $type_coverage == '2'){

                        // TYPE COVERAGE = Reguler (Tidak Ada Atribusi)
                        
                        $rateMin = 1 - $rate;
                        $feeAsuransi = $plafond * $rate;
                        $coveragePinalti = 0;
                        $premi = $feeAsuransi;
                        $premiFinal = round($premi * (($finalDivider * 1) / 100));
                        $premiBeforeAdmin = $premiFinal;
                        $premiFinal = $premiFinal + $adminFee;
                        $biayaNasabah = $feeAsuransi;
                        $biayaAtribusi = $totalBiaya - $feeAsuransi;
                        $limitAsuransi = $plafond;
                        $coverageType = $type_coverages->nama;
                        $biayaAtribusi = $totalBiaya - $biayaNasabah;
                        $hasil = "OKEH Satu";
                        $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;

                    }else{
                        if($type_coverage == '5' || $type_coverage == '3'){

                            // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE ASURANSI
                            // $plafond = 60000000;
                            $dividerRate = 1 - $rate;
                            $biayaNasabah = 0;
                            $amountDivider = ($plafond * $rate) / $dividerRate;
                            $amountRate = $plafond + $amountDivider;
                            $feeAsuransi = $rate * round($amountRate);
                            
                            $totalBiaya = $feeAsuransi + $coveragePinalti;

                            $limitAsuransi = $plafond + $feeAsuransi;
                            $premiDived = round($limitAsuransi * $rate);
                            $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                            $premiBeforeAdmin = $premiFinal;
                            $premiFinal = $premiFinal + $adminFee;
                            $coverageType = $type_coverages->nama;
                            $biayaAtribusi = $totalBiaya - $biayaNasabah;
                            $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                            $hasil = "OKEH Dua";

                        }elseif($type_coverage == '7' || $type_coverage == '4'){

                            // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI

                            $feeAsuransi = $rate * ($plafond + $coveragePinalti);
                            $totalBiaya = $feeAsuransi + $coveragePinalti;
                            $biayaNasabah = $feeAsuransi;
                            $limitAsuransi = $plafond + $coveragePinalti;
                            $biayaAtribusi = $totalBiaya - $biayaNasabah;

                            $premiDived = round($limitAsuransi * $rate);
                            $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                            $premiBeforeAdmin = $premiFinal;
                            $premiFinal = $premiFinal + $adminFee;
                            $coverageType = $type_coverages->nama;
                            $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                            $hasil = "OKEH Tiga";

                        }elseif($type_coverage == '6' || $type_coverage == '8'){

                            // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI + FEE ASURANSI (ALL COVER)
                            
                            $dividerRate = 1 - $rate;
                            $plafondFinal = $plafond + $coveragePinalti;
                            $amountDivider = $rate * ($plafondFinal / $dividerRate);
                            $feeAsuransi = $rate * ($plafondFinal + $amountDivider);

                            $biayaNasabah = 0;
                            $totalBiaya = $feeAsuransi + $coveragePinalti;
                            $limitAsuransi = $plafondFinal + $feeAsuransi + $coveragePinalti;
                            $biayaAtribusi = $totalBiaya + $biayaNasabah;

                            $premiDived = round($limitAsuransi * $rate);
                            $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                            $premiBeforeAdmin = $premiFinal;
                            $premiFinal = $premiFinal + $adminFee;
                            $coverageType = $type_coverages->nama;
                            $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                            $hasil = "OKEH Empat";
                            

                        }
                    }

                }else{

                    $status = 1;
                    $hasil = 'cbc';
                    //CBC hanya set coverage type nya saja
                    $coverageType = $type_coverages->nama;
    
                }
            }
            // var_dump($premiFinal,$premiBeforeAdmin);exit();
			$insArr = array(
                'id_asuransi' => $nama_asuransi,
                'reffnumber' => $reffnum,
                'sales_code' => '-',
                'no_aplikasi' => $noaplikasi,
                'kode_cabang' => $cabang->kode_cabang,
                'nama_cabang' => $cabang->nama,
                'nama' => $nama,
                'ktp' => $ktp,
                'alamat' => $alamat,
                'kodepos' => $kodepos,
                'tanggal_lahir' => $tanggal_lahir,
                // 'jenis_kelamin' => $data_json['jenis_kelamin'],
                'tanggal_lahir' => $tanggal_lahir,
                'nama_asuransi' => $dataAsuransi->nama,
                // 'no_hp' => $no_hp,
                'npwp' => $npwp,
                'produk' => $produk,
                'tipe_produk' => $tipe_produk,
                'jenis_pekerjaan' => $jenis_pekerjaan,
                // 'nama_instansi' => $data_json['nama_instansi'],
                'jenis_agunan' => $jenis_agunan,
                'nomor_agunan' => $nomor_agunan,
                'nilai_agunan' => (float)str_replace('.','',$nilai_agunan),
                'jenis_akad' => $jenis_akad,
                'nomor_akad' => $nomor_akad,
                'plafond' => (float)$plafond,
                'tanggal_akad' => $checkDateStart ? date('Y-m-d',strtotime($tanggal_mulai)) : NULL,
                'tanggal_mulai' => $checkDateStart ? date('Y-m-d',strtotime($tanggal_mulai)) : NULL,
                'tanggal_akhir' => $checkDateEnd ? date('Y-m-d',strtotime($tanggal_akhir)) : NULL,
                'atribusi' => $atribusi,
                'nilai_pengajuan' => (float)str_replace('.','',$nilai_pengajuan),
                'tenor' => $tenor,
                // 'margin' => $margin,
                // 'tanggal_pencairan' => $tanggal_pencairan,
                'premi' => $premiFinal,
                // 'tanggal_bayar_ijk' => empty($tgl_bayar_ijk) ? null : $tgl_bayar_ijk,
                // 'no_ld' => $data_json['no_ld'],
                'type_coverage' => $coverageType,
                'coverage_plafond' => $coveragePlafond,
                'coverage_fee_mitra' => $coverageFeeMitra,
                'coverage_pinalti' => $coveragePinalti,
                'coverage_asuransi' => $coverageAsuransi,
                'type_manfaat' => $type_manfaat,
                // 'ratedef' => $data_json['ratedef'],
                'fee_asuransi' => $feeAsuransi,
                'total_biaya' => $totalBiaya,
                'biaya_nasabah' => $biayaNasabah,
                'limit_asuransi' => $limitAsuransi,
                'fee_ujroh' => $feeUjroh,
                'biaya_atribusi' => $biayaAtribusi,
                'biaya_admin' => $adminFee,
                'premi_before_admin' => $premiBeforeAdmin,
                'bulan_tenor' => $tenor_bulan,
                'usia_saat_akad' => $usia_saat_akad,
                'usia_selesai_akad' => $usia_akhir_akad,
                'fixed_rate' => $ratePercent,
                'coverage_type' => $coverageType,
                'status' => $status,
                'status_data' => 1,
                'status_cbc' => $status_cbc,
                'ket_cbc' => $ket_cbc,
                'createdon' => date('Y-m-d H:i:s'),
                'createdby' => $userSession,
                'stscurl' => 0
            );

			$insert = $this->db->insert($GLOBALS['table'], $insArr);

            if($insert){
                $generatePdf = $this->generateCN($noaplikasi,$reffnum);

                $pdf = '-';
                $path = './upload/dok_cn/'.$generatePdf;
                
                $this->db->query("UPDATE ".$GLOBALS['table']." SET cnPdf = '$generatePdf' WHERE no_aplikasi = '$noaplikasi' AND reffnumber = '$reffnum' ");

                if(!empty($exData)){
                    $this->db->query("UPDATE ".$GLOBALS['table']." SET status_data = '0' WHERE no_aplikasi = '$noaplikasi' AND reffnumber != '$reffnum' "); 
                }

                if(!$iscbc){
                    $return = array(
                        'reffnumber' => $reffnum,
                        'no_aplikasi' => $noaplikasi,
                        'no_covernote' => $noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => $premiFinal,
                        'coverage' => $limitAsuransi,
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Free Cover (CAC) ",
                        'status' => '00',
                        'flag_spk' => 'Sukses'
                    );
                }else{
                    $return = array(
                        'reffnumber' => $reffnum,
                        'no_aplikasi' => $noaplikasi,
                        'no_covernote' => $noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => $premiFinal,
                        'coverage' => $limitAsuransi,
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Non Free Cover (CBC) ",
                        'status' => '00',
                        'flag_spk' => 'Sukses'
                    );
                }
            }else{
                $return = array(
                    "reffnumber" => $reffnum,
                    "no_aplikasi" => $noaplikasi,
                    "keterangan" => "Opps.. Proses gagal mohon dicoba kembali",
                    "status" => '10'
                );
            }

            $this->writeLog('CN_REQUEST-MANUAL','SUCCESS',TR_CN_HIS,NULL,$rawdata,json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');

            return $this->output
			->set_status_header(200)
			->set_content_type('application/json')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;

		} catch (Exception $e) {
			$return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLog('CN_REQUEST-MANUAL','ERROR',TR_CN_HIS,NULL,$rawdata,json_encode($error, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
            
            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;
		}
	}

	function activity_data(){
		$id = base64_decode($_GET['data']);

		$dataActivity = $this->mainModel->getHistoryData($id);
        

		for ($i=0; $i < count($dataActivity) ; $i++) { 

			if($dataActivity[$i]['status'] == 'SUCCESS' ){
				$arr = array(
							'colour' => 'text-success',
							'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
							'url' => '/bsi-midsvc-cnreq'
						);
			}elseif($dataActivity[$i]['status'] == 'FAILED' ){
				$arr = array(
						'colour' => 'text-warning',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}elseif($dataActivity[$i]['status'] == 'ERROR' ){
				$arr = array(
						'colour' => 'text-danger',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}

			// array_push($dataActivity[$i], $arr);

			$dataActivity[$i] = $dataActivity[$i] + $arr;
		}

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($dataActivity));
		die;
	}

	function history_data(){
		$id = base64_decode($_POST['data']);
        $cari = $_POST;
		$data = $this->mainModel->get_history($id,$cari);

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
		die;
	}

    function getHistoryData(){
		$no_app = base64_decode($_GET['no_app']);
		$reffnumber = base64_decode($_GET['reffnumber']);

        $this->db->select('*');
        $this->db->like('datanew', $no_app);
        $this->db->like('datanew', $reffnumber);
        $dataActivity = $this->db->get('tl_cn_his')->result_array();

        for ($i=0; $i < count($dataActivity) ; $i++) { 

			if($dataActivity[$i]['status'] == 'SUCCESS' ){
				$arr = array(
							'colour' => 'text-success',
							'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
							'url' => '/bsi-midsvc-cnreq'
						);
			}elseif($dataActivity[$i]['status'] == 'FAILED' ){
				$arr = array(
						'colour' => 'text-warning',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}elseif($dataActivity[$i]['status'] == 'ERROR' ){
				$arr = array(
						'colour' => 'text-danger',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}

			// array_push($dataActivity[$i], $arr);

			$dataActivity[$i] = $dataActivity[$i] + $arr;
		}

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($dataActivity, TRUE,  JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		die;
	}

	function downloadCnNew(){
		$id = base64_decode($_GET['data']);

		$this->db->select('c_id, cnPdf, status_excel');
		$this->db->where('no_aplikasi', $id);
		$this->db->from('tm_penutupan');
		$this->db->order_by('createdon', 'DESC');
		$dataCn = $this->db->get()->row();

        // if(empty($dataCn->status_excel)){
        //     $this->load->helper('download');
        //     $name = $dataCn->cnPdf;

        //     $data = file_get_contents(str_replace(__FILE__,$_SERVER['DOCUMENT_ROOT'] .'/upload/dok_cn/'.$name,__FILE__));
        //     force_download($name, $data); 
        // }else{
            $this->viewCN($dataCn->c_id);
        // }
	}

	function downloadCn(){
		$id = base64_decode($_GET['data']);
		$reff = base64_decode($_GET['reff']);

		$this->db->select('cnPdf, status_excel, c_id');
		$this->db->where('no_aplikasi', $id);
		$this->db->where('reffnumber', $reff);
		$this->db->from('tm_penutupan');
		$this->db->order_by('createdon', 'DESC');
		$dataCn = $this->db->get()->row();

        // if(empty($dataCn->status_excel)){

		// $this->load->helper('download');
        // $name = $dataCn->cnPdf;

        // $data = file_get_contents(str_replace(__FILE__,$_SERVER['DOCUMENT_ROOT'] .'/upload/dok_cn/'.$name,__FILE__));
        // force_download($name, $data); 

        // }else{
            $this->viewCN($dataCn->c_id);
        // }
	}

	function uploadDataPenutupan(){
        ini_set('max_execution_time', '0');
        ini_set("memory_limit","-1");
        try {
            $file = $_FILES['penutupan_file']['name'];
            $fileName = str_replace(' ', '_', $file);
            $explode = explode(".",$fileName);
            $date = date("Ymd_His");
            $fileexp = $explode[0].'_'.$date.'.'.$explode[1];
            $namafile = '';

            $config['upload_path'] = './upload/penutupan_manual/';
            $config['file_name'] = $fileexp;
            $config['allowed_types'] = '*';
            $config['max_size'] = 10000;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if(! $this->upload->do_upload('penutupan_file') ){
                var_dump($this->upload->display_errors());
                exit();
            }else{
                $uploadData = $this->upload->data(); 
                $namafile = $uploadData['file_name'];
                $datefile = date("Ymd_His");
            }

            $fileNames = './upload/penutupan_manual/'.$namafile;

       
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objPHPExcel = $objReader->load($fileNames);
        } catch(Exception $e) {
            
            // on error
            $errMsg = 'error '. $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine();

            $return = array(
                'status' => 'ERROR',
                'error_message' => $errMsg,
            );

            $this->writeLog('PENUTUPAN-MANUAL-UPLOAD','FAILED',TR_CN_HIS,NULL,'UPLOAD ERROR',json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

            log_message( 'error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine() );

            // return $this->output
			// ->set_status_header(200)
			// ->set_content_type('application/json', 'utf-8')
			// ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			// die;
        }

        $sheet = $objPHPExcel->getSheet(); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestRowAndColumn();

        for ($rows = 2; $rows <= $highestRow; $rows++){

            $rowData = $sheet->toArray(
                NULL,
                FALSE,
                TRUE,
                TRUE
            );
        }

        $data = array();
        
        $numrow = 1;
        foreach($rowData as $row){
        
            if($numrow > 2 
                && !empty($row['B']) && !empty($row['C']) && !empty($row['D']) 
                && !empty($row['E']) && !empty($row['F']) && !empty($row['G']) 
                && !empty($row['H']) && !empty($row['I']) && !empty($row['J']) 
                && !empty($row['T']) && !empty($row['U']) && !empty($row['V']) 
                && !empty($row['W']) && !empty($row['X']) ){

                    array_push($data, array(
                    'no_aplikasi'=> str_replace("'",'',$row['B']),
                    'nama_asuransi'=> $row['C'],
                    'reffnumber'=> $row['D'],
                    'nama_nasabah'=> str_replace("'",'',$row['E']),
                    'kode_cabang'=> $row['F'],
                    'nama_cabang'=> $row['G'],
                    'jenis_kelamin'=> $row['H'],
                    'tgl_lahir'=> $row['I'],
                    'ktp'=> str_replace("'",'',$row['J']),
                    'npwp'=> $row['K'],
                    'produk'=> $row['L'],
                    'tipe_produk'=> $row['M'],
                    'jenis_pekerjaan'=> $row['N'],
                    'type_manfaat'=> $row['O'],
                    'jenis_agunan'=> $row['P'],
                    'nomor_agunan'=> $row['Q'],
                    'jenis_akad'=> $row['R'],
                    'nomor_akad'=> $row['S'],
                    'tgl_akad'=> $row['T'],
                    'tgl_akhir_kredit'=> $row['U'],
                    'plafond'=> (float)str_replace(",",'',str_replace(".",'',$row['V'])),
                    'tenor'=> $row['W'],
                    'type_coverage'=> $row['X'],
                    'fee_mitra'=> (float)str_replace(",",'',str_replace(".",'',$row['Y'])),
                    'fee_asuransi'=> (float)str_replace(",",'',str_replace(".",'',$row['Z'])),
                    'fee_pinalti'=> (float)str_replace(",",'',str_replace(".",'',$row['AA']))
                    ));
            }
        
            $numrow++; 
        }
        
        $dbarr = array();
        $successCount = 0;
        $errorCount = 0;
        $userSession = $this->session->userdata('UserId');
        $this->db->trans_begin();

        // var_dump($data);exit();

        try{
            foreach($data as $data_json)
            {
                $ktp = trim($data_json['ktp']);
                $reffnum = trim($data_json['reffnumber']);
                $noaplikasi = trim($data_json['no_aplikasi']);
                $tanggalLahir = trim($data_json['tgl_lahir']);
                $asuransi = trim($data_json['nama_asuransi']);
                $iscbc = false;
                $ket_cbc = null;
                $status_cbc = 0;
                $next_cbc = false;
    
                $this->db->select('*');
                $this->db->like('kode_cabang', $data_json['kode_cabang'], 'both');
                $cabang = $this->db->get('tm_cabang')->row();
    
                $this->db->select('*');
                $this->db->like('nama_produk', $data_json['produk'],'both');
                $produkData = $this->db->get('tm_product_list')->row();
                $rate_ujroh = $produkData->rate_ujroh;
    
                $this->db->select('*');
                $this->db->like('nama', $data_json['type_manfaat'],'both');
                $type_manfaat = $this->db->get('tm_type_manfaat')->row();
    
                $this->db->select('c_id');
                $this->db->where('no_aplikasi', $noaplikasi);
                $this->db->where('ktp', $ktp);
                $exData = $this->db->get('tm_penutupan')->result_array();
    
                $qMainDb = "SELECT ISNULL(SUM(plafond),0) as total_akumulasi FROM tm_penutupan WHERE status = '3' AND status_klaim is null AND LTRIM(RTRIM(ktp)) = '$ktp'  ";
            
                $plafond = (float)$data_json['plafond'];
                $coveragePlafond = (float)$data_json['plafond'];
                $coverageFeeMitra = (float)$data_json['fee_mitra'];
                $coveragePinalti = (float)$data_json['fee_pinalti'];
                $coverageAsuransi = (float)$data_json['fee_asuransi'];
                $coverageType = $data_json['type_coverage'];
                $atribusi = 'TIDAK';
    
                if($coverageType != 'REGULER (NO ATRIB)'){
                    $atribusi = 'YA';
                }
    
                $type_coverages = $this->db->query("SELECT * FROM tm_type_coverage WHERE nama = '$coverageType' ")->row();
                $type_coverage = $type_coverages->c_id;
    
                $mulaiAkad = null;
                $akhirAkad = null;
                $checkDateStart = true;
                $checkDateEnd = true;
        
                $mulaiAkad = $data_json['tgl_akad'];
                $akhirAkad = $data_json['tgl_akhir_kredit'];
        
                $checkDateStart = $this->validateDate($mulaiAkad,'Y-m-d');
                $checkDateEnd = $this->validateDate($akhirAkad,'Y-m-d');
        
                if(!$checkDateStart && !$checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  "'".$reffnum,
                    "noAplikasi"            => "'".$noaplikasi,
                    "responseDesc"          => 'Tanggal Akad tidak sesuai format : YYYYMMDD'
                    );
                    
                    array_push($dbarr, $return);
                    $errorCount++;
                    continue;
                }
        
                if(!$checkDateStart && $checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  "'".$reffnum,
                    "noAplikasi"            => "'".$noaplikasi,
                    "responseCode"          => "10",
                    "responseDesc"          => 'Tanggal Mulai Akad tidak sesuai format : YYYYMMDD'
                    );
                    
                    array_push($dbarr, $return);
                    $errorCount++;
                    continue;
                }
        
                if($checkDateStart && !$checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  "'".$reffnum,
                    "noAplikasi"            => "'".$noaplikasi,
                    "responseCode"          => "10",
                    "responseDesc"          => 'Tanggal Akhir Kredit Akad tidak sesuai format : YYYYMMDD'
                    );
        
                    array_push($dbarr, $return);
                    $errorCount++;
                    continue;
                }
        
                if(!$checkDateStart && !$checkDateEnd){
                    $return = array(
                    "reffnumber"           =>  "'".$reffnum,
                    "noAplikasi"            => "'".$noaplikasi,
                    "responseCode"          => "10",
                    "responseDesc"          => 'Tanggal Akad & Tanggal Akhir Kredit Akad tidak sesuai format : YYYYMMDD'
                    );
        
                    array_push($dbarr, $return);
                    $errorCount++;
                    continue;
                }
                
                $today = date('Y-m-d');
    
                $akumulasiPlafond = $this->db->query($qMainDb)->row()->total_akumulasi;
    
                $akumulasi = $plafond + $akumulasiPlafond;
    
                $qAsuransi = "SELECT * FROM tm_asuransi WHERE alias = '$asuransi' ";
    
                $dataAsuransi = $this->db->query($qAsuransi)->row();
    
                //GET Max Plafond, Max Usia, Min Usia, Max Jk per type manfaat
    
                $qCbcValidation = "SELECT * FROM tm_type_manfaat_setting 
                                     WHERE id_asuransi = '$dataAsuransi->c_id' AND id_produk = '$produkData->c_id' 
                                     AND id_type_manfaat = '$type_manfaat->c_id' AND status = '1' ";
                
                $getCbcValidation = $this->db->query($qCbcValidation)->row();
    
                if(empty($getCbcValidation)){
                    $return = array(
                        "reffnumber" => "'".$reffnum,
                        "no_aplikasi" => "'".$noaplikasi,
                        "keterangan" => "Data Debitur tidak dapat di submit mohon dilakukan pengecekan kembali",
                        "status" => '10',
                        "error" => 'CBC Validation Not Found'
                    );
    
                    array_push($dbarr, $return);
                    $errorCount++;
                    continue;
                }
    
                if($akumulasi > (float)$dataAsuransi->max_plafond){
                    $return = array(
                        "reffnumber" => "'".$reffnum,
                        "no_aplikasi" => "'".$noaplikasi,
                        "keterangan" => "Debitur telah melewati batas akumulasi plafond, akumulasi : ".number_format($akumulasi,0,',','.').
                                        ", Maksimal Limit Asuransi : ".number_format($dataAsuransi->max_plafond,0,',','.'),
                        "status" => '10',
                        "error" => 'Akumulasi Debitur'
                    );
    
                    array_push($dbarr, $return);
                    $errorCount++;
                    continue;
                    
                }
    
                if($plafond > (float)$getCbcValidation->max_plafond){
                    $iscbc = true;
                    $status_cbc = 1;
                    $ket_cbc = "Plafond debitur Melebihi maximum plafond Asuransi yang dipilih, Maximum plafond yg berjalan dengan asuransi yg di pilih adalah : Rp. ".number_format($getCbcValidation->max_plafond,0,',','.');
                }
    
                $existingData = $this->db->query("SELECT * FROM tm_penutupan WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi' ORDER BY createdon DESC, c_id DESC")->row();
                $existingDataCbc = $this->db->query("SELECT * FROM tm_penutupan WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi' AND status_cbc = '1' ORDER BY createdon DESC, c_id DESC")->row();
    
                if(!empty($existingData)){
                    if($ktp != $existingData->ktp){
                        $return = array(
                            "reffnumber" => "'".$reffnum,
                            "no_aplikasi" => "'".$noaplikasi,
                            "keterangan" => "Data No. Identitas Debitur Pertanggungan tidak sesuai dengan data sebelumnya",
                            "status" => '10'
                        );
                        
                        array_push($dbarr, $return);
                        $errorCount++;
                        continue;
                    }
    
                    if($reffnum == $existingData->reffnumber){
                        $return = array(
                            "reffnumber" => "'".$reffnum,
                            "no_aplikasi" => "'".$noaplikasi,
                            "keterangan" => "Data Reffnumber tidak bisa sama dengan data sebelumnya",
                            "status" => '10'
                        );
    
                        array_push($dbarr, $return);
                        $errorCount++;
                        continue;
                    }
                    
    
                    $datefix = date('Y-m-d',strtotime($tanggalLahir));
                    if($datefix != $existingData->tanggal_lahir){
    
                        $updatedDate = $this->db->query("SELECT tanggal_lahir, is_birth_update FROM ".$GLOBALS['table']." WHERE LTRIM(RTRIM(no_aplikasi)) = '$noaplikasi'")->row();
    
                        if($updatedDate->is_birth_update = 1){
                            $tanggalLahir = $updatedDate->tanggal_lahir;
    
                        }else{
                            $return = array(
                                "reffnumber" => "'".$reffnum,
                                "no_aplikasi" => "'".$noaplikasi,
                                "keterangan" => "Data Tanggal Lahir Debitur Pertanggungan tidak sesuai dengan data sebelumnya",
                                "status" => '10'
                            );
                            
                            array_push($dbarr, $return);
                            $errorCount++;
                            continue;
                        }
                        
                    }
                }
    
                $maxIns = (float)$dataAsuransi->max_plafond;
                $adminFee =(float)$dataAsuransi->admin_fee;
    
                $tenor_bulan = '';
                $usia_saat_akad = '';
                $usia_akhir_akad = '';
    
                //initialize transaction variable
    
                $feeAsuransi = 0;
                $totalBiaya = 0;
                $feeUjroh = 0;
                $premi = 0;
                $premiFinal = 0;
                $premiBeforeAdmin = 0;
                $premiFinal = 0;
                $biayaNasabah = 0;
                $biayaAtribusi = 0;
                $limitAsuransi = 0;
                $coverageType = '-';
                $rate = 0;
                $ratePercent = 0;
                $coverageType = '-';
                $status = 1;
    
                //CHECK Tanggal Akad Valid Atau Tidak
    
                if(!$checkDateStart || !$checkDateEnd){
                    $status = 2;
                    $tenor_bulan = '- (Tanggal Akad Tidak Sesuai)';
                    $usia_saat_akad = '- (Tanggal Akad Tidak Sesuai)';
                    $usia_akhir_akad = '- (Tanggal Akad Tidak Sesuai)';
    
                    $feeAsuransi = 0;
                    $coveragePinalti = 0;
                    $totalBiaya = 0;
                    $premi = 0;
                    $premiFinal = 0;
                    $premiBeforeAdmin = 0;
                    $premiFinal = 0;
                    $biayaNasabah = 0;
                    $biayaAtribusi = 0;
                    $limitAsuransi = 0;
                    $coverageType = '-';
                    $rate = 0;
                    $ratePercent = 0;
                    $coverageType = $type_coverages->nama;
    
                }else{
                
                    $date1 = $tanggalLahir;
                    $date2 = $mulaiAkad;
                    $date3 = $akhirAkad;
    
                    $diffAge = abs(strtotime($date2) - strtotime($date1));
                    $diffAgeEnd = abs(strtotime($date3) - strtotime($date1));
                    $diffTenor = abs(strtotime($date3) - strtotime($date2));
    
                    $yearsAge = floor($diffAge / (365*60*60*24));
                    $monthsAge = floor(($diffAge - $yearsAge * 365*60*60*24) / (30*60*60*24));
                    $daysAge = floor(($diffAge - $yearsAge * 365*60*60*24 - $monthsAge*30*60*60*24)/ (60*60*24));
    
                    $yearsAgeEnd = floor($diffAgeEnd / (365*60*60*24));
                    $monthsAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24) / (30*60*60*24));
                    $daysAgeEnd = floor(($diffAgeEnd - $yearsAgeEnd * 365*60*60*24 - $monthsAgeEnd*30*60*60*24)/ (60*60*24));
    
                    $yearsTenor = floor($diffTenor / (365*60*60*24));
                    $monthsTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24) / (30*60*60*24));
                    $daysTenor = floor(($diffTenor - $yearsTenor * 365*60*60*24 - $monthsTenor*30*60*60*24)/ (60*60*24));
    
                    $monthsAgeEndRound = $monthsAgeEnd;
                    $yearsAgeEndRound = $yearsAgeEnd;
    
                    $monthsAgeRound = $monthsAge;
                    $yearsAgeRound = $yearsAge;
    
                    if($daysAgeEnd > 14){
                        $monthsAgeEndRound = $monthsAgeEnd + 1;
                    }
    
                    if($monthsAgeEndRound > 6){
                        $yearsAgeEndRound = $yearsAgeEnd + 1;
                    }
    
                    if($daysAge > 14){
                        $monthsAgeRound = $monthsAge + 1;
                    }
    
                    if($monthsAgeRound > 6){
                        $yearsAgeRound = $yearsAge + 1;
                    }
    
                    $tenorMonth = ($yearsTenor * 12) + $monthsTenor;
    
                    $tenor_bulan = $data_json['tenor'].' ( '.$tenorMonth.' Bulan '.$daysTenor.' Hari )';
                    $usia_saat_akad = $yearsAgeRound.' Tahun ( '.$yearsAge.' Tahun '.$monthsAge.' Bulan '.$daysAge.' Hari )';
                    $usia_akhir_akad = $yearsAgeEndRound.' Tahun ( '.$yearsAgeEnd.' Tahun '.$monthsAgeEnd.' Bulan '.$daysAgeEnd.' Hari )';
                    
    
                    // printf("%d years, %d months, %d days\n", $yearsAge, $monthsAge, $daysAge);
                    // echo "</br>";
                    // printf("%d years, %d months, %d days\n", $yearsTenor, $monthsTenor, $daysTenor);
    
                    $round = false;
                    if($daysTenor < 14 && $monthsTenor = 0){
                        $round = true;
                    }
    
                    if($daysTenor > 14){
                        $monthsTenor = $monthsTenor + 1;
                        $daysTenor = 0;
                    }
    
                    $notValidYears = false;
    
                    if($yearsTenor > 0){
                        // if($monthsTenor > 6){
                        //     $yearsTenor = $yearsTenor + 1;
                        // }
                    }else{
                        $round = true;
                        $notValidYears = true;
                        $yearsTenor = 1;
                    }
    
                    if($yearsAgeRound > $getCbcValidation->max_usia || $yearsAgeRound < $getCbcValidation->min_usia){
                        $iscbc = true;
                        $status_cbc = 1;
                        $ket_cbc = "Usia debitur tidak termasuk data maximum / minimum asuransi yang dipilih, usia max / min yg berjalan dengan asuransi yg di pilih adalah max : ".$getCbcValidation->max_usia. " / min : $getCbcValidation->min_usia";
                    }
    
                    if($yearsTenor > $getCbcValidation->max_jangka_waktu){
                        $iscbc = true;
                        $status_cbc = 1;
                        $ket_cbc = "Jangka Waktu Melebihi Maximum jangka waktu asuransi yang dipilih, maximum jangka waktu yg berjalan dengan asuransi yg di pilih adalah : ".$getCbcValidation->max_jangka_waktu." Tahun";
                    }
    
                    if(!$iscbc){
    
                        $db_rate = 'tm_rate';
                        $rateAskrida = false;
                        $ins_id = $dataAsuransi->c_id;
                        $rateAskrida = false;
    
                        if($produkData == '2'){
                            $db_rate = 'tm_rate_pensiun';
                        }elseif($produkData == '4'){
                            $db_rate = 'tm_rate_prapensiun';
                        }
                        if($type_manfaat->c_id != '1'){
                            $yearsAge = 0;
                        }
    
                        $qRate = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id' ";
                        $getRate = $this->db->query($qRate)->row();
                        
                        //var_dump($qRate);exit();
                        
                        if(empty($getRate)){
                            
                            $qRate = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id' ";
                            $getRate2 = $this->db->query($qRate)->row();
                            $rateAskrida = true;
                           if(empty($getRate2)){
                                $return = array(
                                    "reffnumber" => "'".$reffnum,
                                    "no_aplikasi" => "'".$noaplikasi,
                                    "keterangan" => "Rate tidak ditemukan untuk usia : ".$yearsAge.", tenor : ".$yearsTenor. " Tahun Dan Asuransi : ".$dataAsuransi->alias,
                                    "keterangan2" => $getRate2,
                                    "status" => '10'
                                );

                                array_push($dbarr, $return);
                                $errorCount++;
                                continue;
                           }
                           
                           $getRate = $getRate2;
                        }
    
                        $finalDivider = 100;
                        if($notValidYears){
                            $qRateMonth = "SELECT * FROM tm_rate_month WHERE bulan = '$monthsTenor'";
                            $getRateMonth = $this->db->query($qRateMonth)->row();
                            $finalDivider = $getRateMonth->rate;
                        }
    
                        $ratePercent = (float)$getRate->rate;
    
                        if(!$round){
                            if($yearsTenor < 15){
                                $yearsTenor = $yearsTenor + 1;
                            }
                            
                            $qRateUp = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id'";
    
                            if($rateAskrida){
                                $qRateUp = "SELECT * FROM ".$db_rate." WHERE id_asuransi = '1' AND usia = '$yearsAge' AND tahun = '$yearsTenor' and id_routing = '$type_manfaat->c_id'";
                            }
                            
                            //var_dump($qRateUp);exit();
                            $montpercent = (float)$monthsTenor / 12;
                            $getRateUp = $this->db->query($qRateUp)->row();
                            $rateUpDown = (((float)$getRateUp->rate * 1) / 100) - (($ratePercent * 1) / 100);
                            $rateDivid = $montpercent * $rateUpDown;
                            $rateDown = (((float)$getRate->rate * 1) / 100);
                            $ratePercent = ($rateDown + $rateDivid) * 100;
    
                        }
    
                        $rate = (($ratePercent * 1) / 100);

                        var_dump($rate);exit();
    
                        $hasil = "";
                        if($type_coverage == '1' || $type_coverage == '2'){
    
                            // TYPE COVERAGE = Reguler (Tidak Ada Atribusi)
    
                            $feeAsuransi = $plafond * $rate;
                            $coveragePinalti = 0;
                            $premi = $feeAsuransi;
                            $premiFinal = round($premi * (($finalDivider * 1) / 100));
                            $premiBeforeAdmin = $premiFinal;
                            $premiFinal = $premiFinal + $adminFee;
                            $biayaNasabah = $feeAsuransi;
                            $limitAsuransi = $plafond;
                            $coverageType = $type_coverages->nama;
                            $totalBiaya = $feeAsuransi + $coveragePinalti;
                            $biayaAtribusi = $totalBiaya - $biayaNasabah;
                            $hasil = "OKEH Satu";
                            $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
    
                        }else{
                            if($type_coverage == '5' || $type_coverage == '3'){
    
                                // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE ASURANSI
    
                                $dividerRate = 1 - $rate;
                                $biayaNasabah = 0;
                                $amountDivider = ($plafond * $rate) / $dividerRate;
                                $amountRate = $plafond + $amountDivider;
                                $feeAsuransi = $rate * round($amountRate);
                                
                                $totalBiaya = $feeAsuransi + $coveragePinalti;
    
                                $limitAsuransi = $plafond + $feeAsuransi;
                                $premiDived = round($limitAsuransi * $rate);
                                $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                                $premiBeforeAdmin = $premiFinal;
                                $premiFinal = $premiFinal + $adminFee;
                                $coverageType = $type_coverages->nama;
                                $biayaAtribusi = $totalBiaya - $biayaNasabah;
                                $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                                $hasil = "OKEH Dua";
    
                            }elseif($type_coverage == '7' || $type_coverage == '4'){
    
                                // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI
    
                                $feeAsuransi = $rate * ($plafond + $coveragePinalti);
                                $totalBiaya = $feeAsuransi + $coveragePinalti;
                                $biayaNasabah = $feeAsuransi;
                                $limitAsuransi = $plafond + $coveragePinalti;
                                $biayaAtribusi = $totalBiaya - $biayaNasabah;
    
                                $premiDived = round($limitAsuransi * $rate);
                                $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                                $premiBeforeAdmin = $premiFinal;
                                $premiFinal = $premiFinal + $adminFee;
                                $coverageType = $type_coverages->nama;
                                $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                                $hasil = "OKEH Tiga";
    
                            }elseif($type_coverage == '6' || $type_coverage == '8'){
    
                                // TYPE COVERAGE = PLAFOND + FEE MITRA + FEE PINALTI + FEE ASURANSI (ALL COVER)
                                
                                $dividerRate = 1 - $rate;
                                $plafondFinal = $plafond + $coveragePinalti;
                                $amountDivider = $rate * ($plafondFinal / $dividerRate);
                                $feeAsuransi = $rate * ($plafondFinal + $amountDivider);
    
                                $biayaNasabah = 0;
                                $totalBiaya = $feeAsuransi + $coveragePinalti;
                                $limitAsuransi = $plafondFinal + $feeAsuransi + $coveragePinalti;
                                $biayaAtribusi = $totalBiaya + $biayaNasabah;
    
                                $premiDived = round($limitAsuransi * $rate);
                                $premiFinal = round($premiDived * (($finalDivider * 1) / 100));
                                $premiBeforeAdmin = $premiFinal;
                                $premiFinal = $premiFinal + $adminFee;
                                $coverageType = $type_coverages->nama;
                                $feeUjroh =( $premiFinal * (int)$rate_ujroh ) / 100;
                                $hasil = "OKEH Empat";
                                
    
                            }
                        }
    
                    }else{
    
                        $status = 1;
                        $hasil = 'cbc';
                        //CBC hanya set coverage type nya saja
                        $coverageType = $type_coverages->nama;
    
                    }
                }
    
                $this->db->select('c_id');
                $this->db->WHERE('nama', $data_json['tipe_produk']);
                $type_produk = $this->db->get($GLOBALS['tableProdukType'])->row()->c_id;
                
                $this->db->select('c_id');
                $this->db->like('kode_produk', 'BSM '.$data_json['produk']);
                $produkId = $this->db->get($GLOBALS['tableProduk'])->row()->c_id;

                $this->db->select('c_id');
                $this->db->where('id_produk', $produkId);
                $this->db->where('nama', $data_json['jenis_pekerjaan']);
                $jenis_pekerjaan = $this->db->get($GLOBALS['tableJenisPekerjaan'])->row()->c_id;

                $this->db->select('c_id');
                $this->db->like('nama', $data_json['jenis_akad']);
                $jenis_akad = $this->db->get($GLOBALS['tableAkad'])->row()->c_id;


                $insArr = array(
                    'id_asuransi' => $dataAsuransi->c_id,
                    'reffnumber' => $reffnum,
                    'no_aplikasi' => $noaplikasi,
                    'kode_cabang' => $cabang->kode_cabang,
                    'nama_cabang' => $cabang->nama,
                    'nama' => $data_json['nama_nasabah'],
                    'ktp' => $ktp,
                    'jenis_kelamin' => $data_json['jenis_kelamin'],
                    'tanggal_lahir' => date('Y-m-d',strtotime($tanggalLahir)),
                    'nama_asuransi' => $dataAsuransi->nama,
                    'npwp' => $data_json['npwp'],
                    'produk' => $produkId,
                    'tipe_produk' => $type_produk,
                    'jenis_pekerjaan' => $jenis_pekerjaan,
                    'jenis_agunan' => $data_json['jenis_agunan'],
                    'nomor_agunan' => $data_json['nomor_agunan'],
                    'jenis_akad' => $jenis_akad,
                    'nomor_akad' => $data_json['nomor_akad'],
                    'plafond' => (float)$data_json['plafond'],
                    'tanggal_akad' => $checkDateStart ? date('Y-m-d',strtotime($mulaiAkad)) : NULL,
                    'tanggal_mulai' => $checkDateStart ? date('Y-m-d',strtotime($mulaiAkad)) : NULL,
                    'tanggal_akhir' => $checkDateEnd ? date('Y-m-d',strtotime($akhirAkad)) : NULL,
                    'atribusi' => $atribusi,
                    'tenor' => $data_json['tenor'],
                    'premi' => $premiFinal,
                    'type_coverage' => $coverageType,
                    'coverage_plafond' => $coveragePlafond,
                    'coverage_fee_mitra' => $coverageFeeMitra,
                    'coverage_pinalti' => $coveragePinalti,
                    'coverage_asuransi' => $coverageAsuransi,
                    'type_manfaat' => $type_manfaat->c_id,
                    'fee_asuransi' => $feeAsuransi,
                    'total_biaya' => $totalBiaya,
                    'biaya_nasabah' => $biayaNasabah,
                    'limit_asuransi' => $limitAsuransi,
                    'biaya_atribusi' => $biayaAtribusi,
                    'fee_ujroh' => $feeUjroh,
                    'biaya_admin' => $adminFee,
                    'premi_before_admin' => $premiBeforeAdmin,
                    'bulan_tenor' => $tenor_bulan,
                    'usia_saat_akad' => $usia_saat_akad,
                    'usia_selesai_akad' => $usia_akhir_akad,
                    'fixed_rate' => $ratePercent,
                    'coverage_type' => $coverageType,
                    'status' => $status,
                    'status_data' => 1,
                    'status_cbc' => $status_cbc,
                    'ket_cbc' => $ket_cbc,
                    'status_excel' => 1,
                    'excel_dok' => $fileNames,
                    'ket_cbc' => $ket_cbc,
                    'createdon' => date('Y-m-d H:i:s'),
                    'createdby' => $userSession,
                    'stscurl' => 0
                );
                
                $insert = $this->db->insert($GLOBALS['table'], $insArr);

                if(!$iscbc){
                    
                    $return = array(
                        'reffnumber' => "'".$reffnum,
                        'no_aplikasi' => "'".$noaplikasi,
                        'no_covernote' => "'".$noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => number_format($premiFinal,0,',','.'),
                        'coverage' => number_format($limitAsuransi,0,',','.'),
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Free Cover (CAC) ",
                        'status' => '00'
                    );

                    $successCount++;

                }else{
                    $return = array(
                        'reffnumber' => "'".$reffnum,
                        'no_aplikasi' => "'".$noaplikasi,
                        'no_covernote' => "'".$noaplikasi.'-'.$reffnum,
                        'tgl_covernote' => $today,
                        'nilai_ijk' => number_format($premiFinal,0,',','.'),
                        'coverage' => number_format($limitAsuransi,0,',','.'),
                        "keterangan" => "Berhasil Melakukan Pengajuan Debitur dengan Status : Case By Case (CBC) ",
                        'status' => '00'
                    );

                    $successCount++;
                }

                if(!empty($exData)){
                    $this->db->query("UPDATE ".$GLOBALS['table']." SET status_data = '0' WHERE no_aplikasi = '$noaplikasi' AND reffnumber != '$reffnum' "); 
                }

                array_push($dbarr, $return);
                
            }
            // End Loop

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();

                $this->generateExcelUpload($dbarr,$namafile);

                $generate_report = 'generated_report_'.$namafile;

                $return = array(
                    'status' => '10',
                    'statusDetail' => 'Failed',
                    'upload_filename' => $fileNames,
                    'generated_report' => $generate_report,
                    'success_debitur' => $successCount,
                    'error_debitur' => $errorCount
                );

                $this->writeLog('PENUTUPAN-MANUAL-UPLOAD','FAILED',TR_CN_HIS,NULL,$return,$generate_report);

            }
            else
            {
                $this->db->trans_commit();

                $this->generateExcelUpload($dbarr,$namafile);

                $generate_report = 'generated_report_'.$namafile;

                $return = array(
                    'status' => '00',
                    'statusDetail' => 'Success',
                    'upload_filename' => $fileNames,
                    'generated_report' => $generate_report,
                    'success_debitur' => $successCount,
                    'error_debitur' => $errorCount
                );

                $this->writeLog('PENUTUPAN-MANUAL-UPLOAD','SUCCESS',TR_CN_HIS,NULL,json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$generate_report);
            }

            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;

        }catch(Exception $e){
            $this->db->trans_rollback();
            log_message( 'error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine() );
            // on error
            $errMsg = 'error '. $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine();

            $return = array(
                'status' => 'ERROR',
                'upload_filename' => $fileNames,
                'error_message' => $errMsg,
            );

            $this->writeLog('PENUTUPAN-MANUAL-UPLOAD','FAILED',TR_CN_HIS,NULL,$fileNames,json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;
        }
    }

    public function generateExcelUpload($arrData,$namafile){
        ini_set('max_execution_time', '0');
        ini_set("memory_limit","-1");

        $data = $arrData;
        
            date_default_timezone_set('Asia/Jakarta');

            $todays = date("Y-m-d H:i:s");
    
            $date = date("Ymd_His");
    
            $styleString = array(
                'font' => array(
                    'color' => array('rgb' => '000000'),
                    'size' => 10,
                    'name' => 'Arial'
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                )
            );
            $styleTitle = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '000000'),
                    'size' => 16,
                    'name' => 'Arial'
                ),
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                )
            );
            $styleTitle2 = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '000000'),
                    'size' => 12,
                    'name' => 'Arial'
                ),
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                )
            );
            $styleTitle3 = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '000000'),
                    'size' => 10,
                    'name' => 'Arial'
                ),
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                )
            );
            $styleTime = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '000000'),
                    'size' => 8,
                    'name' => 'Arial'
                ),
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                )
            );
            $styleHeader = array(
                'fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C9C9C9')
                ),
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '000000'),
                    'size' => 10,
                    'name' => 'Arial'
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                ),
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $styleRow = array(
                'font' => array(
                    'color' => array('rgb' => '000000'),
                    'size' => 10,
                    'name' => 'Arial'
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $styleFooter = array(
                'font' => array(
                    'bold' => true,
                    'color' => array('rgb' => '000000'),
                    'size' => 10,
                    'name' => 'Arial'
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
        
            $objReader = new PHPExcel(); // Empty Sheet
            $objReader->setActiveSheetIndex(0);
            $objReader->getActiveSheet()->setTitle('BSI Rep Upload Data');

            $objReader->getActiveSheet()->setCellValue('A1', 'Report Penutupan Upload');
            $objReader->getActiveSheet()->getStyle('A1')->applyFromArray($styleTitle);
            $objReader->getActiveSheet()->mergeCells('A1:B1');

            $objReader->getActiveSheet()->setCellValue('A2', 'Upload Date : '. date('Y-m-d'));
            $objReader->getActiveSheet()->getStyle('A2')->applyFromArray($styleTitle3);
            $objReader->getActiveSheet()->mergeCells('A2:B2');

            $objReader->getActiveSheet()->setCellValue('A3', 'File Upload : '. $namafile);
            $objReader->getActiveSheet()->getStyle('A3')->applyFromArray($styleTitle3);
            $objReader->getActiveSheet()->mergeCells('A3:B3');

            $objReader->getActiveSheet()->setCellValue('A4', 'Status Upload : '. 'Manual Upload');
            $objReader->getActiveSheet()->getStyle('A4')->applyFromArray($styleTitle3);
            $objReader->getActiveSheet()->mergeCells('A4:B4');
    
            $objReader->getActiveSheet()->setCellValue('A5', 'Generated at : ' . $todays);
            $objReader->getActiveSheet()->getStyle('A5')->applyFromArray($styleTime);
            $objReader->getActiveSheet()->mergeCells('A5:B5');
    
            $objReader->getActiveSheet()
            ->getStyle('A6:W6')
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            
            $objReader->getActiveSheet()->getRowDimension('2')->setRowHeight(25);
            $objReader->getActiveSheet()->getStyle('A6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objReader->getActiveSheet()->getStyle('A6:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objReader->getActiveSheet()->getStyle("A6:E6")->getFont()->setBold( true );
            $objReader->getActiveSheet()->getStyle("A6:E6")->getFont()->setSize(12);
            $objReader->getActiveSheet()->getStyle('A6:E6')->getAlignment()->setWrapText(true); 
    
            for ($col = 'A'; $col !== 'F'; $col++){
                $objReader->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                
            }
    
            for ($col = 'A'; $col !== 'F'; $col++){
                $objReader->getActiveSheet()->getStyle($col.'6')->applyFromArray($styleHeader);
                
            }
            
            $objReader->getActiveSheet()->setCellValue('A6', 'No Aplikasi');
            $objReader->getActiveSheet()->setCellValue('B6', 'Reffnumber');
            $objReader->getActiveSheet()->setCellValue('C6', 'Status');
            $objReader->getActiveSheet()->setCellValue('D6', 'Premi');
            $objReader->getActiveSheet()->setCellValue('E6', 'Keterangan');
            $startrows = 7;
            foreach($data as $arr){

                $premi = 0;

                if($arr['status'] == '00'){
                    $premi = $arr['nilai_ijk'];
                }

                $objReader->getActiveSheet()->setCellValue('A'.$startrows, $arr['no_aplikasi']);
                $objReader->getActiveSheet()->setCellValue('B'.$startrows, $arr['reffnumber']);
                $objReader->getActiveSheet()->setCellValue('C'.$startrows, $arr['status'] == '00' ? 'Sukses' : 'Gagal');
                $objReader->getActiveSheet()->setCellValue('D'.$startrows, $premi);
                $objReader->getActiveSheet()->setCellValue('E'.$startrows, $arr['keterangan']);
                $startrows++;
            }
            
            $objWriter = PHPExcel_IOFactory::createWriter($objReader, 'Excel2007');
            $names = 'generated_report_'.$namafile;
            $filename = './upload/data_upload/'.$names;
            
            $objWriter->save($filename);
            
            // return $filename;
            
    }

    function template(){
        $this->load->helper('download');
        $name = 'template_manual_data.xlsx';

        $data = file_get_contents(str_replace(__FILE__,$_SERVER['DOCUMENT_ROOT'] .'/upload/penutupan_manual/'.$name,__FILE__));
        force_download($name, $data); 
    }

}

