<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
        parent::__construct();

		
    }

	function index()
	{
        $data['title']  		= "Login";
        
		$this->load->view('auth/login',$data);
	}

	function error_404(){
		$this->load->view("errors/404"); 
	}

	function signin(){
		extract($_POST);

		$this->db->select('*');
		$this->db->where('c_username',strtoupper($username));
		$datauser = $this->db->get('tm_user')->row();
		
		if(!empty($datauser)){

			if($password != $datauser->c_password){
				$return = array(
					'statuscode' => '10',
					'message' => 'Password tidak sesuai dengan username '.$datauser->c_username
				);

				$this->output->set_header('HTTP/1.0 200 OK');
				$this->output->set_header('HTTP/1.1 200 OK');
				$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
				$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
				$this->output->set_header('Pragma: no-cache');

				$this->output
				->set_status_header(200)
				->set_content_type('application/json', 'utf-8')
				->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

			}else{
				
				if(!empty($this->session->userdata('login_referrer'))){
					$tmp_referrer = $this->session->userdata('login_referrer');
					$this->session->unset_userdata('login_referrer');

					// var_dump($tmp_referrer);exit();
		
					$data_session = array(
					  'Username' => strtoupper($username),
					  'UserId' => $datauser->c_id,
					  'Role' => strtoupper($datauser->c_role),
					  );
		  
					$this->session->set_userdata($data_session);

					$return = array(
						'statuscode' => '00',
						'message' => 'Success',
						'login_referer' => $tmp_referrer,
					);

				  }else{
					$tmp_referrer = base_url('dashboard');
					$data_session = array(
						'Username' => strtoupper($username),
						'UserId' => $datauser->c_id,
						'Role' => strtoupper($datauser->c_role),
						);
		  
					$this->session->set_userdata($data_session);

					$return = array(
						'statuscode' => '00',
						'message' => 'Success',
						'login_referer' => $tmp_referrer,
					);
				}

				

				  $this->output->set_header('HTTP/1.0 200 OK');
					$this->output->set_header('HTTP/1.1 200 OK');
					$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
					$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
					$this->output->set_header('Pragma: no-cache');

					$this->output
					->set_status_header(200)
					->set_content_type('application/json', 'utf-8')
					->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			}
		}else{
			$return = array(
				'statuscode' => '10',
				'message' => 'Username tidak ditemukan '
			);

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');

			$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		}

	}

	function ubah_password($KodeUser)
	{
		extract($_POST);
		$this->db->query("update tm_user set c_password = '$Password' where c_username = '$KodeUser'");
		$this->session->unset_userdata('Username');
		$this->session->unset_userdata('UserId');
		$this->session->unset_userdata('Role');
		session_destroy();
		redirect('login');
	}

	public function signout() 
	{
		$this->session->unset_userdata('Username');
		$this->session->unset_userdata('UserId');
		$this->session->unset_userdata('Role');
		session_destroy();
		redirect('login');
	}

}

