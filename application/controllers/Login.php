<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
        parent::__construct();

		$userSession = $this->session->userdata('Username');

		if(isset($userSession)){
			redirect('dashboard');
		}
    }

	function index()
	{
        $data['title']  		= "Login";
        
		$this->load->view('auth/login',$data);
	}

	function error_404(){
		$this->load->view("errors/404"); 
	}

	

}

