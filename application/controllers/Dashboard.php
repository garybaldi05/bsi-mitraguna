<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('Dashboard_m','mainModel');
    }

	function index()
	{
        $data['title']  		= "Home";
		$data['penutupan_slide'] = $this->mainModel->getSlidePenutupan();
		// $data['klaim_slide'] = $this->mainModel->getSlideKlaim();
		$data['penutupan_insur'] = $this->mainModel->getPenutupanByInsurance();
		// $data['klaim_insur'] = $this->mainModel->getKlaimByInsurance();
        
		$this->template->load('template','content',$data);
	}

	function error_404(){
		$this->load->view("errors/404"); 
	}

}

