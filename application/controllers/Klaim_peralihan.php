<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once './vendor/autoload.php';
include_once (dirname(__FILE__) . "/./Master_Controller.php");

global $table, $tableUser, $tableProduk, 
       $tableProdukType, $tableTypeManfaat, $tableJenisPekerjaan,
       $tableAkad, $tableCabang ;

$table = DB_NAME_PREFIX.'tm_klaim_peralihan';
$tableUser = DB_NAME_PREFIX.'tm_user';
$tableProduk = DB_NAME_PREFIX.'tm_product_list';
$tableProdukType = DB_NAME_PREFIX.'tm_product_type';
$tableJenisPekerjaan = DB_NAME_PREFIX.'tm_jenis_pekerjaan';
$tableAkad = DB_NAME_PREFIX.'tm_jenis_akad';
$tableTypeManfaat = DB_NAME_PREFIX.'tm_type_manfaat';
$tableCabang = DB_NAME_PREFIX.'tm_cabang';
$tableLog = DB_NAME_PREFIX.'tl_log_data';

class Klaim_peralihan extends Master_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('KlaimPeralihan_m','mainModel');
        $this->load->library('Excel');

        $userSession = $this->session->userdata('Username');
        if(!isset($userSession)){
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

	function index()
	{
		$this->template->load('template','klaim_peralihan/list');
    }

    function printKlaim()
	{   
        $id = $_GET['id'];

        $data['editData'] = $this->mainModel->getDatabyId($id);

		$this->load->view('klaim_peralihan/print',$data);
    }

    function printLod()
	{   
        $id = $_GET['id'];

        $data['editData'] = $this->mainModel->getDatabyId($id);

		$this->load->view('klaim_peralihan/printlod',$data);
    }
    
	function view_data(){
		$cari = $_POST;
		$data = $this->mainModel->get_tables($cari);

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
		die;

	}

	function view(){
		$id = base64_decode($_GET['data']);
		$type = base64_decode($_GET['type']);
		
		$data['editData'] = $this->mainModel->getDatabyId($id,$type);

		$this->template->load('template','klaim_peralihan/edit',$data);
	}

	function edit(){
		$id = ($_GET['data']);
		
		$data['editData'] = $this->mainModel->getDatabyId($id);

		$this->template->load('template','klaim_peralihan/edit',$data);
	}

	function input(){
		$this->template->load('template','klaim_peralihan/input');
	}

    function getDebitur(){
		$id = $_GET['id'];

		// $this->db->select('*');
		// $this->db->where('c_id',$id);
		// $oneData = $this->db->get('tm_penutupan_peralihan')->row();

        $query = "SELECT 
                    a.no_polis,
                    a.tanggal_mulai,
                    a.plafond,
                    a.tanggal_akhir,
                    a.premi,
                    b.branch_name,
                    a.kode_cabang
                    FROM tm_penutupan_peralihan a
                    LEFT JOIN tm_cabang_peralihan b on a.kode_cabang = b.branch_code
                    WHERE a.c_id = '$id'
                 ";
        $oneData = $this->db->query($query)->row();

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($oneData));
		die;
    }

	function submitDataDebitur(){
		extract($_POST);

		date_default_timezone_set('Asia/Jakarta');
        $userSession = $this->session->userdata('UserId');
        $years = date('Ymd');
       
		try {
            $this->db->select('*');
            $this->db->where('c_id',$nama_debitur);
            $this->db->where('status',1);
            $dataDebitur = $this->db->get('tm_penutupan_peralihan')->row();

            $noklaim = $nama_debitur.'/'.$years.'/'.$dataDebitur->kode_cabang;

            $premi = str_replace('.','',$premi);
            $premi = str_replace(',','.',$premi);

            $plafond = str_replace('.','',$plafond);
            $plafond = str_replace(',','.',$plafond);

            $os_pokok_klaim = str_replace('.','',$os_pokok_klaim);
            $os_pokok_klaim = str_replace(',','.',$os_pokok_klaim);

            $os_bunga_klaim = str_replace('.','',$os_bunga_klaim);
            $os_bunga_klaim = str_replace(',','.',$os_bunga_klaim);

            $os_total_klaim = str_replace('.','',$os_total_klaim);
            $os_total_klaim = str_replace(',','.',$os_total_klaim);

            if(empty($os_bunga_klaim)){
                $os_bunga_klaim = 0;
            }

            $arrInsert = array(
                'id_penutupan' => $dataDebitur->c_id,
                'nama' => $dataDebitur->nama,
                'tanggal_lahir' => $dataDebitur->tanggal_lahir,
                'no_polis' => $dataDebitur->no_polis,
                'asuransi' => $dataDebitur->asuransi,
                'tanggal_mulai' => $dataDebitur->tanggal_mulai,
                'tanggal_akhir' => $dataDebitur->tanggal_akhir,
                'tanggal_akad' => $dataDebitur->tanggal_akad,
                'tanggal_lapor' => $tanggal_lapor,
                'tanggal_kejadian' => $tanggal_kejadian,
                'plafond' => $dataDebitur->plafond,
                'premi' => $dataDebitur->premi,
                'os_pokok_klaim' => $os_pokok_klaim,
                'os_bunga_klaim' => $os_bunga_klaim,
                'os_total_klaim' => $os_total_klaim,
                'type_manfaat' => $type_manfaat,
                'kode_cabang' => $dataDebitur->kode_cabang,
                'status_klaim' => 1,
                'noklaim' => $noklaim,
                'createdon' => date('Y-m-d H:i:s'),
                'createdby' => $userSession
            );

            $insert = $this->db->insert('tm_klaim_peralihan', $arrInsert);

            if($insert){
                $insert_id = $this->db->insert_id();
                $update = "UPDATE tm_penutupan_peralihan SET status_klaim = '1' WHERE c_id = '$nama_debitur' ";
                
                $this->db->query($update);

                $return = array(
                    "keterangan" => "Berhasil Input Data Klaim",
                    "status" => '00',
                    "id" => $insert_id
                );
                
                $this->writeLogData('INSERT-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,json_encode($_POST, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$insert_id);
                $this->writeLogStatus('STATUS-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,NULL,'1',$insert_id);

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
                $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                
                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                die;

            }else{
                $return = array(
                    "keterangan" => "Gagal Input Data Klaim",
                    "status" => '10',
                    "id" => ""
                );
                
                $this->writeLogData('INSERT-KLAIM-PERALIHAN','FAILED',TR_KLAIM_PERALIHAN,NULL,json_encode($_POST, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),null);

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
                $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                
                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                die;
            }

		} catch (Exception $e) {
			$return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLogData('INSERT-KLAIM-PERALIHAN','ERROR',TR_KLAIM_PERALIHAN,NULL,json_encode($_POST, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($error, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES), null);

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
            
            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;
		}
	}

    function editDataDebitur(){
		extract($_POST);

		date_default_timezone_set('Asia/Jakarta');
        $userSession = $this->session->userdata('UserId');
        $years = date('Ymd');
       
		try {
            $this->db->select('*');
            $this->db->where('c_id',$nama_debitur);
            $this->db->where('status',1);
            $dataDebitur = $this->db->get('tm_penutupan_peralihan')->row();

            $noklaim = $nama_debitur.'/'.$years.'/'.$dataDebitur->kode_cabang;

            $premi = str_replace('.','',$premi);
            $premi = str_replace(',','.',$premi);

            $plafond = str_replace('.','',$plafond);
            $plafond = str_replace(',','.',$plafond);

            $os_pokok_klaim = str_replace('.','',$os_pokok_klaim);
            $os_pokok_klaim = str_replace(',','.',$os_pokok_klaim);

            $os_bunga_klaim = str_replace('.','',$os_bunga_klaim);
            $os_bunga_klaim = str_replace(',','.',$os_bunga_klaim);

            $os_total_klaim = str_replace('.','',$os_total_klaim);
            $os_total_klaim = str_replace(',','.',$os_total_klaim);

            if(empty($os_bunga_klaim)){
                $os_bunga_klaim = 0;
            }

            $arrInsert = array(
                'tanggal_lapor' => $tanggal_lapor,
                'tanggal_kejadian' => $tanggal_kejadian,
                'os_pokok_klaim' => $os_pokok_klaim,
                'os_bunga_klaim' => $os_bunga_klaim,
                'os_total_klaim' => $os_total_klaim,
                'type_manfaat' => $type_manfaat,
                'updatedon' => date('Y-m-d H:i:s'),
                'updatedby' => $userSession
            );

            $this->db->where('c_id', $isEdit);
            $insert = $this->db->update('tm_klaim_peralihan', $arrInsert);

            if($insert){
              
                $return = array(
                    "keterangan" => "Berhasil Input Data Klaim",
                    "status" => '00',
                    "id" => $isEdit
                );
                
                $this->writeLogData('UPDATE-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,json_encode($_POST, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$isEdit);
                // $this->writeLogStatus('STATUS-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,NULL,'1',$isEdit);

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
                $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                
                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                die;

            }else{
                $return = array(
                    "keterangan" => "Gagal Input Data Klaim",
                    "status" => '10',
                    "id" => ""
                );
                
                $this->writeLogData('UPDATE-KLAIM-PERALIHAN','FAILED',TR_KLAIM_PERALIHAN,NULL,json_encode($_POST, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),null);

                $this->output->set_header('HTTP/1.0 200 OK');
                $this->output->set_header('HTTP/1.1 200 OK');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
                $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                
                return $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                die;
            }

		} catch (Exception $e) {
			$return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLogData('UPDATE-KLAIM-PERALIHAN','ERROR',TR_KLAIM_PERALIHAN,NULL,json_encode($_POST, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($error, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES), null);

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
            
            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;
		}
	}

    function uploadDokumen(){
        extract($_POST);
        // var_dump($_POST,$_FILES[$file_to_upload]['name']);exit();
        $today = date('Y-m-d');
		$dateon = date('Y-m-d H:i:s');
        try{
        $createdby = strtoupper($this->session->userdata('KodeUser'));
        $userSession = $this->session->userdata('UserId');
        $file = "";
        $file = $_FILES[$file_to_upload]['name'];
       
        $this->db->select('*');
        $this->db->where('c_id',$c_id);
        $dataDebitur = $this->db->get('tm_klaim_peralihan')->row();

        $statusOld = $dataDebitur->status_klaim;
        $statusNew = 2;

        if($statusOld = 1){
            $statusNew = $statusNew;
        }else{
            $statusNew = $statusOld;
        }

        $fileName = str_replace(' ', '', $file);
        $explode = explode(".",$fileName);
        $date = date("Ymd_His");
        $noklaim = str_replace("/","",$dataDebitur->noklaim);
        $fileexp = $noklaim."_".$file_to_upload.'_'.$date.'.'.$explode[1];

        $fixpath = "./upload/klaim_peralihan/dok_".$noklaim;
        $filenames = "";

        if (!file_exists($fixpath)) {
            mkdir($fixpath, 0777, true);
        }

        // var_dump($noklaim,$fixpath);exit();

        $config['upload_path'] = $fixpath;
        $config['file_name'] = $fileexp;
        // $config['allowed_types'] = 'xls|xlsx|csv';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(! $this->upload->do_upload($file_to_upload) ){
			var_dump($this->upload->display_errors());
			exit();
        }else{
            $uploadData = $this->upload->data(); 
            $filenames = $uploadData['file_name'];
        }

        $arrInsert = array(
            "id_klaim_peralihan" => $c_id,
            "nama_dokumen" => $filenames,
            "path" => str_replace(".","",$fixpath),
            "type_dokumen" => $file_to_upload,
            "status" => 1,
            "createdby" => $userSession,
            "createdon" => date('Y-m-d H:i:s')
        );

        $insert = $this->db->insert('tm_dok_klaim_peralihan', $arrInsert);

        if($insert){

            $insert_id = $this->db->insert_id();
            $extraStatus = "status_klaim = '2'";
            $extraStatus2 = "tanggal_awal_dokumen = '$today' ,";
            // var_dump($statusOld,$statusNew);exit();
            if($statusOld = 1){
                if(!empty($dataDebitur->tanggal_awal_dokumen)){
                    $extraStatus2 = "";
                }

                $update = "UPDATE tm_klaim_peralihan SET ".$extraStatus2." ".$extraStatus."  WHERE c_id = '$c_id' ";
                $this->db->query($update);
            }

            
            
            $updateDokStatus = "UPDATE tm_dok_klaim_peralihan SET status = '0' WHERE id_klaim_peralihan = '$c_id' AND type_dokumen = '$file_to_upload' AND c_id != '$insert_id' ";
            $this->db->query($updateDokStatus);

            $return = array(
                "keterangan" => "Berhasil Upload Dokumen",
                "status" => '00',
                "id" => $insert_id,
                "type_dokumen" => $file_to_upload,
                "path" => str_replace(".","",$fixpath),
                "nama_dokumen" => $filenames
            );

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );
            
            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

            if($statusNew != $statusOld){
                $this->writeLogStatus('STATUS-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,$statusOld,$statusNew,$c_id);
            }
            

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;

        }else{
            $return = array(
                "keterangan" => "Gagal Upload Dokumen",
                "status" => '10',
                "id" => ""
            );

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );
            
            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','FAILED',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;
        }

        }catch (Exception $e) {
			$return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            // var_dump($escaped_str);exit();

            $msgError = "DB transaction failedr";
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );

            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','ERROR',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($error, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
            
            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;
		}

    }


    function uploadDokumenLod(){
        extract($_POST);
        // var_dump($_POST,$_FILES[$file_to_upload]['name']);exit();
        $today = date('Y-m-d');
		$dateon = date('Y-m-d H:i:s');
        try{
        $createdby = strtoupper($this->session->userdata('KodeUser'));
        $userSession = $this->session->userdata('UserId');
        $file = "";
        $file = $_FILES[$file_to_upload]['name'];
       
        $this->db->select('*');
        $this->db->where('c_id',$c_id);
        $dataDebitur = $this->db->get('tm_klaim_peralihan')->row();

        $statusOld = $dataDebitur->status_klaim;

        if($status_lod == '1'){
            $statusNew = '3';
        }elseif($status_lod == '2'){
            $statusNew = '4';
        }elseif($status_lod == '3'){
            $statusNew = '5';
        }

        if($statusOld == '1'){
            $return = array(
                "keterangan" => "Mohon upload data Dokumen terlebih Dahulu",
                "status" => '10'
            );

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;
        }

        $fileName = str_replace(' ', '', $file);
        $explode = explode(".",$fileName);
        $date = date("Ymd_His");
        $noklaim = str_replace("/","",$dataDebitur->noklaim);
        $fileexp = $noklaim."_".$file_to_upload.'_'.$date.'.'.$explode[1];

        $fixpath = "./upload/klaim_peralihan/dok_".$noklaim;
        $filenames = "";

        // if (!file_exists($fixpath)) {
        //     mkdir($fixpath, 0777, true);
        // }

        // var_dump($noklaim,$fixpath);exit();

        $config['upload_path'] = $fixpath;
        $config['file_name'] = $fileexp;
        // $config['allowed_types'] = 'xls|xlsx|csv';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(! $this->upload->do_upload($file_to_upload) ){
			var_dump($this->upload->display_errors());
			exit();
        }else{
            $uploadData = $this->upload->data(); 
            $filenames = $uploadData['file_name'];
        }

        $type = '';
        if($status_lod == '1'){
            $type = 'dok_lod_asuransi';
        }elseif($status_lod == '2'){
            $type = 'dok_lod_bank_approve';
        }elseif($status_lod == '3'){
            $type = 'dok_lod_reject';
        }

        $arrInsert = array(
            "id_klaim_peralihan" => $c_id,
            "nama_dokumen" => $filenames,
            "path" => str_replace(".","",$fixpath),
            "type_dokumen" => $type,
            "status" => 1,
            "createdby" => $userSession,
            "createdon" => date('Y-m-d H:i:s')
        );

        $insert = $this->db->insert('tm_dok_klaim_peralihan', $arrInsert);

        if($insert){

            $insert_id = $this->db->insert_id();
            $update = "UPDATE tm_klaim_peralihan SET status_klaim = '$statusNew', status_lod = '$status_lod'  WHERE c_id = '$c_id' ";
            $this->db->query($update);

           
            $updateDokStatus = "UPDATE tm_dok_klaim_peralihan SET status = '0' WHERE id_klaim_peralihan = '$c_id' AND type_dokumen = '$type' AND c_id != '$insert_id' ";
            $this->db->query($updateDokStatus);

            $return = array(
                "keterangan" => "Berhasil Upload Dokumen",
                "status" => '00',
                "id" => $insert_id,
                "type_dokumen" => $file_to_upload,
                "path" => str_replace(".","",$fixpath),
                "nama_dokumen" => $filenames
            );

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );
            
            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

            if($statusNew != $statusOld){
                $this->writeLogStatus('STATUS-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,$statusOld,$statusNew,$c_id);
            }
            

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;

        }else{
            $return = array(
                "keterangan" => "Gagal Upload Dokumen",
                "status" => '10',
                "id" => ""
            );

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );
            
            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','FAILED',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;
        }

        }catch (Exception $e) {
			$return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            // var_dump($escaped_str);exit();

            $msgError = "DB transaction failedr";
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );

            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','ERROR',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($error, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
            
            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;
		}

    }


    function updatePembayaran(){
        extract($_POST);
        // var_dump($_POST,$_FILES[$file_to_upload]['name']);exit();
        $today = date('Y-m-d');
		$dateon = date('Y-m-d H:i:s');
        try{
        $createdby = strtoupper($this->session->userdata('KodeUser'));
        $userSession = $this->session->userdata('UserId');
        $file = "";
        $file = $_FILES[$file_to_upload]['name'];
       
        $this->db->select('*');
        $this->db->where('c_id',$c_id);
        $dataDebitur = $this->db->get('tm_klaim_peralihan')->row();

        $statusOld = $dataDebitur->status_klaim;

        $statusNew = 6;

        if($statusOld == '1' || $statusOld == '3'){
            $return = array(
                "keterangan" => "Mohon upload data Dokumen terlebih Dahulu",
                "status" => '10'
            );

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;
        }

        $fileName = str_replace(' ', '', $file);
        $explode = explode(".",$fileName);
        $date = date("Ymd_His");
        $noklaim = str_replace("/","",$dataDebitur->noklaim);
        $fileexp = $noklaim."_".$file_to_upload.'_'.$date.'.'.$explode[1];

        $fixpath = "./upload/klaim_peralihan/dok_".$noklaim;
        $filenames = "";

        // if (!file_exists($fixpath)) {
        //     mkdir($fixpath, 0777, true);
        // }

        // var_dump($noklaim,$fixpath);exit();

        $config['upload_path'] = $fixpath;
        $config['file_name'] = $fileexp;
        // $config['allowed_types'] = 'xls|xlsx|csv';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(! $this->upload->do_upload($file_to_upload) ){
			var_dump($this->upload->display_errors());
			exit();
        }else{
            $uploadData = $this->upload->data(); 
            $filenames = $uploadData['file_name'];
        }

        $arrInsert = array(
            "id_klaim_peralihan" => $c_id,
            "nama_dokumen" => $filenames,
            "path" => str_replace(".","",$fixpath),
            "type_dokumen" => $file_to_upload,
            "status" => 1,
            "createdby" => $userSession,
            "createdon" => date('Y-m-d H:i:s')
        );

        $insert = $this->db->insert('tm_dok_klaim_peralihan', $arrInsert);

        if($insert){

            $klaim_dibayar_asuransi = str_replace('.','',$klaim_dibayar_asuransi);
            $klaim_dibayar_asuransi = str_replace(',','.',$klaim_dibayar_asuransi);

            $klaim_dibayar_broker = str_replace('.','',$klaim_dibayar_broker);
            $klaim_dibayar_broker = str_replace(',','.',$klaim_dibayar_broker);

            if(empty($tanggal_pembayaran_asuransi)){
                $tanggal_pembayaran_asuransi = null;
            }

            if(empty($tanggal_pembayaran_broker)){
                $tanggal_pembayaran_broker = null;
            }

            if(empty($klaim_dibayar_asuransi)){
                $klaim_dibayar_asuransi = null;
            }

            if(empty($klaim_dibayar_broker)){
                $klaim_dibayar_broker = null;
            }

            $insert_id = $this->db->insert_id();
            $update = "UPDATE tm_klaim_peralihan 
                        SET 
                            status_klaim = '$statusNew', 
                            tanggal_pembayaran_asuransi = '$tanggal_pembayaran_asuransi',
                            tanggal_pembayaran_broker = '$tanggal_pembayaran_broker',
                            klaim_dibayar_asuransi = '$klaim_dibayar_asuransi',  
                            klaim_dibayar_broker = '$klaim_dibayar_broker',
                            updatedon = '$dateon',
                            updatedby = '$userSession'  
                        WHERE c_id = '$c_id' ";
            $this->db->query($update);

           
            $updateDokStatus = "UPDATE tm_dok_klaim_peralihan SET status = '0' WHERE id_klaim_peralihan = '$c_id' AND type_dokumen = 'bukti_bayar' AND c_id != '$insert_id' ";
            $this->db->query($updateDokStatus);

            $return = array(
                "keterangan" => "Berhasil Upload Dokumen",
                "status" => '00',
                "id" => $insert_id,
                "type_dokumen" => $file_to_upload,
                "path" => str_replace(".","",$fixpath),
                "nama_dokumen" => $filenames
            );

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );
            
            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

            if($statusNew != $statusOld){
                $this->writeLogStatus('STATUS-KLAIM-PERALIHAN','SUCCESS',TR_KLAIM_PERALIHAN,NULL,$statusOld,$statusNew,$c_id);
            }
            

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;

        }else{
            $return = array(
                "keterangan" => "Gagal Upload Dokumen",
                "status" => '10',
                "id" => ""
            );

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );
            
            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','FAILED',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($return, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

            $this->output->set_header('HTTP/1.0 200 OK');
            $this->output->set_header('HTTP/1.1 200 OK');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            
            return $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            die;
        }

        }catch (Exception $e) {
			$return = array("Result"=>array('status_code' => '13', "message"=>"Error : ".$e->getMessage().", Please Contact your system Administrator"));

            $escaped_str = str_replace("'", "''", $this->db->last_query());
            // var_dump($escaped_str);exit();

            $msgError = "DB transaction failedr";
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $arr = array(
                'c_id' => $c_id,
                'file_to_upload' => $file_to_upload
            );

            $this->writeLogData('UPLOAD-DOKUMEN-KLAIM-PERALIHAN','ERROR',TR_KLAIM_PERALIHAN,NULL,json_encode($arr, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),json_encode($error, TRUE, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),$c_id);

			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
            
            return $this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($return, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
			die;
		}

    }

    function getDokumenData(){
        $id = $_GET['id'];
        $this->db->select('*');
        $this->db->like('id_klaim_peralihan', $id);
        $this->db->like('status', 1);
        $dataActivity = $this->db->get('tm_dok_klaim_peralihan')->result_array();

        $this->output->set_header('HTTP/1.0 200 OK');
        $this->output->set_header('HTTP/1.1 200 OK');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        
        return $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($dataActivity, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        die;

    }

    function getHistoryData(){
		$no_app = base64_decode($_GET['no_app']);
		$reffnumber = base64_decode($_GET['reffnumber']);

        $this->db->select('*');
        $this->db->like('datanew', $no_app);
        $this->db->like('datanew', $reffnumber);
        $dataActivity = $this->db->get('tl_cn_his')->result_array();

        for ($i=0; $i < count($dataActivity) ; $i++) { 

			if($dataActivity[$i]['status'] == 'SUCCESS' ){
				$arr = array(
							'colour' => 'text-success',
							'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
							'url' => '/bsi-midsvc-cnreq'
						);
			}elseif($dataActivity[$i]['status'] == 'FAILED' ){
				$arr = array(
						'colour' => 'text-warning',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}elseif($dataActivity[$i]['status'] == 'ERROR' ){
				$arr = array(
						'colour' => 'text-danger',
						'datetime' => date('d M Y g:i a',strtotime($dataActivity[$i]['createdon'])),
						'url' => '/bsi-midsvc-cnreq'
					);
			}

			// array_push($dataActivity[$i], $arr);

			$dataActivity[$i] = $dataActivity[$i] + $arr;
		}

		return $this->output
		->set_content_type('application/json')
		->set_output(json_encode($dataActivity, TRUE,  JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		die;
	}

}

