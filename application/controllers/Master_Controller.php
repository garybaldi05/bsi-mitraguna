<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once './vendor/autoload.php';

class Master_Controller extends CI_Controller {


    public function writeLog($actionname,$status,$trigger,$jsonold,$jsonnew,$response){

        try{
            $this->db->trans_off();

            // list of database variable
            $table = "";
            $db = DB_NAME_PREFIX;
            $uniqeKey = '';

            //datetime variable
            date_default_timezone_set('Asia/Jakarta');
            $todays = date("Y-m-d H:i:s");
            $username = $this->session->userdata('UserId'); //for testing only change it to session user
            // $usernme = $this->session->userdata('KodeUser');


            if($trigger == TR_CN_HIS){
                $table = 'tl_cn_his';

            }elseif($trigger == TR_REKON){
                $table = 'tl_his_rekon';

            }elseif($trigger == TR_KLAIM_PERALIHAN){
                $table = 'tl_klaim_peralihan_his';
            }

            $this->db->trans_begin();

            $query = "
                INSERT INTO $table
                (
                    actionname,
                    status,
                    dataold,
                    datanew,
                    response,
                    createdon,
                    createdby
                )
                VALUES
                (
                    '$actionname',
                    '$status',
                    '$jsonold',
                    '$jsonnew',
                    '$response',
                    '$todays',
                    '$username'
                )
            ";

            $this->db->query($query);

            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
            }
            else
            {
                    $this->db->trans_commit();
            }

        }catch(Exception $e){
            $this->db->trans_rollback();
            throw new DomainException($e->getMessage(), 0, $e);
        }

    }

    public function writeLogData($actionname,$status,$trigger,$jsonold,$jsonnew,$response,$c_id){

        try{
            $this->db->trans_off();

            // list of database variable
            $table = "";
            $db = DB_NAME_PREFIX;
            $uniqeKey = '';

            //datetime variable
            date_default_timezone_set('Asia/Jakarta');
            $todays = date("Y-m-d H:i:s");
            $username = $this->session->userdata('UserId'); //for testing only change it to session user
            // $usernme = $this->session->userdata('KodeUser');


            if($trigger == TR_CN_HIS){
                $table = 'tl_cn_his';

            }elseif($trigger == TR_REKON){
                $table = 'tl_his_rekon';

            }elseif($trigger == TR_KLAIM_PERALIHAN){
                $table = 'tl_klaim_peralihan_his';
            }

            $this->db->trans_begin();

            $query = "
                INSERT INTO $table
                (
                    actionname,
                    status,
                    dataold,
                    datanew,
                    response,
                    id_data,
                    createdon,
                    createdby
                )
                VALUES
                (
                    '$actionname',
                    '$status',
                    'null',
                    '$jsonnew',
                    '$response',
                    '$c_id',
                    '$todays',
                    '$username'
                )
            ";

            // echo $query;exit();

            $this->db->query($query);

            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
            }
            else
            {
                    $this->db->trans_commit();
            }

        }catch(Exception $e){
            $this->db->trans_rollback();
            print_r($e->getMessage());
        }

    }

    public function writeLogStatus($actionname,$status,$trigger,$jsonold,$jsonnew,$response,$c_id){

        try{
            $this->db->trans_off();

            // list of database variable
            $table = "";
            $db = DB_NAME_PREFIX;
            $uniqeKey = '';

            //datetime variable
            date_default_timezone_set('Asia/Jakarta');
            $todays = date("Y-m-d H:i:s");
            $username = $this->session->userdata('UserId'); //for testing only change it to session user
            // $usernme = $this->session->userdata('KodeUser');


           if($trigger == TR_KLAIM_PERALIHAN){
                $table = 'tl_klaim_peralihan_status';
            }

            $this->db->trans_begin();

            $query = "
                INSERT INTO $table
                (
                    actionname,
                    status,
                    statusold,
                    statusnew,
                    response,
                    id_data,
                    createdon,
                    createdby
                )
                VALUES
                (
                    '$actionname',
                    '$status',
                    '$jsonold',
                    '$jsonnew',
                    '$response',
                    '$c_id',
                    '$todays',
                    '$username'
                )
            ";

            // echo $query;exit();

            $this->db->query($query);

            if ($this->db->trans_status() === FALSE)
            {
                    $this->db->trans_rollback();
            }
            else
            {
                    $this->db->trans_commit();
            }

        }catch(Exception $e){
            $this->db->trans_rollback();
            print_r($e->getMessage());
        }

    }

    function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    function generateCN($noaplikasi, $reffnum){
        
        $this->db->select('*');
        $this->db->where('no_aplikasi', $noaplikasi);
        $this->db->where('reffnumber', $reffnum);
        $dataDebitur = $this->db->get($GLOBALS['table'])->row();

        $this->db->select('*');
        $this->db->where('c_id', $dataDebitur->id_asuransi);
        $dataIns = $this->db->get('tm_asuransi')->row();

        $this->db->select('nama');
        $this->db->where('c_id', $dataDebitur->jenis_pekerjaan);
        $namaPekerjaan = $this->db->get('tm_jenis_pekerjaan')->row()->nama;

        $this->db->select('nama_produk');
        $this->db->where('c_id', $dataDebitur->produk);
        $namaProduk = $this->db->get('tm_product_list')->row()->nama_produk;

        $this->db->select('nama');
        $this->db->where('c_id', $dataDebitur->type_manfaat);
        $namaManfaat = $this->db->get('tm_type_manfaat')->row()->nama;

        $insFullname = $dataIns->fullname;
        $insLogo = $dataIns->logo;
        $insLimit = $dataIns->max_plafond;
        $adminFee = $dataIns->admin_fee;

        $data['debitur'] = $dataDebitur;
        $data['fullname_asuransi'] = $insFullname;
        $data['logo_asuransi'] = $insLogo;
        $data['limit_asuransi'] = $insLimit;
        $data['fee_asuransi'] = $adminFee;
        $data['namaProduk'] = $namaProduk;
        $data['namaPekerjaan'] = $namaPekerjaan;
        $data['namaManfaat'] = $namaManfaat;

        $viewStr = $this->load->view('files/cn-page1', $data, TRUE); 
        $viewStr2 = $this->load->view('files/cn-page2', $data, TRUE);
        // var_dump($insLogo);exit();
        $params = array('no_aplikasi' => $noaplikasi, 'reffnum' => $reffnum);
        $filename = 'KPA_'.$noaplikasi.'_'.$reffnum;
        try {
            $mpdf = new \Mpdf\Mpdf([
                'format' => 'A4-P',
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 5,
                'margin_bottom' => 0,
                'margin_header' => 0,
                'margin_footer' => 0,
            ]);
            $mpdf->simpleTables = true;
            $mpdf->packTableData = true;
            $mpdf->showImageErrors = true;
            $mpdf->SetProtection(array(), 'UserPassword', 'R4h4s14_');
            $mpdf->SetProtection(array('modify','fill-forms'), '', 'R4h4s14_');
            
            $mpdf->WriteHTML($viewStr,\Mpdf\HTMLParserMode::DEFAULT_MODE);
            $mpdf->AddPage();
            $mpdf->WriteHTML($viewStr2,\Mpdf\HTMLParserMode::DEFAULT_MODE);
            $mpdf->debug = true; 
            // $mpdf->Output(); 
            
            // echo $viewStr;
            // echo"</br>";
            // echo $viewStr2;

            $mpdf->Output('upload/dok_cn/'.$filename.'.pdf', \Mpdf\Output\Destination::FILE);
            return $filename.'.pdf';
            
            
        } catch (Exception $e) {
            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLog('CN-REQUEST','ERROR',TR_CN_HIS,json_encode($params),NULL,json_encode($error));
            
            return 'GAGAL';
        }
		
    }

    function generateCN2($c_id){
        
        $this->db->select('*');
        $this->db->where('c_id', $c_id);
        $dataDebitur = $this->db->get($GLOBALS['table'])->row();

        $this->db->select('*');
        $this->db->where('c_id', $dataDebitur->id_asuransi);
        $dataIns = $this->db->get('tm_asuransi')->row();

        $this->db->select('nama');
        $this->db->where('c_id', $dataDebitur->jenis_pekerjaan);
        $namaPekerjaan = $this->db->get('tm_jenis_pekerjaan')->row()->nama;

        $this->db->select('nama_produk');
        $this->db->where('c_id', $dataDebitur->produk);
        $namaProduk = $this->db->get('tm_product_list')->row()->nama_produk;

        $this->db->select('nama');
        $this->db->where('c_id', $dataDebitur->type_manfaat);
        $namaManfaat = $this->db->get('tm_type_manfaat')->row()->nama;

        $insFullname = $dataIns->fullname;
        $insLogo = $dataIns->logo;
        $insLimit = $dataIns->max_plafond;
        $adminFee = $dataIns->admin_fee;

        $data['debitur'] = $dataDebitur;
        $data['fullname_asuransi'] = $insFullname;
        $data['logo_asuransi'] = $insLogo;
        $data['limit_asuransi'] = $insLimit;
        $data['fee_asuransi'] = $adminFee;
        $data['namaProduk'] = $namaProduk;
        $data['namaPekerjaan'] = $namaPekerjaan;
        $data['namaManfaat'] = $namaManfaat;

        $viewStr = $this->load->view('files/cn-page1', $data, TRUE); 
        $viewStr2 = $this->load->view('files/cn-page2', $data, TRUE);
        // var_dump($insLogo);exit();
        // $params = array('no_aplikasi' => $$dataDebitur->no_aplikasi, 'reffnum' => $$dataDebitur->reffnumber);
        $filename = 'KPA_'.$dataDebitur->no_aplikasi.'_'.$dataDebitur->reffnumber.'_CBC';
        try {
            $mpdf = new \Mpdf\Mpdf([
                'format' => 'A4-P',
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 5,
                'margin_bottom' => 0,
                'margin_header' => 0,
                'margin_footer' => 0,
            ]);
            $mpdf->simpleTables = true;
            $mpdf->packTableData = true;
            $mpdf->showImageErrors = true;
            $mpdf->SetProtection(array(), 'UserPassword', 'R4h4s14_');
            $mpdf->SetProtection(array('modify','fill-forms'), '', 'R4h4s14_');
            
            $mpdf->WriteHTML($viewStr,\Mpdf\HTMLParserMode::DEFAULT_MODE);
            $mpdf->AddPage();
            $mpdf->WriteHTML($viewStr2,\Mpdf\HTMLParserMode::DEFAULT_MODE);
            $mpdf->debug = true; 
            // $mpdf->Output(); 
            
            // echo $viewStr;
            // echo"</br>";
            // echo $viewStr2;

            $mpdf->Output('upload/dok_cn/'.$filename.'.pdf', \Mpdf\Output\Destination::FILE);
            return $filename.'.pdf';
            
            
        } catch (Exception $e) {
            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLog('CN-REQUEST','ERROR',TR_CN_HIS,json_encode($params),NULL,json_encode($error));
            
            return 'GAGAL';
        }
		
    }

    function viewCN($c_id){
        
        $this->db->select('*');
        $this->db->where('c_id', $c_id);
        $dataDebitur = $this->db->get($GLOBALS['table'])->row();

        $this->db->select('*');
        $this->db->where('c_id', $dataDebitur->id_asuransi);
        $dataIns = $this->db->get('tm_asuransi')->row();

        $this->db->select('nama');
        $this->db->where('c_id', $dataDebitur->jenis_pekerjaan);
        $namaPekerjaan = $this->db->get('tm_jenis_pekerjaan')->row()->nama;

        $this->db->select('nama_produk');
        $this->db->where('c_id', $dataDebitur->produk);
        $namaProduk = $this->db->get('tm_product_list')->row()->nama_produk;

        $this->db->select('nama');
        $this->db->where('c_id', $dataDebitur->type_manfaat);
        $namaManfaat = $this->db->get('tm_type_manfaat')->row()->nama;

        $insFullname = $dataIns->fullname;
        $insLogo = $dataIns->logo;
        $insLimit = $dataIns->max_plafond;
        $adminFee = $dataIns->admin_fee;

        $data['debitur'] = $dataDebitur;
        $data['fullname_asuransi'] = $insFullname;
        $data['logo_asuransi'] = $insLogo;
        $data['limit_asuransi'] = $insLimit;
        $data['fee_asuransi'] = $adminFee;
        $data['namaProduk'] = $namaProduk;
        $data['namaPekerjaan'] = $namaPekerjaan;
        $data['namaManfaat'] = $namaManfaat;

        $viewStr = $this->load->view('files/cn-page1', $data, TRUE); 
        $viewStr2 = $this->load->view('files/cn-page2', $data, TRUE);
        // var_dump($insLogo);exit();
        // $params = array('no_aplikasi' => $$dataDebitur->no_aplikasi, 'reffnum' => $$dataDebitur->reffnumber);
        $filename = 'KPA_'.$dataDebitur->no_aplikasi.'_'.$dataDebitur->reffnumber.'_CBC';
        try {
            $mpdf = new \Mpdf\Mpdf([
                'format' => 'A4-P',
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 5,
                'margin_bottom' => 0,
                'margin_header' => 0,
                'margin_footer' => 0,
            ]);
            $mpdf->simpleTables = true;
            $mpdf->packTableData = true;
            $mpdf->showImageErrors = true;
            $mpdf->SetProtection(array(), 'UserPassword', 'R4h4s14_');
            $mpdf->SetProtection(array('modify','fill-forms'), '', 'R4h4s14_');
            
            $mpdf->WriteHTML($viewStr,\Mpdf\HTMLParserMode::DEFAULT_MODE);
            $mpdf->AddPage();
            $mpdf->WriteHTML($viewStr2,\Mpdf\HTMLParserMode::DEFAULT_MODE);
            $mpdf->debug = true; 
            // $mpdf->Output(); 
            
            // echo $viewStr;
            // echo"</br>";
            // echo $viewStr2;

            $mpdf->Output();
            
            
        } catch (Exception $e) {
            $escaped_str = str_replace("'", "''", $this->db->last_query());
            $msgError = "DB transaction failed. Error no : ".$e->getCode()." Error msg : ".$e->getMessage()." On Line : ".$e->getLine()." Last Query : ".$escaped_str;
            
            $error = array("Result"=>array('status_code' => '10', "message"=>$msgError));

            $this->writeLog('CN-REQUEST','ERROR',TR_CN_HIS,json_encode($params),NULL,json_encode($error));
            
            return 'GAGAL';
        }
		
    }
}