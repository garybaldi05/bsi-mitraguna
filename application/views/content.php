
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid" ng-controller="BerandaController">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <!--begin::Toolbar container-->
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <!--begin::Page title-->
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Dashboard</h1>
                    <!--end::Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="index.html" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">Dashboards</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
             </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
            <div class="row g-5 g-xl-10">
            <!--begin::Col-->
            <div class="col-xl-6 mb-xl-10">
                <!--begin::Slider Widget 1-->
                <div id="kt_sliders_widget_1_slider" class="card card-flush carousel carousel-custom carousel-stretch slide h-xl-100" data-bs-ride="carousel" data-bs-interval="5000">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h4 class="card-title d-flex align-items-start flex-column">
                            <span class="card-label fw-bold text-gray-800">Data Produksi</span>
                            <!-- <span class="text-gray-600 mt-1 fw-bold fs-7">Debitur Free Cover (CAC)</span> -->
                        </h4>
                        <!--end::Title-->
                        <!--begin::Toolbar-->
                        <div class="card-toolbar">
                            <!--begin::Carousel Indicators-->
                            <ol class="p-0 m-0 carousel-indicators carousel-indicators-bullet carousel-indicators-active-primary">
                            <?php for ($i=0; $i < count($penutupan_slide); $i++) { 
                                $active = '';  
                                $current = '';
                                $a = 0;
                                if($i < 1){
                                    $active = 'active';
                                    $current = 'true';
                                }
                            ?>
                                <li data-bs-target="#kt_sliders_widget_1_slider" data-bs-slide-to="<?=$i?>" class="ms-1 <?=$active?>" aria-current="<?=$current?>"></li>
                            <?php 
                                $a++; 
                            } ?>
                            </ol>
                            <!--end::Carousel Indicators-->
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-6">
                        <!--begin::Carousel-->
                        <div class="carousel-inner mt-n5">
                            <!--begin::Item-->
                        <?php
                            for ($i=0; $i < count($penutupan_slide); $i++) { 
                                $active = '';  
                                
                                if($i < 1){
                                    $active = 'active';
                                }
                            
                        ?>
                            <div class="carousel-item show <?=$active?>">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center mb-5 mt-5">
                                    <!--begin::Chart-->
                                    <div class="w-80px flex-shrink-0 me-2">
                                    <img src="<?=base_url()?>/upload/logo/bsi-footer-small.png" class="w-70px app-sidebar-logo-default">
                                    </div>
                                    <!--end::Chart-->
                                    <!--begin::Info-->
                                    <div class="m-0">
                                        <!--begin::Subtitle-->
                                        <h4 class="fw-bold text-gray-800 mb-3"><?=$penutupan_slide[$i]['title']?></h4>
                                        <!--end::Subtitle-->
                                        <!--begin::Items-->
                                        <div class="d-flex d-grid gap-5">
                                            <!--begin::Item-->
                                            <div class="d-flex flex-column flex-shrink-0 me-4">
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center fs-6 fw-bold text-bsi-green mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon--><?= number_format($penutupan_slide[$i]['val1'],0) ?> Debitur</span>
                                                <!--end::Section-->
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center text-bsi-green fw-bold fs-6 mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. <?= number_format($penutupan_slide[$i]['val2'],0) ?> Total Plafond</span>
                                                <!--end::Section-->
                                                
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center fs-6 fw-bold text-bsi-green mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. <?= number_format($penutupan_slide[$i]['val3'],0) ?> Total Premi</span>
                                                <!--end::Section-->

                                            </div>
                                            <!--end::Item-->
                                            <!-- begin::Item-->
                                            <!-- <div class="d-flex flex-column flex-shrink-0">
                                                
                                            </div> -->
                                            <!--end::Item -->
                                        </div>
                                        <!--end::Items-->
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Action-->
                                <div class="mb-1">
                                    <!-- <a href="#" class="btn btn-sm btn-light me-2">Skip This</a>
                                    <a href="#" class="btn btn-sm btn-primary" >View Data</a> -->
                                </div>
                                <!--end::Action-->
                            </div>
                            <?php } ?>
                            <!--end::Item-->
                        </div>
                        <!--end::Carousel-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Slider Widget 1-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-xl-6 mb-5 mb-xl-10">
                <!--begin::Slider Widget 2-->
                <div id="kt_sliders_widget_2_slider" class="card card-flush carousel carousel-custom carousel-stretch slide h-xl-100" data-bs-ride="carousel" data-bs-interval="5500">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h4 class="card-title d-flex align-items-start flex-column">
                            <span class="card-label fw-bold text-gray-800">Data Klaim</span>
                            <!-- <span class="text-gray-400 mt-1 fw-bold fs-7">24 events on all activities</span> -->
                        </h4>
                        <!--end::Title-->
                        <!--begin::Toolbar-->
                        <div class="card-toolbar">
                            <!--begin::Carousel Indicators-->
                            <ol class="p-0 m-0 carousel-indicators carousel-indicators-bullet carousel-indicators-active-success">
                                <li data-bs-target="#kt_sliders_widget_2_slider" data-bs-slide-to="0" class="ms-1 active" aria-current="true"></li>
                                <li data-bs-target="#kt_sliders_widget_2_slider" data-bs-slide-to="1" class="ms-1"></li>
                                <li data-bs-target="#kt_sliders_widget_2_slider" data-bs-slide-to="2" class="ms-1"></li>
                            </ol>
                            <!--end::Carousel Indicators-->
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-6">
                        <!--begin::Carousel-->
                        <div class="carousel-inner">
                            <!--begin::Item-->
                            <div class="carousel-item show active">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center mb-9">
                                    <!--begin::Symbol-->
                                    <div class="w-80px flex-shrink-0 me-2">
                                    <img src="<?=base_url()?>/upload/logo/bsi-footer-small.png" class="w-70px app-sidebar-logo-default">
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Info-->
                                    <div class="m-0">
                                        <!--begin::Subtitle-->
                                        <h4 class="fw-bold text-gray-800 mb-3">Total Klaim Diajukan</h4>
                                        <!--end::Subtitle-->
                                        <!--begin::Items-->
                                        <div class="d-flex d-grid gap-5">
                                            <!--begin::Item-->
                                            <div class="d-flex flex-column flex-shrink-0 me-4">
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center fs-6 fw-bold text-primary mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->22 Debitur</span>
                                                <!--end::Section-->
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center text-primary fw-bold fs-6 mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. 20.000.000 Total Nilai Klaim </span>
                                                <!--end::Section-->

                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center text-primary fw-bold fs-6">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. 16.000.000 Total Hak Klaim</span>
                                                <!--end::Section-->

                                            </div>
                                            <!--end::Item-->
                                        </div>
                                        <!--end::Items-->
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Action-->
                                <div class="mb-1">
                                    <!-- <a href="#" class="btn btn-sm btn-light me-2">Details</a>
                                    <a href="#" class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#kt_modal_create_campaign">Join Event</a> -->
                                </div>
                                <!--end::Action-->
                            </div>
                            <!--end::Item-->
                            <!--begin::Item-->
                            <div class="carousel-item">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center mb-9">
                                    <!--begin::Symbol-->
                                    <div class="w-80px flex-shrink-0 me-2">
                                    <img src="<?=base_url()?>/upload/logo/bsi-footer-small.png" class="w-70px app-sidebar-logo-default">
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Info-->
                                    <div class="m-0">
                                        <!--begin::Subtitle-->
                                        <h4 class="fw-bold text-gray-800 mb-3">Total OS Klaim</h4>
                                        <!--end::Subtitle-->
                                        <!--begin::Items-->
                                        <div class="d-flex d-grid gap-5">
                                            <!--begin::Item-->
                                            <div class="d-flex flex-column flex-shrink-0 me-4">
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center fs-6 fw-bold text-primary mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->5 Debitur</span>
                                                <!--end::Section-->
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center text-primary fw-bold fs-6 mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. 7.000.000 Total Nilai Klaim </span>
                                                <!--end::Section-->

                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center text-primary fw-bold fs-6">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. 5.600.000 Total Hak Klaim</span>
                                                <!--end::Section-->

                                            </div>
                                            <!--end::Item-->
                                        </div>
                                        <!--end::Items-->
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Action-->
                                <div class="mb-1">
                                    <!-- <a href="#" class="btn btn-sm btn-light me-2">Details</a>
                                    <a href="#" class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#kt_modal_create_campaign">Join Event</a> -->
                                </div>
                                <!--end::Action-->
                            </div>
                            <!--end::Item-->
                            <!--begin::Item-->
                            <div class="carousel-item">
                                <!--begin::Wrapper-->
                                <div class="d-flex align-items-center mb-9">
                                    <!--begin::Symbol-->
                                    <div class="w-80px flex-shrink-0 me-2">
                                    <img src="<?=base_url()?>/upload/logo/bsi-footer-small.png" class="w-70px app-sidebar-logo-default">
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Info-->
                                    <div class="m-0">
                                        <!--begin::Subtitle-->
                                        <h4 class="fw-bold text-gray-800 mb-3">Total Klaim Dibayar</h4>
                                        <!--end::Subtitle-->
                                        <!--begin::Items-->
                                        <div class="d-flex d-grid gap-5">
                                            <!--begin::Item-->
                                            <div class="d-flex flex-column flex-shrink-0 me-4">
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center fs-6 fw-bold text-primary mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->17 Debitur</span>
                                                <!--end::Section-->
                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center text-primary fw-bold fs-6 mb-2">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. 13.000.000 Total Nilai Klaim </span>
                                                <!--end::Section-->

                                                <!--begin::Section-->
                                                <span class="d-flex align-items-center text-primary fw-bold fs-6">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen057.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="currentColor"></rect>
                                                        <path d="M11.9343 12.5657L9.53696 14.963C9.22669 15.2733 9.18488 15.7619 9.43792 16.1204C9.7616 16.5789 10.4211 16.6334 10.8156 16.2342L14.3054 12.7029C14.6903 12.3134 14.6903 11.6866 14.3054 11.2971L10.8156 7.76582C10.4211 7.3666 9.7616 7.42107 9.43792 7.87962C9.18488 8.23809 9.22669 8.72669 9.53696 9.03696L11.9343 11.4343C12.2467 11.7467 12.2467 12.2533 11.9343 12.5657Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->Rp. 11.400.000 Total Hak Klaim</span>
                                                <!--end::Section-->

                                            </div>
                                            <!--end::Item-->
                                        </div>
                                        <!--end::Items-->
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Action-->
                                <div class="mb-1">
                                    <!-- <a href="#" class="btn btn-sm btn-light me-2">Details</a>
                                    <a href="#" class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#kt_modal_create_campaign">Join Event</a> -->
                                </div>
                                <!--end::Action-->
                            </div>
                            <!--end::Item-->
                        </div>
                        <!--end::Carousel-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Slider Widget 2-->
            </div>
            <!--end::Col-->
        <div class="row g-5 g-xl-10">
            <div class="col-xl-12 mb-xl-10">
                <div class="card card-flush mb-xxl-10">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold text-dark">Data Stop Loss Berdasarkan Asuransi</span>
                        </h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin::Nav-->
                        <ul class="nav nav-pills nav-pills-custom mb-3" role="tablist">
                            <!--begin::Item-->
                            
                            <?php
                                    $queryIns = "SELECT * FROM tm_asuransi ORDER BY nama ASC";
                                    $dataIns = $this->db->query($queryIns)->result_array();
                            ?>

                            <?php  
                            for ($i=0; $i < count($dataIns) ; $i++) { 
                                $active = "";
                                if($i == '0'){
                                    $active = 'active';
                                }else{
                                    $active = '';
                                }
                            ?>
                            <li class="nav-item mb-3 me-3 me-lg-6" role="presentation">
                                <a class="nav-link d-flex justify-content-between flex-column flex-center overflow-hidden w-175px h-85px py-4 <?=$active?>" aria-selected="true" data-bs-toggle="pill" href="#kt_stats_widget_2_1_tab_<?=$i?>" role="tab" tabindex="-1">
                                    <!--begin::Icon-->
                                    <div class="nav-icon">
                                        <img alt="" src="<?=base_url()?>upload/logo/<?=$dataIns[$i]['logo']?>" class="">
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Subtitle-->
                                    <span class="nav-text text-gray-700 fw-bold fs-6 lh-1"><?=$dataIns[$i]['nama']?></span>
                                    <!--end::Subtitle-->
                                    <!--begin::Bullet-->
                                    <span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-primary"></span>
                                    <!--end::Bullet-->
                                </a>
                                <!--end::Link-->
                            </li>
                            <?php ; } ?>
                            <!--end::Item-->
                        </ul>
                        <!--end::Nav-->
                        <!--begin::Tab Content-->
                        <div class="tab-content">
                            <!--begin::Tap pane-->
                            <?php  
                            for ($i=0; $i < count($dataIns) ; $i++) { 
                                $active = "";
                                if($i == '0'){
                                    $active = 'active show';
                                }else{
                                    $active = '';
                                }
                                $idins = $dataIns[$i]['c_id'];
                                $totalpremi = 0;
                                $totalos = 0;
                            ?>

                            <div class="tab-pane fade <?=$active?>" id="kt_stats_widget_2_1_tab_<?=$i?>" role="tabpanel">
                                <!--begin::Table container-->
                                <div class="table-responsive">
                                    <!--begin::Table-->
                                    <table class="table align-middle gs-0 gy-4 my-0">
                                        <!--begin::Table head-->
                                        <thead>
                                            <tr class="fs-7 fw-bold text-gray-500">
                                                <th class="p-0 min-w-150px pt-3">Produk</th>
                                                <th class="p-0 min-w-150px pt-3">Gross kontribusi / Premi</th>
                                                <th class="p-0 min-w-150px pt-3">Claim Paid</th>
                                                <th class="p-0 min-w-150px pt-3">Rasio Claim</th>
                                            </tr>
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody>
                                            <?php
                                                 $query = "SELECT 
                                                 'MITRAGUNA' as produk,
                                                 (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '3' AND id_asuransi = '$idins') as premi,
                                                 0 AS claim_paid
                                                 FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '3' GROUP BY produk ";
                                                 
                                                 $dataMit = $this->db->query($query)->row();
                                                 
                                            ?>
                                            <?php if(!empty($dataMit)){
                                                $percentage = 0;
                                                if($dataMit->premi == '0' || $dataMit->claim_paid == '0'){
                                                   
                                                }else{
                                                    $percentage = $dataMit->premi / $dataMit->claim_paid;
                                                    
                                                }

                                                $totalpremi = $totalpremi + $dataMit->premi;
                                                $totalos = $totalos + $dataMit->claim_paid;
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"><?=$dataMit->produk?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->premi,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->claim_paid,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> <?=$percentage?> %</span>
                                                </td>
                                                
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6">MITRAGUNA</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> 0 %</span>
                                                </td>
                                                
                                            </tr>
                                            <?php } ?>

                                            <?php
                                                 $query = "SELECT 
                                                 'PRA PENSIUN' as produk,
                                                 (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '4' AND id_asuransi = '$idins') as premi,
                                                 0 AS claim_paid
                                                 FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '4' GROUP BY produk ";
                                                 
                                                 $dataMit = $this->db->query($query)->row();

                                                
                                            ?>
                                            <?php if(!empty($dataMit)){
                                                $percentage = 0;
                                                if($dataMit->premi > 0 || $dataMit->claim_paid > 0){
                                                   $percentage = $dataMit->premi / $dataMit->claim_paid;
                                                   
                                                }    
                                                $totalpremi = $totalpremi + $dataMit->premi;
                                                $totalos = $totalos + $dataMit->claim_paid;
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"><?=$dataMit->produk?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->premi,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->claim_paid,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> <?=$percentage?> %</span>
                                                </td>
                                                
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6">PRA PENSIUN</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> 0 %</span>
                                                </td>
                                                
                                            </tr>
                                            <?php } ?>

                                            <?php
                                                 $query = "SELECT 
                                                 'PENSIUN' as produk,
                                                 (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '2' AND id_asuransi = '$idins') as premi,
                                                 0 AS claim_paid
                                                 FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '2' GROUP BY produk ";
                                                 
                                                 $dataMit = $this->db->query($query)->row();

                                                 
                                            ?>
                                            <?php if(!empty($dataMit)){
                                                $percentage = 0;
                                                if($dataMit->premi > 0 || $dataMit->claim_paid > 0){
                                                   $percentage = $dataMit->premi / $dataMit->claim_paid;
                                                    
                                                }  
                                                $totalpremi = $totalpremi + $dataMit->premi;
                                                    $totalos = $totalos + $dataMit->claim_paid;  
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"><?=$dataMit->produk?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->premi,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->claim_paid,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> <?=$percentage?> %</span>
                                                </td>
                                                
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6">PENSIUN</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> 0 %</span>
                                                </td>
                                                
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6"> GRAND TOTAL</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6"> Rp. <?=number_format($totalpremi,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6"> Rp. <?=number_format($totalos,0,',','.')?></span>
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <!--end::Table body-->
                                    </table>
                                    <!--end::Table-->
                                </div>
                                <!--end::Table container-->
                            </div>
                        <?php } ?>
                            <!--end::Tap pane-->
                        </div>
                        <!--end::Tab Content-->
                    </div>
                    <!--end: Card Body-->
                </div>
            </div>

          </div>

          <div class="row g-5 g-xl-10">
            <div class="col-xl-12 mb-xl-10">
                <div class="card card-flush mb-xxl-10">
                    <!--begin::Header-->
                    <div class="card-header pt-5">
                        <!--begin::Title-->
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bold text-dark">Data Share Produk Berdasarkan Asuransi</span>
                        </h3>
                        <!--end::Title-->
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin::Nav-->
                        <ul class="nav nav-pills nav-pills-custom mb-3" role="tablist">
                            <!--begin::Item-->
                            
                            <?php
                                    $queryIns = "SELECT * FROM tm_asuransi ORDER BY nama ASC";
                                    $dataIns = $this->db->query($queryIns)->result_array();
                            ?>

                            <?php  
                            for ($i=0; $i < count($dataIns) ; $i++) { 
                                $active = "";
                                if($i == '0'){
                                    $active = 'active';
                                }else{
                                    $active = '';
                                }
                            ?>
                            <li class="nav-item mb-3 me-3 me-lg-6" role="presentation">
                                <a class="nav-link d-flex justify-content-between flex-column flex-center overflow-hidden w-175px h-85px py-4 <?=$active?>" aria-selected="true" data-bs-toggle="pill" href="#kt_stats_widget_3_1_tab_<?=$i?>" role="tab" tabindex="-1">
                                    <!--begin::Icon-->
                                    <div class="nav-icon">
                                        <img alt="" src="<?=base_url()?>upload/logo/<?=$dataIns[$i]['logo']?>" class="">
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Subtitle-->
                                    <span class="nav-text text-gray-700 fw-bold fs-6 lh-1"><?=$dataIns[$i]['nama']?></span>
                                    <!--end::Subtitle-->
                                    <!--begin::Bullet-->
                                    <span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-primary"></span>
                                    <!--end::Bullet-->
                                </a>
                                <!--end::Link-->
                            </li>
                            <?php ; } ?>
                            <!--end::Item-->
                        </ul>
                        <!--end::Nav-->
                        <!--begin::Tab Content-->
                        <div class="tab-content">
                            <!--begin::Tap pane-->
                            <?php  
                            for ($i=0; $i < count($dataIns) ; $i++) { 
                                $active = "";
                                if($i == '0'){
                                    $active = 'active show';
                                }else{
                                    $active = '';
                                }
                                $idins = $dataIns[$i]['c_id'];
                                $totalpremi = 0;
                                $totalos = 0;
                            ?>

                            <div class="tab-pane fade <?=$active?>" id="kt_stats_widget_3_1_tab_<?=$i?>" role="tabpanel">
                                <!--begin::Table container-->
                                <div class="table-responsive">
                                    <!--begin::Table-->
                                    <table class="table align-middle gs-0 gy-4 my-0">
                                        <!--begin::Table head-->
                                        <thead>
                                            <tr class="fs-7 fw-bold text-gray-500">
                                                <th class="p-0 min-w-150px pt-3">Produk</th>
                                                <th class="p-0 min-w-150px pt-3">Gross kontribusi / Premi</th>
                                                <th class="p-0 min-w-150px pt-3">Status Share Produk</th>
                                            </tr>
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody>
                                            <?php
                                                 $query = "SELECT 
                                                 'MITRAGUNA' as produk,
                                                 (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '3' AND id_asuransi = '$idins') as premi,
                                                 0 AS claim_paid
                                                 FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '3' GROUP BY produk ";
                                                 
                                                 $dataMit = $this->db->query($query)->row();
                                                 
                                            ?>
                                            <?php if(!empty($dataMit)){
                                                $percentage = 0;
                                                if($dataMit->premi == '0' || $dataMit->claim_paid == '0'){
                                                   
                                                }else{
                                                    $percentage = $dataMit->premi / $dataMit->claim_paid;
                                                    
                                                }

                                                $totalpremi = $totalpremi + $dataMit->premi;
                                                $totalos = $totalos + $dataMit->claim_paid;
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"><?=$dataMit->produk?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->premi,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-white mb-1 fs-7 badge badge-success"> Dalam batas aman </span>
                                                </td>
                                               
                                                
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6">MITRAGUNA</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-white mb-1 fs-7 badge badge-success"> Dalam batas aman </span>
                                                </td>
                                                
                                            </tr>
                                            <?php } ?>

                                            <?php
                                                 $query = "SELECT 
                                                 'PRA PENSIUN' as produk,
                                                 (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '4' AND id_asuransi = '$idins') as premi,
                                                 0 AS claim_paid
                                                 FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '4' GROUP BY produk ";
                                                 
                                                 $dataMit = $this->db->query($query)->row();

                                                
                                            ?>
                                            <?php if(!empty($dataMit)){
                                                $percentage = 0;
                                                if($dataMit->premi > 0 || $dataMit->claim_paid > 0){
                                                   $percentage = $dataMit->premi / $dataMit->claim_paid;
                                                   
                                                }    
                                                $totalpremi = $totalpremi + $dataMit->premi;
                                                $totalos = $totalos + $dataMit->claim_paid;
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"><?=$dataMit->produk?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->premi,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-white mb-1 fs-7 badge badge-success"> Dalam batas aman </span>
                                                </td>
                                                
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6">PRA PENSIUN</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-white mb-1 fs-7 badge badge-success"> Dalam batas aman </span>
                                                </td>
                                                
                                            </tr>
                                            <?php } ?>

                                            <?php
                                                 $query = "SELECT 
                                                 'PENSIUN' as produk,
                                                 (SELECT ISNULL(SUM(premi_before_admin),0) FROM tm_penutupan WHERE produk = '2' AND id_asuransi = '$idins') as premi,
                                                 0 AS claim_paid
                                                 FROM tm_penutupan WHERE id_asuransi = '$idins' AND produk = '2' GROUP BY produk ";
                                                 
                                                 $dataMit = $this->db->query($query)->row();

                                                 
                                            ?>
                                            <?php if(!empty($dataMit)){
                                                $percentage = 0;
                                                if($dataMit->premi > 0 || $dataMit->claim_paid > 0){
                                                   $percentage = $dataMit->premi / $dataMit->claim_paid;
                                                    
                                                }  
                                                $totalpremi = $totalpremi + $dataMit->premi;
                                                    $totalos = $totalos + $dataMit->claim_paid;  
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"><?=$dataMit->produk?></span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. <?=number_format($dataMit->premi,0,',','.')?></span>
                                                </td>
                                                <td>
                                                    <span class="text-white mb-1 fs-7 badge badge-success"> Dalam batas aman </span>
                                                </td>
                                                
                                            </tr>
                                            <?php }else{ ?>
                                                <tr>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6">PENSIUN</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800  text-hover-primary mb-1 fs-6"> Rp. 0</span>
                                                </td>
                                                <td>
                                                    <span class="text-white mb-1 fs-7 badge badge-success"> Dalam batas aman </span>
                                                </td>
                                                
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    <span class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6"> GRAND TOTAL</span>
                                                </td>
                                                <td>
                                                    <span class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6"> Rp. <?=number_format($totalpremi,0,',','.')?></span>
                                                </td>
                                               
                                            </tr>
                                        </tfoot>
                                        <!--end::Table body-->
                                    </table>
                                    <!--end::Table-->
                                </div>
                                <!--end::Table container-->
                            </div>
                        <?php } ?>
                            <!--end::Tap pane-->
                        </div>
                        <!--end::Tab Content-->
                    </div>
                    <!--end: Card Body-->
                </div>
            </div>

          </div>
        </div>
             </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Content wrapper-->
    <script src="<?=base_url()?>app/controllers/BerandaController.js?v=1.0" type="text/javascript"></script>