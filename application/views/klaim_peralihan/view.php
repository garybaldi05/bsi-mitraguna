<?php 
    $title = ucfirst($this->uri->segment(1, 0)); 
    $title2 = ucfirst($this->uri->segment(2, 0)); 
    $no_app = ($_GET['data']); 
?>
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url()?>" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url($title)?>" class="text-muted text-hover-primary">
                            <?=$title?>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted"><?=$title2?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0"><?=$title2?> Data <?=$title?></h1>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                <!--begin::Search-->
                    <div class="col-md-12 mb-10">
                        <div class="form-group row">
                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No Aplikasi : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="No Aplikasi"  id="no-aplikasi" value="<?=$editData->no_aplikasi?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. Reff : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="No. Reff"  id="no-reff" value="<?=$editData->reffnumber?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. LD : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="No. LD"  id="no-ld" value="<?=$editData->no_ld?>" />
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nama : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Nama"  id="nama" value="<?=$editData->nama?>"/>
                                    </div>
                                </div>
                             </div>
                            
                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">KTP : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="KTP"  id="ktp" value="<?=$editData->ktp?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Npwp : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Npwp"  id="npwp" value="<?=$editData->npwp?>" />
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Kelamin : </label>
                                <div class="col-6 mb-5">
                                    <?php
                                        $l = '';
                                        $p = '';
                                        if(strtoupper($editData->jenis_kelamin) == 'L' || strtoupper($editData->jenis_kelamin) == 'LAKI - LAKI' ){
                                            $l = 'checked';
                                        }else{
                                            $p = 'checked';
                                        }
                                    ?>
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" type="radio" value="L" id="jk-l" <?=$l?>/>
                                        <label class="form-check-label" for="jk-l">
                                            Laki - Laki
                                        </label>
                                    </div>
                                </div>

                                <div class="col-6 mb-5">
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" type="radio" value="P" id="jk-p" <?=$p?>/>
                                        <label class="form-check-label" for="jk-p">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Produk : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <?php
                                            $produk = $this->db->query("SELECT nama_produk FROM tm_product_list WHERE c_id = '$editData->produk' ")
                                                      ->row()->nama_produk;
                                        ?>
                                        <input class="form-control form-control-solid"  readonly placeholder="Produk" id="produk" value="<?=$produk?>" />
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Agunan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Jenis Agunan" id="jenis-agunan" value="<?=$editData->jenis_agunan?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. Agunan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="No. Agunan" id="no-agunan" value="<?=$editData->nomor_agunan?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Akad : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $jenis_akad = $this->db->query("SELECT nama FROM tm_jenis_akad WHERE c_id = '$editData->jenis_akad' ")
                                                      ->row()->nama;
                                        ?>
                                        <input class="form-control form-control-solid"  readonly placeholder="Jenis. Akad" id="jenis-akad" value="<?=$jenis_akad?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. Akad : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="No. Akad" id="no-akad" value="<?=$editData->nomor_akad?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Akad : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly  placeholder="Tanggal Akad" id="tanggal-akad" value="<?=$editData->tanggal_akad?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Bayar IJK : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly  placeholder="Tanggal Bayar IJK" id="tanggal-bayar-ijk" value="<?=$editData->tanggal_bayar_ijk?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Asuransi : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $asuransi = $this->db->query("SELECT nama FROM tm_asuransi WHERE c_id = '$editData->id_asuransi' ")
                                                      ->row()->nama;
                                        ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Asuransi" disabled>
                                        <option value="<?=$editData->id_asuransi?>"><?=$asuransi?></option>
                                    </select>

                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Cabang : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Cabang" disabled>
                                        <option value="<?=$editData->nama_cabang?>"><?=$editData->nama_cabang?></option>
                                    </select>

                                    </div>
                                </div>
                             </div>

                            <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Mulai : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Tanggal Mulai" id="tanggal-mulai" value="<?=$editData->tanggal_mulai?>"/>
                                    </div>
                                </div>
                             </div>
                             

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Selesai : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Tanggal Mulai" id="tanggal-selesai" value="<?=$editData->tanggal_akhir?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Usia Saat Akad : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Usia Saat Akad" id="usia-mulai" value="<?=$editData->usia_saat_akad?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Usia Selesai Akad : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Usia Selesai Akad" id="usia-akhir" value="<?=$editData->usia_selesai_akad?>"/>
                                    </div>
                                </div>
                             </div>

                            <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tenor : </label>
                                <div class="col-md-3 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid " placeholder="Tenor" id="tenor" value="<?=$editData->tenor?>"/>
                                    </div>
                                </div>
                                <div class="col-md-9 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Tenor" id="tenor" value="<?=$editData->bulan_tenor?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nilai Agunan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Nilai Agunan" id="nilai-agunan" value="<?=$editData->nilai_agunan?>" />
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Plafond : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Plafond" id="plafond" value="<?=number_format($editData->plafond,0)?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nilai Pengajuan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Nilai Pengajuan" id="nilai-pengajuan" value="<?= number_format($editData->nilai_pengajuan,0)?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Fee Mitra : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Fee Mitra" id="fee-mitra" value="<?= number_format($editData->coverage_fee_mitra,0)?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Fee Asuransi : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Fee asuransi" id="fee-asuransi" value="<?= number_format($editData->coverage_asuransi,0)?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Fee Pinalti : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Fee Pinalti" id="fee-pinalti" value="<?= number_format($editData->coverage_pinalti,0)?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Rate : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Rate" id="fixed-rate" value="<?= $editData->fixed_rate.' %'?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Premi Before Admin : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Premi" id="premi" value="<?= number_format($editData->premi_before_admin,0)?>"/>
                                    </div>
                                </div>
                             </div>
                             
                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Premi Final : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  readonly placeholder="Premi" id="premi" value="<?= number_format($editData->premi,0)?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="form-group col-md-6 row mb-5 input-group-solid">
                                <label class="form-label fs-5">Status Klaim : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <select class="form-select " data-control="select2" data-placeholder="Select an option">
                                        <option></option>
                                        <option value="1">Unclaim</option>
                                        <option value="2">Claim</option>
                                    </select>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                        <div class="d-flex flex-row mt-5">
                            <button href="#" class="btn btn-sm btn-flex btn-primary fw-bold hover-animate" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                            <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                <i class="bi bi-save fs-5"></i>
                            </span>
                            <!--end::Svg Icon-->Submit</button>

                            <a href="<?=base_url().$title.'/history?data='.$no_app?>" class="btn btn-sm btn-flex btn-info fw-bold hover-animate ms-sm-1">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                            <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                <i class="bi bi-clock-history"></i>
                            </span>
                            <!--end::Svg Icon-->Data History</a>

                            <a href="<?=base_url().$title.'/downloadCnNew?data='.$no_app?>" class="btn btn-sm btn-flex btn-success text-dark fw-bold hover-animate ms-sm-1">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                            <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                <i class="bi bi-download text-dark"></i>
                            </span>
                            <!--end::Svg Icon-->Download CN</a>
                        </div>

                    </div>
                <!--end::Search-->

                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->

</div>