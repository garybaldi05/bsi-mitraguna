<?php 
    $title = ucfirst($this->uri->segment(1, 0)); 
    $title2 = ucfirst($this->uri->segment(2, 0)); 
    $no_app = ($_GET['data']); 
    $c_id = base64_encode($oneData->c_id); 
?>
<div class="d-flex flex-column flex-column-fluid" ng-controller="PenutupanCbcController">
<input type="hidden" id="no_app" value="<?=$no_app?>" />
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url()?>" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url($title)?>" class="text-muted text-hover-primary">
                            <?=$title?>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted"><?=$title2?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <i class="bi bi-info-circle-fill fw-bold fs-2 text-dark"></i>

                            <h1 class="ms-2 page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0"><?=$title2?> Data No Aplikasi : <?=base64_decode($_GET['data']);?></h1>
                           
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <div class="col-md-12 mb-xl-10 ">
                        <div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="history_table">
                                <!--begin::Table head-->
                                <thead>
                                    <!--begin::Table row-->
                                    <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                        <th class="text-center min-w-200px">Download</th>
                                        <th class="text-center min-w-200px">Actions</th>
                                        <th class="min-w-70">Polis</th>
                                        <th class="min-w-70">No. Aplikasi</th>
                                        <th class="min-w-70">Reffnumber</th>
                                        <th class="min-w-150px">Nama</th>
                                        <th class="min-w-150px">Asuransi</th>
                                        <th class="min-w-150px">Cabang</th>
                                        <th class="min-w-150px">Produk</th>
                                        <th class="min-w-150px">Tipe Produk</th>
                                        <th class="min-w-150px">Atribusi</th>
                                        <th class="min-w-150px">Tenor</th>
                                        <th class="min-w-150px">Plafond</th>
                                        <th class="min-w-150px">Fee Mitra</th>
                                        <th class="min-w-150px">Fee Asuransi</th>
                                        <th class="min-w-150px">Fee Pinalti</th>
                                        <th class="min-w-150px">Premi + Admin</th>
                                        <th class="min-w-150px">Premi</th>
                                        <th class="min-w-150px">Tanggal Akad</th>
                                        <th class="min-w-150px">No. Akad</th>
                                        <th class="min-w-150px">Tanggal Lahir</th>
                                        <th class="min-w-150px">Usia Saat Akad</th>
                                        <th class="min-w-150px">Usia Selesai Akad</th>
                                    </tr>
                                    <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="text-gray-600 fw-semibold">
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        <!--end::Table-->
                        </div>
                    </div>
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->

            <div class="card mt-5">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <i class="bi bi-info-circle-fill fw-bold fs-2 text-dark"></i>

                            <h1 class="ms-2 page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0">Activity No. Aplikasi : <?=base64_decode($_GET['data']);?></h1>
                           
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <div class="card-body pt-0">
                    <div class="col-md-12 mb-xl-10 ">
                        <div class="row mb-xl-10">
                            <div class="card mb-5 col-md-6">
                                <!--begin::Header-->
                                <div class="card-header align-items-center border-0 mt-4">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="fw-bold mb-2 text-dark">Activities</span>
                                        <span class="fw-bold fs-7">Total Activity : {{ dataActivity.length }} </span>
                                    </h3>
                                    <div class="card-toolbar">
                                    </div>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="card-body pt-5" >
                                    <!--begin::Timeline-->
                                    <div class="timeline-label" ng-repeat = "o in dataActivity track by $index">
                                        <!--begin::Item-->
                                        <div class="mb-2 fs-6 fw-bold" style="z-index: 9;position: relative;">
                                            Time : {{o.datetime}}
                                        </div>
                                        <div class="timeline-item">
                                            
                                            <!--begin::Label-->
                                            <div class="timeline-label fw-bold text-gray-800 fs-6">POST</div>
                                            <!--end::Label-->
                                            <!--begin::Badge-->
                                            <div class="timeline-badge">
                                                <i class="fa fa-genderless {{o.colour}} fs-1"></i>
                                            </div>
                                            <!--end::Badge-->
                                            <!--begin::Text-->
                                            <div class="fw-mormal text-dark timeline-content ps-3 hover-animate cursor-click" 
                                                 data-bs-toggle="modal" data-bs-target="#kt_modal_scrollable_1" ng-click="modalActivity($event,o.c_id)" > 
                                                Requesting to : {{o.actionname}} , url : {{o.url}}, Status : {{o.status}} 
                                            </div>
                                            <!--end::Text-->
                                        </div>
                                        <!--end::Item-->
                                    </div>
                                    <!--end::Timeline-->
                                </div>
                                <!--end: Card Body-->
                            </div>

                            <div class="card mb-5 col-md-6" id="kt_profile_details_view">
                                <!--begin::Card header-->
                                <div class="card-header cursor-pointer">
                                    <!--begin::Card title-->
                                    <div class="card-title m-0">
                                        <h3 class="fw-bold m-0">Debitur Data</h3>
                                    </div>
                                    <!--end::Card title-->
                                    
                                </div>
                                <!--begin::Card header-->
                                <!--begin::Card body-->
                                <div class="card-body p-9">
                                    <!--begin::Row-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold text-muted">No. Aplikasi</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800"><?=$oneData->no_aplikasi?></span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold text-muted">Nomor Akad</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800"><?=$oneData->nomor_akad?></span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold text-muted">Nama</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800"><?=$oneData->nama?></span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Input group-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold text-muted">KTP</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8 fv-row">
                                            <span class="fw-semibold text-gray-800 fs-6"><?=$oneData->ktp?></span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold text-muted">NPWP</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8 fv-row">
                                            <span class="fw-semibold text-gray-800 fs-6"><?=$oneData->npwp?></span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->
                                    
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold text-muted">Tanggal Lahir</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <a href="#" class="fw-semibold fs-6 text-gray-800 text-hover-primary"><?=date('d M Y',strtotime($oneData->tanggal_lahir))?></a>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->
                                   
                                    
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <div class="d-flex flex-row mt-5">

                             <a href="<?=base_url().$title.'/view?data='.$c_id.'&type=2'?>" class="btn btn-light-primary hover-animate ms-sm-1">
                            <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                <i class="bi bi-arrow-left"></i>
                            </span> Back</a>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->

</div>

<div class="modal fade" tabindex="-1" id="kt_modal_scrollable_1">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 1000px;right: 200px;">
            <div class="modal-header">
                <h5 class="modal-title">API Activity</h5>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-2x"></span>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
             <div class="row">
                <div class="col-md-6">
                    <h4>Request : </h4>
                    <pre id="request-activity"></pre>
                </div>

                <div class="col-md-6">
                    <h4>Response : </h4>
                    <pre id="response-activity"></pre>
                </div>
             </div>
            </div>  
        </div>
    </div>
</div>

<script src="<?=base_url()?>app/controllers/PenutupanCbcController.js?v=1.0.1" type="text/javascript"></script>