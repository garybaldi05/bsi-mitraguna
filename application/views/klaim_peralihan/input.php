<?php 
    $title = ucfirst($this->uri->segment(1, 0)); 
    $title2 = ucfirst($this->uri->segment(2, 0)); 
    $title = str_replace('_',' ',$title);
?>
<div class="d-flex flex-column flex-column-fluid" ng-controller="KlaimPeralihanController">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url()?>" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url($title)?>" class="text-muted text-hover-primary">
                            <?=$title?>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted"><?=$title2?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card body-->
            <div class="card-body">
                <!--begin::Stepper-->
                <div class="stepper stepper-links d-flex flex-column pt-15" id="kt_create_account_stepper" data-kt-stepper="true">
                    <!--begin::Nav-->
                    <div class="stepper-nav mb-5">
                        <!--begin::Step 1-->
                        <div class="stepper-item current" data-kt-stepper-element="nav">
                            <h3 class="stepper-title">Data Debitur</h3>
                        </div>
                        <!--end::Step 1-->
                        <!--begin::Step 2-->
                        <div class="stepper-item" data-kt-stepper-element="nav">
                            <h3 class="stepper-title">Upload Dokumen</h3>
                        </div>
                        <!--end::Step 2-->
                        <!--begin::Step 3-->
                        <div class="stepper-item" data-kt-stepper-element="nav">
                            <h3 class="stepper-title">Approval LD</h3>
                        </div>
                        <!--end::Step 3-->
                        <!--begin::Step 4-->
                        <div class="stepper-item" data-kt-stepper-element="nav">
                            <h3 class="stepper-title">Pembayaran Klaim</h3>
                        </div>
                        <!--end::Step 4-->
                        <!--begin::Step 5-->
                        <!-- <div class="stepper-item" data-kt-stepper-element="nav">
                            <h3 class="stepper-title">Completed</h3>
                        </div> -->
                        <!--end::Step 5-->
                    </div>
                    <!--end::Nav-->
                    <form class="mx-auto mw-1000px w-100 pt-15 pb-10" novalidate="novalidate" id="kt_create_account_form">
                        <!--begin::Step 1-->
                        <div class="current" data-kt-stepper-element="content">
                            <!--begin::Wrapper-->
                            <div class="w-100">
                                <!--begin::Heading-->
                                <div class="pb-10 pb-lg-15">
                                    <!--begin::Title-->
                                    <h2 class="fw-bold d-flex align-items-center text-dark">Input Data Debitur
                                    <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" aria-label="Billing is issued based on your selected account type" data-kt-initialized="1"></i></h2>
                                    <!--end::Title-->
                                    <!--begin::Notice-->
                                    <!-- <div class="text-muted fw-semibold fs-6">If you need more info, please check out 
                                    <a href="#" class="link-primary fw-bold">Help Page</a>.</div> -->
                                    <!--end::Notice-->
                                </div>
                                <!--end::Heading-->
                                <!--begin::Input group-->
                                <div class="fv-row fv-plugins-icon-container">
                                    <!--begin::Form-->
                                    <!-- <form class="mx-auto w-100 pt-15 pb-10 fv-plugins-bootstrap5 fv-plugins-framework" novalidate="novalidate" id="debitur_form"> -->
                                        <!--begin::Row-->
                                        <div class="row">
                                            <!--begin::Col-->
                                            <div class="col-lg-6">
                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Nama Debitur <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                        <input type="hidden" name="isEdit" id = "isEdit" value="" />
                                                        <?php
                                                                $today = date('Y-m-d');
                                                                $debitur = $this->db->query("SELECT c_id,nama FROM tm_penutupan_peralihan WHERE tanggal_akhir > '$today' AND status_klaim is null ")
                                                                        ->result_array();
                                                            ?>
                                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Debitur" name="nama_debitur" id = "nama_debitur">
                                                        <option value=""></option>
                                                        <?php foreach($debitur as $value){?>
                                                            <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                                        <?php } ?>
                                                        </select>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Tanggal Kejadian/Meninggal/PHK/Macet <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data"  placeholder="Tanggal Kejadian"  id="tanggal_kejadian" name="tanggal_kejadian" value=""/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">No Polis <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Nomor Polis"  id="no_polis" name="no_polis" value=""/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Tanggal Mulai <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Tanggal Mulai"  id="tanggal_mulai" name="tanggal_mulai" value=""/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Jenis Klaim <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Klaim" name="type_manfaat" id = "type_manfaat">
                                                        <option value=""></option>
                                                        <option value="PA+ND">Meninggal Dunia</option>
                                                        <option value="PHK">PHK</option>
                                                        <option value="KPP">Kredit Macet</option>
                                                        <option value="PHK+KPP">PHK + Kredit Macet</option>
                                                        </select>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Outstanding Pokok <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data"  placeholder="Outstanding Pokok"  id="os_pokok_klaim" name="os_pokok_klaim" value=""/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Outstanding Total Klaim <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Outstanding Total Klaim"  id="os_total_klaim" name="os_total_klaim" value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="fv-plugins-message-container invalid-feedback"></div>
                                            </div>
                                            <!--end::Col-->
                                            <!--begin::Col-->
                                            <div class="col-lg-6">
                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Tanggal Lapor <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Tanggal Lapor"  id="tanggal_lapor" name="tanggal_lapor" value="<?=date('Y-m-d')?>"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Cabang <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data d-none" readonly  placeholder="Cabang"  id="kode_cabang" name="kode_cabang" value=""/>
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Cabang"  id="nama_cabang" name="nama_cabang" value=""/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Tanggal Akhir <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Tanggal Akhir"  id="tanggal_akhir" name="tanggal_akhir" value=""/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Premi <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Premi"  id="premi" name="premi" value=""/>
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Plafond <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data" readonly  placeholder="Plafond"  id="plafond" name="plafond" value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="fv-row form-group col-md-12 row mb-5">
                                                    <label class="form-label fs-5">Outstanding Bunga <span class="text-danger">*</span> : </label>
                                                    <div class="col-md-12 mb-5">
                                                        <div class="input-group">
                                                            <input class="form-control form-control-solid input-data"  placeholder="Outstanding Bunga"  id="os_bunga_klaim" name="os_bunga_klaim" value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="d-flex flex-row mt-5">
                                                <button class="btn btn-sm btn-flex btn-primary fw-bold hover-animate" id="submitFormDebitur">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                                    <i class="bi bi-save fs-5"></i>
                                                </span>
                                                <!--begin::Indicator label-->
                                                <span class="indicator-label hover-animate-white">Submit</span>
                                                <!--end::Indicator label-->
                                                <!--begin::Indicator progress-->
                                                <span class="indicator-progress">Please wait... 
                                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                <!--end::Indicator progress-->

                                                <!--end::Svg Icon--></button>

                                                <a href="" class="btn btn-sm btn-flex btn-success d-none fw-bold hover-animate ms-sm-1" id="printKlaim">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                                                <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                                    <i class="bi bi-file-earmark-check-fill"></i>
                                                </span>
                                                <!--end::Svg Icon-->Print Pengajuan Klaim</a>
                                            </div>

                                            <!--end::Col-->
                                        </div>
                                        <!--end::Row-->
                                    <!-- </form> -->
                                </div>
                                <!--end::Input group-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Step 1-->
                        <!--begin::Step 2-->
                        <div data-kt-stepper-element="content">
                            <!--begin::Wrapper-->
                            <div class="w-100">
                                <!--begin::Heading-->
                                <div class="pb-10 pb-lg-15">
                                    <!--begin::Title-->
                                    <h2 class="fw-bold text-dark">Upload Dokumen Klaim</h2>
                                    <!--end::Title-->
                                </div>
                                <!--end::Heading-->
                                <div class="row" id="form-dokumen">
                                <div class="row col-lg-12">
                                     <div class="col-lg-8 mb-10">
                                         <h3 class="fw-bold text-dark">Persyaratan Umum : </h3>
                                        <hr class="border border-primary border-2 opacity-50"/>
                                     </div>
                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Form Klaim / Surat Pengajuan Klaim <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="form_klaim" name="form_klaim" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                            <!-- <div class="progress" id="progress_form_klaim">
                                                <div class="bar" id="bar_form_klaim"></div>
                                                <div class="percent" id="percent_form_klaim">0%</div>
                                            </div> -->
                                        </div>
                                        <div class="col-md-4 dok-name pt-12" data-dokumen-name="form_klaim">
                                        </div>

                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Copy Identitas Peserta / E KTP <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="indetitas_peserta" name="indetitas_peserta" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 dok-name pt-12" data-dokumen-name="indetitas_peserta">
                                            
                                        </div>

                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Copy Kartu Keluarga <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="kartu_keluarga" name="kartu_keluarga" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 dok-name pt-12" data-dokumen-name="kartu_keluarga">

                                        </div>

                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Copy Akad Perjajian Pembiayaan berikut lampiran Perjanjian Pembiayaan <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="akad_kredit" name="akad_kredit" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                       <div class="col-md-4 dok-name pt-12" data-dokumen-name="akad_kredit">
                                        </div>

                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Copy bukti realisasi pencarian Pembiayaan <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="bukti_pencairan" name="bukti_pencairan" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 dok-name pt-12" data-dokumen-name="bukti_pencairan">
                                        </div>

                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Copy Rekening Koran 3 bln Terakhir <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="rekening_koran" name="rekening_koran" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 dok-name pt-12" data-dokumen-name="rekening_koran">
                                        </div>

                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Copy Surat Pengangkatan Pegawai / Surat keterangan bekerja <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="keterangan_pegawai" name="keterangan_pegawai" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 dok-name pt-12" data-dokumen-name="keterangan_pegawai">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                     <div class="col-lg-8 mb-10">
                                         <h3 class="fw-bold text-dark">Persyaratan Khusus : </h3>
                                        <hr class="border border-danger border-2 opacity-50"/>
                                     </div>
                                        <div id="dokumen-pand" class="d-none" >
                                            <div class="fv-row form-group col-md-8 row mb-5">
                                                <label class="form-label fs-5">Surat Keterangan Meninggal Dunia <span class="text-danger">*</span> : </label>
                                                <div class="col-md-12 mb-5">
                                                    <div class="input-group">
                                                        <input type="file" id="keterangan_meninggal" name="keterangan_meninggal" class="form-control form-control-solid" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 dok-name pt-12" data-dokumen-name="keterangan_meninggal">
                                            </div>
                                        </div>

                                        <div id="dokumen-kpp" class="d-none">
                                            <div class="fv-row form-group col-md-8 row mb-5">
                                                <label class="form-label fs-5">Copy Surat Tagihan dan Surat Peringatan <span class="text-danger">*</span> : </label>
                                                <div class="col-md-12 mb-5">
                                                    <div class="input-group">
                                                        <input type="file" id="surat_tagihan" name="surat_tagihan" class="form-control form-control-solid" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 dok-name pt-12" data-dokumen-name="surat_tagihan">
                                            </div>
                                            <div class="fv-row form-group col-md-8 row mb-5">
                                                <label class="form-label fs-5">Copy SLIK OJK (Status Pembiayaan Debitur dalam Kolek 4) <span class="text-danger">*</span> : </label>
                                                <div class="col-md-12 mb-5">
                                                    <div class="input-group">
                                                        <input type="file" id="slik_ojk" name="slik_ojk" class="form-control form-control-solid" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 dok-name pt-12" data-dokumen-name="slik_ojk">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row mt-5">
                                        <a href="<?=base_url('klaim_peralihan/printklaim?id=')?>{{idData}}"
                                        target="__blank" class="btn btn-sm btn-flex btn-success fw-bold hover-animate ms-sm-1">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                                        <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                            <i class="bi bi-file-earmark-check-fill"></i>
                                        </span>
                                        <!--end::Svg Icon-->Print Surat Pengajuan Klaim</a>
                                    </div>
                                </div>
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Step 2-->
                        <!--begin::Step 3-->
                        <div data-kt-stepper-element="content">
                            <!--begin::Wrapper-->
                            <div class="w-100">
                                <!--begin::Heading-->
                                <div class="pb-10 pb-lg-15">
                                    <!--begin::Title-->
                                    <!-- <h2 class="fw-bold text-dark">Upload Dokumen Klaim</h2> -->
                                    <!--end::Title-->
                                </div>
                                <!--end::Heading-->
                                <div class="row" id="form-dokumen-lod">
                                    <div class="row col-lg-12">
                                     <div class="col-lg-8 mb-10">
                                         <h3 class="fw-bold text-dark">Upload Dokumen LOD : </h3>
                                        <hr class="border border-primary border-2 opacity-50"/>
                                     </div>

                                         <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Status Dokumen LOD <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    
                                                <select class="form-select" data-control="select2" data-placeholder="Pilih Status Dokumen LOD" name="status_lod" id = "status_lod">
                                                <option value=""></option>
                                                <option value="1">Dokumen diterima dan Sudah dikirim ke Bank</option>
                                                <option value="2">Dokumen Di Approve Bank dan Dilanjutkan ke Asuransi</option>
                                                <option value="3">Dokumen Di Tolak</option>
                                                </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="fv-row form-group col-md-8 row mb-5">
                                            <label class="form-label fs-5">Dokumen LOD <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12 mb-5">
                                                <div class="input-group">
                                                    <input type="file" id="dok_lod" name="dok_lod" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 dok-name pt-12" data-dokumen-name="dok_lod">
                                        </div>

                                        
                                    </div>
                                    <div class="d-flex flex-row mt-5">
                                        <a href="<?=base_url('klaim_peralihan/printklaim?id=')?>{{idData}}"
                                        target="__blank" class="btn btn-sm btn-flex btn-success fw-bold hover-animate ms-sm-1">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                                        <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                            <i class="bi bi-file-earmark-check-fill"></i>
                                        </span>
                                        <!--end::Svg Icon-->Print Surat Pengantar LOD</a>
                                    </div>
                                </div>
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Step 3-->
                        <!--begin::Step 4-->
                        <div data-kt-stepper-element="content">
                            <!--begin::Wrapper-->
                            <div class="w-100">
                                <!--begin::Heading-->
                                <div class="pb-10 pb-lg-15">
                                    <!--begin::Title-->
                                    <h2 class="fw-bold text-dark">Billing Details</h2>
                                    <!--end::Title-->
                                    <!--begin::Notice-->
                                    <div class="text-muted fw-semibold fs-6">If you need more info, please check out 
                                    <a href="#" class="text-primary fw-bold">Help Page</a>.</div>
                                    <!--end::Notice-->
                                </div>
                                <!--end::Heading-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-column mb-7 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="d-flex align-items-center fs-6 fw-semibold form-label mb-2">
                                        <span class="required">Name On Card</span>
                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" aria-label="Specify a card holder's name" data-kt-initialized="1"></i>
                                    </label>
                                    <!--end::Label-->
                                    <input type="text" class="form-control form-control-solid" placeholder="" name="card_name" value="Max Doe">
                                <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-column mb-7 fv-row fv-plugins-icon-container">
                                    <!--begin::Label-->
                                    <label class="required fs-6 fw-semibold form-label mb-2">Card Number</label>
                                    <!--end::Label-->
                                    <!--begin::Input wrapper-->
                                    <div class="position-relative">
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" placeholder="Enter card number" name="card_number" value="4111 1111 1111 1111">
                                        <!--end::Input-->
                                        <!--begin::Card logos-->
                                        <div class="position-absolute translate-middle-y top-50 end-0 me-5">
                                            <img src="../../assets/media/svg/card-logos/visa.svg" alt="" class="h-25px">
                                            <img src="../../assets/media/svg/card-logos/mastercard.svg" alt="" class="h-25px">
                                            <img src="../../assets/media/svg/card-logos/american-express.svg" alt="" class="h-25px">
                                        </div>
                                        <!--end::Card logos-->
                                    </div>
                                    <!--end::Input wrapper-->
                                <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="row mb-10">
                                    <!--begin::Col-->
                                    <div class="col-md-8 fv-row">
                                        <!--begin::Label-->
                                        <label class="required fs-6 fw-semibold form-label mb-2">Expiration Date</label>
                                        <!--end::Label-->
                                        <!--begin::Row-->
                                        <div class="row fv-row fv-plugins-icon-container">
                                            <!--begin::Col-->
                                            <div class="col-6">
                                                <select name="card_expiry_month" class="form-select form-select-solid select2-hidden-accessible" data-control="select2" data-hide-search="true" data-placeholder="Month" data-select2-id="select2-data-13-txbu" tabindex="-1" aria-hidden="true" data-kt-initialized="1">
                                                    <option data-select2-id="select2-data-15-qv8j"></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select><span class="select2 select2-container select2-container--bootstrap5" dir="ltr" data-select2-id="select2-data-14-gx5h" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single form-select form-select-solid" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-card_expiry_month-q8-container" aria-controls="select2-card_expiry_month-q8-container"><span class="select2-selection__rendered" id="select2-card_expiry_month-q8-container" role="textbox" aria-readonly="true" title="Month"><span class="select2-selection__placeholder">Month</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                            <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                            <!--end::Col-->
                                            <!--begin::Col-->
                                            <div class="col-6">
                                                <select name="card_expiry_year" class="form-select form-select-solid select2-hidden-accessible" data-control="select2" data-hide-search="true" data-placeholder="Year" data-select2-id="select2-data-16-vnw5" tabindex="-1" aria-hidden="true" data-kt-initialized="1">
                                                    <option data-select2-id="select2-data-18-hijb"></option>
                                                    <option value="2022">2022</option>
                                                    <option value="2023">2023</option>
                                                    <option value="2024">2024</option>
                                                    <option value="2025">2025</option>
                                                    <option value="2026">2026</option>
                                                    <option value="2027">2027</option>
                                                    <option value="2028">2028</option>
                                                    <option value="2029">2029</option>
                                                    <option value="2030">2030</option>
                                                    <option value="2031">2031</option>
                                                    <option value="2032">2032</option>
                                                </select><span class="select2 select2-container select2-container--bootstrap5" dir="ltr" data-select2-id="select2-data-17-od3n" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single form-select form-select-solid" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-disabled="false" aria-labelledby="select2-card_expiry_year-22-container" aria-controls="select2-card_expiry_year-22-container"><span class="select2-selection__rendered" id="select2-card_expiry_year-22-container" role="textbox" aria-readonly="true" title="Year"><span class="select2-selection__placeholder">Year</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                            <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                            <!--end::Col-->
                                        </div>
                                        <!--end::Row-->
                                    </div>
                                    <!--end::Col-->
                                    <!--begin::Col-->
                                    <div class="col-md-4 fv-row fv-plugins-icon-container">
                                        <!--begin::Label-->
                                        <label class="d-flex align-items-center fs-6 fw-semibold form-label mb-2">
                                            <span class="required">CVV</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" aria-label="Enter a card CVV code" data-kt-initialized="1"></i>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input wrapper-->
                                        <div class="position-relative">
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" minlength="3" maxlength="4" placeholder="CVV" name="card_cvv">
                                            <!--end::Input-->
                                            <!--begin::CVV icon-->
                                            <div class="position-absolute translate-middle-y top-50 end-0 me-3">
                                                <!--begin::Svg Icon | path: icons/duotune/finance/fin002.svg-->
                                                <span class="svg-icon svg-icon-2hx">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M22 7H2V11H22V7Z" fill="currentColor"></path>
                                                        <path opacity="0.3" d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19ZM14 14C14 13.4 13.6 13 13 13H5C4.4 13 4 13.4 4 14C4 14.6 4.4 15 5 15H13C13.6 15 14 14.6 14 14ZM16 15.5C16 16.3 16.7 17 17.5 17H18.5C19.3 17 20 16.3 20 15.5C20 14.7 19.3 14 18.5 14H17.5C16.7 14 16 14.7 16 15.5Z" fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </div>
                                            <!--end::CVV icon-->
                                        </div>
                                        <!--end::Input wrapper-->
                                    <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                    <!--end::Col-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="d-flex flex-stack">
                                    <!--begin::Label-->
                                    <div class="me-5">
                                        <label class="fs-6 fw-semibold form-label">Save Card for further billing?</label>
                                        <div class="fs-7 fw-semibold text-muted">If you need more info, please check budget planning</div>
                                    </div>
                                    <!--end::Label-->
                                    <!--begin::Switch-->
                                    <label class="form-check form-switch form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="1" checked="checked">
                                        <span class="form-check-label fw-semibold text-muted">Save Card</span>
                                    </label>
                                    <!--end::Switch-->
                                </div>
                                <!--end::Input group-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Step 4-->
                        <!--begin::Step 5-->
                        <div data-kt-stepper-element="content">
                            <!--begin::Wrapper-->
                            <div class="w-100">
                                <!--begin::Heading-->
                                <div class="pb-8 pb-lg-10">
                                    <!--begin::Title-->
                                    <h2 class="fw-bold text-dark">Your Are Done!</h2>
                                    <!--end::Title-->
                                    <!--begin::Notice-->
                                    <div class="text-muted fw-semibold fs-6">If you need more info, please 
                                    <a href="../../authentication/sign-in/basic.html" class="link-primary fw-bold">Sign In</a>.</div>
                                    <!--end::Notice-->
                                </div>
                                <!--end::Heading-->
                                <!--begin::Body-->
                                <div class="mb-0">
                                    <!--begin::Text-->
                                    <div class="fs-6 text-gray-600 mb-5">Writing headlines for blog posts is as much an art as it is a science and probably warrants its own post, but for all advise is with what works for your great &amp; amazing audience.</div>
                                    <!--end::Text-->
                                    <!--begin::Alert-->
                                    <!--begin::Notice-->
                                    <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6">
                                        <!--begin::Icon-->
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                                        <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"></rect>
                                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor"></rect>
                                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"></rect>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        <!--end::Icon-->
                                        <!--begin::Wrapper-->
                                        <div class="d-flex flex-stack flex-grow-1">
                                            <!--begin::Content-->
                                            <div class="fw-semibold">
                                                <h4 class="text-gray-900 fw-bold">We need your attention!</h4>
                                                <div class="fs-6 text-gray-700">To start using great tools, please, please 
                                                <a href="#" class="fw-bold">Create Team Platform</a></div>
                                            </div>
                                            <!--end::Content-->
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Notice-->
                                    <!--end::Alert-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Step 5-->
                        <!--begin::Actions-->
                        <div class="d-flex flex-stack pt-15">
                            <!--begin::Wrapper-->
                            <div class="mr-2">
                                <button type="button" class="btn btn-lg btn-light-primary me-3" data-kt-stepper-action="previous">
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                                <span class="svg-icon svg-icon-4 me-1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="currentColor"></rect>
                                        <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="currentColor"></path>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->Back</button>
                            </div>
                            <!--end::Wrapper-->
                            <!--begin::Wrapper-->
                            <div>
                                <button type="button" class="btn btn-lg btn-primary me-3 " data-kt-stepper-action="submit">
                                    <span class="indicator-label">Submit 
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                    <span class="svg-icon svg-icon-3 ms-2 me-0">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor"></rect>
                                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon--></span>
                                    <span class="indicator-progress">Please wait... 
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </button>
                                <button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="next">Continue 
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor"></rect>
                                        <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor"></path>
                                    </svg>
                                </span>
                                <!--end::Svg Icon--></button>
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Stepper-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->

</div>

<script src="<?=base_url()?>app/controllers/KlaimPeralihanController.js?v=1.0.3" type="text/javascript"></script>