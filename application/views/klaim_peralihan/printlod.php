<!DOCTYPE html>
<html>
<head lang="eng">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>assets/images/pjmbroker.ico">

    <title><?= 'SURAT_PENGANTAR_LOD' ?></title>
    
	<link href="<?=base_url()?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet" type="text/css" />
   
     
  </head>

  <style>
    .mg-left-3{
        margin-left:5%;
    }

    .mg-left-20{
        margin-left:30%;
    }

    .mg-top-5{
        margin-top:5%;
    }

    .mg-top-10{
        margin-top:10%;
    }

    .mg-top-3{
        margin-top:3%;
    }
	
	.mg-top-2{
        margin-top:2%;
    }

    .font-bolder{
        font-weight: 500;
    }

    .font-italic{
        
        font-style: italic;
    }

    .font-small{
        font-size:11px;
    }

    h4{
        font-size:15px !important;
        font-weight: 500;
    }

    .table th, .table td {
     padding: 0 !important;
    }

    .table > tbody > tr > td, .table > tbody > tr > th {
    padding: 0 !important;
    }

    .bordered {
        border: 1px solid #000000 !important;
    }

    .bordered2 {
        border: 0.2px solid #000000;
        -webkit-background-clip: padding-box; /* for Safari */
        background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
    }

    .no-border {
        border : none !important;
        margin-left:20px;
        box-shadow: none;
    }
  </style>

  <body class="hold-transition light-skin sidebar-mini theme-primary fixed">
  <!--  <div class="logo-lg">
        <span class="light-logo mg-left-3"><img src="<?=base_url()?>assets/images/Logo_BWS_biru.png" alt="logo" height="53" ></span>
    </div> -->
	<?php
	date_default_timezone_set('Asia/Jakarta');
		$years = date('Y');
        $months = date('m');
        $datenow = date('d  F  Y');
	?>
    <div class="mg-left-3 mt-15">
        <div class ="row mt-2">
            <div class="col-md-12">
                <h4 style="float: right;margin-right: 10px;"><?="Jakarta , ". $datenow?></h4>
                <h4>Nomor Surat : <input class="text-end" type="text" style="border: none !important;width:35px" /><?='/PJM-BSI/KLM/'.$months.'/'.$years?></h4>
            </div>
        </div>
    </div>
    <div class="row mg-left-3">
        <div class="col-md-7 mt-3 fw-bold">
            <h4>Kepada Yth.</h4>
            <h4 class="font-bolder">PT. Asuransi Takaful Umum</h4>
            <h4>Jl. Persada Raya No. 70 C-D</h4>
            <h4>Menteng Dalam, Tebet</h4>
            <h4>Jakarta, 12870</h4>
        </div>
    </div>

    <div class="row ">
        <div class="col-md-12 mt-5">
            <p class="fw-bold text-center fs-6">Perihal : Pemenuhan Letter Of Discharge (LOD)
            </p>
        </div>
    </div>

    <div class="row mg-left-3">
        <div class="col-md-12 mt-10 fs-5">
            <?php
                $this->db->select('branch_name');
                $this->db->where('branch_code', $editData->kode_cabang);
                $cabs = $this->db->get('tm_cabang_peralihan')->row();

                $cabang = '';

                if(!empty($cabs)){
                    $cabang = 'Cabang : '. $cabs->branch_name;
                }
            ?>
            <p class="fs-6 fst-italic">Assalamu’alaikum Warahmatullahi Wabarakatuhu,</p>
            <p class="fs-6">Berikut ini kami sampaikan tambahan dokumen klaim PT. Bank Syariah Indonesia, <?=$cabang?> , sebagai berikut : <p>
            <table class="table table-borderless fs-6 ">
                <tbody>
                    <tr>
                        <td style="width: 300px;"><p>Nama Peserta</p></td>
                        <td><p>:</p></td>
                        <td><p><?= $editData->nama ?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>Tanggal Kejadian</p></td>
                        <td style="width: 20px;"><p>: </p></td>
                        <?php 
                            $tglkejadian = $editData->tanggal_kejadian;
                            $tglkejadian = date("d - M - Y", strtotime($tglkejadian));
                        ?>
                        <td><p><?=$tglkejadian?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>No Polis</p></td>
                        <td style="width: 20px;"><p>:</p></td>
                        <td><p><?= $editData->no_polis ?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>Total Klaim</p></td>
                        <td style="width: 20px;"><p>:</p></td>
                        <td><p>Rp. <?= number_format($editData->os_total_klaim,0,',','.') ?></p></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-6 mt-2">
        <h4>Dokumen yang sudah diterima :</h4>
        <button class="btn btn-primary mb-5" id="addbutton">Add Row</button>
        <table class="table table-bordered" id="table-add">
            <thead>
                <tr>
                    <th class="w-35px"></th>
                    <th></th>
                </tr>
            </thead>
        <tbody>
            <tr>
                <td class="text-center "><input type="text" value="√" class="bordered2 text-center" style="width: 100%;"/></td>
                <td><input type="text" value="Letter Of Discharge" class="bordered2" style="width:60%"  /> </td>
            </tr>
            
        </tbody>
        </table>
        <br/>
        <h4>Demikian, terimakasih atas perhatian dan kerjasama yang baik.</h4>
        <br/>
        <br/>

        <h4>Hormat kami,</h4>
        <h4><b>PT. PROTEKSI JAYA MANDIRI.</b></h4>
        <h4 class="font-italic">Insurance Broker and Consultant</h4>

        <img src="<?=base_url()?>upload/images/ttd.png"  style="width:250px" >
            <div class="col-md-12">
                <table>
            
                    <tbody>
                        <tr>
                            <td><h4>Haposan Bakara, S.Sos., AAAIK, QIP, CIIB</h4></td>
                        </tr>
                        <tr>
                            <td><h4>Direktur<h4></td>
                        </tr>
                        <tr>
                            <td><h4>Cc	:- File</h4></td>
                        </tr>
                        
                    </tbody>
                    
                </table>
                

            </div>
    </div>
    </div>
    

        <div class="row mg-left-3 mg-top-5">
        
            <br/>
        </div>
</body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
$(document).ready(function(){

    window.onbeforeprint = function () {
        $("#table-add tbody tr td span.remove-tr").hide();
        $("#addbutton").hide();
    }

    window.onafterprint = function () {
        $("#table-add tbody tr td span.remove-tr").show();
        $("#addbutton").show();
    }

    $("#table-add").on('click', '.remove-tr', function(){
        var rowCount = $('#table-add tr').length;

        if(rowCount > 1){
            $(this).parent().parent().remove();
        }
    });
    

    $("#removeButton").on('click', function(){
        $("#table-add tbody tr td.remove-tr").hide();
        $("#addbutton").hide();
        $("#removeButton").hide();
        
        window.print();
    });

    $("#addbutton").click(function () {
     $("#table-add").each(function () {
         var tds = '<tr>';
         /* jQuery.each($('tr:last td', this), function (i) {
             tds += '<td class="bordered">' + $(this).html() + '</td>';
         }); */

         tds += '<td class="text-center"><input type="text" value="√" class="bordered2 text-center" style="width:100%" /></td>';
         tds += '<td ><input type="text" value="" class="bordered2" style="width:60%" /><span class="remove-tr text-danger">&nbsp;&nbsp;X</span></td>';

         tds += '</tr>';
         if ($('tbody', this).length > 0) {
             $('tbody', this).append(tds);
         } else {
             $(this).append(tds);
         }
     });

    });
});
</script>
