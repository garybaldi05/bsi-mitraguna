<?php 
$titleOri = ucfirst($this->uri->segment(1, 0)); 
$title = ucfirst($this->uri->segment(1, 0)); 
$title = str_replace('_',' ',$title); 
?>
<div class="d-flex flex-column flex-column-fluid" ng-controller="KlaimPeralihanController">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url()?>" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted"><?=$title?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0">Cari Data <?=$title?></h1>
                        </div>
                        <!--end::Search-->
                    </div>
                    <div class="d-flex my-4 float-right">
                                <a href="<?=base_url().$titleOri.'/input'?>" class="btn btn-sm btn-primary me-2"><i class="bi bi-plus-circle"></i>Tambah Data</a>
                            </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                <!--begin::Search-->
                    <div class="col-md-12 mb-10">
                        <div class="form-group row">

                            <!--begin:Left Search Bar  -->
                             <div class="form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Created Date : </label>
                                <div class="col-md-6 mb-5">
                                    <div class="input-group">
                                        <button class="btn btn-icon btn-dark" id="kt_ecommerce_sales_flatpickr_clear">
                                            <i class="bi bi-calendar3 fs-4"></i>
                                        </button>
                                        <input class="form-control" placeholder="Start Date" id="startdate"/>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-5">
                                    <div class="input-group">
                                        <button class="btn btn-icon btn-dark" id="kt_ecommerce_sales_flatpickr_clear">
                                            <i class="bi bi-calendar3 fs-4"></i>
                                        </button>
                                        <input class="form-control" placeholder="End Date" id="enddate"/>
                                    </div>
                                </div>

                                

                             </div>
                             <!--end:left Search Bar -->

                             <!--begin:right Search Bar -->
                             <div class="form-group col-md-6">

                             <label class="form-label fs-5">Status Klaim : </label>
                                <div class="col-md-12">
                                    <div class="input-group">
                                    <select class="form-select" id="status_klaim_search" data-control="select2" data-placeholder="Pilih Status Klaim">
                                        <option></option>
                                        <option value="1">Klaim disubmit</option>
                                        <option value="2">Proses Kelengkapan Dokumen</option>
                                        <option value="3">Proses LOD dari Asuransi</option>
                                        <option value="4">Proses LOD dari Bank</option>
                                        <option value="6">Proses Pembayaran</option>
                                    </select>

                                    </div>
                                </div>

                                <!-- <div class="col-md-12 mb-5 row">

                                    <div class="col-md-12">
                                    <label class="form-label fs-5">Cabang : </label>
                                        <div class="input-group">
                                       <select class="form-select" id="cabang_search" data-control="select2" data-placeholder="Pilih Cabang">
                                            <option value=""></option>
                                        </select>

                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <!--end:right Search Bar -->
                        </div>

                        <div class="d-flex flex-center mt-5">
                            <button ng-click="searchTable()" class="btn btn-sm btn-flex btn-primary fw-bold hover-animate">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                            <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="currentColor"></path>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->Search</button>
                        </div>

                    </div>
                <!--end::Search-->

                    <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="list_table_klaim">
                        <!--begin::Table head-->
                        <thead>
                            <!--begin::Table row-->
                            <tr class="text-start text-gray-800 fw-bold fs-7 text-uppercase gs-0">
                                <th class="text-center min-w-200px">Actions</th>
                                <th class="min-w-150px">Tanggal Submit Aplikasi</th>
                                <th class="min-w-150px">Tanggal Pelaporan</th>
                                <th class="min-w-150px">Nama</th>
                                <th class="min-w-150px">Asuransi</th>
                                <th class="min-w-150px">Cabang</th>
                                <th class="min-w-150px">Tanggal Lahir</th>
                                <th class="min-w-150px">Tanggal Akad</th>
                                <th class="min-w-150px">Plafond</th>
                                <th class="min-w-150px">Premi</th>
                                <th class="min-w-150px">OS total Klaim</th>
                                <th class="min-w-150px">Jenis Klaim</th>
                            </tr>
                            <!--end::Table row-->
                        </thead>
                        <!--end::Table head-->
                        <!--begin::Table body-->
                        <tbody class="text-gray-600 fw-semibold">
                        </tbody>
                        <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                 </div>
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->
</div>

<script src="<?=base_url()?>app/controllers/KlaimPeralihanController.js?v=1.0.1" type="text/javascript"></script>