<!DOCTYPE html>
<html style=" zoom: 90%;">
<head lang="eng">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>assets/images/pjmbroker.ico">

    <title><?= 'SURAT_PENGAJUAN_KLAIM' ?></title>
    
	<link href="<?=base_url()?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet" type="text/css" />
     
  </head>

  <style>

    .mg-left-3{
        margin-left:5%;
    }

    .mg-left-20{
        margin-left:30%;
    }

    .mg-top-5{
        margin-top:5%;
    }

    .mg-top-10{
        margin-top:10%;
    }

    .mg-top-3{
        margin-top:3%;
    }
	
	.mg-top-2{
        margin-top:2%;
    }

    .font-bolder{
        font-weight: 500;
    }

    .font-italic{
        
        font-style: italic;
    }

    .font-small{
        font-size:11px;
    }

    h4{
        font-size:15px !important;
        font-weight: 500;
    }

  </style>

<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
  <!--  <div class="logo-lg">
        <span class="light-logo mg-left-3"><img src="<?=base_url()?>assets/images/Logo_BWS_biru.png" alt="logo" height="53" ></span>
    </div> -->
	<?php
	date_default_timezone_set('Asia/Jakarta');
		$years = date('Y');
        $months = date('m');
        $datenow = date('d  F  Y');
	?>
    <div class="mg-left-3 mt-15">
        <div class ="row mt-2">
            <div class="col-md-12">
                <h4 style="float: right;margin-right: 10px;"><?="Jakarta , ". $datenow?></h4>
                <h4>Nomor Surat : <input class="text-end" type="text" style="border: none !important;width:35px" /><?='/PJM-BSI/KLM/'.$months.'/'.$years?></h4>
            </div>
        </div>
    </div>
    <div class="row mg-left-3">
        <div class="col-md-7 mt-3 fw-bold">
            <h4>Kepada Yth.</h4>
            <h4 class="font-bolder">PT. Asuransi Takaful Umum</h4>
            <h4>Jl. Persada Raya No. 70 C-D</h4>
            <h4>Menteng Dalam, Tebet</h4>
            <h4>Jakarta, 12870</h4>
        </div>
    </div>

    <?php 
        $type = '';
        if($editData->type_manfaat == 'PA+ND'){
            $type = 'Meninggal Dunia';
        }elseif($editData->type_manfaat == 'PHK'){
            $type = 'PHK';
        }elseif($editData->type_manfaat == 'KPP'){
            $type = 'Kredit Macet';
        }elseif($editData->type_manfaat == 'PHK+KPP'){
            $type = 'PHK & Kredit Macet';
        }
                                          
    ?>

    <div class="row ">
        <div class="col-md-12 mt-5">
            <p class="fw-bold text-center fs-6">Perihal : Surat Pengajuan Klaim Nasabah (<?=$type?>)
            </p>
        </div>
    </div>
    <?php
                $this->db->select('branch_name');
                $this->db->where('branch_code', $editData->kode_cabang);
                $cabs = $this->db->get('tm_cabang_peralihan')->row();

                $cabang = '';

                if(!empty($cabs)){
                    $cabang = 'Cabang : '. $cabs->branch_name;
                }
            ?>
    <div class="row mg-left-3">
        <div class="col-md-12 mg-top-3">
            <p class="fs-6">Assalamu’alaikum Warahmatullahi Wabarakatuhu,</p>
            <p class="fs-6">Segala doa-doa terbaik semoga mengiringi langkah Bapak/Ibu beserta seluruh karyawan juga senantiasa mendapat lindungan dari Allah SWT. <p>
            <p class="fs-6">Berikut ini kami sampaikan dokumen klaim PT. Bank Syariah Indonesia, Cabang :  <?=$cabang?> , sebagai berikut : <p>
            <table>
                <tbody>
                    <tr>
                        <td style="width: 300px;"><p>Nama Peserta</p></td>
                        <td><p>:</p></td>
                        <td><p><?= $editData->nama ?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>No Polis</p></td>
                        <td style="width: 20px;"><p>:</p></td>
                        <td><p><?= $editData->no_polis ?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>Tanggal Kejadian</p></td>
                        <td style="width: 20px;"><p>: </p></td>
                        <?php 
                            $tgllahir = $editData->tanggal_lahir;
                            $tgllahir = date("d - M - Y", strtotime($tgllahir));
                        ?>
                        <td><p><?=$tgllahir?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>Periode Pertanggungan</p></td>
                        <td style="width: 20px;"><p>:</p></td>
                        <?php 
                            $tglmulai = $editData->tanggal_mulai;
                            $tglmulai = date("d - M - Y", strtotime($tglmulai));

                            $tglakhir = $editData->tanggal_akhir;
                            $tglakhir = date("d - M - Y", strtotime($tglakhir));
                            
                        ?>
                        <td><p><?= $tglmulai ?> sampai <?= $tglakhir ?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>Fasilitas Kredit</p></td>
                        <td style="width: 20px;"><p>:</p></td>
                        <td><p> <?= $type ?></p></td>
                    </tr>
                    <tr>
                        <td style="width: 300px;"><p>Total Klaim</p></td>
                        <td style="width: 20px;"><p>:</p></td>
                        <td><p>Rp. <?= number_format($editData->os_total_klaim,0,',','.') ?></p></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-6 mt-2">
        <h4>Dokumen yang sudah diterima :</h4>
        <table class="table-bordered mb-0 ">
        <?php
         $dataDok = $this->db->query("SELECT * FROM tm_dok_klaim_peralihan WHERE id_klaim_peralihan = '$editData->c_id' ")->result_array();
        ?>
        <tbody>
            <tr>
                
            <?php
            $formulirklaim = "";
            $ektp = "";
            $kk = "";
            $akad_kredit = "";
            $pencairan = "";
            $rek_koran = "";
            $surat_pegawai = "";
            $surat_kematian = "";
            $sp_debitur = "";
            $slik_ojk = "";
              foreach($dataDok as $value){
                if($value['type_dokumen'] == 'form_klaim'){
                    $formulirklaim = '√';
                }

                if($value['type_dokumen'] == 'indetitas_peserta'){
                    $ektp = '√';
                }

                if($value['type_dokumen'] == 'kartu_keluarga'){
                    $kk = '√';
                }

                if($value['type_dokumen'] == 'akad_kredit'){
                    $akad_kredit = '√';
                }

                if($value['type_dokumen'] == 'bukti_pencairan'){
                    $pencairan = '√';
                }

                if($value['type_dokumen'] == 'rekening_koran'){
                    $rek_koran = '√';
                }

                if($value['type_dokumen'] == 'keterangan_pegawai'){
                    $surat_pegawai = '√';
                }

                if($value['type_dokumen'] == 'keterangan_meninggal'){
                    $surat_kematian = '√';
                }

                if($value['type_dokumen'] == 'surat_tagihan'){
                    $sp_debitur = '√';
                }

                if($value['type_dokumen'] == 'slik_ojk'){
                    $slik_ojk = '√';
                }
              }
                

            ?>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$formulirklaim?></td>
                <td class="no-border">&nbsp; 1   Formulir Klaim / Surat Pengajuan Kalim</td>
            </tr>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$ektp?></td>
                <td  class="no-border">&nbsp; 2   Foto Copy Identitas / E KTP</td>
            </tr>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$kk?></td>
                <td class="no-border">&nbsp; 3   Foto Copy Kartu Keluarga</td>
            </tr>
            <tr>
            
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$akad_kredit?></td>
                <td class="no-border">&nbsp; 4   Foto Copy Akad Perjanjian Kredit</td>
            </tr>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$pencairan?></td>
                <td class="no-border">&nbsp; 5   Foto Copy bukti realisasi pencarian Pembiayaan</td>
            </tr>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$rek_koran?></td>
                <td class="no-border">&nbsp; 6   Copy Rekening Pembiayaan Peserta selama 3 (tiga) bulan terakhir pada saat timbul hak klaim.</td>
            </tr>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$surat_pegawai?></td>
                <td class="no-border"> &nbsp; 7   Copy surat keputusan pengangkatan pegawai atau surat keterangan bekerja</td>
            </tr>
            <?php if($editData->type_manfaat == 'PA+ND' ){ ?>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$surat_kematian?></td>
                <td class="no-border">&nbsp; -   Surat Keterangan Kematian dari Instansi yang berwenang</td>
            </tr>
            <?php }elseif($editData->type_manfaat == 'KPP' || $editData->type_manfaat == 'PHK+KPP'){ ?>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$sp_debitur?></td>
                <td class="no-border">&nbsp; -   Copy Surat Tagihan dan Surat Peringatan / Teguran 1, 2, 3 dari pihak pertama</td>
            </tr>
            <tr>
                <td class="text-center bordered">&nbsp;&nbsp;&nbsp;&nbsp;<?=$slik_ojk?></td>
                <td class="no-border">&nbsp; -   Copy SLIK OJK yang menyatakan status Pembiayaan Peserta (Sekurang-kurangnya dalam Kolekbilitas 4) </td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
        <br/>
        <h4>Demikian, terimakasih atas perhatian dan kerjasama yang baik.</h4>
        <br/>
        <br/>

        <h4>Hormat kami,</h4>
        <h4><b>PT. PROTEKSI JAYA MANDIRI.</b></h4>
        <h4 class="font-italic">Insurance Broker and Consultant</h4>

        <img src="<?=base_url()?>upload/images/ttd.png"  style="width:250px" >
            <div class="col-md-12">
                <table>
            
                    <tbody>
                        <tr>
                            <td><h4>Haposan Bakara, S.Sos., AAAIK, QIP, CIIB</h4></td>
                        </tr>
                        <tr>
                            <td><h4>Direktur<h4></td>
                        </tr>
                        <tr>
                            <td><h4>Cc	:- File</h4></td>
                        </tr>
                        
                    </tbody>
                    
                </table>
                

            </div>
    </div>
    </div>
    

        <div class="row mg-left-3 mg-top-5">
        
            <br/>
        </div>
</body>

</html>