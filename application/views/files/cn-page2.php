<div class="row mg-left-3">
        <div class ="col-md-12 mg-top-5">
           
            <table style="text-align:center;">
                   <tbody >
                        <tr>
                            <td rowspan="6"> <img src="upload/logo/pjm-logo.png" width="200" height="100"/></td>
                            <td style="width:40%"></td>
                            <td style="font-size:9pt;font-weight:bold;">PT PROTEKSI JAYA MANDIRI</td>
                        </tr>
                      
                       <tr>
                           <td></td>
                           <td style="font-size:8pt;">Gandaria City Office 8 Lt. 9 Unit H</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td style="font-size:8pt;">Jl.Sultan Iskandar Muda Kebayoran Lama</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td style="font-size:8pt;">Jakarta Selatan 12240</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td style="font-size:8pt;">Ph. 021-29036677 Fax :021-29036677</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td style="font-size:8pt;">Email : info.bsm@pjmbroker.com</td>
                       </tr>
                   </tbody>
               </table>
            </div>  
        </div>
    </div>

  
    <div class="row mg-left-3" style="text-align:center;font-size:12px;">
        <div class="col-md-12 mg-top-3">
        <h3>NOTA ASURANSI/PENJAMINAN PEMBIAYAAN MITRAGUNA BERKAH</h3>
        </div>
    </div>

    <div class="row mg-left-3">
        <div class="col-md-10">
             <table style="margin-left:5px;font-size:9.5pt;">
                <tbody>
                    <tr>
                        <td> CABANG </td>
                        <td> : <?=$debitur->nama_cabang?> </td>
                    </tr>
                    <tr>
                        <td>NOMOR APLIKASI</td>
                        <td> : <?=$debitur->no_aplikasi?> </td>
                    </tr>
                    <tr>
                        <td>NOMOR KARTU PESERTA ASURANSI</td>
                        <td> : <?=$debitur->nama_cabang ."-". $debitur->reffnumber?> </td>
                    </tr>
                    <tr>
                        <td>TANGGAL LAHIR</td>
                        <td> : <?=$debitur->tanggal_lahir?> </td>
                    </tr>
                    <tr>
                        <td>NO IDENTITAS</td>
                        <td> : <?=$debitur->ktp?> </td>
                    </tr>
                    <tr>
                        <td>USIA SAAT AKAD</td>
                        <td> : <?=$debitur->usia_saat_akad?> </td>
                    </tr>
                    <tr>
                        <td>USIA SAAT AKHIR ASURANSI </td>
                        <td> : <?=$debitur->usia_selesai_akad?> </td>
                    </tr>
                    <tr>
                        <td>PLAFOND PEMBIAYAAN/NILAI PERTANGGUNGAN </td>
                        <td> : <?=number_format($debitur->plafond,0)?> </td>
                    </tr>
                    <tr>
                        <td>ATRIBUSI ASURANSI </td>
                        <td> : <?=number_format($debitur->coverage_asuransi,0)?> </td>
                    </tr>
                    <tr>
                        <td>ATRIBUSI MITRA</td>
                        <td> : <?=number_format($debitur->coverage_fee_mitra,0)?> </td>
                    </tr>
                    <tr>
                        <td>ATRIBUSI PINALTI</td>
                        <td> : <?=number_format($debitur->coverage_pinalti,0)?></td>
                    </tr>
                    <tr>
                        <td>LIMIT ASURANSI</td>
                        <td> : <?=number_format($debitur->limit_asuransi,0)?></td>
                    </tr>
                    <tr>
                        <td>PREMI SEBELUM BIAYA ADMIN & POLIS</td>
                        <td> : <?=number_format($debitur->premi_before_admin,0)?></td>
                    </tr>
                    <tr>
                        <td>BIAYA ADMIN DAN POLIS</td>
                        <td> : <?=number_format($fee_asuransi,0)?></td>
                    </tr>
                    <tr>
                        <td>PREMI SETELAH BIAYA ADMIN DAN POLIS</td>
                        <td> : <?=number_format($debitur->premi,0)?></td>
                    </tr>
                    <tr>
                        <td>FEE UJROH</td>
                        <td> : <?=number_format($debitur->fee_ujroh,0)?></td>
                    </tr>
                    <tr>
                        <td>JANGKA WAKTU(BULAN)</td>
                        <td> : <?=$debitur->bulan_tenor?> </td>
                    </tr>
                    <tr>
                        <td>MANFAAT ASURANSI</td>
                        <td> : <?=$namaManfaat?></td>
                    </tr>
                    <tr>
                        <td>TYPE COVERAGE</td>
                        <td> : <?=$debitur->coverage_type?></td>
                    </tr>
                    <tr>
                        <td>RATE</td>
                        <td> : <?=$debitur->fixed_rate?> % </td>
                    </tr>
                    <tr>
                        <td>STATUS DATA</td>
                        <td> : <?= $debitur->status_cbc == '1' ? 'Case By Case (CBC)' : 'Free Cover (CAC)'; ?></td>
                    </tr>
                    <tr>
                        <td>PERIODE PERTANGGUNGAN</td>
                        <td> : <?= $debitur->tanggal_mulai.' s.d '.$debitur->tanggal_akhir ?> </td>
                    </tr>
                    <tr>
                        <td>TIPE PEMBIAYAAN</td>
                        <td> : <?=$namaPekerjaan?></td>
                    </tr>
                    <tr>
                        <td>NAMA PERUSAHAAN PENANGUNG</td>
                        <td> : <?=$fullname_asuransi?></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="font-size:8pt;margin-top:20px;">
            <p>Keterangan : </p>
            <p style="font-weight: bold;">
                <?php if(empty($debitur->ket_cbc) || $debitur->premi > 0){?>
                Nilai Premi tersebut diatas harap dibayarkan ke rekening atas nama PT Proteksi Jaya Mandiri
                ( Premi Asuransi/Penjaminan Pembiayaan Mitraguna Berkah ) No. Rek : 7295895734 di 
                Bank BSI KC Mayestik
                <?php }elseif(!empty($debitur->ket_cbc) || empty($debitur->premi)){?>
                    debitur di proses secara case by case karena, <?=$debitur->ket_cbc?> , hubungi PJMbroker atau email ke KPA@pjmbroker.com agar dapat di proses lebih lanjut dengan asuransi yg di pilih
                <?php } ?>
            </p>
            <p style="font-weight: bold;">
                *Surat elektronik ini ditarik secara otomatis dari system sehingga tidak memerlukan tanda tangan
            </p>
        </div>

    </body>
</html>