<!DOCTYPE html>
<html>
<head lang="eng">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/pjmbroker.ico">

    <title><?= 'KPA'.'/'.$debitur->no_aplikasi."/".$debitur->reffnumber ?></title>
     
  </head>

  <style>
    *{
        font-family: sans-serif;
    }

    .mg-left-3{
        margin-left:5%;
    }

    .mg-left-20{
        margin-left:30%;
    }

    .mg-top-5{
        margin-top:5%;
    }

    .mg-top-10{
        margin-top:10%;
    }

    .mg-top-3{
        margin-top:3%;
    }
	
	.mg-top-2{
        margin-top:2%;
    }

    .font-bolder{
        font-weight: 500;
    }

    .font-italic{
        
        font-style: italic;
    }

    .font-small{
        font-size:11px;
    }

    h4{
        font-size:15px !important;
        font-weight: 500;
    }

    .table th, .table td {
     padding: 0 !important;
    }

    .table > tbody > tr > td, .table > tbody > tr > th {
    padding: 0 !important;
    
    }

    .bordered {
        border: 1px solid #000000 !important;
    }

    .no-border {
        border : none !important;
        margin-left:20px;
    }
  </style>

<body class="hold-transition light-skin sidebar-mini theme-primary fixed">
	<?php
	date_default_timezone_set('Asia/Jakarta');
		$years = date('Y');
        $months = date('m');
        $datenow = date('d  F  Y');
	?>
    <div class="row mg-left-3">
        <div class ="col-md-12 mg-top-5">
           
            <table style="text-align:center;font-weight:bold;font-size:9pt;">
                   <tbody >
                        <tr>
                            <td rowspan="3"> <img src="upload/logo/<?= $logo_asuransi ?>" width="130"/></td>
                            <?php if($fullname_asuransi == 'PT. ASKRIDA SYARIAH' ){ ?>
                            <td style="width:35%"></td>
                            <?php }elseif($fullname_asuransi == 'PT. TAKAFUL UMUM'){ ?>
                            <td style="width:50%"></td>
                            <?php }elseif($fullname_asuransi == 'PT. AL AMIN'){ ?>
                            <td style="width:50%"></td>
                            <?php } ?>
                            <td><?= $fullname_asuransi ?></td>
                        </tr>
                       <?php if($fullname_asuransi == 'PT. ASKRIDA SYARIAH' ){ ?>
                       <tr>
                           <td></td>
                           <td>Jl. Ruko Cemp. Mas No.36, RW.8</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td>Sumur Batu, Kec. Kemayoran, Kota Jakarta Pusat</td>
                       </tr>
                       <?php }elseif($fullname_asuransi == 'PT. TAKAFUL UMUM'){ ?>
                        <tr>
                           <td></td>
                           <td>Jl. Persada Raya No. 70 C-D</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td>Menteng Dalam, Tebet, Jakarta</td>
                       </tr>
                       <?php }elseif($fullname_asuransi == 'PT. AL AMIN'){ ?>
                        <tr>
                           <td></td>
                           <td>Jl. Sultan Agung No. 11-12</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td>Setiabudi, Jakarta Selatan 12980</td>
                       </tr>
                       <?php } ?>
                   </tbody>
               </table>

            </div>  
        </div>
    </div>

  
    <div class="row mg-left-3" style="text-align:center;font-size:12px;margin-bottom:20px;">
    
        <div class="col-md-12 mg-top-3">
        
            <h3>
                 NOTA PENUTUPAN ASURANSI (COVER NOTE)
            </h3>
            <span>No : <?= $debitur->no_aplikasi."-".$debitur->reffnumber ?></span>
            <table>
                <tbody>
                    <tr>
                        <td><img src="upload/images/assalamualaikum-2.png" width="130"/></td>
                    </tr>
                    <tr>
                        <td>
                        <p style="font-size: 12px;">Atas permintaan Tertanggung, bersama ini kami konfirmasikan penerimaan penutupan asuransi sbb :</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row mg-left-3">
        <div class="col-md-10">
            <table style="margin-left:5px;font-size:9pt;">
                <tbody>
                    <tr>
                        <td>NOMOR APLIKASI</td>
                        <td> : <?= $debitur->no_aplikasi ?></td>
                    </tr>
                    <tr>
                        <td>NAMA DEBITUR</td>
                        <td> : <?= $debitur->nama_cabang ?></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td> : <?= $debitur->nama?></td>
                    </tr>
                    <tr>
                        <td>TANGGAL LAHIR / USIA</td>
                        <td> : <?= $debitur->tanggal_lahir?></td>
                    </tr>
                    <tr>
                        <td>NO REFERENSI</td>
                        <td> : <?=$debitur->reffnumber?> </td>
                    </tr>
                    <tr>
                        <td>JENIS PERTANGGUNGAN</td>
                        <td> : <?=$namaProduk?> </td>
                    </tr>
                    <tr>
                        <td>PLAFOND PEMBIAYAAN/NILAI PERTANGGUNGAN </td>
                        <td> : <?=number_format($debitur->plafond,0)?></td>
                    </tr>
                    <tr>
                        <td>PREMI ASURANSI </td>
                        <td> : <?=number_format($debitur->premi_before_admin,0)?></td>
                    </tr>
                    <tr>
                        <td>BIAYA ADMIN DAN POLIS </td>
                        <td> : <?=number_format($debitur->biaya_admin,0)?></td>
                    </tr>
                    <tr>
                        <td>TOTAL PREMI DAN BIAYA ADMIN DAN POLIS</td>
                        <td> : <?=number_format($debitur->premi,0)?></td>
                    </tr>
                    <tr>
                        <td>JANGKA WAKTU</td>
                        <td> : <?=$debitur->tenor?></td>
                    </tr>
                    <tr>
                        <td>PERIODE PERTANGGUNGAN</td>
                        <td> : <?= $debitur->tanggal_mulai.' s.d '.$debitur->tanggal_akhir ?></td>
                    </tr>
                    <tr>
                        <td>RESIKO YANG DIJAMIN/JENIS ASURANSI</td>
                        <td> : <?=$namaManfaat?></td>
                    </tr>
                    <tr>
                        <td>TIPE COVERAGE</td>
                        <td> : <?=$debitur->coverage_type?></td>
                    </tr>
                    <tr>
                        <td>NAMA INSTANSI</td>
                        <td> : <?=$debitur->nama_cabang?></td>
                    </tr>
                    <tr>
                        <td>TIPE PEMBIAYAAN</td>
                        <td> : <?=$namaPekerjaan ?></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-6 mg-top-3" style="width:100%;display:inline-block;">
            <table>
                <tbody>
                    <tr sytle="margin-bottom:10px;">
                        <td><p style="font-size: 12px;margin-bottom:10px;">Masa Berlaku Cover Note ini akan berakhir bila polis sudah diterbitkan</p></td>
                    </tr>
                    
                    <tr>
                        <td>
                        <p style="font-size: 12px;font-weight: bold;margin-bottom:5px;">Pembayaran Premi Ke Rekening No. 7295895734</p>
                        <p style="font-size: 12px;font-weight: bold;margin-bottom:5px;">PT. Bank Syariah Indonesia KC Mayestik</p>
                        <p style="font-size: 12px;font-weight: bold;margin-bottom:5px;">Atas Nama PT. PROTEKSI JAYA MANDIRI</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="upload/images/wassalam.png" width="150"/></td>
                    </tr>
                </tbody>
            </table>
         <table style="font-size:13px;margin-top:10px;">
             <tr>
                <td>JAKARTA, <?=date('d F Y',strtotime($debitur->createdon))?></td>
             </tr>
             <tr>
                <td>Untuk dan Atas Nama</td>
             </tr>
             <tr>
                <td style="font-weight:bold;"><?=$fullname_asuransi?></td>
             </tr>
             
         </table>
            
        </div>
        <div style="font-size:8pt;margin-top:20px;">
            <p style="font-weight: bold;">Kartu ini merupakan bagian yang tidak terpisahkan dari Polis Induk Asuransi/ Sertifikat Asuransi /
Perjanjian Kerja sama</p>
            <p style="font-weight: bold;">*Surat elektronik ini ditarik secara otomatis dari system sehingga tidak memerlukan tanda tangan</p>
        </div>