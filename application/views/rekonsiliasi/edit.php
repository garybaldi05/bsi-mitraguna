<?php 
    $title = ucfirst($this->uri->segment(1, 0)); 
    $title2 = ucfirst($this->uri->segment(2, 0)); 
    $no_app = ($_GET['data']);
?>
<div class="d-flex flex-column flex-column-fluid" ng-controller="RekonsiliasiController">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-dark">
                        <a href="<?=base_url()?>" class="text-dark text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-dark">
                        <a href="<?=base_url($title)?>" class="text-dark text-hover-primary">
                            <?=$title?>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-dark"><?=$title2?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">

        <div class="d-flex flex-column flex-xl-row gap-7 gap-lg-10 mb-10">
            <!--begin::Order details-->
            <div class="card card-flush py-4 flex-row-fluid">
                <!--begin::Card header-->
                <div class="card-header">
                <div class="card-title">
                    <h2>Detail Debitur</h2>
                </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                <div class="table-responsive">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                    <!--begin::Table body-->
                    <tbody class="fw-semibold text-dark">
                        <!--begin::Date-->
                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/files/fil002.svg-->
                            <i class="bi bi-1-square-fill text-primary me-2"></i>
                            <!--end::Svg Icon-->No Aplikasi
                            </div>
                        </td>
                        <td class="fw-bold ms-2">: <?=$editData->no_aplikasi?></td>
                        </tr>
                        <!--end::Date-->
                        <!--begin::Payment method-->
                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/finance/fin008.svg-->
                            <i class="bi bi-credit-card-fill text-primary me-2"></i>
                            <!--end::Svg Icon-->Reffnumber
                            </div>
                        </td>
                        <td class="fw-bold">: <?=$editData->reffnumber?>
                        </td>
                        </tr>
                        <!--end::Payment method-->
                        <!--begin::Date-->
                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm006.svg-->
                            <i class="bi bi-person-fill text-primary me-2"></i>
                            <!--end::Svg Icon-->Nama
                            </div>
                        </td>
                        <td class="fw-bold">: <?=$editData->nama?></td>
                        </tr>
                        <!--end::Date-->
                        
                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm006.svg-->
                            <i class="bi bi-ui-checks-grid text-primary me-2"></i>
                            <!--end::Svg Icon-->Produk
                            </div>
                        </td>
                        <td class="fw-bold">: <?=$editData->produk?></td>
                        </tr>

                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm006.svg-->
                            <i class="bi bi-circle-square text-primary me-2"></i>
                            <!--end::Svg Icon-->Tipe Manfaat
                            </div>
                        </td>
                        <td class="fw-bold">: <?=$editData->type_manfaat?></td>
                        </tr>

                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm006.svg-->
                            <i class="bi bi-bandaid-fill text-primary me-2"></i>
                            <!--end::Svg Icon-->Tipe Coverage
                            </div>
                        </td>
                        <td class="fw-bold">: <?=$editData->coverage_type?></td>
                        </tr>

                    </tbody>
                    <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                </div>
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Order details-->
            <!--begin::Customer details-->
            <div class="card card-flush py-4 flex-row-fluid">
                <!--begin::Card header-->
                <div class="card-header">
                <div class="card-title">
                    <h2>Detail Pembayaran</h2>
                </div>
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                <div class="table-responsive">
                    <!--begin::Table-->
                    <table class="table align-middle table-row-bordered mb-0 fs-6 gy-5 min-w-300px">
                    <!--begin::Table body-->
                    <tbody class="fw-semibold text-dark">
                        <!--begin::Customer name-->
                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com006.svg-->
                            <i class="bi bi-cash-stack text-primary me-2"></i>
                            <!--end::Svg Icon-->Total Yang Harus Dibayar
                            </div>
                        </td>
                        <td class="fw-bold ">
                            Rp. <?=number_format($editData->premi,0,',','.')?>
                        </td>
                        </tr>
                        <!--end::Customer name-->
                        <!--begin::Customer email-->
                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com011.svg-->
                            <i class="bi bi-clipboard-check-fill text-primary me-2"></i>
                            <!--end::Svg Icon-->Total Yang Sudah Dibayar
                            </div>
                        </td>
                        <td class="fw-bold ">
                            Rp. <?php echo empty($editData->bayar_rekon) ? 0 : number_format($editData->bayar_rekon,0,',','.') ?>
                        </td>
                        </tr>

                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com011.svg-->
                            <i class="bi bi-arrow-left-right text-primary me-2"></i>
                            <!--end::Svg Icon-->Selisih Pembayaran (Premi)
                            </div>
                        </td>
                        <td class="fw-bold ">
                            Rp. 
                            <?php $selisih_premi = (float)$editData->bayar_rekon - (float)$editData->premi;
                                  echo number_format($selisih_premi,0,',','.');
                            ?>
                        </td>
                        </tr>

                        <!--end::Payment method-->
                        <!--begin::Date-->
                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/electronics/elc003.svg-->
                            <i class="bi bi-wallet-fill text-primary me-2"></i>
                            <!--end::Svg Icon-->Fee Ujroh
                            </div>
                        </td>
                        <td class="fw-bold ">Rp. <?=number_format($editData->fee_ujroh,0,',','.')?></td>
                        </tr>
                        <!--end::Date-->

                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/electronics/elc003.svg-->
                            <i class="bi bi-patch-check-fill text-primary me-2"></i>
                            <!--end::Svg Icon-->Total Pembayaran Fee Ujroh
                            </div>
                        </td>
                        <td class="fw-bold ">Rp. <?= empty($editData->bayar_ujroh) ? 0 : number_format($editData->bayar_ujroh,0,',','.') ?></td>
                        </tr>

                        <tr>
                        <td class="text-dark">
                            <div class="d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com011.svg-->
                            <i class="bi bi-arrow-left-right text-primary me-2"></i>
                            <!--end::Svg Icon-->Selisih Pembayaran (Fee Ujroh)
                            </div>
                        </td>
                        <td class="fw-bold ">
                            Rp. 
                            <?php $selisih_ujroh = (float)$editData->bayar_ujroh -  (float)$editData->fee_ujroh;
                                  echo number_format($selisih_ujroh,0,',','.');
                            ?>
                        </td>
                        </tr>
                    </tbody>
                    <!--end::Table body-->
                    </table>
                    <!--end::Table-->
                </div>
                </div>
                <!--end::Card body-->
            </div>
            <!--end::Customer details-->
            
            </div>

            <!--begin::Card-->
            <div class="card card-flush gap-7 gap-lg-10 mb-10">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                             <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0">Input Data <?=$title?></h1>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                 <span class="text-danger text-bold">* wajib diisi</span>
                        <div class="d-flex row mt-5">
                        <form id="rekonsiliasi_form" class="form" action="#" autocomplete="off">
                            <input type="hidden" id='id_penutupan' name="id_penutupan" value="<?=$editData->c_id?>" /> 
                            <div class="form-group row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label required fw-semibold fs-6">Jenis Pembayaran</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-6 fv-row fv-plugins-icon-container">
                                    <div class="input-group">
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Pembayaran" name="jenis_bayar" id = "jenis_bayar">
                                            <option value=""></option>
                                            <option value="1">Pembayaran Bank</option>
                                            <option value="2">Penarikan Bank</option>
                                            <option value="3">Pengembalian Fee Ujroh Dari Bank</option>
                                            <option value="4">Penarikan Fee Ujroh Dari Bank</option>
                                        </select>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <!--end::Col-->
                            </div>

                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label required fw-semibold fs-6">Jumlah Pembayaran</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-6 fv-row fv-plugins-icon-container">
                                     <div class="input-group">
                                        <button class="btn btn-icon btn-primary">
                                                    <i class="bi bi-cash-stack fs-4"></i>
                                        </button>
                                        <input type="text" name="jumlah_bayar" id="jumlah_bayar" class="form-control form-control-lg form-control-solid" placeholder="Input Jumlah Pembayaran" value="">
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                     </div>
                            </div>
                                <!--end::Col-->
                            </div>

                            <div class="form-group row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label required fw-semibold fs-6">Tanggal Pembayaran</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-6 fv-row fv-plugins-icon-container">
                                    <div class="input-group">
                                        <button class="btn btn-icon btn-primary" id="kt_ecommerce_sales_flatpickr_clear">
                                                    <i class="bi bi-calendar3 fs-4"></i>
                                        </button>
                                        <input class="form-control" class="col-md-10" placeholder="Tanggal Pembayaran" name="tanggal_bayar" id="tanggal_bayar"/>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <!--end::Col-->
                            </div>
                        </form>
                            <div class="d-flex flex-row mt-5">
                                <button ng-click="submitData($event)" class="btn btn-sm btn-flex btn-primary fw-bold hover-animate" id="submitForm">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                                <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                    <i class="bi bi-save fs-5"></i>
                                </span>
                                <!--begin::Indicator label-->
                                <span class="indicator-label hover-animate-white">Submit</span>
                                <!--end::Indicator label-->
                                <!--begin::Indicator progress-->
                                <span class="indicator-progress">Please wait... 
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                <!--end::Indicator progress-->

                                <!--end::Svg Icon--></button>
                            </div>

                        </div>
                    

                </div>
                <!--end::Search-->

                </div>
                <!--end::Card body-->

                <div class="card card-flush gap-7 gap-lg-10 mb-10">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                             <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0">History Data <?=$title?></h1>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                        <div class="d-flex row mt-5">
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5" id="history_table">
                                    <!--begin::Table head-->
                                    <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-gray-800 fw-bold fs-7 text-uppercase gs-0 bg-light">
                                            <th class="text-center min-w-150px">Actions</th>
                                            <th class="min-w-100px">Tanggal Pembayaran</th>
                                            <th class="min-w-150px">Keterangan</th>
                                            <th class="min-w-150px">Uang Masuk</th>
                                            <th class="min-w-150px">Uang Keluar</th>
                                            <th class="min-w-150px">Status</th>
                                            <th class="min-w-100px">Dibuat Oleh</th>
                                            <th class="min-w-100px">Tanggal Dibuat</th>
                                            <th class="min-w-100px">Diupdate Oleh</th>
                                            <th class="min-w-100px">Tanggal Update</th>
                                        </tr>
                                        <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="text-gray-600 fw-semibold">
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                            </div>
                            </div>
                        </div>

                </div>
                <!--end::Search-->

                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->

        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->

</div>

<div class="modal fade" tabindex="-1" id="modal_edit_rekon">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 1000px;right: 200px;">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Rekon</h5>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal">
                    <i class="bi bi-x-lg text-danger"></i>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
             <div class="row">
                <div class="col-md-12">
                    <form id="rekonsiliasi_form_edit" class="form" action="#" autocomplete="off">
                    <input type="hidden" id='id_penutupan_edit' name="id_penutupan_edit" value="<?=$editData->c_id?>" /> 
                        <input type="hidden" name="c_id" id='c_id' />
                            <div class="form-group row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label required fw-semibold fs-6">Jenis Pembayaran</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-6 fv-row fv-plugins-icon-container">
                                    <div class="input-group">
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Pembayaran" name="jenis_bayar_edit" id = "jenis_bayar_edit">
                                            <option value=""></option>
                                            <option value="1">Pembayaran Bank</option>
                                            <option value="2">Penarikan Bank</option>
                                            <option value="3">Pengembalian Fee Ujroh Dari Bank</option>
                                            <option value="4">Penarikan Fee Ujroh Dari Bank</option>
                                        </select>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <!--end::Col-->
                            </div>

                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label required fw-semibold fs-6">Jumlah Pembayaran</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-6 fv-row fv-plugins-icon-container">
                                     <div class="input-group">
                                        <button class="btn btn-icon btn-primary">
                                                    <i class="bi bi-cash-stack fs-4"></i>
                                        </button>
                                        <input type="text" name="jumlah_bayar_edit" id="jumlah_bayar_edit" class="form-control form-control-lg form-control-solid" placeholder="Input Jumlah Pembayaran" value="">
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                     </div>
                            </div>
                                <!--end::Col-->
                            </div>

                            <div class="form-group row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label required fw-semibold fs-6">Tanggal Pembayaran</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-6 fv-row fv-plugins-icon-container">
                                    <div class="input-group">
                                        <button class="btn btn-icon btn-primary" id="kt_ecommerce_sales_flatpickr_clear">
                                                    <i class="bi bi-calendar3 fs-4"></i>
                                        </button>
                                        <input class="form-control" class="col-md-10" placeholder="Tanggal Pembayaran" name="tanggal_bayar_edit" id="tanggal_bayar_edit"/>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                                <!--end::Col-->
                            </div>
                        </form>
                            <div class="d-flex flex-row mt-5">
                                <button ng-click="submitDataEdit($event)" class="btn btn-sm btn-flex btn-primary fw-bold hover-animate" id="submitForm">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                                <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                    <i class="bi bi-save fs-5"></i>
                                </span>
                                <!--begin::Indicator label-->
                                <span class="indicator-label hover-animate-white">Submit</span>
                                <!--end::Indicator label-->
                                <!--begin::Indicator progress-->
                                <span class="indicator-progress">Please wait... 
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                <!--end::Indicator progress-->

                                <!--end::Svg Icon--></button>
                            </div>
                </div>
             </div>
            </div>  
        </div>
    </div>
</div>

<script src="<?=base_url()?>app/controllers/RekonsiliasiController.js?v=1.0.1" type="text/javascript"></script>