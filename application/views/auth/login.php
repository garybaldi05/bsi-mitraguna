<!DOCTYPE html>
<html lang="en">
<head>
		<title>PT. Proteksi Jaya Mandiri</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="PT. Proteksi Jaya Mandiri" />
		<meta property="og:url" content="<?=base_url()?>" />
		<meta property="og:site_name" content="PT. Proteksi Jaya Mandiri" />
		<link rel="shortcut icon" href="<?=base_url()?>upload/logo/pjmbroker.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="<?=base_url()?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?=base_url()?>assets/css/custom.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->

		<script src="<?=base_url()?>./assets/js/angularjs/angular.min.js"></script>
		<script src="<?=base_url()?>./assets/js/angularjs/ui-bootstrap-tpls-2.5.0.min.js"></script>
		<script src="<?=base_url()?>./assets/js/angularjs/angular-sanitize.min.js"></script>
		<script src="<?=base_url()?>./assets/js/angularjs/angular-route.min.js"></script>
		<script src="<?=base_url()?>./assets/js/angularjs/angular-resource.js"></script>
		<script src="<?=base_url()?>./assets/js/angularjs/angular-ui-tree.min.js"></script>
		<script src="<?=base_url()?>./assets/js/angularjs/angular-animate.min.js"></script>
		<script src="<?=base_url()?>./app/main.js?v=1.0"></script>
		
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body data-kt-name="metronic" id="kt_body" class="app-blank app-blank bgi-size-cover bgi-position-center bgi-no-repeat" ng-app="mainApp">
		<!--begin::Theme mode setup on page load-->
		<script>if ( document.documentElement ) { const defaultThemeMode = "light"; const name = document.body.getAttribute("data-kt-name"); let themeMode = localStorage.getItem("kt_" + ( name !== null ? name + "_" : "" ) + "theme_mode_value"); if ( themeMode === null ) { if ( defaultThemeMode === "system" ) { themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light"; } else { themeMode = defaultThemeMode; } } document.documentElement.setAttribute("data-theme", themeMode); }</script>
		<!--end::Theme mode setup on page load-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root" id="kt_app_root">
			<!--begin::Page bg image-->
			<style>body { background-image: url('<?=base_url()?>assets/media/auth/bg4.jpg'); } [data-theme="dark"] body { background-image: url('<?=base_url()?>assets/media/auth/bg4-dark.jpg'); }</style>
			<!--end::Page bg image-->
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-column-fluid flex-lg-row">
				<!--begin::Aside-->
				<div class="d-flex flex-center w-lg-50 pt-15 pt-lg-0 px-10">
					<!--begin::Aside-->
					<div class="d-flex flex-column">
						<!--begin::Logo-->
						<a href="../../../index.html" class="mb-7">
							<!-- <img alt="Logo" src="<?=base_url()?>upload/logo/pjm-logo.png" /> -->
						</a>
						<!--end::Logo-->
						<!--begin::Title-->
						<div class="fs-2 fw-semibold text-white mb-3">
							<span class="fs-1 lh-1 text-white-700">“</span><span class="text-center fw-bold fs-1"> PT. PROTEKSI JAYA MANDIRI atau PJM Broker </span>
							<br>
							<span class="text-white-700 me-1">adalah perusahaan konsultan dan pialang asuransi atau Broker Asuransi, sebagai Lembaga Keuangan Non Bank (LKNB) dalam kegiatannya berada di bawah pengaturan </span>
							<span class="fs-1 lh-1 text-white-700">pengawasan OJK (Otoritas Jasa Keuangan).“</span>
						</div>
						<!--end::Title-->
					</div>
					<!--begin::Aside-->
				</div>
				<!--begin::Aside-->
				<!--begin::Body-->
				<div class="d-flex flex-center w-lg-50 p-10">
					<!--begin::Card-->
					<div class="card rounded-3 w-md-550px">
						<!--begin::Card body-->
						<div class="card-body p-10 p-lg-20">
							<!--begin::Form-->
							<input type="hidden" id="base-url" value="<?=base_url()?>" />
							<form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" data-kt-redirect-url="<?=base_url('dashboard')?>" action="#">
								<!--begin::Heading-->
								<div class="text-center mb-11">
									<!--begin::Title-->
									<h1 class="text-dark fw-bolder mb-3"><img alt="Logo" src="<?=base_url()?>upload/logo/pjm-logo.png" width="250" /></h1>
									<!--end::Title-->
									<!--begin::Subtitle-->
									<div class="text-gray-500 fw-semibold fs-6">Silahkan Login dengan Username & Password anda !</div>
									<!--end::Subtitle=-->
								</div>
								<!--begin::Heading-->
								<!--begin::Login options-->
								<!--end::Login options-->
								<!--begin::Separator-->
								<!-- <div class="separator separator-content my-14">
									<span class="w-125px text-gray-500 fw-semibold fs-7">Or with email</span>
								</div> -->
								<!--end::Separator-->
								<!--begin::Input group=-->
								<div class="fv-row mb-8">
									<!--begin::Email-->
									<input type="text" id="username" placeholder="Username" name="username" autocomplete="off" class="form-control bg-transparent" />
									<!--end::Email-->
								</div>
								<!--end::Input group=-->
								<div class="fv-row mb-3">
									<!--begin::Password-->
									<input type="password" id="password" placeholder="Password" name="password" autocomplete="off" class="form-control bg-transparent" />
									<!--end::Password-->
								</div>
								<!--end::Input group=-->
								<!--begin::Wrapper-->
								<div class="d-flex flex-stack flex-wrap gap-3 fs-base fw-semibold mb-8">
									<div></div>
									<!--begin::Link-->
									<!-- <a href="reset-password.html" class="link-primary">Forgot Password ?</a> -->
									<!--end::Link-->
								</div>
								<!--end::Wrapper-->
								<!--begin::Submit button-->
								<div class="d-grid mb-10">
									<button type="submit" id="kt_sign_in_submit" class="btn btn-primary" ng-click="swall()">
										<!--begin::Indicator label-->
										<span class="indicator-label hover-animate-white">Sign In</span>
										<!--end::Indicator label-->
										<!--begin::Indicator progress-->
										<span class="indicator-progress">Please wait... 
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
										<!--end::Indicator progress-->
									</button>
								</div>
								<!--end::Submit button-->
								<!--begin::Sign up-->
								<div class=" text-center fw-semibold fs-6 hover-animate">© 2022 PT. Proteksi Jaya Mandiri 
								</div>
								<!--end::Sign up-->
							</form>
							<!--end::Form-->
						</div>
						<!--end::Card body-->
					</div>
					<!--end::Card-->
				</div>
				<!--end::Body-->
			</div>
			<!--end::Authentication - Sign-in-->
		</div>
		<!--end::Root-->
		<!--begin::Javascript-->
		<script>var hostUrl = "<?=base_url()?>";</script>
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="<?=base_url()?>assets/plugins/global/plugins.bundle.js"></script>
		<script src="<?=base_url()?>assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Custom Javascript(used by this page)-->
		<script src="<?=base_url()?>assets/js/custom/authentication/sign-in/general.js"></script>
		<!--end::Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->

</html>