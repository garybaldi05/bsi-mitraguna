<?php 
    $title = ucfirst($this->uri->segment(1, 0)); 
    $title2 = ucfirst($this->uri->segment(2, 0)); 
?>
<div class="d-flex flex-column flex-column-fluid" ng-controller="PenutupanController">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url()?>" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url($title)?>" class="text-muted text-hover-primary">
                            <?=$title?>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted"><?=$title2?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0"><?=$title2?> Data <?=$title?></h1>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                <span class="text-danger text-bold">* wajib diisi</span>
                </br>
                </br>
                <!--begin::Search-->
                <div class="col-md-12 mb-10">
                
                <!-- Begin::Form -->
                    <form id="penutupan_input" class="form" action="#" autocomplete="off">
                        <div class="form-group row">
                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No Aplikasi <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid input-data"  placeholder="No Aplikasi"  id="no_aplikasi" name="no_aplikasi" value="" maxlength="17"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nama <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Nama" name="nama" id="nama" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Lahir <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom" placeholder="Tanggal Lahir" id="tanggal_lahir" name="tanggal_lahir" value=""/>
                                    </div>
                                </div>
                             </div>
                            
                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">KTP <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid" maxlength="13"  placeholder="KTP" name="ktp" id="ktp" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Npwp <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid" maxlength="13"  placeholder="Npwp" name="npwp" id="npwp" value="" />
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Kelamin <span class="text-danger">*</span> : </label>
                                <div class="col-6 mb-5">
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" type="radio" value="L" id="jk-l" />
                                        <label class="form-check-label" for="jk-l">
                                            Laki - Laki
                                        </label>
                                    </div>
                                </div>

                                <div class="col-6 mb-5">
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" type="radio" value="P" id="jk-p" />
                                        <label class="form-check-label" for="jk-p">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Asuransi <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $asuransi = $this->db->query("SELECT * FROM tm_asuransi ")
                                                      ->result_array();
                                        ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Asuransi" name="nama_asuransi" id = "nama_asuransi">
                                    <option value=""></option>
                                    <?php foreach($asuransi as $value){?>
                                        <option value="<?=$value['c_id']?>"><?=$value['alias']?></option>
                                    <?php } ?>
                                    </select>

                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Akad <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $jenis_akad = $this->db->query("SELECT c_id, nama FROM tm_jenis_akad ")
                                                      ->result_array();
                                        ?>
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Akad" id="jenis_akad" name="jenis_akad">
                                            <option value="">Pilih Jenis Akad</option>
                                            <?php foreach($jenis_akad as $value){?>
                                                <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Produk <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                     
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Produk" id="produk" name="produk">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Type Manfaat <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Type Manfaat" id="type_manfaat" name="type_manfaat">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Pekerjaan <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Pekerjaan" id="jenis_pekerjaan" name="jenis_pekerjaan">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tipe Produk <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Tipe Produk" id="tipe_produk" name="tipe_produk">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Agunan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  placeholder="Jenis Agunan" name="jenis_agunan" id="jenis_agunan" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. Agunan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="No. Agunan" id="nomor_agunan" name="nomor_agunan" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Alamat <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <textarea class="form-control form-control-solid"   placeholder="Alamat" id="alamat" name="alamat" value="">
                                        </textarea>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Kodepos <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  placeholder="Kodepos" id="kodepos" name="kodepos" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-3 row mb-5">
                                <label class="form-label fs-5">Area <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $cabang = $this->db->query("SELECT * FROM tm_cabang where status = 1 and c_type = 'AREA' ")
                                                      ->result_array();
                                        ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Area" id="kode_area" name="kode_area">
                                    <option value=""></option>
                                    <?php foreach($cabang as $value){?>
                                        <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                    <?php } ?>
                                    </select>

                                    </div>
                                </div>
                             </div>
                             
                             <div class="fv-row form-group col-md-3 row mb-5">
                             <label class="form-label fs-5">Cabang <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Cabang" id="kode_cabang" name="kode_cabang">
                                    <option value=""></option>
                                    </select>

                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. Akad <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid" placeholder="No. Akad" id="nomor_akad" name="nomor_akad" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Akad <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom" placeholder="Tanggal Akad" id="tanggal_akad" name="tanggal_akad" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-3 row mb-5">
                                <label class="form-label fs-5">Tanggal Mulai <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom" readonly  placeholder="Tanggal Mulai" id="tanggal_mulai" name="tanggal_mulai" value=""/>
                                    </div>
                                </div>
                             </div>
                             

                             <div class="fv-row form-group col-md-3 row mb-5">
                                <label class="form-label fs-5">Tanggal akhir jatuh tempo kredit <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom"   placeholder="Tanggal Mulai" id="tanggal_akhir" name="tanggal_akhir" value=""/>
                                    </div>
                                </div>
                             </div>

                            <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tenor <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control readonly form-control-solid " placeholder="Tenor" id="tenor" value="" name="tenor"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nilai Agunan <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Nilai Agunan" id="nilai_agunan" name="nilai_agunan" value="" />
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Plafond <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Plafond" id="plafond" name="plafond" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nilai Pengajuan <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Nilai Pengajuan" id="nilai_pengajuan" value="" name="nilai_pengajuan"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Type Coverage <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $cabang = $this->db->query("SELECT * FROM tm_type_coverage where status = 1 ")
                                                      ->result_array();
                                        ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Type Coverage" id="type_coverage" name="type_coverage">
                                    <option value=""></option>
                                    <?php foreach($cabang as $value){?>
                                        <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                    <?php } ?>
                                    </select>

                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5" id="coverage_fee_mitra_row">
                                <label class="form-label fs-5">Fee Mitra <span class="text-danger">*</span>: </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid" placeholder="Fee Mitra" id="coverage_fee_mitra" value="" name="coverage_fee_mitra"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5" id="coverage_asuransi_row">
                                <label class="form-label fs-5">Fee Asuransi <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Fee asuransi" id="coverage_asuransi" name="coverage_asuransi" value=""/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5" id="coverage_pinalti_row">
                                <label class="form-label fs-5">Fee Pinalti <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Fee Pinalti" id="coverage_pinalti" name="coverage_pinalti" value=""coverage_pinalti/>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </form>
                    <!-- End::Form -->
                    
                </div>

                    <!-- <div class ="col-md-12 mb-10">
                      <div class="form-group row">
                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Usia Saat Akad : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Usia Saat Akad" id="usia_saat_akad" name="usia_saat_akad" value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Usia Selesai Akad : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Usia Selesai Akad" id="usia_selesai_akad" name="usia_selesai_akad"/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Tenor Dalam Bulan : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Tenor" id="bulan_tenor" value="" name="bulan_tenor"/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Rate : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Rate" id="fixed_rate" name="fixed_rate" value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Premi Before Admin : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Premi" id="premi_before_admin" name="premi_before_admin" value=""/>
                                </div>
                            </div>
                        </div>
                        
                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Premi Final : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Premi" id="premi" name="premi"/>
                                </div>
                            </div>
                        </div>

                      </div>
                    </div> -->

                        <div class="d-flex flex-row mt-5">
                            <button ng-click="validateForm($event)" class="btn btn-sm btn-flex btn-primary fw-bold hover-animate" id="submitForm">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                            <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                <i class="bi bi-save fs-5"></i>
                            </span>
                            <!--begin::Indicator label-->
                            <span class="indicator-label hover-animate-white">Submit</span>
                            <!--end::Indicator label-->
                            <!--begin::Indicator progress-->
                            <span class="indicator-progress">Please wait... 
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            <!--end::Indicator progress-->

                            <!--end::Svg Icon--></button>
                        </div>

                    </div>
                <!--end::Search-->

                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->

</div>

<script src="<?=base_url()?>app/controllers/PenutupanController.js?v=1.0.3" type="text/javascript"></script>