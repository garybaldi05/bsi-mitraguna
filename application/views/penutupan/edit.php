<?php 
    $title = ucfirst($this->uri->segment(1, 0)); 
    $title2 = ucfirst($this->uri->segment(2, 0)); 
    $no_app = ($_GET['data']);
?>
<div class="d-flex flex-column flex-column-fluid" ng-controller="PenutupanController">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url()?>" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url($title)?>" class="text-muted text-hover-primary">
                            <?=$title?>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted"><?=$title2?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content-->
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Card-->
            <div class="card">
                <!--begin::Card header-->
                <div class="card-header border-0 pt-6 mb-8">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0"><?=$title2?> Data <?=$title?></h1>
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--begin::Card title-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                <span class="text-danger text-bold">* wajib diisi</span>
                </br>
                </br>
                <!--begin::Search-->
                <div class="col-md-12 mb-10">
                
                <!-- Begin::Form -->
                    <form id="penutupan_input" class="form" action="#" autocomplete="off">
                        <div class="form-group row">
                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No Aplikasi <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid input-data"  placeholder="No Aplikasi"  id="no_aplikasi" name="no_aplikasi" value="<?=$editData->no_aplikasi?>" maxlength="17"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nama <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Nama" name="nama" id="nama" value="<?=$editData->nama?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Lahir <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom" placeholder="Tanggal Lahir" id="tanggal_lahir" name="tanggal_lahir" value="<?=$editData->tanggal_lahir?>"/>
                                    </div>
                                </div>
                             </div>
                            
                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">KTP <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="KTP" name="ktp" id="ktp" value="<?=$editData->ktp?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Npwp <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Npwp" name="npwp" id="npwp" value="<?=$editData->npwp?>" />
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Kelamin <span class="text-danger">*</span> : </label>
                                <div class="col-6 mb-5">
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" type="radio" value="L" id="jk-l" checked='true' />
                                        <label class="form-check-label" for="jk-l">
                                            Laki - Laki
                                        </label>
                                    </div>
                                </div>

                                <div class="col-6 mb-5">
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" type="radio" value="P" id="jk-p" />
                                        <label class="form-check-label" for="jk-p">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                             </div>



                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Asuransi <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                        $asuransi = $this->db->query("SELECT * FROM tm_asuransi where c_id != '$editData->id_asuransi' ")
                                        ->result_array();

                                        $asuransiselect = $this->db->query("SELECT nama FROM tm_asuransi where c_id = '$editData->id_asuransi' ")
                                                        ->row()->nama;
                                    ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Asuransi" name="nama_asuransi" id = "nama_asuransi">
                                        <option value="<?=$editData->id_asuransi?>"><?=$asuransiselect?></option>
                                        <?php foreach($asuransi as $value){?>
                                            <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                        <?php } ?>
                                    </select>

                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Akad <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                        $jenis_akad = $this->db->query("SELECT * FROM tm_jenis_akad where c_id != '$editData->jenis_akad' ")
                                        ->result_array();

                                        $jenis_akadselect = $this->db->query("SELECT nama FROM tm_jenis_akad where c_id = '$editData->jenis_akad' ")
                                                        ->row()->nama;
                                    ?>
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Akad" id="jenis_akad" name="jenis_akad">
                                            <option value="<?=$editData->jenis_akad?>"><?=$jenis_akadselect?></option>
                                            <?php foreach($jenis_akad as $value){?>
                                                <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                             </div>
                             

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Produk <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <?php
                                            $produk = $this->db->query("SELECT * FROM tm_product_list where c_id != '$editData->produk' ")
                                                      ->result_array();

                                            $produkselect = $this->db->query("SELECT nama_produk FROM tm_product_list where c_id = '$editData->produk' ")
                                                            ->row()->nama_produk;
                                        ?>
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Produk" id="produk" name="produk">
                                            <option value="<?=$editData->produk?>"><?=$produkselect?></option>
                                            <?php foreach($produk as $value){?>
                                                <option value="<?=$value['c_id']?>"><?=$value['nama_produk']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Type Manfaat <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <?php
                                            $typemanfaat = $this->db->query("SELECT * FROM tm_type_manfaat where id_produk = '$editData->produk' AND c_id != '$editData->type_manfaat' ")
                                            ->result_array();

                                            $typemanfaatselect = $this->db->query("SELECT nama FROM tm_type_manfaat where c_id = '$editData->type_manfaat' ")
                                                            ->row()->nama;
                                        ?>

                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Type Manfaat" id="type_manfaat" name="type_manfaat">
                                            <option value="<?=$editData->type_manfaat?>"><?=$typemanfaatselect?></option>
                                            <?php foreach($typemanfaat as $value){?>
                                                <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Pekerjaan <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                        $jenispekerjaan = $this->db->query("SELECT * FROM tm_jenis_pekerjaan where id_produk = '$editData->produk' AND c_id != '$editData->jenis_pekerjaan' ")
                                        ->result_array();

                                        $jenispekerjaanselect = $this->db->query("SELECT nama FROM tm_jenis_pekerjaan where c_id = '$editData->jenis_pekerjaan' ")
                                                        ->row()->nama;
                                    ?>
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Pekerjaan" id="jenis_pekerjaan" name="jenis_pekerjaan">
                                            <option value="<?=$editData->jenis_pekerjaan?>"><?=$jenispekerjaanselect?></option>
                                            <?php foreach($jenispekerjaan as $value){?>
                                                <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tipe Produk <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                        $tipe_produk = $this->db->query("SELECT * FROM tm_product_type where id_produk = '$editData->produk' AND c_id != '$editData->tipe_produk' ")
                                        ->result_array();

                                        $tipe_produkselect = $this->db->query("SELECT nama FROM tm_product_type where c_id = '$editData->tipe_produk' ")
                                                        ->row()->nama;
                                    ?>
                                        <select class="form-select" data-control="select2" data-placeholder="Pilih Tipe Produk" id="tipe_produk" name="tipe_produk">
                                            <option value="<?=$editData->tipe_produk?>"><?=$tipe_produkselect?></option>
                                            <?php foreach($tipe_produk as $value){?>
                                                <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Jenis Agunan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  placeholder="Jenis Agunan" name="jenis_agunan" id="jenis_agunan" value="<?=$editData->jenis_agunan?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. Agunan : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="No. Agunan" id="nomor_agunan" name="nomor_agunan" value="<?=$editData->nomor_agunan?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Alamat <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <textarea class="form-control form-control-solid"   placeholder="Alamat" id="alamat" name="alamat" value="<?=$editData->alamat?>">
                                            <?=$editData->alamat?>
                                        </textarea>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Kodepos <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"  placeholder="Kodepos" id="kodepos" name="kodepos" value="<?=$editData->kodepos?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-3 row mb-5">
                                <label class="form-label fs-5">Area <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $cabangselect = $this->db->query("SELECT * FROM tm_cabang where kode_cabang = '$editData->kode_cabang' ")
                                            ->row();
                                            // var_dump($cabangselect);
                                            $areaselect = $this->db->query("SELECT * FROM tm_cabang where status = 1 and c_type = 'AREA' and nama = '$cabangselect->c_area' ")
                                            ->row();

                                            $area = $this->db->query("SELECT * FROM tm_cabang where status = 1 and c_type = 'AREA' and c_id != '$areaselect->c_id' ")
                                                      ->result_array();
                                            
                                           
                                        ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Area" id="kode_area" name="kode_area">
                                    <option value="<?=$areaselect->c_id?>"><?=$areaselect->nama?></option>
                                    <?php foreach($area as $value){?>
                                        <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                    <?php } ?>
                                    </select>

                                    </div>
                                </div>
                             </div>
                             
                             <div class="fv-row form-group col-md-3 row mb-5">
                             <label class="form-label fs-5">Cabang <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            
                                        $cabang = $this->db->query("SELECT * FROM tm_cabang 
                                                                        where status = 1 and c_type != 'AREA' and c_id != '$cabangselect->c_id' ")->result_array();

                                           
                                    ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Cabang" id="kode_cabang" name="kode_cabang">
                                    <option value="<?=$cabangselect->c_id?>"><?=$cabangselect->nama?></option>
                                    <?php foreach($cabang as $value){?>
                                        <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                    <?php } ?>
                                    </select>

                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">No. Akad <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid" placeholder="No. Akad" id="nomor_akad" name="nomor_akad" value="<?=$editData->nomor_akad?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tanggal Akad <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom" placeholder="Tanggal Akad" id="tanggal_akad" name="tanggal_akad" value="<?=$editData->tanggal_akad?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-3 row mb-5">
                                <label class="form-label fs-5">Tanggal Mulai <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom"   placeholder="Tanggal Mulai" id="tanggal_mulai" name="tanggal_mulai" value="<?=$editData->tanggal_mulai?>"/>
                                    </div>
                                </div>
                             </div>
                             

                             <div class="fv-row form-group col-md-3 row mb-5">
                                <label class="form-label fs-5">Tanggal Selesai <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid date-custom"   placeholder="Tanggal Mulai" id="tanggal_akhir" name="tanggal_akhir" value="<?=$editData->tanggal_akhir?>"/>
                                    </div>
                                </div>
                             </div>

                            <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Tenor <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid " placeholder="Tenor" id="tenor" value="<?=$editData->tenor?>" name="tenor"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nilai Agunan <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Nilai Agunan" id="nilai_agunan" name="nilai_agunan" value="<?=number_format($editData->nilai_agunan,0,',','.')?>" />
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Plafond <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Plafond" id="plafond" name="plafond" value="<?=number_format($editData->plafond,0,',','.')?>"/>
                                    </div>
                                </div>
                             </div>

                             

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Nilai Pengajuan <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Nilai Pengajuan" id="nilai_pengajuan" value="<?=number_format($editData->nilai_pengajuan,0,',','.')?>" name="nilai_pengajuan"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5">
                                <label class="form-label fs-5">Type Coverage <span class="text-danger">*</span> : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                    <?php
                                            $cabang = $this->db->query("SELECT * FROM tm_type_coverage where status = 1 ")
                                                      ->result_array();
                                            $typeCoverage = $editData->type_coverage;
                                            $type_coverage = $this->db->query("SELECT * FROM tm_type_coverage where nama = '$typeCoverage' AND status = 1 ")
                                                      ->row();
                                        ?>
                                    <select class="form-select" data-control="select2" data-placeholder="Pilih Type Coverage" id="type_coverage" name="type_coverage">
                                    <option value="<?=$type_coverage->c_id?>"><?=$type_coverage->nama?></option>
                                    <?php foreach($cabang as $value){?>
                                        <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                    <?php } ?>
                                    </select>

                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5" id="coverage_fee_mitra_row">
                                <label class="form-label fs-5">Fee Mitra : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Fee Mitra" id="coverage_fee_mitra" value="<?=number_format($editData->coverage_fee_mitra,0,',','.')?>" name="coverage_fee_mitra"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5" id="coverage_asuransi_row">
                                <label class="form-label fs-5">Fee Asuransi : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Fee asuransi" id="coverage_asuransi" name="coverage_asuransi" value="<?=number_format($editData->coverage_asuransi,0,',','.')?>"/>
                                    </div>
                                </div>
                             </div>

                             <div class="fv-row form-group col-md-6 row mb-5" id="coverage_pinalti_row">
                                <label class="form-label fs-5">Fee Pinalti : </label>
                                <div class="col-md-12 mb-5">
                                    <div class="input-group">
                                        <input class="form-control form-control-solid"   placeholder="Fee Pinalti" id="coverage_pinalti" name="coverage_pinalti" value="<?=number_format($editData->coverage_pinalti,0,',','.')?>"/>
                                    </div>
                                </div>
                             </div>
                        </div>
                    </form>
                    <!-- End::Form -->
                    
                </div>

                    <!-- <div class ="col-md-12 mb-10">
                      <div class="form-group row">
                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Usia Saat Akad : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Usia Saat Akad" id="usia_saat_akad" name="usia_saat_akad" value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Usia Selesai Akad : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Usia Selesai Akad" id="usia_selesai_akad" name="usia_selesai_akad"/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Tenor Dalam Bulan : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Tenor" id="bulan_tenor" value="" name="bulan_tenor"/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Rate : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Rate" id="fixed_rate" name="fixed_rate" value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Premi Before Admin : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Premi" id="premi_before_admin" name="premi_before_admin" value=""/>
                                </div>
                            </div>
                        </div>
                        
                        <div class="fv-row form-group col-md-6 row mb-5">
                            <label class="form-label fs-5">Premi Final : </label>
                            <div class="col-md-12 mb-5">
                                <div class="input-group">
                                    <input class="form-control form-control-solid"   placeholder="Premi" id="premi" name="premi"/>
                                </div>
                            </div>
                        </div>

                      </div>
                    </div> -->

                        <div class="d-flex flex-row mt-5">
                            <button ng-click="validateForm($event)" class="btn btn-sm btn-flex btn-primary fw-bold hover-animate" id="submitForm">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                            <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                <i class="bi bi-save fs-5"></i>
                            </span>
                            <!--begin::Indicator label-->
                            <span class="indicator-label hover-animate-white">Submit</span>
                            <!--end::Indicator label-->
                            <!--begin::Indicator progress-->
                            <span class="indicator-progress">Please wait... 
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            <!--end::Indicator progress-->

                            <!--end::Svg Icon--></button>
                        </div>

                    </div>
                <!--end::Search-->

                </div>
                <!--end::Card body-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content container-->
    </div>
    <!--end::Content-->

</div>

<script src="<?=base_url()?>app/controllers/PenutupanController.js?v=1.0.3" type="text/javascript"></script>