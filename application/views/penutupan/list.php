<?php 
    $title = ucfirst($this->uri->segment(1, 0)); 
?>
<div class="d-flex flex-column flex-column-fluid" ng-controller="PenutupanController">

<!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0"><?=$title?> Data</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="<?=base_url()?>" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted"><?=$title?></li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Content--> 
    <div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Card-->
            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x fs-6 bg-white ps-4" style="border-radius: 0.625rem;">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#kt_tab_pane_1">Data Penutupan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-bs-toggle="tab" href="#kt_tab_pane_2">Upload Data Penutupan</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="kt_tab_pane_1" role="tabpanel">
                    <div class="card">
                        <!--begin::Card header-->
                        <div class="card-header border-0 pt-6 mb-8">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <!--begin::Search-->
                                <div class="d-flex align-items-center position-relative my-1">
                                    <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0">Cari Data <?=$title?></h1>
                                </div>
                                <!--end::Search-->
                            </div>

                            <div class="d-flex my-4">
                                <a href="<?=base_url().$title.'/input'?>" class="btn btn-sm btn-primary me-2"><i class="bi bi-plus-circle"></i>Tambah Data</a>
                            </div>
                            <!--begin::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                        <!--begin::Search-->
                            <div class="col-md-12 mb-10">
                                <div class="form-group row">

                                    <!--begin:Left Search Bar  -->
                                    <div class="form-group col-md-6 row mb-5">
                                        <label class="form-label fs-5">Created Date : </label>
                                        <div class="col-md-6 mb-5">
                                            <div class="input-group">
                                                <button class="btn btn-icon btn-dark" id="kt_ecommerce_sales_flatpickr_clear">
                                                    <i class="bi bi-calendar3 fs-4"></i>
                                                </button>
                                                <input class="form-control" placeholder="Start Date" id="startdate"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-5">
                                            <div class="input-group">
                                                <button class="btn btn-icon btn-dark" id="kt_ecommerce_sales_flatpickr_clear">
                                                    <i class="bi bi-calendar3 fs-4"></i>
                                                </button>
                                                <input class="form-control" placeholder="End Date" id="enddate"/>
                                            </div>
                                        </div>

                                        <label class="form-label fs-5">Asuransi : </label>
                                        <div class="col-md-12 mb-5">
                                        <div class="input-group">
                                            <?php
                                                    $asuransi = $this->db->query("SELECT * FROM tm_asuransi ")
                                                            ->result_array();
                                                ?>
                                            <select class="form-select" data-control="select2" data-placeholder="Pilih Asuransi" name="asuransi_search" id = "asuransi_search">
                                            <option value=""></option>
                                            <?php foreach($asuransi as $value){?>
                                                <option value="<?=$value['c_id']?>"><?=$value['alias']?></option>
                                            <?php } ?>
                                            </select>

                                            </div>
                                        </div>

                                        <label class="form-label fs-5">Nama Debitur : </label>
                                    <div class="col-md-12 mb-5">
                                        <div class="input-group">
                                            <input class="form-control form-control-solid input-data"  placeholder="Nama Debitur"  id="nama_search" name="nama_search" value=""/>
                                        </div>
                                    </div>

                                    </div>
                                    <!--end:left Search Bar -->

                                    <!--begin:right Search Bar -->
                                    <div class="form-group col-md-6">

                                        <div class="col-md-12 mb-5 row">
                                            <div class="col-md-6">
                                            <label class="form-label fs-5">Area : </label>
                                                <div class="input-group">
                                                <?php
                                                        $cabang = $this->db->query("SELECT * FROM tm_cabang where status = 1 and c_type = 'AREA' ")
                                                                ->result_array();
                                                    ?>
                                                <select class="form-select" id="area_search" data-control="select2" data-placeholder="Pilih Area">
                                                    <option value=""></option>
                                                    <?php foreach($cabang as $value){?>
                                                        <option value="<?=$value['c_id']?>"><?=$value['nama']?></option>
                                                    <?php } ?>
                                                </select>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                            <label class="form-label fs-5">Cabang : </label>
                                                <div class="input-group">
                                            <select class="form-select" id="cabang_search" data-control="select2" data-placeholder="Pilih Cabang">
                                                    <option value=""></option>
                                                </select>

                                                </div>
                                            </div>
                                        </div>

                                        <label class="form-label fs-5">Status : </label>
                                        <div class="col-md-12">
                                            <div class="input-group">
                                            <select class="form-select" id="status_data_search" data-control="select2" data-placeholder="Pilih Status">
                                                <option></option>
                                                <option value="1">Free Cover (CAC)</option>
                                                <option value="2">Non Free Cover (CBC)</option>
                                            </select>

                                            </div>
                                        </div>
                                    </div>
                                    <!--end:right Search Bar -->
                                </div>

                                <div class="d-flex flex-center mt-5">
                                    <button ng-click="searchTable()" class="btn btn-sm btn-flex btn-primary fw-bold hover-animate">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                                    <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="currentColor"></path>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->Search</button>
                                </div>

                            </div>
                        <!--end::Search-->

                            <!--begin::Table-->
                        <div class="table-responsive">
                            <div class="col-lg-6 ms-5 mb-10 mt-10">
                                <table class="table align-middle table-sm table-striped fs-6 gy-5" id="summary_table">
                                    <thead>
                                        <tr class="text-center text-gray-800 fw-bold fs-7 text-uppercase gs-0">
                                            <th>Jumlah Debitur</th>
                                            <th>Total Plafond</th>
                                            <th>Total Premi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="text-center text-gray-600 fw-bold fs-7 text-uppercase gs-0">
                                            <td><span id="jumlah_deb" > </span></td>
                                            <td><span id="total_plafond" >  </span></td>
                                            <td><span id="total_premi" >  </span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="list_table">
                                <!--begin::Table head-->
                                <thead>
                                    <!--begin::Table row-->
                                    <tr class="text-start text-gray-800 fw-bold fs-7 text-uppercase gs-0">
                                        <th class="text-center min-w-100px">Download</th>
                                        <th class="text-center min-w-200px">Actions</th>
                                        <th class="min-w-100px">Polis</th>
                                        <th class="min-w-200px">No. Aplikasi</th>
                                        <th class="min-w-150px">Nama</th>
                                        <th class="min-w-150px">Asuransi</th>
                                        <th class="min-w-150px">Cabang</th>
                                        <th class="min-w-150px">Produk</th>
                                        <th class="min-w-150px">Tipe Produk</th>
                                        <th class="min-w-150px">Atribusi</th>
                                        <th class="min-w-150px">Tenor</th>
                                        <th class="min-w-150px">Plafond</th>
                                        <th class="min-w-150px">Fee Mitra</th>
                                        <th class="min-w-150px">Fee Asuransi</th>
                                        <th class="min-w-150px">Fee Pinalti</th>
                                        <th class="min-w-150px">Premi + Admin</th>
                                        <th class="min-w-150px">Premi</th>
                                        <th class="min-w-150px">Tanggal Akad</th>
                                        <th class="min-w-150px">No. Akad</th>
                                        <th class="min-w-150px">Tanggal Lahir</th>
                                        <th class="min-w-200px">Usia Saat Akad</th>
                                        <th class="min-w-200px">Usia Selesai Akad</th>
                                        
                                        
                                    </tr>
                                    <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="text-gray-600 fw-semibold">
                                </tbody>
                                <!--end::Table body-->
                            </table>
                            <!--end::Table-->
                        </div>
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>

                <div class="tab-pane fade" id="kt_tab_pane_2" role="tabpanel">
                    <div class="card">
                        <!--begin::Card header-->
                        <div class="card-header border-0 pt-6 mb-8">
                            <!--begin::Card title-->
                            <div class="card-title col-md-6 mb-8">
                                <!--begin::Search-->
                                <div class="d-flex align-items-center position-relative my-1">
                                    <h1 class="page-heading d-flex text-dark fw-bold fs-5 flex-column justify-content-center my-0">Upload Data <?=$title?></h1>
                                </div>
                                <!--end::Search-->
                            </div>
                            <div class="col-mg-5 d-flex my-4">
                                    <a href="<?=base_url().$title.'/template'?>" class="btn btn-sm btn-success me-2"><i class="bi bi-file-earmark-arrow-down"></i>Download Template</a>
                            </div>

                            
                            <!--begin::Card title-->
                            <div class="card-body">
                                <form id="uploadForm" enctype="multipart/form-data">
                                    <div class="col-md-12 mb-10">
                                        <div class="form-group row">
                                        <label class="form-label fs-7">Upload File <span class="text-danger">*</span> : </label>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="file" id="fileInput" name="penutupan_file" class="form-control form-control-solid" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                
                                <div class="card mb-5 col-md-5 bg-light" id="upload-report">
                                <!--begin::Card header-->
                                <div class="card-header cursor-pointer">
                                    <!--begin::Card title-->
                                    <div class="card-title m-0">
                                        <h3 class="fw-bold m-0">Report Data</h3>
                                    </div>
                                    <!--end::Card title-->
                                    
                                </div>
                                <!--begin::Card header-->
                                <!--begin::Card body-->
                                <div class="card-body p-9">
                                    <!--begin::Row-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 text-dark fw-semibold">Status Upload</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{detailUploadData.statusDetail}}</span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold">Success Debitur</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{detailUploadData.success_debitur}}</span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold">Invalid Debitur</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">{{detailUploadData.error_debitur}}</span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <div class="row mb-7">
                                        <!--begin::Label-->
                                        <label class="col-lg-4 fw-semibold">Generated Report File</label>
                                        <!--end::Label-->
                                        <!--begin::Col-->
                                        <div class="col-lg-8">
                                            <span class="fw-bold fs-6 text-gray-800">
                                                <a href="<?=base_url('upload/data_upload/')?>{{detailUploadData.generated_report}}">{{detailUploadData.generated_report}}</a>
                                            </span>
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Row-->
                                    
                                </div>
                                <!--end::Card body-->
                            </div>

                            </div>
                        </div>
                    </div>
                    <!--end::Card-->
                </div>
            </div>
        </div>
    <!--end::Content container-->
    </div>
    <!--end::Content-->
</div>

<script src="<?=base_url()?>app/controllers/PenutupanController.js?v=1.0.2" type="text/javascript"></script>