mainApp.controller('KlaimPeralihanController', function ($scope, $rootScope){

    // Data initialize //
    //getSelect2Data("penutupan/getInsuranceData", "asuransi",null, "Asuransi");
    // end of data initialize //

   // Local variable initialize //

    let baseUrl = $("#url").val();
    let segmentUrl = $("#segment").val();
    let headsegmentUrl = $("#headsegment").val();
    let no_app = $("#no_app").val();
    let isEdit = $("#isEdit").val();
    var form = document.getElementById('penutupan_input');
    const submitButton = document.getElementById('submitFormDebitur');
    const printKlaimButton = document.getElementById('printKlaim');

    var stepper;
	var form;
	var formSubmitButton;
	var formContinueButton;

    // Variables
	var stepperObj;
	var validations = [];

    var table = $("#list_table");
    
    var target = document.querySelector("#kt_app_root");
    var blockUI = new KTBlockUI(target);
    var tableHistory = null;
  
    $scope.totalItems = 0;
    $scope.idData = 0;
    $scope.jenisKlaim = $("#type_manfaat").val();
    $scope.showTable = true;
    $scope.showView = false;
    $scope.showSuccessUpload = false;
    $scope.showShareData = false;
    $scope.submitDataDebitur = false;
    $scope.testDetailShared = {
        data : []
    }

    $scope.dokData = [];
    
    $scope.dataActivity = [];
    $scope.dataActivityLog = [];
    $scope.dataJenisPekerjaan = [];
    $scope.insurance = [];
    $scope.detailUploadData = {
        statusDetail : '',
        success_debitur : 0,
        error_debitur : 0,
        generated_report : ''
    };

    if ($('#startdate').length > 0) {
        $("#startdate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#enddate').length > 0) {
        $("#enddate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_pembayaran_broker').length > 0) {
        $("#tanggal_pembayaran_broker").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_pembayaran_asuransi').length > 0) {
        $("#tanggal_pembayaran_asuransi").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if($('#isEdit').val()){
        $scope.idData = $("#isEdit").val();
        $scope.submitDataDebitur = true;
        getDokumenData();
    }

    if ($('#form-dokumen').length > 0) {
        $('#form-dokumen').find('input[type=file]').each(function(){
            var dokumen_type = $(this).attr("name");
            $(this).change(function(){
                $scope.uploadDok(dokumen_type);
            });
        });
    }

    if ($('#dok_lod').length > 0) {
        $("#dok_lod").change(function(){
            $scope.uploadDokLOD('dok_lod');
        });
    }

    /*
    if ($('#indetitas_peserta').length > 0) {
        $("#indetitas_peserta").change(function(){
            $scope.uploadDok('indetitas_peserta');
        });
    }

    if ($('#kartu_keluarga').length > 0) {
        $("#kartu_keluarga").change(function(){
            $scope.uploadDok('kartu_keluarga');
        });
    }

    if ($('#akad_kredit').length > 0) {
        $("#akad_kredit").change(function(){
            $scope.uploadDok('akad_kredit');
        });
    }

    if ($('#bukti_pencairan').length > 0) {
        $("#bukti_pencairan").change(function(){
            $scope.uploadDok('bukti_pencairan');
        });
    }

    if ($('#rekening_koran').length > 0) {
        $("#rekening_koran").change(function(){
            $scope.uploadDok('rekening_koran');
        });
    }

    if ($('#keterangan_pegawai').length > 0) {
        $("#keterangan_pegawai").change(function(){
            $scope.uploadDok('keterangan_pegawai');
        });
    }

    if ($('#keterangan_meninggal').length > 0) {
        $("#keterangan_meninggal").change(function(){
            $scope.uploadDok('keterangan_meninggal');
        });
    }

    if ($('#surat_tagihan').length > 0) {
        $("#surat_tagihan").change(function(){
            $scope.uploadDok('surat_tagihan');
        });
    }

    if ($('#slik_ojk').length > 0) {
        $("#slik_ojk").change(function(){
            $scope.uploadDok('slik_ojk');
        });
    }
    */
    if ($('#tanggal_kejadian').length > 0) {
        $("#tanggal_kejadian").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_kejadian').length > 0) {
        $("#tanggal_kejadian").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if($scope.isEdit != 0){

    }

    if ($('#nama_debitur').length > 0) {
        $('#nama_debitur').on('change',function () {
            var id = $(this).val();
            var urlDebitur = baseUrl+'klaim_peralihan/getDebitur?id='+id;

            $.ajax({
                type: 'GET',
                url: urlDebitur,
                success: function (response) {
                    console.log(data);
                    var data = response;
                    var plafond = data.plafond.split('.');
                    var premi = data.premi.replace('.',',');
                    var branch = data.kode_cabang + ' | - ';
                    if(data.branch_name){
                        branch = data.kode_cabang + ' | ' + data.branch_name;
                    }
                    
                    if(data){
                        $('#no_polis').val(data.no_polis);
                        $('#tanggal_mulai').val(data.tanggal_mulai);
                        $('#plafond').val(formatRupiah(plafond[0],'Rp. '));
                        $('#nama_cabang').val(branch);
                        $('#kode_cabang').val(data.kode_cabang);
                        $('#tanggal_akhir').val(data.tanggal_akhir);
                        $('#premi').val(formatRupiah(premi,'Rp. '));
                    }
                   
                },
                error: function (err) {
                    console.log(err);
                }
            });
            
        });
    }

    if ($('#klaim_dibayar_asuransi').length > 0) {
        $('#klaim_dibayar_asuransi').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#klaim_dibayar_broker').length > 0) {
        $('#klaim_dibayar_broker').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#os_pokok_klaim').length > 0) {
        $('#os_pokok_klaim').on('keyup', function(){
            let val = $('#os_pokok_klaim').val().replace(/\./g,"").replace(",",".");
            let val2 = $('#os_bunga_klaim').val().replace(/\./g,"").replace(",",".");
            console.log(val);
            console.log(val2);
            if(isNaN(parseFloat(val2))){
                val2 = 0;
            }

            let total = parseFloat(val) + parseFloat(val2);
            total = total.toString().replace(".",",");
            console.log(total);
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }

            $('#os_total_klaim').val(formatRupiah(total,'Rp. '));
        });
    }

    if ($('#os_bunga_klaim').length > 0) {
        $('#os_bunga_klaim').on('keyup', function(){
            let val = $('#os_bunga_klaim').val().replace(/\./g,"").replace(",",".");
            let val2 = $('#os_pokok_klaim').val().replace(/\./g,"").replace(",",".");
            console.log(val);
            console.log(val2);

            if(isNaN(parseFloat(val2))){
                val2 = 0;
            }

            let total = parseFloat(val) + parseFloat(val2);
            console.log(total);
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }

            $('#os_total_klaim').val(formatRupiah(total.toString().replace(".",","),'Rp. '));
        });
    }

    if ($('#list_table_klaim').length > 0) {
        var table = $("#list_table_klaim").DataTable({
            autoWidth: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            "ordering": false,
            ajax: {
                url: baseUrl+"klaim_peralihan/view_data",
                // url: "http://localhost:3301/bsi-mid-svc/penutupanDtl/getAllData",
                type: "POST",
                data: function ( data ) {
                    data.cabang_search = $('#cabang_search').val();
                    data.startdate = $('#startdate').val();
                    data.enddate = $('#enddate').val();
                    data.status_klaim_search = $('#status_klaim_search').val();
                }
            },
            lengthMenu: [
                [10, 20, 30, 50],
                [10, 20, 30, 50]
            ],
            deferRender: true,
            columns: [
                { data: null },
                { "data": "createdon", "className":"font12 text-center" },
                { "data": "tanggal_lapor", "className":"font12 text-center" },
                { "data": "nama", "className":"font12 text-center" },
                { "data": "asuransi", "className":"font12 text-center" },
                { "data": "kode_cabang", "className":"font12 text-center" },
                { "data": "tanggal_lahir", "className":"font12 text-center" },
                { "data": "tanggal_akad", "className":"font12 text-center" },
                { "data": "plafond", "className":"font12 text-center" },
                { "data": "premi", "className":"font12 text-center" },
                { "data": "os_total_klaim", "className":"font12 text-center" },
                { "data": "type_manfaat", "className":"font12 text-center" }
            ],
            columnDefs: [
                {
                    targets:0,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.c_id;
                        // console.log(check);
                        var enc = window.btoa(check);
                       
                        let urlDownload = baseUrl + headsegmentUrl+'edit?data=' + check;

                        return ` 
                                <a class="badge badge-success" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Edit Data" href = '`+urlDownload+`'> 
                                    <i class="fw-bold text-white fs-4 bi bi-pencil-square"></i> <span class="ms-2">Edit Data</span>
                                </a>
                                `;
                    },
                },
                {
                    targets:1,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var createdon = formatDate(row.createdon);
                            return ` 
                                    <p class="text-gray-800">`+createdon+`</p>
                                    `;
                    }
                },
                {
                    targets:2,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var tanggal_lapor = formatDate(row.tanggal_lapor);
                            return ` 
                                    <p class="text-gray-800">`+tanggal_lapor+`</p>
                                    `;
                    },
                },
                {
                    targets:6,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var tanggal_lahir = formatDate(row.tanggal_lahir);
                            return ` 
                                    <p class="text-gray-800">`+tanggal_lahir+`</p>
                                    `;
                    },
                },
                {
                    targets:7,
                    data: null,
                    className: 'text-center fw-bold fs-6',
                    render: function( data, type, row, meta ) {
                        var tanggal_akad = formatDate(row.tanggal_akad);
                            return ` 
                                    <p class="text-gray-800">`+tanggal_akad+`</p>
                                    `;
                    },
                },
                {
                    targets:8,
                    data: null,
                    className: 'text-center fw-bold fs-6',
                    render: function( data, type, row, meta ) {
                        var check = row.plafond;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:9,
                    data: null,
                    className: 'text-center fw-bold fs-6',
                    render: function( data, type, row, meta ) {
                        var check = row.premi;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:10,
                    data: null,
                    className: 'text-center fw-bold fs-6',
                    render: function( data, type, row, meta ) {
                        var check = row.os_total_klaim;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                }
                
                
            ],
        });
        $('.dataTables_filter input[type=search]').attr('placeholder', 'Cari Nama...');
        $('.dataTables_length select').select2({
            minimumResultsForSearch: "-1"
        });
    }

    // tableHis();
    
    // end of variable init //

    // Share Data Variabel initialize //
    
    $scope.shareData = [];
    stepper = document.querySelector('#kt_create_account_stepper');

    if ( stepper ) {
        form = stepper.querySelector('#kt_create_account_form');
        formSubmitButton = stepper.querySelector('[data-kt-stepper-action="submit"]');
        formContinueButton = stepper.querySelector('[data-kt-stepper-action="next"]');
    
        validations.push(FormValidation.formValidation(
            form,
            {
                fields: {
                    type_manfaat: {
                        validators: {
                            notEmpty: {
                                message: 'Jenis Klaim tidak boleh Kosong'
                            }
                        }
                    },
                    nama_debitur: {
                        validators: {
                            notEmpty: {
                                message: 'Nama Debitur tidak boleh Kosong'
                            }
                        }
                    },
                    os_total_klaim: {
                        validators: {
                            notEmpty: {
                                message: 'Outstanding Total Klaim tidak boleh kosong'
                            }
                        }
                    },
                    os_pokok_klaim: {
                        validators: {
                            notEmpty: {
                                message: 'Outstanding Pokok Klaim tidak boleh kosong'
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: '.fv-row',
                        eleInvalidClass: '',
                        eleValidClass: ''
                    })
                }
            }
        ));

        // Initialize Stepper
        stepperObj = new KTStepper(stepper);
    
        // Stepper change event
        stepperObj.on('kt.stepper.changed', function (stepper) {
            if (stepperObj.getCurrentStepIndex() === 4) {
                formSubmitButton.classList.add('d-none');
                formContinueButton.classList.add('d-none');
            } else if (stepperObj.getCurrentStepIndex() === 5) {
                formSubmitButton.classList.add('d-none');
                formContinueButton.classList.add('d-none');
            } else {
                formSubmitButton.classList.remove('d-inline-block');
                formSubmitButton.classList.remove('d-none');
                formContinueButton.classList.remove('d-none');
            }
        });

        // Validation before going to next page
        stepperObj.on('kt.stepper.next', function (stepper) {
            console.log('stepper.next');

            // Validate form before change stepper step
            var currStep = stepper.getCurrentStepIndex(); // get validator for currnt step
            console.log(currStep);
            if (currStep == '1') {
                
                if(!$scope.submitDataDebitur){
                    Swal.fire({
                        text: "Mohon submit Data Debitur terlebih Dahulu",
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });
                }else{
                    var jenisKlaim = $("#type_manfaat").val();
                    var pand = document.getElementById("dokumen-pand");
                    var kpp = document.getElementById("dokumen-kpp");
                    var phk = document.getElementById("progress_form_klaim");
                    console.log($scope.jenisKlaim);

                    if(jenisKlaim == 'PA+ND'){
                        pand.classList.remove('d-none');
                        kpp.classList.add('d-none')
                    }else if(jenisKlaim == 'KPP'){
                        pand.classList.add('d-none');
                        kpp.classList.remove('d-none')
                    }else if(jenisKlaim == 'PHK+KPP'){
                        pand.classList.add('d-none');
                        kpp.classList.remove('d-none')
                    }

                    stepper.goNext();

                    KTUtil.scrollTop();
                }
            } else {
                stepper.goNext();

                KTUtil.scrollTop();
            }
        });

        // Prev event
        stepperObj.on('kt.stepper.previous', function (stepper) {
            console.log('stepper.previous');

            stepper.goPrevious();
            KTUtil.scrollTop();
        });
    }

    function tableHis(){
        if ($('#history_table').length > 0) {
            var id_penutupan = window.btoa($('#id_penutupan').val());
            tableHistory = $("#history_table").DataTable({
                autoWidth: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: baseUrl+"rekonsiliasi/getHistoryRekon",
                    type: "POST",
                    data: function ( data ) {
                        data.id = id_penutupan;
                    }
                    
                },
                lengthMenu: [
                    [10, 20, 30, 50],
                    [10, 20, 30, 50]
                ],
                deferRender: true,
                columns: [
                    { data: null },
                    { "data": "tanggal_pembayaran", "className":"font12 text-center" },
                    { "data": "keterangan", "className":"font12 text-center" },
                    { data: null },
                    { data: null },
                    { "data": "status", "className":"font12 text-center" },
                    { "data": "createdby", "className":"font12 text-center" },
                    { "data": "createdon", "className":"font12 text-center" },
                    { "data": "updatedby", "className":"font12 text-center" },
                    { "data": "updatedon", "className":"text-center" }
                ],
                columnDefs: [
                    {
                        targets:0,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                             var check = row.c_id;
                            // var check2 = row.reffnumber;
                            // console.log(check);
                             var enc = window.btoa(check);
                            //  let urlDelete = baseUrl + headsegmentUrl+'delete?data=' + enc;
    
                            // let urlHis = baseUrl + headsegmentUrl+'history?data=' + enc ;
    
                            // let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;
    
                            return ` 
                                    <a class="ms-2 badge badge-success edit-data" data-bs-toggle="modal" data-bs-target="#modal_edit_rekon"  title="Edit/View Data"> 
                                        <i class="fw-bold fs-4 bi bi-pencil-square text-white"></i> <span class="ms-2 hover-animate">Edit</span>
                                    </a>
                                    
                                    <span class="badge badge-info delete-data" id="id;`+check+`" data-bs-toggle="modal" data-bs-target="#kt_modal_scrollable_1" title="Log Api"  > 
                                        <i class="fw-bold fs-4 bi bi-clock-history text-white"></i> <span class="ms-2 hover-animate">Delete</span>
                                    </span>
                                    `;
                        },
                    },
                    {
                        targets:1,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var tanggal_pembayaran = formatDate(row.tanggal_pembayaran);
                            return ` <p class="fw-bold fs-7 text-dark">`+tanggal_pembayaran+`</p>
                                    `;
                        },
                    },
                    {
                        targets:2,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.keterangan;
                            var name = '';

                            if(check == '1'){
                                name = 'Pembayaran Bank';
                            }else if(check == '2'){
                                name = 'Penarikan Bank';
                            }else if(check == '3'){
                                name = 'Pengembalian Fee Ujroh Dari Bank';
                            }else if(check == '4'){
                                name = 'Penarikan Fee Ujroh Dari Bank';
                            }

                            return name;
                        },
                    },
                    {
                        targets:3,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var nominal = row.nominal;
                            var jenis = row.keterangan;

                            if(jenis == '1' || jenis == '3'){
                                return parseFloat(nominal).toLocaleString('en-US');
                            }else{
                                return 0;
                            }
                        },
                    },
                    {
                        targets:4,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var nominal = row.nominal;
                            var jenis = row.keterangan;

                            if(jenis == '2' || jenis == '4'){
                                return parseFloat(nominal).toLocaleString('en-US');
                            }else{
                                return 0;
                            }
                        },
                    },
                    {
                        targets:5,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.status;
                            var name = '';

                            if(check == '1'){
                                name = 'Pembayaran Premi Sudah Sesuai';
                            }else if(check == '2'){
                                name = 'Lebih Bayar Premi';
                            }else if(check == '3'){
                                name = 'Kurang Bayar Premi';
                            }else if(check == '4'){
                                name = 'Pembayaran Fee Ujroh Sudah Sesuai';
                            }else if(check == '5'){
                                name = 'Lebih Bayar Fee Ujroh';
                            }else if(check == '6'){
                                name = 'Kurang Bayar Fee Ujroh';
                            }

                            return name;
                        },
                    },
                    {
                        targets:7,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var created_date = formatDate(row.createdon);
                            return ` <p class="fw-bold fs-7 text-dark">`+created_date+`</p>
                                    `;
                        },
                    },
                    {
                        targets:8,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.updatedby;
                            var returns = '-';

                            if(check){
                                returns = check
                            }

                            return returns;
                        },
                    },
                    {
                        targets:9,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.updatedon;
                            var returns = '-';

                            if(check){
                                returns = formatDate(check)
                            }

                            return returns;
                        },
                    },
                    
                ],
            });
            $('.dataTables_length select').select2({
                minimumResultsForSearch: "-1"
            });
          }
    };

    $scope.searchTable = function(){
        table.ajax.reload();
        $scope.$apply();
    }

    $scope.uploadDok = function(elem){
        var file = $("#"+elem)[0].files[0];
        var dataimg = new FormData();
        dataimg.append('c_id', $scope.idData);
        dataimg.append('file_to_upload',elem);
        dataimg.append( elem, file);

        console.log(file);
        console.log($("#"+elem)[0].files[0]);

        let fileUpload = document.getElementById(elem);
        var bar = $('#bar_form_klaim');
        var percent = $('#percent_form_klaim');
        var progress = document.getElementById("progress_form_klaim");

        $.ajax({
            url: baseUrl+"klaim_peralihan/uploadDokumen",
            type:"POST",
            contentType: false,
            cache: false,
            processData:false,
            data: dataimg,
            beforeSend: function(){
                fileUpload.disabled = true;
                blockUI.block();
            },
            success: function(rsp){
                fileUpload.disabled = false;
                blockUI.release();
                if(rsp.status == '00'){
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Oke",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    $('.dok-name').each(function(){
                        var name = this.getAttribute('data-dokumen-name');
                        // console.log(name);
                        var basedUrl = window.location.host;
                        if(name == elem){
                            var url = baseUrl + rsp.path + '/' + rsp.nama_dokumen;
                            var appendData = '<a class=" fs-7 fw-bold hover-animate" href="'+url+'" target="__blank" >'+rsp.nama_dokumen+'</a>';
                            $(this).empty();
                            $(this).append(appendData);
                        }
                     });
                    
                }else{
                    document.getElementById(elem).value = "";
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    // progress.classList.add('d-none');
                    fileUpload.disabled = false;
                }
                $scope.$apply();
            },
            error : function(err){
                blockUI.release();
                document.getElementById(elem).value = "";
                fileUpload.disabled = false;
                blockUI.release();
                Swal.fire({
                    text: 'Opps... submit data gagal mohon hubungi Administrator',
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
                $scope.$apply();
            }
        });
    }

    $scope.uploadDokLOD = function(elem){
        var file = $("#"+elem)[0].files[0];
        var dataimg = new FormData();
        var status_lod = $("#status_lod").val();

        let fileUpload = document.getElementById(elem);
        var progress = document.getElementById("progress_form_klaim");

        if(!status_lod){
            Swal.fire({
                text: 'Status LOD tidak boleh kosong',
                icon: "warning",
                buttonsStyling: false,
                confirmButtonText: "Ok, got it!",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });

            document.getElementById(elem).value = "";
                fileUpload.disabled = false;

            return false;
        }

        dataimg.append('c_id', $scope.idData);
        dataimg.append('file_to_upload',elem);
        dataimg.append('status_lod',status_lod);
        dataimg.append( elem, file);

        console.log(file);
        console.log($("#"+elem)[0].files[0]);

        $.ajax({
            url: baseUrl+"klaim_peralihan/uploadDokumenLod",
            type:"POST",
            contentType: false,
            cache: false,
            processData:false,
            data: dataimg,
            beforeSend: function(){
                fileUpload.disabled = true;
                blockUI.block();
            },
            success: function(rsp){
                fileUpload.disabled = false;
                blockUI.release();
                if(rsp.status == '00'){
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Oke",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    $('.dok-name').each(function(){
                        var name = this.getAttribute('data-dokumen-name');
                        // console.log(name);
                        var basedUrl = window.location.host;
                        if(name == elem){
                            var url = baseUrl + rsp.path + '/' + rsp.nama_dokumen;
                            var appendData = '<a class=" fs-7 fw-bold hover-animate" href="'+url+'" target="__blank" >'+rsp.nama_dokumen+'</a>';
                            $(this).empty();
                            $(this).append(appendData);
                        }
                     });
                    
                }else{
                    document.getElementById(elem).value = "";
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    progress.classList.add('d-none');
                    fileUpload.disabled = false;
                }
                $scope.$apply();
            },
            error : function(err){
                blockUI.release();
                document.getElementById(elem).value = "";
                fileUpload.disabled = false;
                blockUI.release();
                Swal.fire({
                    text: 'Opps... submit data gagal mohon hubungi Administrator',
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
                $scope.$apply();
            }
        });
    }

    $('body').on('click', '#submitFormPembayaran', function(){
        var validator = validations[1];
        var dataimg = new FormData();
        var file = $("#bukti_bayar")[0].files[0];
        var tanggal_pembayaran_asuransi = $("#tanggal_pembayaran_asuransi").val();
        var klaim_dibayar_asuransi = $("#klaim_dibayar_asuransi").val();
        var tanggal_pembayaran_broker = $("#tanggal_pembayaran_broker").val();
        var klaim_dibayar_broker = $("#klaim_dibayar_broker").val();
        dataimg.append('c_id', $scope.idData);
        dataimg.append('file_to_upload','bukti_bayar');
        dataimg.append('tanggal_pembayaran_asuransi',tanggal_pembayaran_asuransi);
        dataimg.append('klaim_dibayar_asuransi',klaim_dibayar_asuransi);
        dataimg.append('tanggal_pembayaran_broker',tanggal_pembayaran_broker);
        dataimg.append('klaim_dibayar_broker',klaim_dibayar_broker);
        dataimg.append( 'bukti_bayar', file);

        console.log('validated!');
            $.ajax({
                type: 'POST',
                url: baseUrl+'klaim_peralihan/updatePembayaran',
                data: dataimg,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    blockUI.block();
                },
                success: function(rsp){
                    blockUI.release();
                    if(rsp.status == '00'){
                        $scope.idData = rsp.id;
                        Swal.fire({
                            text: rsp.keterangan,
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Oke",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                        
                    }else{
                        
                        Swal.fire({
                            text: rsp.keterangan,
                            icon: "warning",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                        
                    }
                    $scope.$apply();
                },
                error : function(err){
                    blockUI.release();
                    Swal.fire({
                        text: 'Opps... submit data gagal mohon hubungi Administrator',
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                    $scope.$apply();
                }
            });
    });


    $('body').on('click', '#submitFormDebitur', function(){
        var validator = validations[0];
        let formData = new FormData($("#kt_create_account_form")[0]);
        var path = 'klaim_peralihan/submitDataDebitur';
        var redirect = true;
        if (validator) {
            validator.validate().then(function (status) {
                console.log('validated!');
                if(isEdit){
                    path = 'klaim_peralihan/editDataDebitur';
                    redirect = false;
                }
                if (status == 'Valid') {
                    $.ajax({
                        type: 'POST',
                        url: baseUrl + path,
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function(){
                            submitButton.setAttribute('data-kt-indicator', 'on');
                            submitButton.disabled = true;
                            blockUI.block();
                        },
                        success: function(rsp){
                            submitButton.removeAttribute('data-kt-indicator');
                            submitButton.disabled = false;
                            blockUI.release();
                            if(rsp.status == '00'){
                                $scope.submitDataDebitur = true;
                                $scope.idData = rsp.id;
                                Swal.fire({
                                    text: rsp.keterangan,
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Oke",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                                if(!$('#isEdit').val()){
                                    submitButton.classList.add('d-none');
                                    printKlaim.classList.remove('d-none');
                                }

                                if(redirect){
                                    location.href =  baseUrl+ 'klaim_peralihan/edit?data='+rsp.id;
                                }
                                
                            }else{
                              
                                Swal.fire({
                                    text: rsp.keterangan,
                                    icon: "warning",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                                if(!$('#isEdit').val()){
                                    $scope.submitDataDebitur = false;
                                    submitButton.classList.remove('d-none');
                                    printKlaim.classList.add('d-none');
                                }
                                
                            }
                            $scope.$apply();
                        },
                        error : function(err){
                            if(!$('#isEdit').val()){
                                $scope.submitDataDebitur = false;
                                submitButton.classList.remove('d-none');
                                printKlaim.classList.add('d-none');
                            }

                            submitButton.classList.remove('d-none');
                            printKlaim.classList.add('d-none');
                            submitButton.removeAttribute('data-kt-indicator');
                            submitButton.disabled = false;
                            blockUI.release();
                            Swal.fire({
                                text: 'Opps... submit data gagal mohon hubungi Administrator',
                                icon: "warning",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                            $scope.$apply();
                        }
                    });
                } else {
                    $scope.submitDataDebitur = false;
                    $scope.jenisKlaim = "";
                    Swal.fire({
                        text: "Mohon lengkapi field data Debitur",
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-light"
                        }
                    }).then(function () {
                        KTUtil.scrollTop();
                    });

                    $scope.$apply();
                }
            });
        } else {

            KTUtil.scrollTop();
        }


    });

    $('body').on('click', '#history_table .edit-data', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#history_table").dataTable().fnGetPosition(tr);
        var data = $("#history_table").dataTable().fnGetData(rowIndex);
        let jenis_bayar = data.keterangan;
        let nominal = formatRupiah(data.nominal, 'Rp');
        let tanggal_bayar = data.tanggal_pembayaran;

        $("#jenis_bayar_edit").val(jenis_bayar).trigger('change');
        $("#jumlah_bayar_edit").val(nominal);
        $("#tanggal_bayar_edit").val(tanggal_bayar);
        $("#c_id").val(data.c_id);
    });

    $scope.submitData = function(){
        let formData = new FormData($("#rekonsiliasi_form")[0]);
        $.ajax({
            type: 'POST',
            url: baseUrl+'rekonsiliasi/submitData',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                submitButton.setAttribute('data-kt-indicator', 'on');
                submitButton.disabled = true;
                blockUI.block();
            },
            success: function(rsp){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                if(rsp.status == '00'){
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Oke",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    // tableHistory.ajax.reload();
                    // clearForm();

                    location.href = rsp.login_referer;

                }else{
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }

            },
            error : function(err){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                Swal.fire({
                    text: 'Opps... submit data gagal mohon hubungi Administrator',
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });

            }
        });
    }

    $scope.submitDataEdit = function(){
        let formData = new FormData($("#rekonsiliasi_form_edit")[0]);
        $.ajax({
            type: 'POST',
            url: baseUrl+'rekonsiliasi/editData',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                submitButton.setAttribute('data-kt-indicator', 'on');
                submitButton.disabled = true;
                blockUI.block();
            },
            success: function(rsp){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                if(rsp.status == '00'){
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Oke",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    tableHistory.ajax.reload();
                    clearForm();

                }else{
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }

            },
            error : function(err){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                Swal.fire({
                    text: 'Opps... submit data gagal mohon hubungi Administrator',
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });

            }
        });
    }

    function monthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    function getActivityData(){
        var data = no_app;
        if ($('#history_table').length > 0) {
            $.ajax({
                type: 'GET',
                url: baseUrl+'penutupan/activity_data?data='+data,
                success: function (response) {
                    $scope.dataActivity = response;

                    $scope.$apply();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function getDokumenData(){
        $.ajax({
            type: 'GET',
            url: baseUrl+'klaim_peralihan/getDokumenData?id='+$scope.idData,
            success: function (response) {
                $scope.dokData = response;

                $('.dok-name').each(function(){
                    var name = this.getAttribute('data-dokumen-name');
                    // console.log(name);
                    var basedUrl = window.location.host;
                    for (let index = 0; index < response.length; index++) {
                        const element = response[index];
                        if(name == element.type_dokumen){
                            var url = baseUrl + element.path + '/' + element.nama_dokumen;
                            var appendData = '<a class="fs-7 fw-bold hover-animate" href="'+url+'" target="__blank" >'+element.nama_dokumen+'</a>';
                            $(this).append(appendData);
                        }
                    }
                 });

                $scope.$apply();
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function formatRupiah(angka, prefix)
    {
        if(parseFloat(angka) > 0){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split    = number_string.split(','),
                sisa     = split[0].length % 3,
                rupiah     = split[0].substr(0, sisa),
                ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
                
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah ;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }else{
            return 0;
        }
    }

    function getSelect2data(url,elm){
        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                console.log(data);
                var data = response;
                $('#' + elm + '').find('option').remove();
                $('#' + elm + '').append('<option value =""></option>');
                if(data){
                    for (i = 0; i < data.length; i++) {
                        var vals = '';
                        if(elm == 'produk'){
                            vals = data[i].nama_produk;
                        }else{
                            vals = data[i].nama;
                        }
                        $('#' + elm + '').append('<option class="font12" value="' + data[i].c_id + '">' + vals + '</option>');
                    }
                }
               
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    $scope.backToTable = function(){
       var data = []
       $scope.detailData = data;
       $scope.showView = false;
       $scope.showTable = true;
    };

    $scope.postData = function(){
        Swal.fire({ 
            title: "Are you sure?", 
            text: "You won't be able to revert this", 
            type: "warning", 
            showCancelButton: !0, 
            confirmButtonColor: "#FF7400", 
            cancelButtonColor: "#d33", 
            confirmButtonText: "Yes, Post Data", 
            confirmButtonClass: "btn btn-blue-simple", 
            cancelButtonClass: "btn btn-danger ml-1", 
            buttonsStyling: !1 })
    .then(
            function (t) { 
                console.log(t.value);
                    $scope.postingData(); 
                }) 
    };

    function typeCoverageInit(){
        if($('#coverage_fee_mitra_row').length > 0){
            $('#coverage_fee_mitra_row').hide();
            $('#coverage_asuransi_row').hide();
            $('#coverage_pinalti_row').hide();

            $('#coverage_fee_mitra').val('0');
            $('#coverage_asuransi').val('0');
            $('#coverage_pinalti').val('0');
        }
    };

    function formatDate(d) 
        {
          var date = new Date(d);

         if ( isNaN( date .getTime() ) ) 
         {
            return d;
         }
         else
        {
          
          var month = new Array();
          month[0] = "Jan";
          month[1] = "Feb";
          month[2] = "Mar";
          month[3] = "Apr";
          month[4] = "May";
          month[5] = "Jun";
          month[6] = "Jul";
          month[7] = "Aug";
          month[8] = "Sept";
          month[9] = "Oct";
          month[10] = "Nov";
          month[11] = "Dec";

          day = date.getDate();
          
          if(day < 10)
          {
             day = "0"+day;
          }
          
          return    day  + " " +month[date.getMonth()] + " " + date.getFullYear();
          }
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
      }

    function clearForm(){
        $('#tanggal_bayar').val();
        $('#jumlah_bayar').val('0');
        $('#jenis_bayar').val('').trigger("change");
    }
     
  });