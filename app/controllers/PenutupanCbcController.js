mainApp.controller('PenutupanCbcController', function ($scope, $rootScope){

    // Data initialize //
    //getSelect2Data("penutupan/getInsuranceData", "asuransi",null, "Asuransi");
    // end of data initialize //

   // Local variable initialize //

    let baseUrl = $("#url").val();
    let segmentUrl = $("#segment").val();
    let headsegmentUrl = $("#headsegment").val();
    let no_app = $("#no_app").val();
    var form = document.getElementById('penutupan_input');
    var table = $("#list_table");
    const submitButton = document.getElementById('submitForm');
    

  
    $scope.totalItems = 0;
    $scope.showTable = true;
    $scope.showView = false;
    $scope.showSuccessUpload = false;
    $scope.showShareData = false;
    $scope.testDetailShared = {
        data : []
    }

    $scope.response = 'test';
    $scope.request = 'test';
    
    $scope.dataActivity = [];
    $scope.dataJenisPekerjaan = [];
    $scope.insurance = [];

    if ($('#startdate').length > 0) {
        $("#startdate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#enddate').length > 0) {
        $("#enddate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_akad').length > 0) {
        $("#tanggal_akad").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_mulai').length > 0) {
        $("#tanggal_mulai").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_akhir').length > 0) {
        $("#tanggal_akhir").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_lahir').length > 0) {
        $("#tanggal_lahir").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_bayar_ijk').length > 0) {
        $("#tanggal_bayar_ijk").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    $('#plafond').on('keyup', function(){
        // let val = $('#total_klaim_bayar').val()
        if($(this).val()){
            $(this).val(formatRupiah($(this).val(),'Rp. '));
        }
    });

    $('#nilai_agunan').on('keyup', function(){
        // let val = $('#total_klaim_bayar').val()
        if($(this).val()){
            $(this).val(formatRupiah($(this).val(),'Rp. '));
        }
    });

    $('#nilai_pengajuan').on('keyup', function(){
        // let val = $('#total_klaim_bayar').val()
        if($(this).val()){
            $(this).val(formatRupiah($(this).val(),'Rp. '));
        }
    });

    $('#coverage_fee_mitra').on('keyup', function(){
        // let val = $('#total_klaim_bayar').val()
        if($(this).val()){
            $(this).val(formatRupiah($(this).val(),'Rp. '));
        }
    });

    $('#coverage_asuransi').on('keyup', function(){
        // let val = $('#total_klaim_bayar').val()
        if($(this).val()){
            $(this).val(formatRupiah($(this).val(),'Rp. '));
        }
    });

    $('#coverage_pinalti').on('keyup', function(){
        // let val = $('#total_klaim_bayar').val()
        if($(this).val()){
            $(this).val(formatRupiah($(this).val(),'Rp. '));
        }
    });
    

    // end of variable init //

    // Share Data Variabel initialize //
    
    $scope.shareData = [];
    tableHis();
    getActivityData();
    
    // end of variable init //

    $('#produk').on('change',function () {
        var id = $(this).val();
        console.log(id);
        var urlJenisPekerjaan = baseUrl+'penutupan/getJenisPekerjaan?id='+id
        var urlTypeProduk = baseUrl+'penutupan/getTypeProduk?id='+id
        var urltypeManfaat = baseUrl+'penutupan/getTypeManfaat?id='+id

        getSelect2data(urlJenisPekerjaan,'jenis_pekerjaan');
        getSelect2data(urlTypeProduk,'tipe_produk');
        getSelect2data(urltypeManfaat,'type_manfaat');
    });

    $('#kode_area').on('change',function () {
        var id = $(this).val();
        console.log(id);
        var urlCabang = baseUrl+'penutupan/getCabang?id='+id

        getSelect2data(urlCabang,'kode_cabang');
    });

    $('#area_search').on('change',function () {
        var id = $(this).val();
        console.log(id);
        var urlCabang = baseUrl+'penutupan/getCabang?id='+id

        getSelect2data(urlCabang,'cabang_search');
    });
    
    if ($('#list_table').length > 0) {
        var table = $("#list_table").DataTable({
            autoWidth: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: baseUrl+"penutupancbc/view_data",
                type: "POST",
                data: function ( data ) {
                    data.startdate = $('#startdate').val();
                    data.enddate = $('#enddate').val();
                    data.cabang_search = $('#cabang_search').val();
                    data.area_search = $('#area_search').val();
                    data.asuransi_search = $('#asuransi_search').val();
                    data.status_data_search = $('#status_data_search').val();
                },
                complete: function(rsp){
                    summaryData = rsp.responseJSON.summary;
                    // console.log(rsp.responseJSON.summary);
                    var totalPremi = summaryData.sumPremi.toString().replace('.',',')
                    var totalPlafond = summaryData.sumPlafond.toString().replace('.',',')
                    var countDeb = summaryData.countDeb.toString().replace('.',',')

                    $("#jumlah_deb").text(formatRupiah(countDeb,'Rp. '));
                    $("#total_plafond").text(formatRupiah(totalPlafond,'Rp. '));
                    $("#total_premi").text(formatRupiah(totalPremi,'Rp. '));
                }
            },
            lengthMenu: [
                [5, 10, 20, 50],
                [5, 10, 20, 50]
            ],
            deferRender: true,
            columns: [
                { data: null },
                { data: null },
                { "data": "polis", "className":"font12 text-center" },
                { "data": "no_aplikasi", "className":"font12 text-center" },
                { "data": "nama", "className":"font12 text-center" },
                { "data": "nama_asuransi", "className":"font12 text-center" },
                { "data": "nama_cabang", "className":"font12 text-center" },
                { "data": "nama_produk", "className":"font12 text-center" },
                { "data": "tipe_produk", "className":"font12 text-center" },
                { "data": "atribusi", "className":"font12 text-center" },
                { "data": "bulan_tenor", "className":"text-center" },
                { "data": "plafond", "className":"text-center" },
                { "data": "coverage_fee_mitra", "className":"text-center" },
                { "data": "coverage_asuransi", "className":"text-center" },
                { "data": "coverage_pinalti", "className":"text-center" },
                { "data": "premi", "className":"text-center" },
                { "data": "premi_before_admin", "className":"text-center" },
                { "data": "tanggal_akad", "className":"text-center" },
                { "data": "nomor_akad", "className":"text-center" },
                { "data": "tanggal_lahir", "className":"text-center" },
                { "data": "usia_saat_akad", "className":"text-center" },
                { "data": "usia_selesai_akad", "className":"text-center" }
            ],
            columnDefs: [
                {
                    targets:1,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.no_aplikasi;
                        console.log(check);
                        var enc = window.btoa(check);
                        let urlEdit = baseUrl + headsegmentUrl+'edit?data=' + enc;

                        let urlHis = baseUrl + headsegmentUrl+'history?data=' + enc ;

                        let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;

                        return ` 
                                <a class="ms-2 badge badge-success" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Edit/View Data" href = '`+urlEdit+`'> 
                                    <i class="fw-bold fs-4 bi bi-pencil-square text-white"></i> <span class="ms-2 hover-animate">Edit</span>
                                </a>
                                
                                <a class="badge badge-info" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="History Data" href = '`+urlHis+`'> 
                                    <i class="fw-bold fs-4 bi bi-clock-history text-white"></i> <span class="ms-2 hover-animate">History</span>
                                </a>
                                `;
                    },
                },
                {
                    targets:0,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.no_aplikasi;
                        console.log(check);
                        var enc = window.btoa(check);
                       
                        let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;

                        return ` 
                                <a target="__blank" class="badge badge-primary" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Download CN" href = '`+urlDownload+`'> 
                                    <i class="fw-bold text-white fs-4 bi bi-download"></i> <span class="ms-2 hover-animate">Download KPA</span>
                                </a>
                                `;
                    },
                },
                {
                    targets:2,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.polis;
                        console.log(check);
                        if(!check){
                            return ` 
                                    <a target="__blank" class="badge badge-warning" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse"> 
                                        <i class="fw-bold text-dark fs-4 bi bi-exclamation-square"></i> <span class="ms-2 text-dark">On Proses</span>
                                    </a>
                                    `;
                        }
                    },
                },
                {
                    targets:3,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var count = row.count_deb;
                        var check = row.no_aplikasi;
                        var date = row.createdon;
                        console.log(check);
                            return ` 
                                    <p>`+check+`<span class="fw-bold text-info fs-7"> (`+count+`)</span></p>
                                    <p class="fw-bold fs-7 text-dark">`+new Date(date).toLocaleString()+`</p>
                                    `;
                    },
                },
                {
                    targets:11,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.plafond;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:12,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.coverage_fee_mitra;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:13,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.coverage_asuransi;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:14,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.coverage_pinalti;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:15,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.premi;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:16,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.premi_before_admin;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                
                
            ],
        });
        $('.dataTables_filter input[type=search]').attr('placeholder', 'Cari no aplikasi...');
        $('.dataTables_length select').select2({
            minimumResultsForSearch: "-1"
        });
    }

    function tableHis(){
        if ($('#history_table').length > 0) {
            var tablehis = $("#history_table").DataTable({
                autoWidth: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: baseUrl+"penutupancbc/history_data",
                    type: "POST",
                    data: function ( data ) {
                        data.data = no_app;
                        data.enddate = $('#enddate').val();
                        data.cabang = $('#cabang').val();
                    }
                },
                lengthMenu: [
                    [5, 10, 20, 50],
                    [5, 10, 20, 50]
                ],
                deferRender: true,
                columns: [
                    { data: null },
                    { data: null },
                    { "data": "polis", "className":"font12 text-center" },
                    { "data": "no_aplikasi", "className":"font12 text-center" },
                    { "data": "reffnumber", "className":"font12 text-center" },
                    { "data": "nama", "className":"font12 text-center" },
                    { "data": "nama_asuransi", "className":"font12 text-center" },
                    { "data": "nama_cabang", "className":"font12 text-center" },
                    { "data": "nama_produk", "className":"font12 text-center" },
                    { "data": "tipe_produk", "className":"font12 text-center" },
                    { "data": "atribusi", "className":"font12 text-center" },
                    { "data": "bulan_tenor", "className":"text-center" },
                    { "data": "plafond", "className":"text-center" },
                    { "data": "coverage_fee_mitra", "className":"text-center" },
                    { "data": "coverage_asuransi", "className":"text-center" },
                    { "data": "coverage_pinalti", "className":"text-center" },
                    { "data": "premi", "className":"text-center" },
                    { "data": "premi_before_admin", "className":"text-center" },
                    { "data": "tanggal_akad", "className":"text-center" },
                    { "data": "nomor_akad", "className":"text-center" },
                    { "data": "tanggal_lahir", "className":"text-center" },
                    { "data": "usia_saat_akad", "className":"text-center" },
                    { "data": "usia_selesai_akad", "className":"text-center" }
                ],
                columnDefs: [
                    {
                        targets:1,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.no_aplikasi;
                            console.log(check);
                            var enc = window.btoa(check);
                            let urlEdit = baseUrl + headsegmentUrl+'edit?data=' + enc;
    
                            let urlHis = baseUrl + headsegmentUrl+'history?data=' + enc ;
    
                            let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;
    
                            return ` 
                                    <a class="ms-2 badge badge-success" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Edit/View Data" href = '`+urlEdit+`'> 
                                        <i class="fw-bold fs-4 bi bi-pencil-square text-white"></i> <span class="ms-2 hover-animate">Edit</span>
                                    </a>
                                    
                                    <a class="badge badge-info" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="History Data" href = '`+urlHis+`'> 
                                        <i class="fw-bold fs-4 bi bi-clock-history text-white"></i> <span class="ms-2 hover-animate">History</span>
                                    </a>
                                    `;
                        },
                    },
                    {
                        targets:0,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.no_aplikasi;
                            console.log(check);
                            var enc = window.btoa(check);
                           
                            let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;
    
                            return ` 
                                    <a target="__blank" class="badge badge-primary" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Download CN" href = '`+urlDownload+`'> 
                                        <i class="fw-bold text-white fs-4 bi bi-download"></i> <span class="ms-2 hover-animate">Download KPA</span>
                                    </a>
                                    `;
                        },
                    },
                    {
                        targets:2,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.polis;
                            console.log(check);
                            if(!check){
                                return ` 
                                        <a target="__blank" class="badge badge-warning" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse"> 
                                            <i class="fw-bold text-dark fs-4 bi bi-exclamation-square"></i> <span class="ms-2 text-dark">On Proses</span>
                                        </a>
                                        `;
                            }
                        },
                    },
                    {
                        targets:11,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.plafond;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:12,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.coverage_fee_mitra;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:13,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.coverage_asuransi;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:14,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.coverage_pinalti;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:15,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.premi;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:16,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.premi_before_admin;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    
                ],
            });
            $('.dataTables_length select').select2({
                minimumResultsForSearch: "-1"
            });
          }
    };

    $scope.searchTable = function(){
        table.ajax.reload();
        $scope.$apply();
    }
    
    $scope.modalActivity = function(event, id){
        event.preventDefault();
        console.log(id);

        $("#request-activity").text('');
        $("#reponse-activity").text('');

        for (let index = 0; index < $scope.dataActivity.length; index++) {
            const element = $scope.dataActivity[index];

            if(element.c_id == id){
                $("#request-activity").text(JSON.stringify(JSON.parse(element.datanew),undefined,2));
                $("#response-activity").text(JSON.stringify(JSON.parse(element.response),undefined,2));
                // console.log($scope.response);
                // console.log($scope.request);
                
                
            }
            
        }
    }

    $scope.submitData = function(){
        let formData = new FormData($("#penutupancbc_form")[0]);
        $.ajax({
            type: 'POST',
            url: baseUrl+'penutupancbc/update',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                submitButton.setAttribute('data-kt-indicator', 'on');
                submitButton.disabled = true;
            },
            success: function(rsp){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;

                if(rsp.status == '00'){
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Oke",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    if (baseUrl) {
                        location.href = baseUrl + 'penutupan';
                    }

                }else{
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }

            },
            error : function(err){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;

                Swal.fire({
                    text: 'Opps... submit data gagal mohon hubungi Administrator',
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });

            }
        });
    }

    function getActivityData(){
        var data = no_app;
        if ($('#history_table').length > 0) {
            $.ajax({
                type: 'GET',
                url: baseUrl+'penutupan/activity_data?data='+data,
                success: function (response) {
                    $scope.dataActivity = response;

                    $scope.$apply();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function formatRupiah(angka, prefix)
    {
        if(parseFloat(angka) > 0){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split    = number_string.split(','),
                sisa     = split[0].length % 3,
                rupiah     = split[0].substr(0, sisa),
                ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
                
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah ;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }else{
            return 0;
        }
    }

    function getSelect2data(url,elm){
        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                console.log(data);
                var data = response;
                $('#' + elm + '').find('option').remove();
                $('#' + elm + '').append('<option value =""></option>');
                if(data){
                    for (i = 0; i < data.length; i++) {
                        $('#' + elm + '').append('<option class="font12" value="' + data[i].c_id + '">' + data[i].nama + '</option>');
                    }
                }
               
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    $("#fileInput").change(function(){
        var allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','text/plain'];
        var file = this.files[0];
        var fileType = file.type;
        console.log(file.type);
        if(!allowedTypes.includes(fileType)){
            //alertify.error('Please select a valid file (PDF/DOC/DOCX/TXT).');
            toastr.error(
                            "Please select a valid file (PDF/DOC/DOCX/TXT).",
                            "Warning",
                            {
                                closeButton:!0,
                                tapToDismiss:!1,
                                progressBar:!0
                            }
                        );
            $("#fileInput").val('');
            return false;
        }
    });

    $scope.backToTable = function(){
       var data = []
       $scope.detailData = data;
       $scope.showView = false;
       $scope.showTable = true;
    };

    $scope.postData = function(){
        Swal.fire({ 
            title: "Are you sure?", 
            text: "You won't be able to revert this", 
            type: "warning", 
            showCancelButton: !0, 
            confirmButtonColor: "#FF7400", 
            cancelButtonColor: "#d33", 
            confirmButtonText: "Yes, Post Data", 
            confirmButtonClass: "btn btn-blue-simple", 
            cancelButtonClass: "btn btn-danger ml-1", 
            buttonsStyling: !1 })
    .then(
            function (t) { 
                console.log(t.value);
                    $scope.postingData(); 
                }) 
    };
  
    
     
  });