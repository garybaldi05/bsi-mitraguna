mainApp.controller('KlaimController', function ($scope, $rootScope){
    
    getSelect2Data("penutupan/getInsuranceData", "asuransi",null, "Asuransi");

    $scope.data = [];
    $scope.pageNumber = 1;
    $scope.libraryTemp = {};
    $scope.totalItemsTemp = {};
  
    $scope.totalItems = 0;
    $scope.showTable = true;
    $scope.showView = false;
    $scope.pdfDtlView = false;
    $scope.detailData = [];
    
    let baseUrl = $("#url").val();

    $rootScope.menu = {
        Dashboard : '',
        Penutupan : '',
        Klaim : 'active'
    };

    $('#asuransi').select2({
        allowClear: true,
        placeholder: "Pilih Asuransi",
        class: "font12",
    });

    $('#cabang').select2({
        allowClear: true,
        placeholder: "Pilih Cabang",
        class: "font12",
    });

    $('#statusdok').select2({
        allowClear: true,
        placeholder: "Pilih Status Dokumen",
        class: "font12",
    });

    $('#tunggakan_biaya').on('keyup', function(){
        calculateSum();
    });

    $('#tunggakan_denda').on('keyup', function(){
        calculateSum();
    });

    $('#tunggakan_bunga').on('keyup', function(){
        calculateSum();
    });

    $("#statusdok").on('change',function(){
        if($(this).val() == '4'){
            $('#tunggakan_denda').prop('disabled', false);
            $('#tunggakan_bunga').prop('disabled', false);
            $("#tunggakan_biaya").prop('disabled', false);
            $("#tanggal_bayar").prop('disabled', false);
        }else{
            $('#tunggakan_denda').val('0');
            $('#tunggakan_bunga').val('0');
            $("#tunggakan_biaya").val('0');
            $("#tanggal_bayar").val('');

            clearForm();
           
        }
    });

    clearForm();

    var table = $("#klaim-table").DataTable({
        autoWidth: true,
        stateSave: true,
        processing: true,
        serverSide: true,
        ordering: false,
        responsive:true,
        scrollX: true,
        ajax: {
            url: baseUrl+"klaim/view_data",
            type: "POST",
            data: function ( data ) {
                data.startdate = $('#startdate').val();
                data.enddate = $('#enddate').val();
                data.cabang = $('#cabang').val();
                data.asuransi = $('#asuransi').val();
            }
        },
        lengthMenu: [
            [5, 10, 20, 50],
            [5, 10, 20, 50]
        ],
        deferRender: true,
        "columns": [
            { "data": "id_klaim", "className": "not-show" },
            { "data": "id_klaim", "className":"font12 text-center",
                    "render": 
                    function( data, type, row, meta ) {
                        return '<a href="javascript:void(0);" class="btn btn-sm btn-orange text-white me-2 btn-view"><i class="far fa-eye me-1"></i> View</a>';
                    }
                },
            { "data": "nama", "className":"font12 text-center" },
            { "data": "norek_pinjam", "className":"font12 text-center" },
            { "data": "no_pk", "className":"font12 text-center" },
            { "data": "max_kredit", "className":"text-center" },
            { "data": "baki_debet", "className":"text-center" },
            { "data": "tunggakan_biaya", "className":"text-center" },
            { "data": "tunggakan_bunga", "className":"text-center" },
            { "data": "tunggakan_denda", "className":"text-center" },
            { "data": "klaim_dibayar_asuransi", "className":"text-center" },
            { "data": "tanggal_bayar_asuransi", "className":"text-center" },
            { "data": "asuransi", "className":"text-center" },
            { "data": "jangka_waktu", "className":"text-center" },
            { "data": "kolek", "className":"text-center" },
            { "data": "ktp", "className":"text-center" },
            { "data": "tanggal_lahir", "className":"text-center" },
            { "data": "cabang", "className":"text-center" },
            
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Cari no rek pinjam...');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });

    $scope.swall = function() {
        Swal.fire({
            title: "Confirmation",
            text: "Are you sure want to leave this page and lose your changes?",
            icon: "warning"
        })
        .then((willDelete) => {
            if (willDelete) {
            }
        });
    };

    $scope.formData = function(){
        $scope.showTable = true;
        $scope.showView = false;
        $scope.showUpload = false;
        table.ajax.reload();
        $scope.$apply();

    }

    $scope.formUpload = function(){
        $scope.showTable = false;
        $scope.showView = false;
        $scope.showUpload = true;

    }

    $scope.uploadData = function(){
        let formData = new FormData($("#uploadForm")[0]);
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    // if (evt.lengthComputable) {
                    //     var percentComplete = ((evt.loaded / evt.total) * 100);
                    //     $(".progress-bar").width(percentComplete + '%');
                    //     $(".progress-bar").html(percentComplete+'%');
                    // }
                }, false);
                return xhr;
            },
            type: 'POST',
            url: baseUrl+'/klaim/uploadData',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $(".progress-bar").width('0%');
                $(".modal-loading").show();
            },
            error:function(){
                $(".modal-loading").hide();
                toastr.error(
                    "Upload Error.",
                    "Warning",
                    {
                        closeButton:!0,
                        tapToDismiss:!1,
                        progressBar:!0
                    }
                );
                console.log('error');
            },
            success: function(resp){
                $(".modal-loading").hide();
                if(resp.status == '00'){
                    $('#uploadForm')[0].reset();
                    toastr.success(
                        "Upload Success.",
                        "Success",
                        {
                            closeButton:!0,
                            tapToDismiss:!1,
                            progressBar:!0
                        }
                    );
                }else{
                    $(".modal-loading").hide();
                    toastr.error(
                        "Upload Error.",
                        "Warning",
                        {
                            closeButton:!0,
                            tapToDismiss:!1,
                            progressBar:!0
                        }
                    );
                }
            }
        });
    }

    $('body').on('click', '#klaim-table_wrapper .btn-view', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#klaim-table").dataTable().fnGetPosition(tr);
        var data = $("#klaim-table").dataTable().fnGetData(rowIndex);
        let IdEdit = data.id_klaim;
        $scope.editform = false;
        $scope.viewform = true;
        $('#card-body').hide();
        
        $.ajax({
            type: 'POST',
            url: baseUrl+'klaim/getDatabyId',
            data:{
                id : IdEdit
            },
            success: function (response) {
                $scope.showTable = false;
                $scope.showView = true;
                var data = response;
                console.log(response);
                $scope.detailData = data;

                var total_klaim = parseFloat(data.tunggakan_biaya) + parseFloat(data.tunggakan_denda) + parseFloat(data.tunggakan_bunga);
                var pdfDtl =  baseUrl + data.dirfile + "_DTL.pdf";
                var pdfRk =  baseUrl + data.dirfile + "_RK.pdf";

                var newdates = '';
                if(data.tanggal_bayar_asuransi){
                    var tglbayar = data.tanggal_bayar_asuransi.split("-");
                    newdates = tglbayar[2] + '-' + tglbayar[1] + '-' +tglbayar[0];
                }
                $("#statusdok").val(data.status).trigger("change");
                $("#tanggal_bayar").val(newdates);
                $("#tunggakan_biaya").val(data.tunggakan_biaya);
                $("#tunggakan_denda").val(data.tunggakan_denda);
                $("#tunggakan_bunga").val(data.tunggakan_bunga);
                $("#keterangan").val(data.keterangan);
                $("#total_klaim").val(total_klaim);

                $('#pdf-view-detail').attr('src',pdfDtl);
                $('#pdf-view-rk').attr('src',pdfRk);
                

                if(data.status == '4'){
                    $('#tunggakan_denda').prop('disabled', false);
                    $('#tunggakan_bunga').prop('disabled', false);
                    $("#tunggakan_biaya").prop('disabled', false);
                    $("#tanggal_bayar").prop('disabled', false);
                }
               
                $scope.$apply();
            },
            error: function (error) {
                console.log(error);
            }
        });

    });

    $("#fileInput").change(function(){
        var allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document','text/plain','application/zip'];
        var file = this.files[0];
        var fileType = file.type;
        console.log(file.type);
        if(!allowedTypes.includes(fileType)){
            //alertify.error('Please select a valid file (PDF/DOC/DOCX/TXT).');
            toastr.error(
                            "Please select a valid file (PDF/DOC/DOCX/TXT/ZIP).",
                            "Warning",
                            {
                                closeButton:!0,
                                tapToDismiss:!1,
                                progressBar:!0
                            }
                        );
            $("#fileInput").val('');
            return false;
        }else{
            $scope.uploadData();
        }
    });

    $scope.submitData = function(){
        let tunggakan_denda = $('#tunggakan_denda').val();
        let tunggakan_bunga = $('#tunggakan_bunga').val();
        let tunggakan_biaya = $("#tunggakan_biaya").val();
        let tanggal_bayar =  $("#tanggal_bayar").val();
        let status =  $("#statusdok").val();
        let keterangan=  $("#keterangan").val();
        let IdEdit=  $("#id_val").val();
        let total_klaim=  $("#total_klaim").val();

        $.ajax({
            type: 'POST',
            url: baseUrl+'klaim/submitData',
            data:{
                id : IdEdit,
                tunggakan_denda : tunggakan_denda,
                tunggakan_bunga : tunggakan_bunga,
                tunggakan_biaya : tunggakan_biaya,
                tanggal_bayar : tanggal_bayar,
                status : status,
                total_klaim : total_klaim,
                keterangan : keterangan
            },
            success: function (response) {
                var data = response;
                console.log(response);

                if(data.status == '00'){
                    toastr.success(
                        "Update data berhasil",
                        "Warning",
                        {
                            closeButton:!0,
                            tapToDismiss:!1,
                            progressBar:!0
                        }
                    );
                }else{
                    toastr.error(
                        "Update data gagal",
                        "Warning",
                        {
                            closeButton:!0,
                            tapToDismiss:!1,
                            progressBar:!0
                        }
                    );
                }
            },
            error: function (error) {
                console.log(error);
                toastr.error(
                    "Update data gagal",
                    "Warning",
                    {
                        closeButton:!0,
                        tapToDismiss:!1,
                        progressBar:!0
                    }
                );
            }
        });


    }

    function calculateSum() {
        var suma = 0;
        var a = $("#tunggakan_bunga").val().replace(/\./g,"").replace(",",".");
        var b = $("#tunggakan_biaya").val().replace(/\./g,"").replace(",",".");
        var c = $("#tunggakan_denda").val().replace(/\./g,"").replace(",",".");
        
            if (!isNaN(a) && !isNaN(b) && !isNaN(c)) {
              if(a == ''){
                  a = 0;
                }  
              
                if(b == ''){
                  b = 0;
                }
                if(c == ''){
                    c = 0;
                  }
                suma = parseFloat(a) + parseFloat(b) + parseFloat(c);
                
            }
            
        if(isNaN(suma)){
          suma = 0;
        }
        console.log(parseFloat(suma));
        var z = formatRupiah(suma.toString().replace('.',','), 'Rp. ');
         $("#total_klaim").val(z);
    }

    function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

    $scope.backToTable = function(){
       
        var data = []
        $scope.detailData = data;
       $scope.showView = false;
       $scope.showTable = true;
       table.ajax.reload();
        
    };
  
    function clearForm() {
        $('#tunggakan_denda').prop('disabled', true);
        $('#tunggakan_bunga').prop('disabled', true);
        $("#tunggakan_biaya").prop('disabled', true);
        $("#tanggal_bayar").prop('disabled', true);
        // $scope.$apply();
        // $('#select-regional').prop('disabled', false);
    };

    function getSelect2Data(url, elm, initvalue = null,placeholder) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                var data = response;
                $('#' + elm + '').find('option').remove();
                $('#' + elm + '').append('<option></option>');
                if(data){
                  // console.log(response.data);
                  $('#' + elm + '').append('<option class="font12" value="ALL">ALL</option>');
                    for (i = 0; i < data.length; i++) {
                        $('#' + elm + '').append('<option class="font12" value="' + data[i].id_asuransi + '">' + data[i].nama + '</option>');
                    }
                }
                $('#' + elm + '').select2({
                    allowClear: true,
                    placeholder: "Pilih "+placeholder,
                    class: "font12",
                });
                if (initvalue) {
                    $("#" + elm).val(initvalue).trigger("change");
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    };
     
  });
