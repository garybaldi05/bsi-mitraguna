mainApp.controller('PenutupanPeralihanController', function ($scope, $rootScope){

    // Data initialize //
    //getSelect2Data("penutupan/getInsuranceData", "asuransi",null, "Asuransi");
    // end of data initialize //

   // Local variable initialize //

    let baseUrl = $("#url").val();
    let segmentUrl = $("#segment").val();
    let headsegmentUrl = $("#headsegment").val();
    let no_app = $("#no_app").val();
    var form = document.getElementById('penutupan_input');
    const submitButton = document.getElementById('submitForm');
    var table = $("#list_table");
    
    var target = document.querySelector("#kt_app_root");
    var blockUI = new KTBlockUI(target);
    var tableHistory = null;
    var summaryData = [];
  
    $scope.totalItems = 0;
    $scope.showTable = true;
    $scope.showView = false;
    $scope.showSuccessUpload = false;
    $scope.showShareData = false;
    $scope.testDetailShared = {
        data : []
    }
    
    $scope.dataActivity = [];
    $scope.dataActivityLog = [];
    $scope.dataJenisPekerjaan = [];
    $scope.insurance = [];
    $scope.detailUploadData = {
        statusDetail : '',
        success_debitur : 0,
        error_debitur : 0,
        generated_report : ''
    };

    if ($('#startdate').length > 0) {
        $("#startdate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#enddate').length > 0) {
        $("#enddate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_bayar').length > 0) {
        $("#tanggal_bayar").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    // if ($('#jumlah_bayar').length > 0) {
    //     $('#jumlah_bayar').on('keyup', function(){
    //         // let val = $('#total_klaim_bayar').val()
    //         if($(this).val()){
    //             $(this).val(formatRupiah($(this).val(),'Rp. '));
    //         }
    //     });
    // }

    // if ($('#jumlah_bayar_edit').length > 0) {
    //     $('#jumlah_bayar_edit').on('keyup', function(){
    //         // let val = $('#total_klaim_bayar').val()
    //         if($(this).val()){
    //             $(this).val(formatRupiah($(this).val(),'Rp. '));
    //         }
    //     });
    // }

    // tableHis();
    
    // end of variable init //

    // Share Data Variabel initialize //
    
    $scope.shareData = [];
    // $scope.summaryData = [];
    
    if ($('#list_table').length > 0) {
        var table = $("#list_table").DataTable({
            autoWidth: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            "ordering": false,
            ajax: {
                url: baseUrl+"penutupan_peralihan/view_data",
                // url: "http://localhost:3301/bsi-mid-svc/penutupanDtl/getAllData",
                type: "POST",
                data: function ( data ) {
                    data.cabang_search = $('#cabang_search').val();
                    data.nama = $('#nama_search').val();
                    data.startdate = $('#startdate').val();
                    data.enddate = $('#enddate').val();
                    data.status_data_search = $('#status_data_search').val();
                    data.status_klaim_search = $('#status_klaim_search').val();
                },
                complete: function(rsp){
                    summaryData = rsp.responseJSON.summary;
                    // console.log(rsp.responseJSON.summary);
                    var totalPremi = summaryData.sumPremi.toString().replace('.',',')
                    var totalPlafond = summaryData.sumPlafond.toString().replace('.',',')
                    var countDeb = summaryData.countDeb.toString().replace('.',',')

                    $("#jumlah_deb").text(formatRupiah(countDeb,'Rp. '));
                    $("#total_plafond").text(formatRupiah(totalPlafond,'Rp. '));
                    $("#total_premi").text(formatRupiah(totalPremi,'Rp. '));
                }
            },
            lengthMenu: [
                [10, 20, 30, 50],
                [10, 20, 30, 50]
            ],
            deferRender: true,
            columns: [
                { data: null },
                { "data": "no_polis", "className":"font12 text-center" },
                { "data": "nama", "className":"font12 text-center" },
                { "data": "asuransi", "className":"font12 text-center" },
                { "data": "kode_cabang", "className":"font12 text-center" },
                { "data": "tanggal_awal", "className":"font12 text-center" },
                { "data": "tanggal_akhir", "className":"font12 text-center" },
                { "data": "plafond", "className":"font12 text-center" },
                { "data": "premi", "className":"font12 text-center" },
                { "data": "tanggal_akad", "className":"font12 text-center" },
                { "data": "tanggal_lahir", "className":"font12 text-center" }
            ],
            columnDefs: [
                {
                    targets:0,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.c_id;
                        console.log(check);
                        var enc = window.btoa(check);
                       
                        let urlDownload = baseUrl + headsegmentUrl+'view?data=' + enc;

                        return ` 
                                <a target="__blank" class="badge badge-primary" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Rekon Data" href = '`+urlDownload+`'> 
                                    <i class="fw-bold text-white fs-4 bi bi-pencil-square"></i> <span class="ms-2 hover-animate">View Data</span>
                                </a>
                                `;
                    },
                },
                {
                    targets:1,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var tanggal_akhir = new Date(row.tanggal_akhir);
                        var nowDates = new Date();
                        var returns = '';
                        if(nowDates > tanggal_akhir){
                            returns = ` 
                                    <a class="badge badge-warning" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse"> 
                                        <i class="fw-bold text-dark fs-4 bi bi-exclamation-square"></i> <span class="ms-2 text-dark">Tidak Aktif</span>
                                    </a>
                                    `;
                        }else{
                            returns = ` 
                                    <a target="__blank" class="badge badge-success" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse"> 
                                        <i class="fw-bold text-dark fs-4 bi bi-check2-square"></i> <span class="ms-2 text-dark">`+row.no_polis+`</span>
                                    </a>
                                    `;
                        }

                        return returns;
                    }
                },
                {
                    targets:5,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var tanggal_mulai = row.tanggal_mulai;
                        dates = formatDate(tanggal_mulai);
                            return ` 
                                    <p class="text-gray-800">`+dates+`</p>
                                    `;
                    },
                },
                {
                    targets:6,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var tanggal_akhir = formatDate(row.tanggal_akhir);
                            return ` 
                                    <p class="text-gray-800">`+tanggal_akhir+`</p>
                                    `;
                    },
                },
                {
                    targets:7,
                    data: null,
                    className: 'text-center fw-bold fs-6',
                    render: function( data, type, row, meta ) {
                        var check = row.plafond;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:8,
                    data: null,
                    className: 'text-center fw-bold fs-6',
                    render: function( data, type, row, meta ) {
                        var check = row.premi;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:9,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var tanggal_akad = formatDate(row.tanggal_akad);
                            return ` 
                                    <p class="text-gray-800">`+tanggal_akad+`</p>
                                    `;
                    },
                },
                {
                    targets:10,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var tanggal_lahir = formatDate(row.tanggal_lahir);
                        return ` 
                                <p class="text-gray-800">`+tanggal_lahir+`</p>
                                `;
                    },
                }
                
                
            ],
        });
        $('.dataTables_filter input[type=search]').attr('placeholder', 'Cari Nama...');
        $('.dataTables_length select').select2({
            minimumResultsForSearch: "-1"
        });
    }

    function tableHis(){
        if ($('#history_table').length > 0) {
            var id_penutupan = window.btoa($('#id_penutupan').val());
            tableHistory = $("#history_table").DataTable({
                autoWidth: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: baseUrl+"rekonsiliasi/getHistoryRekon",
                    type: "POST",
                    data: function ( data ) {
                        data.id = id_penutupan;
                    }
                    
                },
                lengthMenu: [
                    [10, 20, 30, 50],
                    [10, 20, 30, 50]
                ],
                deferRender: true,
                columns: [
                    { data: null },
                    { "data": "tanggal_pembayaran", "className":"font12 text-center" },
                    { "data": "keterangan", "className":"font12 text-center" },
                    { data: null },
                    { data: null },
                    { "data": "status", "className":"font12 text-center" },
                    { "data": "createdby", "className":"font12 text-center" },
                    { "data": "createdon", "className":"font12 text-center" },
                    { "data": "updatedby", "className":"font12 text-center" },
                    { "data": "updatedon", "className":"text-center" }
                ],
                columnDefs: [
                    {
                        targets:0,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                             var check = row.c_id;
                            // var check2 = row.reffnumber;
                            // console.log(check);
                             var enc = window.btoa(check);
                            //  let urlDelete = baseUrl + headsegmentUrl+'delete?data=' + enc;
    
                            // let urlHis = baseUrl + headsegmentUrl+'history?data=' + enc ;
    
                            // let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;
    
                            return ` 
                                    <a class="ms-2 badge badge-success edit-data" data-bs-toggle="modal" data-bs-target="#modal_edit_rekon"  title="Edit/View Data"> 
                                        <i class="fw-bold fs-4 bi bi-pencil-square text-white"></i> <span class="ms-2 hover-animate">Edit</span>
                                    </a>
                                    
                                    <span class="badge badge-info delete-data" id="id;`+check+`" data-bs-toggle="modal" data-bs-target="#kt_modal_scrollable_1" title="Log Api"  > 
                                        <i class="fw-bold fs-4 bi bi-clock-history text-white"></i> <span class="ms-2 hover-animate">Delete</span>
                                    </span>
                                    `;
                        },
                    },
                    {
                        targets:1,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var tanggal_pembayaran = formatDate(row.tanggal_pembayaran);
                            return ` <p class="fw-bold fs-7 text-dark">`+tanggal_pembayaran+`</p>
                                    `;
                        },
                    },
                    {
                        targets:2,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.keterangan;
                            var name = '';

                            if(check == '1'){
                                name = 'Pembayaran Bank';
                            }else if(check == '2'){
                                name = 'Penarikan Bank';
                            }else if(check == '3'){
                                name = 'Pengembalian Fee Ujroh Dari Bank';
                            }else if(check == '4'){
                                name = 'Penarikan Fee Ujroh Dari Bank';
                            }

                            return name;
                        },
                    },
                    {
                        targets:3,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var nominal = row.nominal;
                            var jenis = row.keterangan;

                            if(jenis == '1' || jenis == '3'){
                                return parseFloat(nominal).toLocaleString('en-US');
                            }else{
                                return 0;
                            }
                        },
                    },
                    {
                        targets:4,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var nominal = row.nominal;
                            var jenis = row.keterangan;

                            if(jenis == '2' || jenis == '4'){
                                return parseFloat(nominal).toLocaleString('en-US');
                            }else{
                                return 0;
                            }
                        },
                    },
                    {
                        targets:5,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.status;
                            var name = '';

                            if(check == '1'){
                                name = 'Pembayaran Premi Sudah Sesuai';
                            }else if(check == '2'){
                                name = 'Lebih Bayar Premi';
                            }else if(check == '3'){
                                name = 'Kurang Bayar Premi';
                            }else if(check == '4'){
                                name = 'Pembayaran Fee Ujroh Sudah Sesuai';
                            }else if(check == '5'){
                                name = 'Lebih Bayar Fee Ujroh';
                            }else if(check == '6'){
                                name = 'Kurang Bayar Fee Ujroh';
                            }

                            return name;
                        },
                    },
                    {
                        targets:7,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var created_date = formatDate(row.createdon);
                            return ` <p class="fw-bold fs-7 text-dark">`+created_date+`</p>
                                    `;
                        },
                    },
                    {
                        targets:8,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.updatedby;
                            var returns = '-';

                            if(check){
                                returns = check
                            }

                            return returns;
                        },
                    },
                    {
                        targets:9,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.updatedon;
                            var returns = '-';

                            if(check){
                                returns = formatDate(check)
                            }

                            return returns;
                        },
                    },
                    
                ],
            });
            $('.dataTables_length select').select2({
                minimumResultsForSearch: "-1"
            });
          }
    };

    $scope.searchTable = function(){
        table.ajax.reload();
    }

    $('body').on('click', '#history_table .edit-data', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#history_table").dataTable().fnGetPosition(tr);
        var data = $("#history_table").dataTable().fnGetData(rowIndex);
        let jenis_bayar = data.keterangan;
        let nominal = formatRupiah(data.nominal, 'Rp');
        let tanggal_bayar = data.tanggal_pembayaran;

        $("#jenis_bayar_edit").val(jenis_bayar).trigger('change');
        $("#jumlah_bayar_edit").val(nominal);
        $("#tanggal_bayar_edit").val(tanggal_bayar);
        $("#c_id").val(data.c_id);
    });

    $scope.submitData = function(){
        let formData = new FormData($("#rekonsiliasi_form")[0]);
        $.ajax({
            type: 'POST',
            url: baseUrl+'rekonsiliasi/submitData',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                submitButton.setAttribute('data-kt-indicator', 'on');
                submitButton.disabled = true;
                blockUI.block();
            },
            success: function(rsp){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                if(rsp.status == '00'){
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Oke",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    tableHistory.ajax.reload();
                    clearForm();

                }else{
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }

            },
            error : function(err){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                Swal.fire({
                    text: 'Opps... submit data gagal mohon hubungi Administrator',
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });

            }
        });
    }

    $scope.submitDataEdit = function(){
        let formData = new FormData($("#rekonsiliasi_form_edit")[0]);
        $.ajax({
            type: 'POST',
            url: baseUrl+'rekonsiliasi/editData',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                submitButton.setAttribute('data-kt-indicator', 'on');
                submitButton.disabled = true;
                blockUI.block();
            },
            success: function(rsp){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                if(rsp.status == '00'){
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Oke",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    tableHistory.ajax.reload();
                    clearForm();

                }else{
                    Swal.fire({
                        text: rsp.keterangan,
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }

            },
            error : function(err){
                submitButton.removeAttribute('data-kt-indicator');
                submitButton.disabled = false;
                blockUI.release();
                Swal.fire({
                    text: 'Opps... submit data gagal mohon hubungi Administrator',
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, got it!",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });

            }
        });
    }

    function monthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    function getActivityData(){
        var data = no_app;
        if ($('#history_table').length > 0) {
            $.ajax({
                type: 'GET',
                url: baseUrl+'penutupan/activity_data?data='+data,
                success: function (response) {
                    $scope.dataActivity = response;

                    $scope.$apply();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function formatRupiah(angka, prefix)
    {
        if(parseFloat(angka) > 0){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split    = number_string.split(','),
                sisa     = split[0].length % 3,
                rupiah     = split[0].substr(0, sisa),
                ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
                
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah ;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }else{
            return 0;
        }
    }

    function getSelect2data(url,elm){
        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                console.log(data);
                var data = response;
                $('#' + elm + '').find('option').remove();
                $('#' + elm + '').append('<option value =""></option>');
                if(data){
                    for (i = 0; i < data.length; i++) {
                        var vals = '';
                        if(elm == 'produk'){
                            vals = data[i].nama_produk;
                        }else{
                            vals = data[i].nama;
                        }
                        $('#' + elm + '').append('<option class="font12" value="' + data[i].c_id + '">' + vals + '</option>');
                    }
                }
               
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    $scope.backToTable = function(){
       var data = []
       $scope.detailData = data;
       $scope.showView = false;
       $scope.showTable = true;
    };

    $scope.postData = function(){
        Swal.fire({ 
            title: "Are you sure?", 
            text: "You won't be able to revert this", 
            type: "warning", 
            showCancelButton: !0, 
            confirmButtonColor: "#FF7400", 
            cancelButtonColor: "#d33", 
            confirmButtonText: "Yes, Post Data", 
            confirmButtonClass: "btn btn-blue-simple", 
            cancelButtonClass: "btn btn-danger ml-1", 
            buttonsStyling: !1 })
    .then(
            function (t) { 
                console.log(t.value);
                    $scope.postingData(); 
                }) 
    };

    function typeCoverageInit(){
        if($('#coverage_fee_mitra_row').length > 0){
            $('#coverage_fee_mitra_row').hide();
            $('#coverage_asuransi_row').hide();
            $('#coverage_pinalti_row').hide();

            $('#coverage_fee_mitra').val('0');
            $('#coverage_asuransi').val('0');
            $('#coverage_pinalti').val('0');
        }
    };

    function formatDate(d) 
        {
          var date = new Date(d);

         if ( isNaN( date .getTime() ) ) 
         {
            return d;
         }
         else
        {
          
          var month = new Array();
          month[0] = "Jan";
          month[1] = "Feb";
          month[2] = "Mar";
          month[3] = "Apr";
          month[4] = "May";
          month[5] = "Jun";
          month[6] = "Jul";
          month[7] = "Aug";
          month[8] = "Sept";
          month[9] = "Oct";
          month[10] = "Nov";
          month[11] = "Dec";

          day = date.getDate();
          
          if(day < 10)
          {
             day = "0"+day;
          }
          
          return    day  + " " +month[date.getMonth()] + " " + date.getFullYear();
          }
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
      }

    function clearForm(){
        $('#tanggal_bayar').val();
        $('#jumlah_bayar').val('0');
        $('#jenis_bayar').val('').trigger("change");
    }
    
     
  });