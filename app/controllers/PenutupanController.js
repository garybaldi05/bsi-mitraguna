mainApp.controller('PenutupanController', function ($scope, $rootScope){

    // Data initialize //
    //getSelect2Data("penutupan/getInsuranceData", "asuransi",null, "Asuransi");
    // end of data initialize //

   // Local variable initialize //

    let baseUrl = $("#url").val();
    let segmentUrl = $("#segment").val();
    let headsegmentUrl = $("#headsegment").val();
    let no_app = $("#no_app").val();
    var form = document.getElementById('penutupan_input');
    var table = $("#list_table");
    
    var target = document.querySelector("#kt_app_root");
    var blockUI = new KTBlockUI(target);
  
    $scope.totalItems = 0;
    $scope.showTable = true;
    $scope.showView = false;
    $scope.showSuccessUpload = false;
    $scope.showShareData = false;
    $scope.testDetailShared = {
        data : []
    }
    
    $scope.dataActivity = [];
    $scope.dataActivityLog = [];
    $scope.dataJenisPekerjaan = [];
    $scope.insurance = [];
    $scope.detailUploadData = {
        statusDetail : '',
        success_debitur : 0,
        error_debitur : 0,
        generated_report : ''
    };

    if ($('#startdate').length > 0) {
        $("#startdate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#enddate').length > 0) {
        $("#enddate").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_akad').length > 0) {
        $("#tanggal_akad").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_mulai').length > 0) {
        $("#tanggal_mulai").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_akhir').length > 0) {
        $("#tanggal_akhir").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_lahir').length > 0) {
        $("#tanggal_lahir").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#tanggal_bayar_ijk').length > 0) {
        $("#tanggal_bayar_ijk").flatpickr({
            dateFormat: "Y-m-d",
        });
    }

    if ($('#plafond').length > 0) {
        $('#plafond').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#nilai_agunan').length > 0) {
        $('#nilai_agunan').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#nilai_pengajuan').length > 0) {
        $('#nilai_pengajuan').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#coverage_fee_mitra').length > 0) {
        $('#coverage_fee_mitra').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#coverage_asuransi').length > 0) {
        $('#coverage_asuransi').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#coverage_asuransi').length > 0) {
        $('#coverage_pinalti').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#coverage_asuransi').length > 0) {
        $('#coverage_pinalti').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    // end of variable init //

    // Share Data Variabel initialize //
    
    $scope.shareData = [];
    tableHis();
    getActivityData();
    typeCoverageInit();
    
    // end of variable init //
    if ($('#nama_asuransi').length > 0) {
        $('#nama_asuransi').on('change',function () {
            var id = $(this).val();
            var url = baseUrl+'penutupan/getProduk?id='+id

            getSelect2data(url,'produk');
        });
    }

    if($("#upload-report").length > 0){
        $("#upload-report").hide();
    }

    if ($('#produk').length > 0) {
        $('#produk').on('change',function () {
            var id = $(this).val();
            var asuransi = $('#nama_asuransi').val();
            var urlJenisPekerjaan = baseUrl+'penutupan/getJenisPekerjaan?id='+id;
            var urlTypeProduk = baseUrl+'penutupan/getTypeProduk?id='+id;
            var urltypeManfaat = baseUrl+'penutupan/getTypeManfaat?id='+id + '&id_asuransi='+asuransi;

            getSelect2data(urlJenisPekerjaan,'jenis_pekerjaan');
            getSelect2data(urlTypeProduk,'tipe_produk');
            getSelect2data(urltypeManfaat,'type_manfaat');
        });
    }

    if ($('#produk').length > 0) {
        $('#produk').on('change',function () {
            var id = $(this).val();
            var urlCabang = baseUrl+'penutupan/getCabang?id='+id

            getSelect2data(urlCabang,'kode_cabang');
        });
    }

    if ($('#area_search').length > 0) {
        $('#area_search').on('change',function () {
            var id = $(this).val();
            var urlCabang = baseUrl+'penutupan/getCabang?id='+id

            getSelect2data(urlCabang,'cabang_search');
        });
    }

    if ($('#tanggal_akhir').length > 0) {
        $('#tanggal_akhir').on('change',function () {
            var enddate = new Date($(this).val());
            var startdate = new Date($('#tanggal_mulai').val());
            var month = 0;
            
            console.log(enddate);
            console.log(startdate);

            if(startdate && enddate){
                month = monthDiff(startdate,enddate);
                $("#tenor").val(month);
            }
        });
    }

    if ($('#tanggal_akad').length > 0) {
        $('#tanggal_akad').on('change',function () {
            var tgl_akad = $(this).val();
            $("#tanggal_mulai").val(tgl_akad);
        });
    }
    
    if ($('#type_coverage').length > 0) {
        $('#type_coverage').on('change',function () {
            var vals = $(this).val();
            if(vals == '1'){
                typeCoverageInit();
            }else if(vals == '2'){
                typeCoverageInit();

                $('#coverage_fee_mitra_row').show();
            }else if(vals == '3'){
                typeCoverageInit();

                $('#coverage_asuransi_row').show();
            }else if(vals == '4'){
                typeCoverageInit();

                $('#coverage_pinalti_row').show();
            }else if(vals == '5'){
                typeCoverageInit();

                $('#coverage_fee_mitra_row').show();
                $('#coverage_asuransi_row').show();
            }else if(vals == '6'){
                typeCoverageInit();
                
                $('#coverage_pinalti_row').show();
                $('#coverage_asuransi_row').show();
            }else if(vals == '7'){
                typeCoverageInit();
                
                $('#coverage_pinalti_row').show();
                $('#coverage_fee_mitra_row').show();
            }else if(vals == '8'){
                typeCoverageInit();
                
                $('#coverage_pinalti_row').show();
                $('#coverage_fee_mitra_row').show();
                $('#coverage_asuransi_row').show();
            }
        });
        var vals = $('#type_coverage').val();
        if(vals){
            $('#type_coverage').val(vals).change();
        }
    }

    if ($('#coverage_asuransi').length > 0) {
        $('#coverage_pinalti').on('keyup', function(){
            // let val = $('#total_klaim_bayar').val()
            if($(this).val()){
                $(this).val(formatRupiah($(this).val(),'Rp. '));
            }
        });
    }

    if ($('#penutupan_input').length > 0) {
       
        var validator = FormValidation.formValidation(
            form,
            {
                fields: {
                    'no_aplikasi': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            },
                            stringLength: {
                                max: 17,
                                min: 17,
                                message: 'No Aplikasi harus 17 karakter',
                            },
                            numeric: {
                                message: 'Hanya dapat diisi angka',
                            }
                        }
                    },
                    'nama': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'tanggal_lahir': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'ktp': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            },
                            numeric: {
                                message: 'Hanya dapat diisi angka',
                            },
                            stringLength: {
                                max: 13,
                                min: 13,
                                message: 'KTP harus 13 karakter',
                            }
                        }
                    },
                    'npwp': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            },
                            numeric: {
                                message: 'Hanya dapat diisi angka',
                            },
                            stringLength: {
                                max: 13,
                                min: 13,
                                message: 'KTP harus 13 karakter',
                            }
                        }
                    },
                    'tanggal_akad': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'tanggal_lahir': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'produk': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'tipe_produk': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'jenis_pekerjaan': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'jenis_agunan': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'nomor_agunan': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'alamat': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'kodepos': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            },
                            numeric: {
                                message: 'Hanya dapat diisi angka',
                            },
                            stringLength: {
                                min: 5,
                                message: 'Kodepos harus minimal 5 karakter',
                            }
                        }
                    },
                    'nama_asuransi': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    // 'kode_cabang': {
                    //     validators: {
                    //         notEmpty: {
                    //             message: 'Field wajib diisi'
                    //         }
                    //     }
                    // },
                    'nomor_akad': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'tanggal_mulai': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'tanggal_akhir': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'tanggal_bayar_ijk': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'tenor': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'nilai_agunan': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'plafond': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'nilai_pengajuan': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'coverage_fee_mitra': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'coverage_asuransi': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'coverage_pinalti': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'type_manfaat': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'kode_area': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'kode_cabang': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'jenis_akad': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'type_coverage': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'coverage_pinalti': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'coverage_fee_mitra': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    },
                    'coverage_asuransi': {
                        validators: {
                            notEmpty: {
                                message: 'Field wajib diisi'
                            }
                        }
                    }
                    
                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: '.fv-row',
                        eleInvalidClass: '',
                        eleValidClass: ''
                    })
                }
            }
        );
    }

    
    
    if ($('#list_table').length > 0) {
        var table = $("#list_table").DataTable({
            autoWidth: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: baseUrl+"penutupan/view_data",
                // url: "http://localhost:3301/bsi-mid-svc/penutupanDtl/getAllData",
                type: "POST",
                data: function ( data ) {
                    data.startdate = $('#startdate').val();
                    data.enddate = $('#enddate').val();
                    data.cabang_search = $('#cabang_search').val();
                    data.area_search = $('#area_search').val();
                    data.asuransi_search = $('#asuransi_search').val();
                    data.status_data_search = $('#status_data_search').val();
                },
                complete: function(rsp){
                    summaryData = rsp.responseJSON.summary;
                    // console.log(rsp.responseJSON.summary);
                    var totalPremi = summaryData.sumPremi.toString().replace('.',',')
                    var totalPlafond = summaryData.sumPlafond.toString().replace('.',',')
                    var countDeb = summaryData.countDeb.toString().replace('.',',')

                    $("#jumlah_deb").text(formatRupiah(countDeb,'Rp. '));
                    $("#total_plafond").text(formatRupiah(totalPlafond,'Rp. '));
                    $("#total_premi").text(formatRupiah(totalPremi,'Rp. '));
                }
            },
            lengthMenu: [
                [5, 10, 20, 50],
                [5, 10, 20, 50]
            ],
            deferRender: true,
            columns: [
                { data: null },
                { data: null },
                { "data": "polis", "className":"font12 text-center" },
                { "data": "no_aplikasi", "className":"font12 text-center" },
                { "data": "nama", "className":"font12 text-center" },
                { "data": "nama_asuransi", "className":"font12 text-center" },
                { "data": "nama_cabang", "className":"font12 text-center" },
                { "data": "nama_produk", "className":"font12 text-center" },
                { "data": "tipe_produk", "className":"font12 text-center" },
                { "data": "atribusi", "className":"font12 text-center" },
                { "data": "bulan_tenor", "className":"text-center" },
                { "data": "plafond", "className":"text-center" },
                { "data": "coverage_fee_mitra", "className":"text-center" },
                { "data": "coverage_asuransi", "className":"text-center" },
                { "data": "coverage_pinalti", "className":"text-center" },
                { "data": "premi", "className":"text-center" },
                { "data": "premi_before_admin", "className":"text-center" },
                { "data": "tanggal_akad", "className":"text-center" },
                { "data": "nomor_akad", "className":"text-center" },
                { "data": "tanggal_lahir", "className":"text-center" },
                { "data": "usia_saat_akad", "className":"text-center" },
                { "data": "usia_selesai_akad", "className":"text-center" }
            ],
            columnDefs: [
                {
                    targets:1,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.no_aplikasi;
                        console.log(check);
                        var enc = window.btoa(check);
                        let urlEdit = baseUrl + headsegmentUrl+'edit?data=' + enc;

                        let urlHis = baseUrl + headsegmentUrl+'history?data=' + enc ;

                        let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;

                        return ` 
                                <a class="ms-2 badge badge-success" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Edit/View Data" href = '`+urlEdit+`'> 
                                    <i class="fw-bold fs-4 bi bi-pencil-square text-white"></i> <span class="ms-2 hover-animate">Edit</span>
                                </a>
                                
                                <a class="badge badge-info" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="History Data" href = '`+urlHis+`'> 
                                    <i class="fw-bold fs-4 bi bi-clock-history text-white"></i> <span class="ms-2 hover-animate">History</span>
                                </a>
                                `;
                    },
                },
                {
                    targets:0,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.no_aplikasi;
                        console.log(check);
                        var enc = window.btoa(check);
                       
                        let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;

                        return ` 
                                <a target="__blank" class="badge badge-primary" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Download CN" href = '`+urlDownload+`'> 
                                    <i class="fw-bold text-white fs-4 bi bi-download"></i> <span class="ms-2 hover-animate">Download KPA</span>
                                </a>
                                `;
                    },
                },
                {
                    targets:2,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.polis;
                        console.log(check);
                        if(!check){
                            return ` 
                                    <a target="__blank" class="badge badge-warning" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse"> 
                                        <i class="fw-bold text-dark fs-4 bi bi-exclamation-square"></i> <span class="ms-2 text-dark">On Proses</span>
                                    </a>
                                    `;
                        }
                    },
                },
                {
                    targets:3,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var count = row.count_deb;
                        var check = row.no_aplikasi;
                        var date = row.createdon;
                        console.log(check);
                            return ` 
                                    <p>`+check+`<span class="fw-bold text-info fs-7"> (`+count+`)</span></p>
                                    <p class="fw-bold fs-7 text-dark">`+new Date(date).toLocaleString()+`</p>
                                    `;
                    },
                },
                {
                    targets:11,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.plafond;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:12,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.coverage_fee_mitra;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:13,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.coverage_asuransi;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:14,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.coverage_pinalti;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:15,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.premi;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                {
                    targets:16,
                    data: null,
                    className: 'text-center',
                    render: function( data, type, row, meta ) {
                        var check = row.premi_before_admin;
                        return parseFloat(check).toLocaleString('en-US');
                    },
                },
                
                
            ],
        });
        $('.dataTables_filter input[type=search]').attr('placeholder', 'Cari no aplikasi...');
        $('.dataTables_length select').select2({
            minimumResultsForSearch: "-1"
        });
    }

    function tableHis(){
        if ($('#history_table').length > 0) {
            var tablehis = $("#history_table").DataTable({
                autoWidth: true,
                searchDelay: 500,
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: baseUrl+"penutupan/history_data",
                    type: "POST",
                    data: function ( data ) {
                        data.data = no_app;
                        data.enddate = $('#enddate').val();
                        data.cabang = $('#cabang').val();
                    }
                },
                lengthMenu: [
                    [5, 10, 20, 50],
                    [5, 10, 20, 50]
                ],
                deferRender: true,
                columns: [
                    { data: null },
                    { data: null },
                    { "data": "polis", "className":"font12 text-center" },
                    { "data": "no_aplikasi", "className":"font12 text-center" },
                    { "data": "reffnumber", "className":"font12 text-center" },
                    { "data": "nama", "className":"font12 text-center" },
                    { "data": "nama_asuransi", "className":"font12 text-center" },
                    { "data": "nama_cabang", "className":"font12 text-center" },
                    { "data": "nama_produk", "className":"font12 text-center" },
                    { "data": "tipe_produk", "className":"font12 text-center" },
                    { "data": "atribusi", "className":"font12 text-center" },
                    { "data": "bulan_tenor", "className":"text-center" },
                    { "data": "plafond", "className":"text-center" },
                    { "data": "coverage_fee_mitra", "className":"text-center" },
                    { "data": "coverage_asuransi", "className":"text-center" },
                    { "data": "coverage_pinalti", "className":"text-center" },
                    { "data": "premi", "className":"text-center" },
                    { "data": "premi_before_admin", "className":"text-center" },
                    { "data": "tanggal_akad", "className":"text-center" },
                    { "data": "nomor_akad", "className":"text-center" },
                    { "data": "tanggal_lahir", "className":"text-center" },
                    { "data": "usia_saat_akad", "className":"text-center" },
                    { "data": "usia_selesai_akad", "className":"text-center" }
                ],
                columnDefs: [
                    {
                        targets:1,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.no_aplikasi;
                            var check2 = row.reffnumber;
                            console.log(check);
                            var enc = window.btoa(check);
                            let urlEdit = baseUrl + headsegmentUrl+'edit?data=' + enc;
    
                            let urlHis = baseUrl + headsegmentUrl+'history?data=' + enc ;
    
                            let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;
    
                            return ` 
                                    <a class="ms-2 badge badge-success" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Edit/View Data" href = '`+urlEdit+`'> 
                                        <i class="fw-bold fs-4 bi bi-pencil-square text-white"></i> <span class="ms-2 hover-animate">Edit</span>
                                    </a>
                                    
                                    <span class="badge badge-info check-Log" data-bs-toggle="modal" data-bs-target="#kt_modal_scrollable_1" title="Log Api"  > 
                                        <i class="fw-bold fs-4 bi bi-clock-history text-white"></i> <span class="ms-2 hover-animate">Log Api</span>
                                    </span>
                                    `;
                        },
                    },
                    {
                        targets:0,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.no_aplikasi;
                            console.log(check);
                            var enc = window.btoa(check);
                           
                            let urlDownload = baseUrl + headsegmentUrl+'downloadCnNew?data=' + enc;
    
                            return ` 
                                    <a target="__blank" class="badge badge-primary" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse" data-bs-placement="bottom" title="Download CN" href = '`+urlDownload+`'> 
                                        <i class="fw-bold text-white fs-4 bi bi-download"></i> <span class="ms-2 hover-animate">Download KPA</span>
                                    </a>
                                    `;
                        },
                    },
                    {
                        targets:2,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.polis;
                            console.log(check);
                            if(!check){
                                return ` 
                                        <a target="__blank" class="badge badge-warning" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-inverse"> 
                                            <i class="fw-bold text-dark fs-4 bi bi-exclamation-square"></i> <span class="ms-2 text-dark">On Proses</span>
                                        </a>
                                        `;
                            }
                        },
                    },
                    {
                        targets:11,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.plafond;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:12,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.coverage_fee_mitra;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:13,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.coverage_asuransi;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:14,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.coverage_pinalti;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:15,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.premi;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    {
                        targets:16,
                        data: null,
                        className: 'text-center',
                        render: function( data, type, row, meta ) {
                            var check = row.premi_before_admin;
                            return parseFloat(check).toLocaleString('en-US');
                        },
                    },
                    
                ],
            });
            $('.dataTables_length select').select2({
                minimumResultsForSearch: "-1"
            });
          }
    };

    if ($('#fileInput').length > 0) {
        $("#fileInput").change(function(){
            var allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.ms-office',
                                 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','text/plain'];

            var file = this.files[0];
            var fileType = file.type;
            console.log(file.type);
            if(!allowedTypes.includes(fileType)){
                //alertify.error('Please select a valid file (PDF/DOC/DOCX/TXT).');
                    toastr.error(
                                    "Please select a valid file .",
                                    "Warning",
                                    {
                                        closeButton:!0,
                                        tapToDismiss:!1,
                                        progressBar:!0
                                    }
                                );
                    $("#fileInput").val('');
                    return false;
            }else{
                $scope.uploadData();
            }
        });
    }

    $scope.uploadData = function(){
        let formData = new FormData($("#uploadForm")[0]);
        $.ajax({
            type: 'POST',
            url: baseUrl+'penutupan/uploadDataPenutupan',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                
                blockUI.block();
                // $('#kt_app_root').addClass('overlay overlay-block"');
            },
            success: function(rsp){
                // $('#kt_app_root').removeClass('overlay overlay-block"');
                blockUI.release();
                if(rsp.status == '00'){
                    Swal.fire({
                        text: "Berhasil Upload Data Penutupan",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    $scope.detailUploadData.statusDetail = rsp.statusDetail;
                    $scope.detailUploadData.success_debitur = rsp.success_debitur;
                    $scope.detailUploadData.error_debitur = rsp.error_debitur;
                    $scope.detailUploadData.generated_report = rsp.generated_report;

                }else{
                    Swal.fire({
                        text: "Gagal upload data, mohon untuk dicoba beberapa saat lagi",
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });

                    $scope.detailUploadData.statusDetail = rsp.statusDetail;
                    $scope.detailUploadData.success_debitur = rsp.success_debitur;
                    $scope.detailUploadData.error_debitur = rsp.error_debitur;
                    $scope.detailUploadData.generated_report = rsp.generated_report;
                }

                $("#upload-report").show();

                $scope.$apply();
            },
            error: function(err){
                // $('#kt_app_root').removeClass('overlay overlay-block"');
                $("#upload-report").show();
                blockUI.release();
                console.log(err.message)
                Swal.fire({
                    text: "Gagal upload data, mohon untuk dicoba beberapa saat lagi",
                    icon: "warning",
                    buttonsStyling: false,
                    confirmButtonText: "Ok",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
            }
        });
    }

    $scope.searchTable = function(){
        table.ajax.reload();
        $scope.$apply();
    }
    
    $scope.modalActivity = function(event, id){
        event.preventDefault();
        console.log(id);

        $("#request-activity").text('');
        $("#reponse-activity").text('');

        for (let index = 0; index < $scope.dataActivity.length; index++) {
            const element = $scope.dataActivity[index];

            if(element.c_id == id){
                $("#request-activity").text(JSON.stringify(JSON.parse(element.datanew),undefined,2));
                $("#response-activity").text(JSON.stringify(JSON.parse(element.response),undefined,2));
                // console.log($scope.response);
                // console.log($scope.request);
                
                
            }
            
        }
    }

    $('body').on('click', '#history_table .check-Log', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#history_table").dataTable().fnGetPosition(tr);
        var data = $("#history_table").dataTable().fnGetData(rowIndex);
        let no_app = window.btoa(data.no_aplikasi);
        let reffnumber = window.btoa(data.reffnumber);

        $("#request-activity").text('');
        $("#reponse-activity").text('');

        $.ajax({
            type: 'GET',
            url: baseUrl+'penutupan/getHistoryData?no_app='+no_app+'&reffnumber='+reffnumber,
            success: function (rsp) {
                $scope.dataActivityLog = rsp;
                var datanew = $scope.dataActivityLog[0].datanew;
                var response = $scope.dataActivityLog[0].response;
                console.log($scope.dataActivityLog);
                $("#request-activity").text(JSON.stringify(JSON.parse(datanew),undefined,2));
                $("#response-activity").text(JSON.stringify(JSON.parse(response),undefined,2));

                $scope.$apply();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $scope.validateForm = function(event){
        event.preventDefault();
       
        if (validator) {
            validator.validate().then(function (status) {
                console.log('validated!');
                const submitButton = document.getElementById('submitForm');

                if (status == 'Valid') {
                    // Show loading indication
                    submitButton.setAttribute('data-kt-indicator', 'on');
                    let formData = new FormData($("#penutupan_input")[0]);

                    const plainFormData = Object.fromEntries(formData.entries());
	                const formDataJsonString = JSON.stringify(plainFormData);

                    // Disable button to avoid multiple click
                    submitButton.disabled = true;
                    // validator.on('core.form.valid', function() {
                        // Send the form data to back-end
                        // You need to grab the form data and create an Ajax request to send them
                        var submitUrl = baseUrl + 'penutupan/submitData';
                        FormValidation.utils.fetch(submitUrl, {
                            method: 'POST',
                            headers: {
                                "Content-Type": "application/json",
                                Accept: "application/json",
                            },
                            params: {
                                no_aplikasi : $('#no_aplikasi').val(),
                                nama : $('#nama').val(),
                                tanggal_lahir : $('#tanggal_lahir').val(),
                                ktp : $('#ktp').val(),
                                npwp : $('#npwp').val(),
                                plafond : $('#plafond').val(),
                                nama_asuransi : $('#nama_asuransi').val(),
                                produk : $('#produk').val(),
                                tipe_produk : $('#tipe_produk').val(),
                                jenis_pekerjaan : $('#jenis_pekerjaan').val(),
                                jenis_agunan : $('#jenis_agunan').val(),
                                jenis_akad : $('#jenis_akad').val(),
                                nomor_agunan : $('#nomor_agunan').val(),
                                alamat : $('#alamat').val(),
                                nomor_akad : $('#nomor_akad').val(),
                                tenor : $('#tenor').val(),
                                nilai_pengajuan : $('#nilai_pengajuan').val(),
                                nilai_agunan : $('#nilai_agunan').val(),
                                tanggal_mulai : $('#tanggal_mulai').val(),
                                tanggal_akhir : $('#tanggal_akhir').val(),
                                tanggal_akad : $('#tanggal_akad').val(),
                                tanggal_bayar_ijk : $('#tanggal_bayar_ijk').val(),
                                type_manfaat : $('#type_manfaat').val(),
                                type_coverage : $('#type_coverage').val(),
                                kodepos : $('#kodepos').val(),
                                kode_cabang : $('#kode_cabang').val(),
                                coverage_fee_mitra : $('#coverage_fee_mitra').val(),
                                coverage_asuransi : $('#coverage_asuransi').val(),
                                coverage_pinalti : $('#coverage_pinalti').val()
                            },
                        }).then(function(json){
                            var rsp = json;
                            
                            submitButton.removeAttribute('data-kt-indicator');
                            submitButton.disabled = false;

                            if(rsp.status == '00'){
                                Swal.fire({
                                    text: rsp.keterangan,
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Oke",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });

                                if (baseUrl) {
                                    location.href = baseUrl + 'penutupan';
                                }

                            }else{
                                Swal.fire({
                                    text: rsp.keterangan,
                                    icon: "warning",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            }

                                
                        });

                        
                    // });

                }else{
                    Swal.fire({
                        text: "Data yang di input belum sesuai",
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }
            });
        }
    };

    function monthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    function getActivityData(){
        var data = no_app;
        if ($('#history_table').length > 0) {
            $.ajax({
                type: 'GET',
                url: baseUrl+'penutupan/activity_data?data='+data,
                success: function (response) {
                    $scope.dataActivity = response;

                    $scope.$apply();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }

    function formatRupiah(angka, prefix)
    {
        if(parseFloat(angka) > 0){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split    = number_string.split(','),
                sisa     = split[0].length % 3,
                rupiah     = split[0].substr(0, sisa),
                ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
                
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah ;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }else{
            return 0;
        }
    }

    function getSelect2data(url,elm){
        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                console.log(data);
                var data = response;
                $('#' + elm + '').find('option').remove();
                $('#' + elm + '').append('<option value =""></option>');
                if(data){
                    for (i = 0; i < data.length; i++) {
                        var vals = '';
                        if(elm == 'produk'){
                            vals = data[i].nama_produk;
                        }else{
                            vals = data[i].nama;
                        }
                        $('#' + elm + '').append('<option class="font12" value="' + data[i].c_id + '">' + vals + '</option>');
                    }
                }
               
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    $scope.backToTable = function(){
       var data = []
       $scope.detailData = data;
       $scope.showView = false;
       $scope.showTable = true;
    };

    $scope.postData = function(){
        Swal.fire({ 
            title: "Are you sure?", 
            text: "You won't be able to revert this", 
            type: "warning", 
            showCancelButton: !0, 
            confirmButtonColor: "#FF7400", 
            cancelButtonColor: "#d33", 
            confirmButtonText: "Yes, Post Data", 
            confirmButtonClass: "btn btn-blue-simple", 
            cancelButtonClass: "btn btn-danger ml-1", 
            buttonsStyling: !1 })
    .then(
            function (t) { 
                console.log(t.value);
                    $scope.postingData(); 
                }) 
    };

    function typeCoverageInit(){
        if($('#coverage_fee_mitra_row').length > 0){
            $('#coverage_fee_mitra_row').hide();
            $('#coverage_asuransi_row').hide();
            $('#coverage_pinalti_row').hide();

            $('#coverage_fee_mitra').val('0');
            $('#coverage_asuransi').val('0');
            $('#coverage_pinalti').val('0');
        }
    };

    function formatDate(d) 
        {
          var date = new Date(d);

         if ( isNaN( date .getTime() ) ) 
         {
            return d;
         }
         else
        {
          
          var month = new Array();
          month[0] = "Jan";
          month[1] = "Feb";
          month[2] = "Mar";
          month[3] = "Apr";
          month[4] = "May";
          month[5] = "Jun";
          month[6] = "Jul";
          month[7] = "Aug";
          month[8] = "Sept";
          month[9] = "Oct";
          month[10] = "Nov";
          month[11] = "Dec";

          day = date.getDate();
          
          if(day < 10)
          {
             day = "0"+day;
          }
          
          return    day  + " " +month[date.getMonth()] + " " + date.getFullYear();
          }
    }
  
    
     
  });