mainApp.controller('BerandaController', function ($scope, $rootScope){
   
    $scope.data = [];
    $scope.pageNumber = 1;
    $scope.libraryTemp = {};
    $scope.totalItemsTemp = {};
  
    $scope.totalItems = 0;

    $rootScope.menu = {
        Dashboard : 'active',
        Penutupan : '',
        Klaim : ''
    };

    $scope.pageChanged = function(newPage) {
      getResultsPage(newPage);
    };

    $scope.swall = function() {
        Swal.fire({
            title: "Confirmation",
            text: "Are you sure want to leave this page and lose your changes?",
            icon: "warning"
        })
        .then((willDelete) => {
            if (willDelete) {
            }
        });
    };
  
    
     
  });