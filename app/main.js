var mainApp =  angular.module('mainApp',['ngRoute','ngSanitize','ui.bootstrap', 'ui.tree', 'ngAnimate']);

mainApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            // when('/beranda', {
            //     templateUrl: 'templates/beranda.html',
            //     controller: 'BerandaController'
            // }).
            when('/items', {
                templateUrl: 'templates/items.html',
                controller: 'ItemController'
            });
}]);

mainApp.run(function ($rootScope, $location) {

    $rootScope.menu = {
        Dashboard : '',
        Penutupan : '',
        Klaim : '',
        Subrogasi : '',
        Admin : '',
        Master : ''
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // custom notify
    $rootScope.pNotifySuccess = function (title, msg) {
        new PNotify({
            title: 'Success',
            text: msg,
            type: 'success'
        });
    }
    $rootScope.pNotifyError = function (title, msg) {
        new PNotify({
            title: 'Error',
            id: 'pnotify-danger',
            text: msg,
            type: 'error'
        });
    }
    $rootScope.pNotifyWarning = function (title, msg) {
        new PNotify({
            title: 'Success',
            text: msg,
            type: 'success'
        });
    }
    $rootScope.pNotifyInfo = function (title, msg) {
        new PNotify({
            title: 'Success',
            text: msg,
            type: 'success'
        });
    }
    ////////////////////////////////////
    $rootScope.notification = function (msg) {
        $.growl({
            icon: 'feather icon-layers',
            title: '&nbsp;Notifications',
            message: '<br>' + msg,
            url: ''
        }, {
                element: 'body',
                type: 'inverse',
                allow_dismiss: true,
                placement: {
                    from: 'top',
                    align: 'center'
                },
                offset: {
                    x: 30,
                    y: 30
                },
                spacing: 10,
                z_index: 999999,
                delay: 2500,
                timer: 100000000,
                url_target: '_blank',
                mouse_over: false,
                animate: {
                    enter: 'animated flipInX',
                    exit: 'animated flipOutX'
                },
                icon_type: 'class',
                template: '<div data-growl="container" style="width:600px!important;" class="alert" role="alert">' +
                    '<button type="button" class="close" data-growl="dismiss">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '<span class="sr-only">Close</span>' +
                    '</button>' +
                    '<span data-growl="icon"></span>' +
                    '<span data-growl="title"></span>' +
                    '<span data-growl="message"></span>' +
                    '<a href="#!" data-growl="url"></a>' +
                    '</div>'
            });
    };
    $rootScope.messagesuccess = function (msg) {
        new PNotify({
            title: 'Success',
            text: msg,
            type: 'success'
        });
    };
    $rootScope.messageerror = function (msg) {
        new PNotify({
            title: 'Error',
            text: msg,
            type: 'error'
        });
    };

    $rootScope.syntaxHighlight = function (jsonObj) {
        jsonObj = jsonObj.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return jsonObj.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    };

    $rootScope.block = function () {
        $.blockUI({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
    };
    $rootScope.unblock = function () {
        $.unblockUI();
    };

    $rootScope.blockcomponent = function (attr) {
        $("#" + attr).block({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
    };
    $rootScope.unblockcomponent = function (attr) {
        $("#" + attr).unblock();
    };

    
});

mainApp.controller('homeCtrl', function ($scope, $rootScope) {
    $scope.swall = function() {
        Swal.fire(
            'Good job!',
            'You clicked the button!',
            'success'
          )
      };

});