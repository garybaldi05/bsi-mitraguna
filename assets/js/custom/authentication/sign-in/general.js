"use strict";

// Class definition
var KTSigninGeneral = function() {
    // Elements
    var form;
    var submitButton;
    var validator;

    // Handle form
    var handleForm = function(e) {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validator = FormValidation.formValidation(
			form,
			{
				fields: {					
					'username': {
                        validators: {
							notEmpty: {
								message: 'Username is required'
							}
						}
					},
                    'password': {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            }
                        }
                    } 
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: '.fv-row',
                        eleInvalidClass: '',  // comment to enable invalid state icons
                        eleValidClass: '' // comment to enable valid state icons
                    })
				}
			}
		);		

        // Handle form submit
        submitButton.addEventListener('click', function (e) {
            // Prevent button default action
            e.preventDefault();

            // Validate form
            validator.validate().then(function (status) {
                if (status == 'Valid') {
                    // Show loading indication
                    submitButton.setAttribute('data-kt-indicator', 'on');

                    // Disable button to avoid multiple click 
                    submitButton.disabled = true;
                    var baseUrl = $('#base-url').val();
                    var username = document.querySelector('#username');
                    var password = document.querySelector('#password');
                    var submitUrl = baseUrl + 'auth/login/signin'

                    FormValidation.utils.fetch(submitUrl, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            Accept: "application/json",
                        },
                        params: {
                           username : $('#username').val(),
                           password : $('#password').val()
                        },
                    }).then(function(json){
                        var rsp = json;
                        
                        submitButton.removeAttribute('data-kt-indicator');
                        submitButton.disabled = false;

                        if(rsp.statuscode == '00'){
                            Swal.fire({
                                text: "Login successfully",
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "Oke",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });

                            if (baseUrl) {
                                location.href = rsp.login_referer;
                            }

                        }else{
                            Swal.fire({
                                text: rsp.message,
                                icon: "warning",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                        }

                            
                    });

                } else {
                    // Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                    Swal.fire({
                        text: "Username or Password is Required",
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }
            });
		});
    }

    // Public functions
    return {
        // Initialization
        init: function() {
            form = document.querySelector('#kt_sign_in_form');
            submitButton = document.querySelector('#kt_sign_in_submit');
            
            
            handleForm();
        }
    };
}();

// On document ready
KTUtil.onDOMContentLoaded(function() {
    KTSigninGeneral.init();
});
