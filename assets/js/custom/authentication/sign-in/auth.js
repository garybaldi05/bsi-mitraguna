$(function() {
    $( "#loginSubmit" ).on( "click", function(e) {

        e.preventDefault();

        var submitButton = document.querySelector('#loginSubmit');
        var baseUrl = $('#base-url').val();
        var username = document.querySelector('#username');
        var password = document.querySelector('#password');
        
        $.ajax({
            type: 'POST',
            url: baseUrl+'auth/login/signin',
            data:{
                username : username,
                password : password
            },
            success: function (rsp) {
                if(rsp.statuscode == '00'){

                    Swal.fire({
                        text: "You have successfully logged in!",
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    }).then(function (result) {
                        if (result.isConfirmed) { 
                            form.querySelector('[name="username"]').value= "";
                            form.querySelector('[name="password"]').value= "";  
                                                            
                            //form.submit(); // submit form
                            if (baseUrl) {
                                location.href = baseUrl;
                            }
                        }
                    });

                }else{
                    Swal.fire({
                        text: rsp.message,
                        icon: "info",
                        buttonsStyling: false,
                        confirmButtonText: "Ok, got it!",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    });
                }
            }
        });
    });

});